export default {
  items: [
    {
      name: 'Inicio',
      url: '/Inicio',
      icon: '',
      badge: {
        variant: 'info',
        text: 'HOSPITAL',
      },
    },
    { /* farmacia */
      name: 'INVENTARIO',
      url: '/Inventario',
      icon: 'fa fa-list-alt',
      children: [
      
        {
          name: 'Inventario General',
          url: '/Inventario/InventarioGeneral',
          icon: 'fa fa-book',
          children: [
            {
              name: 'Medicamentos',
              url: '/Inventario/InventarioGeneral/Medicamentos',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Insumos',
              url: '/Inventario/InventarioGeneral/Insumos',
              icon: 'fa fa-medkit',
            },

            {        
              name: 'Reactivos',
              url: '/Inventario/InventarioGeneral/Reactivos',
              icon: 'fa fa-medkit',
            },
            {        
              name: 'Ver Todo',
              url: '/Inventario/InventarioGeneral/Productos',
              icon: 'fa fa-eye',    
            },
          ],

        },

        {
          name: 'Inventario por Lote',
          url: '/Inventario/InventarioPorLote',
          icon: 'fa fa-book',
          children: [
    
            {
              name: 'Medicamentos',
              url: '/Inventario/InventarioPorLote/Medicamentos',
              icon: 'fa fa-medkit',
            },

            {
              name: 'Insumos',
              url: '/Inventario/InventarioPorLote/Insumos',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Reactivos',
              url: '/Inventario/InventarioPorLote/Reactivos',
              icon: 'fa fa-medkit',
            },

            {

              name: 'Ver Todo',
              url: '/Inventario/InventarioPorLote/Productos',
              icon: 'fa fa-eye',
            },

          ],
          },
        
          {
            name: 'Inventario de Farmacia',
            url: '/Inventario/InventarioEnFarmacia',
            icon: 'fa fa-book',
            children: [
  
              {
                name: 'Medicamentos',
                url: '/Inventario/InventarioEnFarmacia/Medicamentos',
                icon: 'fa fa-medkit',
              },
              {
                name: 'Insumos',
                url: '/Inventario/InventarioEnFarmacia/Insumos',
                icon: 'fa fa-medkit',
              },
              {
                name: 'Reactivos',
                url: '/Inventario/InventarioEnFarmacia/Reactivos',
                icon: 'fa fa-medkit',
              },
              {
                name: 'Ver Todo',
                url: '/Inventario/InventarioEnFarmacia/Productos',
                icon: 'fa fa-eye',
              },
  
            ],
          },
          


        {
          name: 'Inventario de Almacen',
          url: '/Inventario/InventarioEnAlmacen',
          icon: 'fa fa-book',
          children: [

            {
              name: 'Medicamentos',
              url: '/Inventario/InventarioEnAlmacen/Medicamentos',
              icon: 'fa fa-medkit',
            },

            {
              name: 'Insumos',
              url: '/Inventario/InventarioEnAlmacen/Insumos',
              icon: 'fa fa-medkit',
            },
          
            {
              name: 'Reactivos',
              url: '/Inventario/InventarioEnAlmacen/Reactivos',
              icon: 'fa fa-medkit',
            },
    
            {
              name: 'Ver Todo',
              url: '/Inventario/InventarioEnAlmacen/Productos',
              icon: 'fa fa-eye',
            },

          ],

        },

        {
          name: 'Inventario Todos los Productos',
          url: '/Inventario/InventarioTodosLosProductos',
          icon: 'fa fa-book',
        
        },    
            
            ],
    },

  
    { /* farmacia Recetas */
      name: 'SALDOS MINIMOS',
      url: '/SaldosMinimos',
      icon: 'fa fa-tasks',
      children: [

        {
          name: 'Medicamentos',
          url: '/SaldosMinimos/Medicamentos',
          icon: 'fa fa-medkit',
        },
        {
          name: 'Insumos',
          url: '/SaldosMinimos/Insumos',
          icon: 'fa fa-medkit',
        },
        {
          name: 'Reactivos',
          url: '/SaldosMinimos/Reactivos',
          icon: 'fa fa-medkit',
        },
        {
          name: 'Ver Todo',
          url: '/SaldosMinimos/Productos',
          icon: 'fa fa-medkit',
        },
      ],
    },


    // { /* Consultorio Recetas */
    //   name: 'RECETAS EN CONSULTORIO',
    //   url: '/RecetasEnConsultorio',
    //   icon: 'fa fa-user-md',
    //   children: [
    //     {
    //       name: 'Consulta',
    //       url: '/RecetasEnConsultorio/Consulta',
    //       icon: 'fa fa-clipboard',

    //     },

    //     {
    //       name: 'Recetar',
    //       url: '/RecetasEnConsultorio/Recetar',
    //       icon: 'fa fa-edit',

    //     },

    //     {
    //       name: 'Recetas Emitidas',
    //       url: '/RecetasEnConsultorio/RecetasEmitidas',
    //       icon: 'fa fa-list-ul',
    //     },
        
    //   ],
    // },


    { /* farmacia Recetas */
      name: 'RECETAS EN FARMACIA',
      url: '/RecetasEnFarmacia',
      icon: 'fa fa-id-card',
      children: [

        {
          name: 'Recetas por entregar',
          url: '/RecetasEnFarmacia/RecetasPorEntregar',
          icon: 'fa fa-list-ol',

        },
        
        {
          name: 'Recetas Entregadas',
          url: '/RecetasEnFarmacia/RecetasEntregadas',
          icon: 'fa fa-list-ol',

        },
        {
          name: 'Recetas Anuladas',
          url: '/RecetasEnFarmacia/RecetasAnuladas',
          icon: 'fa fa-list-ol',

        },
        
      ],
    },

    // {
    //   name: 'PEDIDOS UNIDADES',
    //   url: '/PedidoUnidad',
    //   icon: 'fa fa-handshake-o',
    //   children: [
    //     {
    //       name: 'Realizar Pedido',
    //       url: '/PedidoUnidad/NuevoPedido',
    //       icon: 'fa fa-pencil-square-o',

    //     },
    //     {
    //       name: 'Pedidos Emitidos',
    //       url: '/PedidoUnidad/PedidosEmitidos',
    //       icon: 'fa fa-list-ol',

    //     },
    //   ],
    // },

    {
      name: 'PEDIDOS FARMACIA',
      url: '/PedidosFarmacia',
      icon: 'icon-note',
      children: [

        {
          name: 'Pedidos Unidad',
          url: '/PedidosFarmacia/PedidosUnidad',
          icon: 'fa fa-book',
          children: [

            {
              name: 'Pedidos Por Entregar',
              url: '/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Pedidos Entregados',
              url: '/PedidosFarmacia/PedidosUnidad/PedidosEntregados',
              icon: 'fa fa-medkit',
            },

          ],
        },
        {
          name: 'Pedidos Almacen',
          url: '/PedidosFarmacia/PedidoAlmacen',
          icon: 'fa fa-book',
          children: [

            {
              name: 'Realizar Pedido Almacen',
              url: '/PedidosFarmacia/PedidoAlmacen/RealizarPedidoAlmacen',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Pedidos Emitidos Almacen',
              url: '/PedidosFarmacia/PedidoAlmacen/PedidosEmitidosAlmacen',
              icon: 'fa fa-medkit',
            },

          ],
        },
        {
          name: 'Pedidos SSU',
          url: '/PedidosFarmacia/PedidosSSU',
          icon: 'fa fa-book',
          children: [

            {
              name: 'Realizar Pedido SSU',
              url: '/PedidosFarmacia/PedidosSSU/RealizarPedidoSSU',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Pedidos Emitidos SSU',
              url: '/PedidosFarmacia/PedidosSSU/PedidosEmitidosSSU',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Recibir Pedido SSU',
              url: '/PedidosFarmacia/PedidosSSU/RecibirPedido',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Pedidos Recibidos SSU',
              url: '/PedidosFarmacia/PedidosSSU/PedidosRecibidos',
              icon: 'fa fa-medkit',
            },
            
     
          ],
        },
        
      ],
    },


    {
      name: 'PEDIDOS ALMACEN',
      url: '/PedidosAlmacen',
      icon: 'icon-note',
      children: [

        {
          name: 'Pedidos Unidad',
          url: '/PedidosAlmacen/PedidosUnidades',
          icon: 'fa fa-book',
          children: [

            {
              name: 'Pedidos Por Entregar',
              url: '/PedidosAlmacen/PedidosUnidades/PedidosPorEntregar',
              icon: 'fa fa-medkit',
            },
            {
              name: 'Pedidos Entregados',
              url: '/PedidosAlmacen/PedidosUnidades/PedidosEntregados',
              icon: 'fa fa-medkit',
            },

          ],
        },


        {
          name: 'Pedidos Farmacia',
          url: '/PedidosAlmacen/PedidoFarmacia',
          icon: 'fa fa-book',
          children: [

            {
              name: 'Pedidos Por Entregar',
              url: '/PedidosAlmacen/PedidoFarmacia/PedidosPorEntregar',
              icon: 'fa fa-medkit',
            },

            {
              name: 'Pedidos Entregados',
              url: '/PedidosAlmacen/PedidoFarmacia/PedidosEntregados',
              icon: 'fa fa-medkit',
            },
      

          ],
        },
        
      ],
    },

    {
      name: 'BIND CARD',
      url: '/BindCard',
      icon: 'fa fa-list',
      children: [
        {
          name: 'BindCard Farmacia',
          url: '/BindCard/BindCardFarmacia',
          icon: 'icon-cursor',
        },
        
        {
          name: 'BindCard Almacen',
          url: '/BindCard/BindCardAlmacen',
          icon: 'icon-cursor',
        },
      ],
    },
    {
      name: 'REPORTES',
      url: '/Reportes',
      icon: 'icon-star',
      children: [
        {
          name: 'Medicamentos',
          url: '/Reportes/Medicamentos',
          icon: 'icon-star',
        },
        {
          name: 'Insumos',
          url: '/Reportes/Insumos',
          icon: 'icon-star',
        },
        {
          name: 'Reactivos',
          url: '/Reportes/Reactivos',
          icon: 'icon-star',
        },
        {
          name: 'Ver Todo',
          url: '/Reportes/Productos',
          icon: 'icon-star',
        },
       
      ],
    },
    {
      name: 'MANUAL DE USUARIO',
      url: 'https://drive.google.com/open?id=1DL4bRdrWLKClcoyfSFHDB0FCYR_07PBa',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'success',
    },

   ],
};
