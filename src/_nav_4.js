export default {
  items: [
    {
      name: 'Inicio',
      url: '/Inicio',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'PROMES',
      },
    },
    {
      name: 'CONSULTA',
      url: '/base',
      icon: 'fa fa-medkit',
      children: [
        { /* mi codigo*/
          name: 'Consulta Nueva',
          url: '/PrimeraConsulta',
          icon: 'fa fa-file-archive-o',
        },
        {
          name: 'Tratamiento',
          url: '/Tratamiento',
          icon: 'fa fa-stethoscope',
        },
        {
          name: 'Consulta',
          url: '/RecetasEnConsultorio/Consulta',
          icon: 'fa fa-clipboard',

        },

        {
          name: 'Recetar',
          url: '/RecetasEnConsultorio/Recetar',
          icon: 'fa fa-edit',

        },

        {
          name: 'Recetas Emitidas',
          url: '/RecetasEnConsultorio/RecetasEmitidas',
          icon: 'fa fa-list-ul',
        },
      ],
    },
    {
      name: 'INSUMOS MEDICOS',
      url: '/insumos',
      icon: 'fa fa-thermometer-empty',
      children: [
        {
          name: 'Nuevo Bind Card',
          url: '/Insumos/BindCard',          
          icon: 'fa fa-folder-open',
        },       
        {
          name: 'Registrar Entrada',
          url: '/Insumos/RegistroEntrada',
          icon: 'fa fa-mail-forward (alias)',
        },
        {
          name: 'Registrar Salida',
          url: '/Insumos/RegistroSalida',
          icon: 'fa fa-mail-reply (alias)',
        },
        {
          name: 'Lista General',
          url: '/Insumos/ListaGeneral',
          icon: 'fa fa-file-text',
        },
        {
          name: 'Lista Detalle',
          url: '/Insumos/ListaDetalle',
          icon: 'fa fa-file-text-o',
        },        
        
      ],
    },
    {
      name: 'PEDIDOS',
      url: '/Pedido',
      icon: 'fa fa-file-text',
      children: [
        {
          name: 'Realizar Pedido',
          url: '/PedidoUnidad/NuevoPedido',
          icon: 'fa fa-pencil-square-o',

        },
        {
          name: 'Pedidos Emitidos',
          url: '/PedidoUnidad/PedidosEmitidos',
          icon: 'fa fa-list-ol',

        },      
        // {
        //   name: 'Pedido de medicamentos y/o insumos',
        //   url: '/Pedido/PedidoMedicamentoInsumo',
        //   icon: 'fa fa-file-text-o',
        // },
        {
          name: 'Ver todo',
          url: '/Pedido/VerTodo',
          icon: 'fa fa-paste (alias)',
        },
      ]
    },
    {
      name: 'CAMPAÑAS DE PREVENCIÓN',
      url: '/CampPrevencion',
      icon: 'fa fa-calendar',
      children: [
        {
          name: 'Agregar Campaña y/o Tareas',
          url: '/CampPrevencion/CampTareas',
          icon: 'fa fa-calendar-plus-o',
        },
        {
          name: 'Detalle de Cronograma',
          url: '/CampPrevencion/VerAsignacionTareas',
          icon: 'fa fa-calendar-minus-o',
        },
        {
          name: 'Lista de Campañas Finalizadas',
          url: '/CampPrevencion/Cronograma/ListaCronogramaFin',
          icon: 'fa fa-calendar-times-o',
        },
        {
          name: 'Registra Nuevo Cronograma',
          url: '/CampPrevencion/Cronograma/Registrar',
          icon: 'fa fa-calendar-plus-o',
        },
        {
          name: 'Cronograma de Campañas',
          url: '/CampPrevencion/Calendario',
          icon: 'fa fa-calendar-check-o',
        },
      ],
    },
    // {
    //   name: 'MANUAL DE USUARIO',
    //   url: 'https://drive.google.com/file/d/1go0LAbsihM36Wz_ic9v80Sxww_5KG3SY/view?usp=sharing',
    //   icon: 'icon-cloud-download',
    //   class: 'mt-auto',
    // },
    {
      name: 'MANUAL DE USUARIO',
      url: 'https://drive.google.com/open?id=1DL4bRdrWLKClcoyfSFHDB0FCYR_07PBa',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'success',
    },



   ],
};