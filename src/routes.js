import React from 'react';
import Loadable from 'react-loadable'

import DefaultLayout from './containers/DefaultLayout';

function Loading() {
  return <div>Loading...</div>;
}


const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});
const Dashboard1 = Loadable({
  loader: () => import('./views/Dashboard/Dashboard1'),
  loading: Loading,
});
const Dashboard2 = Loadable({
  loader: () => import('./views/Dashboard/Dashboard2'),
  loading: Loading,
});
const Dashboard3 = Loadable({
  loader: () => import('./views/Dashboard/Dashboard3'),
  loading: Loading,
});
const Dashboard4 = Loadable({
  loader: () => import('./views/Dashboard/Dashboard4'),
  loading: Loading,
});
const Dashboard5 = Loadable({
  loader: () => import('./views/Dashboard/Dashboard5'),
  loading: Loading,
});
const Dashboard6 = Loadable({
  loader: () => import('./views/Dashboard/Dashboard6'),
  loading: Loading,
});
/*codigo FARMACIA*/


const NuevoMedicamento = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Medicamentos/NuevoMedicamento'),
  loading: Loading,
});

const EstadoMedicamento = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Medicamentos/EstadoMedicamento'),
  loading: Loading,
});

const NuevoInsumo = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Insumos/NuevoInsumo'),
  loading: Loading,
});

const EstadoInsumo = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Insumos/EstadoInsumo'),
  loading: Loading,
});

const NuevoReactivo = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Reactivos/NuevoReactivo'),
  loading: Loading,
});

const EstadoReactivo = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Reactivos/EstadoReactivo'),
  loading: Loading,
});

const NuevoProducto = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Productos/NuevoProducto'),
  loading: Loading,
});

const EstadoProducto = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Productos/EstadoProducto'),
  loading: Loading,
});

const LlevarDepositoMedicamentos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Medicamentos/LlevarDeposito'),
  loading: Loading,
});

const DepositoMedicamentos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Medicamentos/Deposito'),
  loading: Loading,
});

const LlevarDepositoInsumos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Insumos/LlevarDeposito'),
  loading: Loading,
});

const DepositoInsumos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Insumos/Deposito'),
  loading: Loading,
});

const LlevarDepositoReactivos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Reactivos/LlevarDeposito'),
  loading: Loading,
});

const DepositoReactivos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Reactivos/Deposito'),
  loading: Loading,
});

const LlevarDepositoProductos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Productos/LlevarDeposito'),
  loading: Loading,
});

const DepositoProductos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Productos/Deposito'),
  loading: Loading,
});



const Tables = Loadable({
  loader: () => import('./views/Formulacion/Tables'),
  loading: Loading,
});



//###########PRODUCTOS FARMACEUTICOS
// Lista Medicamentos

const Medicamentos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Medicamentos/'),
  loading: Loading,
});

const Insumos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Insumos/Insumos'),
  loading: Loading,
});

const Reactivos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Reactivos/Reactivos'),
  loading: Loading,
});

const Productos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaProductos/Productos/Productos'),
  loading: Loading,
});


// Lista Productos Lote
const ProductosLote = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Productos/Productos'),
  loading: Loading,
});

const MedicamentosLote = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Medicamentos/Medicamentos'),
  loading: Loading,
});

const InsumosLote = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Insumos/Insumos'),
  loading: Loading,
});


const ReactivosLote = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/ListaLote/Reactivos/Reactivos'),
  loading: Loading,
});

// Lista Productos (Almacen)
const ProductosAlmacen = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Almacen/Productos/Productos'),
  loading: Loading,
});

const MedicamentosAlmacen = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Almacen/Medicamentos/Medicamentos'),
  loading: Loading,
});

const InsumosAlmacen = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Almacen/Insumos/Insumos'),
  loading: Loading,
});

const ReactivosAlmacen = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Almacen/Reactivos/Reactivos'),
  loading: Loading,
});

// Lista Productos (Farmacia)
const ProductosFarmacia = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Farmacia/Productos/Productos'),
  loading: Loading,
});

const MedicamentosFarmacia = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Farmacia/Medicamentos/Medicamentos'),
  loading: Loading,
});

const InsumosFarmacia = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Farmacia/Insumos/Insumos'),
  loading: Loading,
});

const ReactivosFarmacia = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Farmacia/Reactivos/Reactivos'),
  loading: Loading,
});
  
//### PRODUCTOS TODOS MAS UBICACION
const ProductosTodos = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Productos_Todos/Productos_Todos'),
  loading: Loading,
});

//#####HACER PRUEBAS
const Hola = Loadable({
  loader: () => import('./views/ProductosFarmaceuticos/Holas/Hola/Hola'),
  loading: Loading,
});

//########### SALDOS MINIMOS
// Saldos Minimos Medicamentos
const SaldosMedicamentos = Loadable({
  loader: () => import('./views/SaldosMinimos/Medicamentos'),
  loading: Loading,
});

const SaldosInsumos = Loadable({
  loader: () => import('./views/SaldosMinimos/Insumos'),
  loading: Loading,
});

const SaldosReactivos = Loadable({
  loader: () => import('./views/SaldosMinimos/Reactivos'),
  loading: Loading,
});

const SaldosProductos = Loadable({
  loader: () => import('./views/SaldosMinimos/Productos'),
  loading: Loading,
});


// const Productos_Todos = Loadable({
//   loader: () => import('./views/ProductosFarmaceuticos/Productos_Todos/Productos_Todos'),
//   loading: Loading,
// });

// const Productos_Almacen = Loadable({
//   loader: () => import('./views/ProductosFarmaceuticos/Productos_Almacen/Productos_Almacen'),
//   loading: Loading,
// });

// const Productos_Farmacia = Loadable({
//   loader: () => import('./views/ProductosFarmaceuticos/Productos_Farmacia/Productos_Farmacia'),
//   loading: Loading,
// });







//ROUTES RECETAS-CONSULTORIO

const NuevaReceta2 = Loadable({
  loader: () => import('./views/RecetasConsultorio/NuevaReceta2'),
  loading: Loading,
});
const RecetasEmitidas = Loadable({
  loader: () => import('./views/RecetasConsultorio/RecetasEmitidas'),
  loading: Loading,
});

// const DetalleReceta= Loadable({
//   loader: () => import('./views/RecetasConsultorio/DetalleReceta'),
//   loading: Loading,
// });



const ElegirReceta = Loadable({
  loader: () => import('./views/RecetasConsultorio/RecetasEmitidas/ElegirReceta'),
  loading: Loading,
});

const Consulta = Loadable({
  loader: () => import('./views/RecetasConsultorio/Consulta'),
  loading: Loading,
});



const LLenarReceta = Loadable({
  loader: () => import('./views/RecetasConsultorio/NuevaReceta2/NuevaReceta3'),
  loading: Loading,
});


//ROUTES RECETAS-FARMACIA
const RecetasPorEntregar = Loadable({
  loader: () => import('./views/RecetasFarmacia/RecetasPorEntregar'),
  loading: Loading,
});

const ElegirRecetaF = Loadable({
  loader: () => import('./views/RecetasFarmacia/RecetasPorEntregar/ElegirRecetaF'),
  loading: Loading,
});


const RecetasEntregadas = Loadable({
  loader: () => import('./views/RecetasFarmacia/RecetasEntregadas'),
  loading: Loading,
});

const ElegirRecetaE = Loadable({
  loader: () => import('./views/RecetasFarmacia/RecetasEntregadas/ElegirRecetaE'),
  loading: Loading,
});

const AnularReceta = Loadable({
  loader: () => import('./views/RecetasFarmacia/AnularReceta'),
  loading: Loading,
});

const ElegirRecetaA = Loadable({
  loader: () => import('./views/RecetasFarmacia/AnularReceta/ElegirRecetaA'),
  loading: Loading,
});


const RecetasAnuladas = Loadable({
  loader: () => import('./views/RecetasFarmacia/RecetasAnuladas'),
  loading: Loading,
});

const ERecetaA = Loadable({
  loader: () => import('./views/RecetasFarmacia/RecetasAnuladas/ElegirRecetaA'),
  loading: Loading,
});

// CANTIDAD DE MEDICAMENTOS

const CantMedicamento = Loadable({
  loader: () => import('./views/RecetasConsultorio/NuevaReceta2/NuevaReceta2'),
  loading: Loading,
});

//ROUTES PEDIDOS UNIDADES 
const NuevoPedido = Loadable({
  loader: () => import('./views/PedidosUnidad/NuevoPedido/NuevoPedido'),
  loading: Loading,
});

const PedidosEmitidos = Loadable({
  loader: () => import('./views/PedidosUnidad/PedidosEmitidos/PedidosEmitidos'),
  loading: Loading,
});

const ElegirPedido = Loadable({
  loader: () => import('./views/PedidosUnidad/PedidosEmitidos/ElegirPedido'),
  loading: Loading,
});

//ROUTES PEDIDOS FARMACIA
const PedidosPorEntregar = Loadable({
  loader: () => import('./views/PedidosFarmacia/PedidosPorEntregar/PedidosPorEntregar'),
  loading: Loading,
});

const ElegirPedidoF = Loadable({
  loader: () => import('./views/PedidosFarmacia/PedidosPorEntregar/ElegirPedidoF'),
  loading: Loading,
});

const PedidosEntregados = Loadable({
  loader: () => import('./views/PedidosFarmacia/PedidosEntregados/PedidosEntregados'),
  loading: Loading,
});

const ElegirPedidoE = Loadable({
  loader: () => import('./views/PedidosFarmacia/PedidosEntregados/ElegirPedidoE'),
  loading: Loading,
});


const RealizarPedidoA= Loadable({
  loader: () => import('./views/PedidosFarmacia/RealizarPedidoA/RealizarPedidoA'),
  loading: Loading,
});


const PEmitidosAlmacen= Loadable({
  loader: () => import('./views/PedidosFarmacia/PEmitidosAlmacen/PEmitidosAlmacen'),
  loading: Loading,
});
const ElegirPA = Loadable({
  loader: () => import('./views/PedidosFarmacia/PEmitidosAlmacen/ElegirPA'),
  loading: Loading,
});

//REALIZAR PEDIDO  SSU 
const RealizarPedidoSSU= Loadable({
  loader: () => import('./views/PedidosFarmacia/RealizarPedidoSSU/RealizarPedidoSSU'),
  loading: Loading,
});

//PEDIDOS EMITIDOS SSU 
const PEmitidosSSU= Loadable({
  loader: () => import('./views/PedidosFarmacia/PEmitidosSSU/PEmitidosSSU'),
  loading: Loading,
});

const ElegirPSSU = Loadable({
  loader: () => import('./views/PedidosFarmacia/PEmitidosSSU/ElegirPSSU'),
  loading: Loading,
});


//RECIBIR PEDIDO SSU 
const RecibirPedidoSSU= Loadable({
  loader: () => import('./views/PedidosFarmacia/RecibirPedidoSSU/RecibirPedidoSSU'),
  loading: Loading,
});

const ElegirRSSU = Loadable({
  loader: () => import('./views/PedidosFarmacia/RecibirPedidoSSU/ElegirRSSU'),
  loading: Loading,
});

//PEDIDOS RECIBIDOS SSU 
const PedidosRecibidosSSU= Loadable({
  loader: () => import('./views/PedidosFarmacia/PedidosRecibidosSSU/PedidosRecibidosSSU'),
  loading: Loading,
});


const ElegirRecibidoSSU= Loadable({
  loader: () => import('./views/PedidosFarmacia/PedidosRecibidosSSU/ElegirRecibidoSSU'),
  loading: Loading,
});
//ROUTES PEDIDOS ALMACEN 

const PedidosPorEntregarA = Loadable({
  loader: () => import('./views/PedidosAlmacen/PedidosPorEntregarA/PedidosPorEntregarA'),
  loading: Loading,
});

const ElegirPedidoA = Loadable({
  loader: () => import('./views/PedidosAlmacen/PedidosPorEntregarA/ElegirPedidoA'),
  loading: Loading,
});

const PedidosEntregadosA = Loadable({
  loader: () => import('./views/PedidosAlmacen/PedidosEntregadosA/PedidosEntregadosA'),
  loading: Loading,
});

const ElegirPedidoAE = Loadable({
  loader: () => import('./views/PedidosAlmacen/PedidosEntregadosA/ElegirPedidoAE'),
  loading: Loading,
});

const PorEntregarAF = Loadable({
  loader: () => import('./views/PedidosAlmacen/PorEntregarAF/PorEntregarAF'),
  loading: Loading,
});

const EntregadosAF = Loadable({
  loader: () => import('./views/PedidosAlmacen/EntregadosAF/EntregadosAF'),
  loading: Loading,
});

const EPedidoAF= Loadable({
  loader: () => import('./views/PedidosAlmacen/PorEntregarAF/EPedidoAF'),
  loading: Loading,
});

const EPedidoAFE= Loadable({
  loader: () => import('./views/PedidosAlmacen/EntregadosAF/EPedidoAFE'),
  loading: Loading,
});

//ROUTES BIND CARD 
const BindCardFarmacia= Loadable({
  loader: () => import('./views/BindCard/BindCardFarmacia/BindCardFarmacia'),
  loading: Loading,
});

const BindCardAlmacen= Loadable({
  loader: () => import('./views/BindCard/BindCardAlmacen/BindCardAlmacen'),
  loading: Loading,
});

//ROUTES REPORTES
const ReporteMedicamentos= Loadable({
  loader: () => import('./views/Reportes/Medicamentos'),
  loading: Loading,
});

const ReporteInsumos= Loadable({
  loader: () => import('./views/Reportes/Insumos'),
  loading: Loading,
});

const ReporteReactivos= Loadable({
  loader: () => import('./views/Reportes/Reactivos'),
  loading: Loading,
});

const ReporteProductos= Loadable({
  loader: () => import('./views/Reportes/Productos'),
  loading: Loading,
});
const Login = Loadable({
  loader: () => import('./views/Login'),
  loading: Loading,
});
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
 
  { path: '/', exact: true, name: 'Inicio', component: DefaultLayout },
  { path: '/dashboard', name: 'Pagina Principal', component: Dashboard },
  { path: '/dashboard1', name: 'Pagina Principal', component: Dashboard1 },
  { path: '/dashboard2', name: 'Pagina Principal', component: Dashboard2 },
  { path: '/dashboard3', name: 'Pagina Principal', component: Dashboard3 },
  { path: '/dashboard4', name: 'Pagina Principal', component: Dashboard4 },
  { path: '/dashboard5', name: 'Pagina Principal', component: Dashboard5 },
  { path: '/dashboard6', name: 'Pagina Principal', component: Dashboard6 },
  { path: '/login', name: 'Login', component: Login },
  // { path: '/theme', exact: true, name: 'Theme', component: Colors },
  // { path: '/theme/colors', name: 'Colors', component: Colors },
  // { path: '/theme/typography', name: 'Typography', component: Typography },
  // { path: '/base', exact: true, name: 'Base', component: Cards },
  // { path: '/base/cards', name: 'Cards', component: Cards },
  // { path: '/base/forms', name: 'Forms', component: Forms },
  // { path: '/base/switches', name: 'Switches', component: Switches },
  // { path: '/base/tables', name: 'Tables', component: Tables },
  // { path: '/base/tabs', name: 'Tabs', component: Tabs },
  // { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  // { path: '/base/carousels', name: 'Carousel', component: Carousels },
  // { path: '/base/collapses', name: 'Collapse', component: Collapses },
  // { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  // { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  // { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  // { path: '/base/navbars', name: 'Navbars', component: Navbars },
  // { path: '/base/navs', name: 'Navs', component: Navs },
  // { path: '/base/paginations', name: 'Paginations', component: Paginations },
  // { path: '/base/popovers', name: 'Popovers', component: Popovers },
  // { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  // { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  // { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
  // { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  // { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
  // { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  // { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  // { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  // { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  // { path: '/icons/flags', name: 'Flags', component: Flags },
  // { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  // { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  // { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  // { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  // { path: '/notifications/badges', name: 'Badges', component: Badges },
  // { path: '/notifications/modals', name: 'Modals', component: Modals },
  // { path: '/widgets', name: 'Widgets', component: Widgets },
  // { path: '/charts', name: 'Charts', component: Charts },
  // { path: '/users', exact: true,  name: 'Users', component: Users },
  // { path: '/users/:id', exact: true, name: 'User Details', component: User },
  /* codigoF */
  { path: '/NuevoMedicamento', name: 'NuevoMedicamento', component: NuevoMedicamento },
  { path: '/Formulacion/tables', name: 'ListaObjetvo', component: Tables },


// --------- PRODUCTOS -----------------
  //##--LISTA PRODUCTOS
  

  { path: '/Inventario', exact: true, name: 'Inventario', component: ProductosTodos },
  { path: '/Inventario/InventarioGeneral', exact: true, name: 'Inventario General', component: Productos },
  { path: '/Inventario/InventarioGeneral/Medicamentos', exact: true, name: 'Medicamentos', component: Medicamentos },
  { path: '/Inventario/InventarioGeneral/Medicamentos/NuevoMedicamento', exact:true, name: 'Nuevo Medicamento', component: NuevoMedicamento },
  { path: '/Inventario/InventarioGeneral/Medicamentos/EstadoMedicamento', exact:true, name: 'Habilitar-Inhabilitar Medicamento', component: EstadoMedicamento },

  { path: '/Inventario/InventarioGeneral/Insumos', exact: true, name: 'Insumos', component: Insumos },
  { path: '/Inventario/InventarioGeneral/Insumos/NuevoInsumo', exact:true, name: 'Nuevo Insumo', component: NuevoInsumo },
  { path: '/Inventario/InventarioGeneral/Insumos/EstadoInsumo', exact:true, name: 'Habilitar-Inhabilitar Insumo', component: EstadoInsumo },

  { path: '/Inventario/InventarioGeneral/Reactivos', exact:true, name: 'Reactivos', component: Reactivos },
  { path: '/Inventario/InventarioGeneral/Reactivos/NuevoReactivo', exact:true, name: 'Nuevo Reactivo', component: NuevoReactivo },
  { path: '/Inventario/InventarioGeneral/Reactivos/EstadoReactivo', exact:true, name: 'Habilitar-Inhabilitar Reactivo', component: EstadoReactivo },

  { path: '/Inventario/InventarioGeneral/Productos', exact:true, name: 'Productos', component: Productos },
  { path: '/Inventario/InventarioGeneral/Productos/NuevoProducto', exact:true, name: 'Nuevo Producto', component: NuevoProducto },
  { path: '/Inventario/InventarioGeneral/Productos/EstadoProducto', exact:true, name: 'Habilitar-Inhabilitar Producto', component: EstadoProducto },

  //##--LISTA PRODUCTOS POR LOTE
  { path: '/Inventario/InventarioPorLote', exact: true, name: 'Inventario por Lote', component: ProductosLote },
  { path: '/Inventario/InventarioPorLote/Productos', exact: true, name: 'Productos', component: ProductosLote },
  { path: '/Inventario/InventarioPorLote/Productos/LlevarDepositoProductos', exact: true, name: 'Llevar a Deposito', component: LlevarDepositoProductos },
  { path: '/Inventario/InventarioPorLote/Productos/DepositoProductos', exact: true, name: 'Deposito', component: DepositoProductos },

  { path: '/Inventario/InventarioPorLote/Medicamentos', exact: true, name: 'Medicamentos', component: MedicamentosLote },
  { path: '/Inventario/InventarioPorLote/Medicamentos/LlevarDepositoMedicamentos', exact: true, name: 'Llevar a Deposito', component: LlevarDepositoMedicamentos },
  { path: '/Inventario/InventarioPorLote/Medicamentos/DepositoMedicamentos', exact: true, name: 'Deposito', component: DepositoMedicamentos },

  { path: '/Inventario/InventarioPorLote/Insumos', exact:true, name: 'Insumos', component: InsumosLote },
  { path: '/Inventario/InventarioPorLote/Insumos/LlevarDepositoInsumos', exact: true, name: 'Llevar a Deposito', component: LlevarDepositoInsumos },
  { path: '/Inventario/InventarioPorLote/Insumos/DepositoInsumos', exact: true, name: 'Deposito', component: DepositoInsumos },

  { path: '/Inventario/InventarioPorLote/Reactivos', exact:true, name: 'Reactivos', component: ReactivosLote },
  { path: '/Inventario/InventarioPorLote/Reactivos/LlevarDepositoReactivos', exact: true, name: 'Llevar a Deposito', component: LlevarDepositoReactivos },
  { path: '/Inventario/InventarioPorLote/Reactivos/DepositoReactivos', exact: true, name: 'Deposito', component: DepositoReactivos },

  //##------ PRUEBA
  { path: '/ProductosFarmaceuticos/Holas/Hola', name: 'Prueba', component: Hola },



  //##--LISTA PRODUCTOS POR UBICACION: ALMACEN
  { path: '/Inventario/InventarioEnAlmacen', exact: true, name: 'Inventario en Almacen', component: ProductosAlmacen },
  { path: '/Inventario/InventarioEnAlmacen/Productos', name: 'Productos', component: ProductosAlmacen },
  { path: '/Inventario/InventarioEnAlmacen/Medicamentos', name: 'Medicamentos', component: MedicamentosAlmacen },
  { path: '/Inventario/InventarioEnAlmacen/Insumos', name: 'Insumos', component: InsumosAlmacen },
  { path: '/Inventario/InventarioEnAlmacen/Reactivos', name: 'Reactivos', component: ReactivosAlmacen },


 //##--LISTA PRODUCTOS POR UBICACION: FARMACIA
 { path: '/Inventario/InventarioEnFarmacia', exact: true, name: 'Inventario en Farmacia', component: ProductosFarmacia },

 { path: '/Inventario/InventarioEnFarmacia/Productos', name: 'Productos', component: ProductosFarmacia },
 { path: '/Inventario/InventarioEnFarmacia/Medicamentos', name: 'Medicamentos', component: MedicamentosFarmacia },
 { path: '/Inventario/InventarioEnFarmacia/Insumos', name: 'Insumos', component: InsumosFarmacia },
 { path: '/Inventario/InventarioEnFarmacia/Reactivos', name: 'Reactivos', component: ReactivosFarmacia },
  
 //##--LISTA PRODUCTOS TODOS MAS UBICACION

 { path: '/Inventario/InventarioTodosLosProductos', name: 'Todos los Productos', component: ProductosTodos},


  //{ path: '/ProductosFarmaceuticos/Productos_Todos', name: 'Productos_Todos', component: Productos_Todos },

  // { path: '/ProductosFarmaceuticos/Productos_Almacen', name: 'Productos_Almacen', component: Productos_Almacen },
  // { path: '/ProductosFarmaceuticos/Productos_Farmacia', name: 'Productos_Farmacia', component: Productos_Farmacia },
  
  // { path: '/ProductosFarmaceuticos/Productos', name: 'Productos', component: Productos },
  // { path: '/ProductosFarmaceuticos/Medicamentos', name: 'Medicamentos', component: Medicamentos },
  // { path: '/ProductosFarmaceuticos/Reactivos', name: 'Reactivos', component: Reactivos },
  // { path: '/ProductosFarmaceuticos/Insumos', name: 'Insumos', component: Insumos },


  // { path: '/Formulacion/tabs', name: 'Tabs', component: Tabs },

// --------- SALDOS MINIMOS -----------------
  //##-- SALDO MEDICAMENTOS
  { path: '/SaldosMinimos', exact: true, name: 'Saldos Minimos', component: SaldosMedicamentos },
  { path: '/SaldosMinimos/Medicamentos', exact: true, name: 'Medicamentos', component: SaldosMedicamentos },
  { path: '/SaldosMinimos/Insumos', exact: true, name: 'Insumos', component: SaldosInsumos },
  { path: '/SaldosMinimos/Reactivos', exact: true, name: 'Reactivos', component: SaldosReactivos },
  { path: '/SaldosMinimos/Productos', exact: true, name: 'Productos', component: SaldosProductos },

  // -------RECETAS-CONSULTORIO
  { path: '/RecetasEnConsultorio', exact: true, name: 'Receta En Consultorio', component: RecetasEmitidas },
  { path: '/RecetasEnConsultorio/Consulta', exact: true, name: 'Consulta', component: Consulta },
  { path: '/RecetasEnConsultorio/Recetar', exact: true, name: 'Recetar', component: NuevaReceta2 },
  { path: '/RecetasEnConsultorio/RecetasEmitidas', exact: true, name: 'Recetas Emitidas', component: RecetasEmitidas },
  
  // { path: '/RecetasConsultorio/DetalleReceta', exact: true, name: 'DetalleReceta', component: DetalleReceta },
  { path: '/RecetasEnConsultorio/RecetasEmitidas/ElegirReceta/:id_receta/:id_consulta/:codigoAfiliacion/:nombresA/:apellidoPaternoA/:apellidoMaternoA/:nombresM/:apellidoPaternoM/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Detalle Receta', component: ElegirReceta },
  
  { path: '/RecetasEnConsultorio/Consulta/Recetar/:id_consulta/:codigoA/:nombreA/:apellido_pA/:apellido_mA/:nombreM/:apellido_pM/:apellido_mM/:fecha', exact: true, name: 'Recetar', component: LLenarReceta },

  // -------RECETAS-FARMACIA
  { path: '/RecetasEnFarmacia', exact: true, name: 'Recetas', component: RecetasPorEntregar },
  { path: '/RecetasEnFarmacia/RecetasPorEntregar', exact: true, name: 'Recetas Por Entregar', component: RecetasPorEntregar },
  { path: '/RecetasEnFarmacia/RecetasPorEntregar/ElegirReceta/:id_receta/:id_consulta/:codigoAfiliacion/:nombresA/:apellidoPaternoA/:apellidoMaternoA/:nombresM/:apellidoPaternoM/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Detalle Receta', component: ElegirRecetaF },
 
  { path: '/RecetasEnFarmacia/RecetasEntregadas', exact: true, name: 'Recetas Entregadas', component: RecetasEntregadas },
  { path: '/RecetasEnFarmacia/RecetasEntregadas/ElegirReceta/:id_receta/:id_consulta/:codigoAfiliacion/:nombresA/:apellidoPaternoA/:apellidoMaternoA/:nombresM/:apellidoPaternoM/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Receta Entrega', component: ElegirRecetaE },
  { path: '/RecetasEnFarmacia/RecetasAnuladas', exact: true, name: 'Recetas Anuladas', component: RecetasAnuladas },
  { path: '/RecetasEnFarmacia/RecetasAnuladas/ElegirReceta/:id_receta/:id_consulta/:codigoAfiliacion/:nombresA/:apellidoPaternoA/:apellidoMaternoA/:nombresM/:apellidoPaternoM/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Receta Anular', component: ERecetaA },

  { path: '/RecetasEnFarmacia/RecetasAnuladas/AnularReceta', exact: true, name: 'Anular Receta', component: AnularReceta },
  { path: '/RecetasEnFarmacia/RecetasAnuladas/AnularReceta/ElegirReceta/:id_receta/:id_consulta/:codigoAfiliacion/:nombresA/:apellidoPaternoA/:apellidoMaternoA/:nombresM/:apellidoPaternoM/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Receta Entrega', component: ElegirRecetaA },

  { path: '/cantidadMedicamento/:num', exact: true, name: 'Cantidad', component: CantMedicamento },

  //------  PEDIDOS UNIDADES ------
  { path: '/PedidoUnidad', exact: true, name: 'Pedidos Unidad', component: PedidosEmitidos },
  { path: '/PedidoUnidad/NuevoPedido', exact: true, name: 'Realizar Pedido', component: NuevoPedido },                      
  
  { path: '/PedidoUnidad/PedidosEmitidos', exact: true, name: 'Pedidos Emitidos', component: PedidosEmitidos },
  { path: '/PedidoUnidad/PedidosEmitidos/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido', component: ElegirPedido },

  //------- PEDIDOS FARMACIA -------
  { path: '/PedidosFarmacia', exact: true, name: 'Pedidos Farmacia', component: PedidosPorEntregar },
  { path: '/PedidosFarmacia/PedidosUnidad', exact: true, name: 'Pedidos Unidad', component: PedidosPorEntregar },
  { path: '/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar', exact: true, name: 'Pedidos Por Entregar', component: PedidosPorEntregar },

  { path: '/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido', component: ElegirPedidoF },

  { path: '/PedidosFarmacia/PedidosUnidad/PedidosEntregados', exact: true, name: 'Pedidos Entregados', component: PedidosEntregados },
  { path: '/PedidosFarmacia/PedidosUnidad/PedidosEntregados/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido', component: ElegirPedidoE },

  { path: '/PedidosFarmacia/PedidoAlmacen', exact: true, name: 'Pedidos Almacen', component: PEmitidosAlmacen },
  { path: '/PedidosFarmacia/PedidoAlmacen/RealizarPedidoAlmacen', exact: true, name: 'Realizar Pedido', component: RealizarPedidoA },
  { path: '/PedidosFarmacia/PedidoAlmacen/PedidosEmitidosAlmacen', exact: true, name: 'Pedidos Emitidos a Almacen', component: PEmitidosAlmacen},
  { path: '/PedidosFarmacia/PedidoAlmacen/PedidosEmitidosAlmacen/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedidos', component: ElegirPA },

  //PEDIDO SSSU (EXTERNO)
  { path: '/PedidosFarmacia/PedidosSSU', exact: true, name: 'Pedidos SSU', component: PEmitidosSSU },
  { path: '/PedidosFarmacia/PedidosSSU/RealizarPedidoSSU', exact: true, name: 'Realizar Pedido a SSU', component: RealizarPedidoSSU },

  { path: '/PedidosFarmacia/PedidosSSU/PedidosEmitidosSSU', exact: true, name: 'Pedidos Emitidos a SSU', component: PEmitidosSSU },
  { path: '/PedidosFarmacia/PedidosSSU/PedidosEmitidosSSU/ElegirPedidoSSU/:id_pedido/:unidadId/:unidad/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:personalE/:fecha_recibida/:hora_recibida', exact: true, name: 'Datos Pedido', component: ElegirPSSU },
  
  { path: '/PedidosFarmacia/PedidosSSU/RecibirPedido', exact: true, name: 'Recibir Pedido SSU', component: RecibirPedidoSSU },  
  { path: '/PedidosFarmacia/PedidosSSU/RecibirPedido/ElegirPedidoSSU/:id_pedido/:unidadId/:unidad/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:personalE/:fecha_recibida/:hora_recibida', exact: true, name: 'Datos Pedido', component: ElegirRSSU },

  { path: '/PedidosFarmacia/PedidosSSU/PedidosRecibidos', exact: true, name: 'Pedidos Recibidos SSU', component: PedidosRecibidosSSU },
  { path: '/PedidosFarmacia/PedidosSSU/PedidosRecibidos/ElegirPedidoSSU/:id_pedido/:unidadId/:unidad/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:personalE/:fecha_recibida/:hora_recibida', exact: true, name: 'Datos Pedidos Recibido', component: ElegirRecibidoSSU },


  //----- PEDIDOS ALMACEN ---------------------
  { path: '/PedidosAlmacen', exact: true, name: 'Pedidos Almacen', component: PedidosPorEntregarA },
  { path: '/PedidosAlmacen/PedidosUnidades', exact: true, name: 'Pedidos Unidades', component: PedidosPorEntregarA },
  { path: '/PedidosAlmacen/PedidosUnidades/PedidosPorEntregar', exact: true, name: 'Pedidos Por Entregar', component: PedidosPorEntregarA },

  { path: '/PedidosAlmacen/PedidosUnidades/PedidosPorEntregar/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido Por Entregar Almacen', component: ElegirPedidoA },

  { path: '/PedidosAlmacen/PedidosUnidades/PedidosEntregados', exact: true, name: 'Pedidos Entregados Almacen', component: PedidosEntregadosA },
  { path: '/PedidosAlmacen/PedidosUnidades/PedidosEntregados/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido Entregado Almacen', component: ElegirPedidoAE },

  { path: '/PedidosAlmacen/PedidoFarmacia', exact: true, name: 'Pedidos Farmacia', component: PorEntregarAF },
  { path: '/PedidosAlmacen/PedidoFarmacia/PedidosPorEntregar', exact: true, name: 'Pedidos por Entregar', component: PorEntregarAF },
  
  { path: '/PedidosAlmacen/PedidoFarmacia/PedidosPorEntregar/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido Por Entregar', component: EPedidoAF },
  
  { path: '/PedidosAlmacen/PedidoFarmacia/PedidosEntregados', exact: true, name: 'Pedidos Entregados', component: EntregadosAF },
  { path: '/PedidosAlmacen/PedidoFarmacia/PedidosEntregados/ElegirPedido/:id_pedido/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion/:fecha_emision/:hora_emision/:farmaceuticaN/:farmaceuticaA/:fecha_entrega/:hora_entrega', exact: true, name: 'Datos Pedido Por Entregar', component: EPedidoAFE },



  { path: '/BindCard', exact: true, name: 'Bind Card', component: BindCardFarmacia },
  { path: '/BindCard/BindCardFarmacia', exact: true, name: 'Bind Card Farmacia', component: BindCardFarmacia },

  { path: '/BindCard/BindCardAlmacen', exact: true, name: 'Bind Card Almacen', component: BindCardAlmacen },

  { path: '/Reportes', exact: true, name: 'Reportes', component: ReporteMedicamentos },

  { path: '/Reportes/Medicamentos', exact: true, name: 'Reportes Medicamento', component: ReporteMedicamentos },
  { path: '/Reportes/Insumos', exact: true, name: 'Reportes Insumos', component: ReporteInsumos },
  { path: '/Reportes/Reactivos', exact: true, name: 'Reportes Reactivos', component: ReporteReactivos },
  { path: '/Reportes/Productos', exact: true, name: 'Reportes Productos', component: ReporteProductos },
 
];

export default routes;
  