export default {
  items: [
    {
      name: 'Inicio',
      url: '/Inicio',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'HOSPITAL',
      },
    },
    {
      name: 'CATALOGO',
      url: '/catalogo',
      icon: 'fa fa-edit fa-lg mt-4',
      children: [
        {
          name: 'Areas de Laboratorio',
          url: '/catalogo/areas',
          icon: 'fa fa-angle-right fa-lg mt-4',
        },
        {
          name: 'Examenes',
          url: '/catalogo/examenes',
          icon: 'fa fa-angle-right fa-lg mt-4',
        },
      ],
    },
    {
      name: 'O/ DE LABORATORIO',
      url: '/ordenesLaboratorio',
      icon: 'fa fa-list-alt fa-lg mt-4',

    },
    {
      name: 'O/ CANCELADAS',
      url: '/ordenesLaboratorio/cancelado',
      icon: 'fa fa-trash fa-lg mt-4',
    },
    {
      name: 'O/ FINALIZADAS',
      url: '/ordenesLaboratorio/finalizado',
      icon: 'fa fa-check-circle fa-lg mt-4',
    },
    {
      name: 'ESTADISTICAS',
      url: '/estadisticas',
      icon: 'fa fa-bar-chart fa-lg mt-4',
    },
  
  {
    name: 'PEDIDOS',
    url: '/PedidoUnidad',
    icon: 'fa fa-handshake-o',
    children: [
      {
        name: 'Realizar Pedido',
        url: '/PedidoUnidad/NuevoPedido',
        icon: 'fa fa-pencil-square-o',

      },
      {
        name: 'Pedidos Emitidos',
        url: '/PedidoUnidad/PedidosEmitidos',
        icon: 'fa fa-list-ol',

      },
    ],
  },
    {
      name: 'REACTIVOS',
      url: '/hitorico',
      icon: 'fa fa-male fa-lg mt-4',
    },
    {
      name: 'MANUAL DE USUARIO',
      url: 'https://drive.google.com/open?id=1DL4bRdrWLKClcoyfSFHDB0FCYR_07PBa',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'success',
    },

    
   ],
};