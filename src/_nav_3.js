export default {
    items: [
      {
        name: 'Inicio',
        url: '/Inicio',
        icon: 'icon-speedometer',
        badge: {
          variant: 'info',
          text: 'HOSPITAL',
        },
      },

      {
        name: 'ATENCIÓN DIARIA',
        url: '/Tratamientos',
        icon: 'fa fa-heartbeat',
        children: [
          {
            name: 'Registrar',
            url: '/Tratamientos/RegistrarTratamiento',
            icon: 'fa fa-pencil-square-o',
          },
          {
            name: 'Buscar',
            url: '/Tratamientos/BuscarTratamiento',
            icon: 'fa fa-search',
          },
          {
            name: 'Listado general',
            url: '/Tratamientos/VerTodo',
            icon: 'fa fa-eye',
          },
        ]
      },
      {
        name: 'CONTROL SIGNOS VITALES, PESO Y TALLA',
        url: '/SignosVitalesPesoTalla',
        icon: 'fa fa-thermometer-1',
        children: [
          {
            name: 'Registrar',
            url: '/SignosVitalesPesoTalla/RegistrarSignosVitalesPesoTalla',
            icon: 'fa fa-pencil-square-o',
          },
          {
            name: 'Buscar',
            url: '/SignosVitalesPesoTalla/BuscarPaciente',
            icon: 'fa fa-search',
          },
          {
            name: 'Listado general',
            url: '/SignosVitalesPesoTalla/VerTodo',
            icon: 'fa fa-eye',
          },
        ]
      },
      {
        name: 'EVENTOS DE SALUD',
        url: '/EventosSalud',
        icon: 'fa fa-hospital-o',
        
        children: [
          {
            name: 'Organizar rol',
            url: '/EventosSalud/OrganizarRol',
            icon: 'fa fa-calendar',
            // variant: 'info', 
          },
          {
            name: 'Ver cronograma',
            url: '/EventosSalud/VerCronograma',
            icon: 'fa fa-calendar',
            // variant: 'info',
          },
          {
            name: 'Registrar plan de charlas',
            url: '/EventosSalud/RegistrarPlanDeCharla',
            icon: 'fa fa-pencil-square-o',
          },
          {
            name: 'Ver planes',
            url: '/EventosSalud/VerPlanes',
            icon: 'fa fa-eye',
          },
          
        ]
      },
      {
        name: 'FARMACIA AUXILIAR',
        url: '/farmaciaAuxiliar',
        icon: 'fa fa-medkit',
        children: [
          {
            name: 'Buscar medicamento y/o insumo',
            url: '/farmaciaAuxiliar/BuscarMedicamentoInsumo',
            icon: 'fa fa-search',
          },
          {
            name: 'Salida de medicamento y/o insumo',
            url: '/farmaciaAuxiliar/SalidaMedicamentoInsumo',
            icon: 'fa fa-hand-lizard-o',
          },
          {
            name: 'Inventario',
            url: '/farmaciaAuxiliar/Inventario',
            icon: 'fa fa-book',
          },
          {
            name: 'Reposición',
            url: '/farmaciaAuxiliar/ReposicionMed',
            icon: 'fa fa-pencil-square-o',
          },
        ]
      },
      // {
      //   name: 'INTERNACIÓN',
      //   url: '/Internacion',
      //   icon: 'fa fa-bed',
        
      // },
      {
        name: 'NOVEDADES Y OBSERVACIONES',
        url: '/Novedades',
        icon: 'fa fa-sticky-note-o',
        children: [
          {
            name: 'Registrar',
            url: '/Novedades/RegistrarNovedadObservacion',
            icon: 'fa fa-pencil-square-o',
          },
          {
            name: 'Listado general',
            url: '/Novedades/VerTodo',
            icon: 'fa fa-eye',
          },
        ]
      },
      {
        name: 'PEDIDOS',
        url: '/PedidosDevoluciones',
        icon: 'fa fa-handshake-o',
        children: [

          // {
          //   name: 'Pedido de medicamentos y/o insumos',
          //   url: '/PedidosDevoluciones/PedidoMedicamentoInsumo',
          //   icon: 'fa fa-pencil',
          // },
          {
            name: 'Realizar Pedido',
            url: '/PedidoUnidad/NuevoPedido',
            icon: 'fa fa-pencil-square-o',
  
          },
          {
            name: 'Pedidos Emitidos',
            url: '/PedidoUnidad/PedidosEmitidos',
            icon: 'fa fa-list-ol',
  
          },
          {
            name: 'Pedido de materiales',
            url: '/PedidosDevoluciones/PedidoMateriales',
            icon: 'fa fa-pencil',
          },
          {
            name: 'Listado general',
            url: '/PedidosDevoluciones/VerTodo',
            icon: 'fa fa-eye',
          },
        ]
      },
      {
        name: 'ROTACIÓN DE PERSONAL',
        url: '/RotacionPersonal',
        icon: 'fa fa-users',
        children: [
          {
            name: 'Registrar',
            url: '/RotacionPersonal/RealizarRotacion',
            icon: 'fa fa-pencil-square-o',
          },
          {
            name: 'Listado general',
            url: '/RotacionPersonal/VerTodo',
            icon: 'fa fa-eye',
          },
        ]
      },
      
      // {
      //   name: 'MANUAL DE USUARIO',
      //   url: 'http://coreui.io/react/',
      //   icon: 'fa fa-book',
      //   class: 'mt-auto',
      //   variant: 'primary',
      // },
      {
        name: 'MANUAL DE USUARIO',
        url: 'https://drive.google.com/open?id=1DL4bRdrWLKClcoyfSFHDB0FCYR_07PBa',
        icon: 'icon-cloud-download',
        class: 'mt-auto',
        variant: 'success',
      },
  
     ],
  };
  