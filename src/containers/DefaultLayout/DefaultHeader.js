import React, { Component } from 'react';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import {  AppHeaderDropdown,  AppSidebarToggler } from '@coreui/react';
// import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
// import logo from '../../assets/img/brand/logo_promes.svg'
// import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};
const defaultProps = {};

class DefaultHeader extends Component {
    constructor(props) {
      super(props);
      this.state = {
        logged_in: localStorage.getItem('token') ? true : false,
      };
    }
    async componentDidMount() {
      if(this.props.UnidadD==='MEDICINA GENERAL'){
        
      }
    }
    handle_logout = () => {
      localStorage.removeItem('token');
      this.setState({ logged_in: false, username: '' });
      window.location.href = '#/Login'
    };
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    // var srcc;
    // if(this.props.UnidadD==='MEDICINA GENERAL' && this.props.genero === 'M'){
    //   srcc='assets/img/avatars/doctor.jpg';
    // }
    // if(this.props.UnidadD==='MEDICINA GENERAL' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/doctora.jpg';
    // }
    // if(this.props.UnidadD==='FARMACIA' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/farmaceutica.png';
    // }
    // if(this.props.UnidadD==='ENFERMERIA' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/enfermera.png';
    // }
    // if(this.props.UnidadD==='LABORATORIO' && this.props.genero === 'M'){
    //   srcc='assets/img/avatars/laboratoriaM.svg';
    // }
    // if(this.props.UnidadD==='LABORATORIO' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/laboratorioF1.png';
    // }
    // if(this.props.UnidadD==='ODONTOLOGIA' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/dentistaF.png';
    // }
    // if(this.props.UnidadD==='ODONTOLOGIA' && this.props.genero === 'M'){
    //   srcc='assets/img/avatars/dentistaM.png';
    // }
    // if(this.props.UnidadD==='ADMINISTRACION' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/sasuke.jpg';
    // }
    // if(this.props.UnidadD==='NINGUNO' && this.props.genero === 'F'){
    //   srcc='assets/img/avatars/usuario.png';
    // }
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        {/* aumentado */}
        <NavLink href={'http://localhost:3000/#'+this.props.dashboard}><h4><strong>{this.props.UnidadD}</strong></h4></NavLink>
        {/* aumentado */}
        <AppSidebarToggler className="d-md-down-none" display="lg"/>
        <Nav className="d-md-down-none" navbar>
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="px-3">
          {/* SI-GESHO-PROMES */}
            <NavLink href={'http://localhost:3000/#'+this.props.dashboard}><h4><strong>HOSPITAL</strong></h4></NavLink>
          </NavItem>
          <NavItem className="px-1">
            {this.props.nombres}
            <br/>
            <strong>{this.props.UsuarioActual}</strong>
          </NavItem>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>

              <img src={this.props.perfil} className="img-avatar" alt="admin@bootstrapmaster.com" />
 
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem>{this.props.UsuarioActual}</DropdownItem>
              <DropdownItem>{this.props.UnidadD}</DropdownItem>
              <DropdownItem onClick={this.handle_logout}><i className="fa fa-lock"></i> Cerrar Sesión</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
