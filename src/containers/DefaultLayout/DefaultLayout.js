import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
// import navigation from '../../_nav';
import navigation from '../../_nav';
import navigation1 from '../../_nav_1';
import navigation2 from '../../_nav_2';
import navigation3 from '../../_nav_3';
import navigation4 from '../../_nav_4';
import navigation5 from '../../_nav_5';
// routes config
import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged_in: localStorage.getItem('token') ? true : false,
      username: '',
      nombre:'',
      paterno:'',
      materno:'',
      genero:'',
      perfil:'',
      nombreUnidad:'',
      unidad:'',
      UnidaP:0,
      cambioRol:3
    };
  }
  async componentDidMount() {
    console.log("que es esto" +this.logged_in)
    if (this.state.logged_in) {
      const respuesta7 = await fetch('http://localhost:8000/core/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
        })
        const datosUsuario = await respuesta7.json();
        this.setState({
          username:datosUsuario.username
        });
        try{
          // alert(this.state.username)
          const respuesta7 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.username+'/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
            const datosUsuario = await respuesta7.json();
            var cadena = datosUsuario[0].unidad.nombre_unidad;
            cadena = cadena.toUpperCase(cadena);
            this.setState({
              nombreUnidad:cadena,
              nombre:datosUsuario[0].nombres,
              paterno: datosUsuario[0].primer_apellido,
              materno: datosUsuario[0].segundo_apellido,
              genero: datosUsuario[0].genero,
              perfil: datosUsuario[0].foto_perfil,
            });
        } catch (e) {
            console.log(e);
          }
        // alert(this.state.perfil)
    }else{
      window.location.href = '#/Login/'
      // this.props.history.push('/login')
    }   
  }
render() {
console.log("usuario"+this.state.username)
console.log("respuesta"+this.state.nombreUnidad)

// const roles = 
//     (this.state.nombreUnidad === 'FARMACIA') ?
//     navigation1
//     :
//     ((this.state.nombreUnidad === 'D') ?

//     navigation2 : navigation)
var roles=navigation;
var dashboard="";
if(this.state.nombreUnidad==='FARMACIA'){
  roles=navigation1
  dashboard="/dashboard1"
}
if(this.state.nombreUnidad==='MEDICINA GENERAL'){
  roles=navigation2
  dashboard="/dashboard2"
}
if(this.state.nombreUnidad==='ENFERMERIA'){
  roles=navigation3
  dashboard="/dashboard3"
}
if(this.state.nombreUnidad==='ODONTOLOGIA'){
  roles=navigation4
  dashboard="/dashboard4"
}
if(this.state.nombreUnidad==='LABORATORIO'){
  roles=navigation5
  dashboard="/dashboard5"
}
if(this.state.nombreUnidad==='ADMINISTRACION'){
  roles=navigation
  dashboard="/dashboard"
}
if(this.state.nombreUnidad==='NINGUNO'){
  roles=navigation
  dashboard="/dashboard"
}
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader UsuarioActual={this.state.username} UnidadD={this.state.nombreUnidad} 
          nombres={this.state.nombre + ' ' +this.state.paterno + ' ' + this.state.materno}
          genero={this.state.genero}
          dashboard={dashboard}
          perfil={'http://localhost:8000'+this.state.perfil}
          />
          {/* {this.state.nombreUnidad} */}
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader/>
            <AppSidebarForm/>
            <AppSidebarNav navConfig={roles} {...this.props}/>
            <AppSidebarFooter/>
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
        
            <AppBreadcrumb appRoutes={routes}/>
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
                {this.state.username === ''  ? <Redirect from="/" to="/login" /> : <Redirect from="/" to={dashboard} />}
              </Switch>
            </Container>
          </main>
          <AppAside fixed hidden>
            <DefaultAside/>
          </AppAside>
        </div>
        <AppFooter>
          <DefaultFooter dashboard={dashboard}/>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;

