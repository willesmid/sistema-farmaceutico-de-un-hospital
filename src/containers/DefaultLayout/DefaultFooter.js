import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        {/*  <img src={'assets/img/icon_promes.png'} alt="icon-promes" height="42" width="42"/><span><a href={'http://localhost:3000/#'+this.props.dashboard}>PROGRAMA MÉDICO ESTUDIANTIL</a></span>*/}
        <span className="ml-auto"><a href={'http://localhost:3000/#'+this.props.dashboard}>UNIDAD DE FARMACIA</a></span>
        {/* <span><a href="https://coreui.io">CoreUI</a> &copy; 2018 creativeLabs.</span>
        <span className="ml-auto">Powered by <a href="https://coreui.io/react">CoreUI for React</a></span>      */}
        {/* Desarrollado por Mariluz Vargas Hilari - luzmar7.luz@gmail.com - 2019 */}
        <span className="ml-auto"> Desarrollado por William Luque Rojas - willesmid69@gmail.com - 2020</span>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
