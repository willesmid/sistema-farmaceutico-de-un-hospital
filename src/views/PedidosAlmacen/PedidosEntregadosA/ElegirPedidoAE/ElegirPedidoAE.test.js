import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPedidoAE from './ElegirPedidoAE';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPedidoAE />, div);
  ReactDOM.unmountComponentAtNode(div);
});

