import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup,
  Button

} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import ValidarPersonal2 from './ValidarPersonal2'
const styles2 = { color: 'white', backgroundColor: '#2b2c2c', textAlign:'center' }
class ElegirPedidoAE extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url_pedido: '#/PedidosUnidad/PedidosEmitidos',
      productos: [],
      personalList:[],
      idUnidad:0,
      nombreP:'',
      apellidosP:'',
      entregado:'',
      fecha_entrega:'',
      prf:'',
    }
    this.print = this.print.bind(this)
    console.log(this.props.match.params)
  }

componentDidMount() {
  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoBusca/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productos: findresponse,
          url_pedido : this.state.url_pedido +'/'+ this.props.match.params.id_pedido +'/'+ this.props.match.params.fecha +'/'+ this.props.match.params.unidadNombre +'/'+ this.props.match.params.personalNombre +'/'+this.props.match.params.personalApellidos+'/'+ this.props.match.localizacion +'/resultados',
        })
    });
    if(this.props.match.params.farmaceuticaN !=='Ninguno'){
      this.setState({
        entregado:'Dr. '+this.props.match.params.farmaceuticaN+' '+ this.props.match.params.farmaceuticaA
      })
    }
    if(this.props.match.params.fecha_entrega !=='null'){
      this.setState({
        fecha_entrega:this.props.match.params.fecha_entrega
      })
    }
    if(this.props.match.params.unidadId!== '2' ){
      this.setState({prf:'Dr.'})
    }else{
      this.setState({prf:'Lic.'})
    }
}

print(){
  /*Actual time and date */
    /*Actual time and date */
    var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }
    var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }
    var unidad = this.props.match.params.unidadNombre;
    unidad = unidad.toUpperCase(unidad);
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();
  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 9, 7, 20,20);
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(53, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(51, 19, "\"PROMES\"", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(158, 15,  "NUMERO DE PEDIDO:");
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(195, 15, " "+this.props.match.params.id_pedido);
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(158, 19, "LOCALIZACION :");

  doc.setFontType("normal");
  doc.setFontSize(8);
  doc.text(185, 19, "  "+this.props.match.params.localizacion)
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 25, "PEDIDOS DE MEDICAMENTOS", null, null, 'center');

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 29, "E INSUMOS MEDICOS", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(7);
  doc.text(189,23, showDate +' '+ showTime, null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.setFontType("bold");
  doc.text(10,36, "UNIDAD SOLICITANTE:");
  
  doc.setFontType("normal");
  doc.text(48, 36, "PROMES - "+unidad)
  
  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
  
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 38, left: 10, right: 52}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
  });

  doc.setFontType("bold");
  doc.setFontSize(8);
  doc.text(165, 40, "Realizado Por: ");

  doc.setFontType("normal");
  doc.text(165, 51, this.state.prf+" "+this.props.match.params.personalNombre+" "+this.props.match.params.personalApellidos);
  doc.setFontType("bold");
  doc.text(165, 55, "Nombre");

  doc.setFontType("normal");
  doc.text(165, 62, ""+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision);
  doc.setFontType("bold");
  doc.text(165, 66, "Fecha");

  doc.setFontType("normal");
  doc.text(165, 78, "................................................");
  doc.setFontType("bold");
  doc.text(165, 82, "Firma");

  doc.setFontType("bold");
  doc.text(165, 85, "________________________");

  doc.setFontType("bold");
  doc.text(165, 90, "Entregado por: ");


  if(this.state.entregado!==''){
    doc.setFontType("normal");
    doc.text(165, 101, ""+this.state.entregado);
    doc.setFontType("bold");
    doc.text(165, 105, "Nombre");

    doc.setFontType("normal");
    doc.text(165, 112, ""+this.props.match.params.fecha_entrega+" "+this.props.match.params.hora_entrega);
    doc.setFontType("bold");
    doc.text(165, 116, "Fecha");

  }else{
    doc.setFontType("normal");
    doc.text(165, 101, "................................................");
    doc.setFontType("bold");
    doc.text(165, 105, "Nombre");

    doc.setFontType("normal");
    doc.text(165, 112, ".............../.............../...............");
    doc.setFontType("bold");
    doc.text(165, 116, "Fecha");
  }

  doc.setFontType("normal");
  doc.text(165, 128, "................................................");
  doc.setFontType("bold");
  doc.text(165, 132, "Firma");

  doc.setFontSize(10);
  doc.text(10, 155, "_______________________");
  doc.text(13, 160, "UNIDAD SOLICITANTE");
  
  doc.text(72, 155, "__________________________________");
  doc.text(75, 160, "Vo. Bo. JEFE UNIDAD SOLICITANTE");

  doc.text(150, 155, "___________________________");
  doc.text(154, 160, "Vo. Bo. GERENTE MEDICO");
  this.setState({ showImp: true, showAlert:false});
  window.open(doc.output('bloburl'), '_blank'); 
}


render() {
  var unidad = this.props.match.params.unidadNombre;
  unidad = unidad.toUpperCase(unidad);
  const table=this.state.productos.map((u, i)=> {
    if(u.cantidad_entregada!==null){
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.codigo.codigo} </td>
        <td>{u.codigo.nombre}</td>
        <td>{u.unidadProd}</td>
        <td>{u.cantidad_solicitada}</td>
        <td>{u.cantidad_entregada+" ("+u.lote_salida+"  "+u.fecha_expiracion+")"}</td>
      </tr>
    )
  }else{
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.codigo.codigo} </td>
        <td>{u.codigo.nombre}</td>
        <td>{u.unidadProd}</td>
        <td>{u.cantidad_solicitada}</td>
        <td></td>
      </tr>
    )
    }
  }) 
  return (
  <div>
    <Table responsive striped hidden id="tabla">
    <thead>
        <tr>
          <th>No.</th>
          <th>CODIGO</th>
          <th>DETALLE</th>
          <th>UNIDAD</th>
          <th>Solicitada</th>
          <th>Entregada</th>            
        </tr>
    </thead>
    <tbody>
    {table}
    </tbody>
    </Table>  
    <Row>
      <Col xs="12" md="12">
      <Card>
        <CardHeader>
        <i className="fa fa-list-alt"></i><strong>Detalle Pedido Entregado de Almacen</strong>
        <Button  onClick={this.print} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Pedido
        </Button>
        </CardHeader>
          <CardBody>
            <Form >
              <FormGroup row>  
                <Col md="2">
                <Label htmlFor="text-input">UNIDAD SOLICITANTE</Label>
                </Col> 
                <Col md="4" >        
                <Input type="text" id="text-input" name="codigoAlifiacion" value={"PROMES - "+unidad}/>           
                </Col> 
                <Col md="1">
                <Label htmlFor="text-input">Localización</Label>                     
                </Col>
                <Col md="2">
                <Input type="text" id="text-input" name="cedulaIdentidad" value={this.props.match.params.localizacion}  disabled/>
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">N°Pedido</Label>
                </Col>
                <Col md="2">
                <Input type="text"  id="text-input" name="id_recetaA" value={this.props.match.params.id_pedido}  disabled/>
                </Col>  
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input">Pedido Realizado por</Label>                     
                </Col>
                <Col md="4">
                <ValidarPersonal2 idUnidad={this.props.match.params.unidadId} nombreP={this.props.match.params.personalNombre} apellidosP={this.props.match.params.personalApellidos}/>
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">F/Emisión</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="date-input" name="fecha" placeholder="Fecha" value={this.props.match.params.fecha_emision} disabled />
                </Col>
                <Col md="1">
                <Label htmlFor="hora">H/Emisión</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control" value={this.props.match.params.hora_emision} disabled  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input"> Pedido Entregado por</Label>
                </Col> 
                <Col md="4" >
                <Input type="text" name="entregado" id="text" value={this.state.entregado}  disabled/>
                </Col> 
                <Col md="1">
                <Label htmlFor="fecha">F/Entrega</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="date-input" name="fecha" placeholder="Fecha" value={this.props.match.params.fecha_entrega} disabled/>
                </Col>
                <Col md="1">
                <Label htmlFor="hora">H/Entrega</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control" value={this.props.match.params.hora_entrega} disabled  />
                </Col>
              </FormGroup>
              <hr/>
            </Form>

              <Table hover bordered striped responsive size="sm">
              <thead style={styles2}>
                <tr >
                  <th rowspan="2"><p>Numero</p></th>
                  <th rowspan="2"><p>Codigo</p></th>
                  <th rowspan="2"><p>Detalle</p></th>
                  <th rowspan="2"><p>Unidad</p></th>
                  <th colspan="2"><center>Cantidad</center></th> 
                  <th colspan="2"><center>Lote</center></th> 
                  <th rowspan="2"><p>Obs.</p></th>                  
                </tr>
                <tr>
                  <th>Solicitada</th>
                  <th>Entregada</th> 
                  <th>Codigo</th>
                  <th>F/Expiración</th>                              
                </tr>
                </thead>
                <tbody>
                  {this.state.productos.map((u, i)=> {
                    return (
                      <tr key = {i} >
                        <td style={{maxWidth: '5px', textAlign: 'center'}}>{i+1}</td>
                        <td style={{maxWidth: '50px', }} >{u.codigo.codigo} </td>
                        <td style={{maxWidth: '150px'}}>{u.codigo.nombre}</td>
                        <td style={{maxWidth: '200px', textAlign: 'center'}}>{u.unidadProd}</td>
                        <td style={{maxWidth: '30px', textAlign: 'center'}}>{u.cantidad_solicitada}</td>
                        <td style={{maxWidth: '30px', textAlign: 'center'}}>{u.cantidad_entregada}</td>
                        <td style={{maxWidth: '30px', textAlign: 'center'}}>{u.lote_salida}</td>
                        <td style={{maxWidth: '30px', textAlign: 'center'}}>{u.fecha_expiracion}</td>
                        <td style={{maxWidth: '10px'}}>                            
                        </td>
                      </tr>
                      )
                  })}             
                </tbody>
              </Table>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </div>
  )
}
}

export default ElegirPedidoAE;
