import React, { Component } from 'react';
import { 
  Button, 
  NavLink, 
  Table,
  Badge,
} from 'reactstrap';

import ValidarEstado from './ValidarEstado'
import ValidarPersonal from './ValidarPersonal'
let url_pedido = '#/PedidosAlmacen/PedidosUnidades/PedidosEntregados/ElegirPedido'

const tdStyle = {
  fontSize: '0.9em'
}

const trNavLink = {
  padding: '0',
  margin: '0'
}

class ListaEntregadoA extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, 
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }
    const pedidos = this.props.lista_pedido.map((item, indice)=>{
      return(
        <tr key={item.indice}>
          <td id={indice} style={tdStyle}>
            {indice+1}
          </td>
          <td id={indice} style={tdStyle}>
            {item.id_pedido}
          </td>
          <td id={indice} style={tdStyle}>
            {item.unidad.nombre_unidad}
          </td>
         <td style={tdStyle}>
            <ValidarPersonal idUnidad={item.unidad.id} />
            {item.personal.nombres+" "+item.personal.primer_apellido+" "+item.personal.segundo_apellido}
          </td>
          <td id={indice} style={tdStyle}>
            {item.fecha_emision}
          </td>
          <td id={indice} style={tdStyle}>
            {item.fecha_entrega}
          </td >
          <td id={indice} style={tdStyle}>
            {item.localizacion.descripcion}
          </td>
            <ValidarEstado estadoR={item.estado} />
          
          <td id={indice}>
              <NavLink style={trNavLink} href = {url_pedido +'/'+ item.id_pedido +'/'+ item.unidad.id +'/'+ item.unidad.nombre_unidad +'/'+ item.personal.nombres +'/'+ (item.personal.primer_apellido+" "+item.personal.segundo_apellido) +'/'+ item.localizacion.descripcion +'/'+ item.fecha_emision +'/'+ item.hora_emision + '/'+ item.farmaceutica.nombres +'/'+ (item.farmaceutica.primer_apellido+" "+item.farmaceutica.segundo_apellido) + '/' + item.fecha_entrega + '/' + item.hora_entrega} >
                <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" ><i className="fa fa-eye"></i><span>Ver</span></Button>
              </NavLink>
          </td>
        </tr>
      )
    })
  
  return (
    <div className="animated fadeIn">
      <Badge className="mr-1" color="light"><h6><i className="fa fa-align-justify"></i>{' '+this.props.titulo}</h6></Badge>      
      <Table responsive>
        <thead>
          <tr>
            <th>Numero</th>
            <th>ID Pedido</th>
            <th>Unidad</th>
            <th>Realizado por</th>
            <th>F/Emision</th>
            <th>F/Entrega</th>
            <th>Localizacion</th>
            <th>Estado</th>
            <th colSpan="2" > <center>Acciones</center></th>
          </tr>
        </thead>
        <tbody>
          {pedidos}
        </tbody>
      </Table>
    </div>
  );
  }
}
export default ListaEntregadoA;
