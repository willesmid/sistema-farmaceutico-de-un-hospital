import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPedidoAFE from './ElegirPedidoAFE';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPedidoAFE />, div);
  ReactDOM.unmountComponentAtNode(div);
});

