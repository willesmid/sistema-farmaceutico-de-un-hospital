import React, { Component } from 'react';
import { 
  Form,
  Col,
  Input,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Alert,

} from 'reactstrap';

var urlRegistraProd = 'http://localhost:8000/farmacia/almacena/v1/EstaUbicadoRegistra/'
class EntregaProducto extends Component {
  constructor(props) {
    super(props)
    this.state = {     
    list_lote: [],
    cantidadLote: 0,
    cantidadAlmacen:0,
    cantidadGeneral:0,
    lote:'',
    codP1:'',
    lote1:'',
    numSaldo:0,
    numSaldoF:0,
    showEntrega:true,   
    
    primary:false,
    primary2:false,
    menssage:'',
    showYaEntregado:false,
    showCantFarmacia: false,

    cantidadEntrega:0,
    existeProductoFarmacia:[],
    showExisteProductoS:true,
    showExisteProductoN:false,
    primaryRegistra:false,

    fecha_expiracion:'',
    };
    
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onValidacion = this.onValidacion.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)  
    this.toggleRegistraProd=this.toggleRegistraProd.bind(this)
 
    this.onRegistraProducto =this.onRegistraProducto.bind(this)
    
}


togglePrimary() {
  this.setState({ primary: !this.state.primary});
}

togglePrimary2() {
  this.setState({ primary2: !this.state.primary2});
}

toggleRegistraProd() {
  this.setState({ primaryRegistra: !this.state.primaryRegistra});
}

componentDidMount(){
    //console.log(value); CardBody
    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoAlmacenBuscaCodP/'+this.props.codP+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
        this.setState({
          list_lote: findresponse
          
        })  
  })
}

handleChange(){
  fetch('http://localhost:8000/farmacia/almacena/v1/LoteProductoBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadLote: findresponse[0].cantidad
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoAlmacenBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadAlmacen: findresponse[0].stockAlmacen
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.props.codP+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadGeneral: findresponse[0].stockGeneral
    })
  });


  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      existeProductoFarmacia:findresponse
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadFarmacia: findresponse[0].stockAlmacen
    })
  });

  this.setState({
    showExisteProductoN:false,
    showExisteProductoS:true,

  })

  fetch('http://localhost:8000/farmacia/almacena/v1/LoteBusca/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      fecha_expiracion: findresponse[0].fecha_expiracion
    })
  }); 
  
}


onValidacion(e){

  const numT=this.state.cantidadAlmacen - this.state.cantidadEntrega
  
  if(this.state.lote === '' ){
    this.setState({     
      primary: !this.state.primary,
      message: 'Ingrese Lote ',
    })
  }
  else{
    if(this.state.cantidadEntrega<0){
      this.setState({     
        primary: !this.state.primary,
        message: 'Cantidad a entregar debe ser mayor a 0',
      })

    }else{
      if(this.state.cantidadEntrega>this.props.cantidadP){
        this.setState({     
          primary: !this.state.primary,
          message: 'Cantidad a Entregar debe ser menor o igual a la Cantidad Solicitada',
        })

      }else{
        if(numT<0){
          this.setState({     
            primary: !this.state.primary,
            message: 'Cantidad Insuficiente ',
            showCantFarmacia: true,
          
          })
        }else{
          if(this.state.existeProductoFarmacia.length>0){
          this.setState({
          numSaldo: this.state.cantidadAlmacen - this.state.cantidadEntrega,
          numSaldoF: this.state.cantidadFarmacia + parseInt(this.state.cantidadEntrega,10),
          primary2: !this.state.primary2, 
        })
        }else{
          this.setState({     
            primary: !this.state.primary,
            message: ' ',
            showCantFarmacia: true,
            showExisteProductoN:true,
            showExisteProductoS:false,
          
          })
        
        }
      }}
    }      
  }

}


onRegistraProducto(e) {

  let dataProducto={

    stockAlmacen: 0,
    producto_farmaceutico: this.props.codP,
    lote: this.state.lote,
    almacen: 1,
  }
  try{
    fetch(urlRegistraProd, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(dataProducto), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }

this.setState({
  
  primaryRegistra: !this.state.primaryRegistra,
  showExisteProductoS: true,
  showExisteProductoN: false,
  lote:'',
  showCantFarmacia:false,

})
return 0
}


onFormSubmit(e) {
 
  let urlAlmacen = ''
  let urlFarmacia = ''
  let dataAlmacen = {}
  let dataFarmacia = {}

  urlAlmacen = 'http://localhost:8000/farmacia/almacena/v1/EstaAlmacenEdita/'+this.state.lote+'/'
  urlFarmacia = 'http://localhost:8000/farmacia/almacena/v1/EstaFarmaciaEdita/'+this.state.lote+'/'

  const numFarmacia = this.state.cantidadFarmacia + parseInt(this.state.cantidadEntrega,10)

  const numAlmacen = this.state.cantidadAlmacen - this.state.cantidadEntrega

  console.log('nueva cantidadLOTE jijiji: '+ this.state.cantidadLote)
  console.log('nueva cantidadENTREGAR jijiji: '+ this.state.cantidadEntrega)
  console.log('nueva cantidadGENERAL jijiji: '+ this.state.cantidadGeneral)
  console.log('nueva cantidadPRESCRIPCION jijiji: '+ this.state.cantidadEntrega)


  dataAlmacen = {
    stockAlmacen : numAlmacen
  }

  dataFarmacia = {
    stockAlmacen : numFarmacia
  }
 
    try {

      fetch(urlAlmacen, {
      method: 'PUT',
      body: JSON.stringify(dataAlmacen),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }

     
    try {

      fetch(urlFarmacia, {
      method: 'PUT',
      body: JSON.stringify(dataFarmacia),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }

    
    this.setState({
      showEntrega:false,
    })


    
//EDITA PRODUCTO-PEDIDO (CANTIDAD ENTREGADA)
let urlCantidadEntregada = ''
let dataCantidadEntregada = {}
urlCantidadEntregada = 'http://localhost:8000/farmacia/pedir/v1/ProductoPedidoCantidadEdita/'+this.props.idProd+'/'
dataCantidadEntregada = {


  cantidad_entregada:this.state.cantidadEntrega,
  lote_salida:this.state.lote,
  fecha_expiracion:this.state.fecha_expiracion,
}

  try {
    fetch(urlCantidadEntregada, {
    method: 'PUT',
    body: JSON.stringify(dataCantidadEntregada),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
  } catch (e) {
      console.log(e);
  }

//FIN CANTIDAD ENTREGADA
    this.setState({
     
      primary2: !this.state.primary2,
      showYaEntregado: true
    
    
    })
  }

render() {
      return (
        <div >
           <Form onSubmit={this.props.onAddEntrega}>
          <Table hover bordered striped responsive size="sm">
       
            <tr>
          <td style={{maxWidth: '250px', backgroundColor: '#ffffff'}}>
          <Input 
                type="select" 
                name="lote" 
                id="lote" 
                value={this.state.lote}
                required
                onChange={e => { this.setState({ lote: e.target.value }) }}
           
                onClick={ this.handleChange.bind(this)}>
                <option value="" >Lote..   </option>
                  {this.state.list_lote.map(item1 => (
                      <option value={item1.lote.id_lote} >{item1.lote.fecha_expiracion+' ('+item1.lote.id_lote+')'}</option>
                      ))}
              </Input>
              </td>
              <td bgcolor="#ffffff" style={{maxWidth: '80px'}}>
              <Input  type="text"  value ={this.state.cantidadAlmacen} name="cantidad"  id="text-input" onChange={ this.handleChange.bind(this)}></Input>
              </td>
              <td bgcolor="#ffffff" style={{maxWidth: '80px'}}>
              <Input  type="number"  name="cantidadEntrega" onChange={e => this.setState({ cantidadEntrega: e.target.value })} id="text-input" placeholder="0"></Input>
 
              </td>
              <td bgcolor="#ffffff" style={{maxWidth: '90px'}}>
              
            
         {this.state.showEntrega && 
         <center>
        <Button size="sm" className="btn-twitter btn-brand mr-1 mb-1"  onClick={this.onValidacion}><i className="fa fa-hand-lizard-o"></i><span>Confirmar</span></Button>
        </center>
         }
         {this.state.showYaEntregado &&
         <center>
          <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check-square-o"></i><span>Entregado</span></Button>
          </center>
        }

          </td >

          <td bgcolor="#ffffff" style={{maxWidth: '60px'}}>     
          {this.state.showExisteProductoS && 
          <center>      
            <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><span>Existe</span></Button>
          </center> 
          }
          {this.state.showExisteProductoN &&  
          <center>       
            <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1"  onClick={this.toggleRegistraProd} ><span>Registrar</span></Button>
          </center> 
          }
            </td>
               
        
         </tr>

         <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
        <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
          <ModalBody>
            <strong>{this.state.message}</strong>
            { this.state.showCantFarmacia && <Alert color="success">
                  
                  <strong>{"Registre Producto en Farmacia"}</strong>                  
                  </Alert>}
               
          </ModalBody>
        <ModalFooter>
          <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
        </ModalFooter>
      </Modal>

              <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
                <form onSubmit={this.props.onAddEntrega}>
                <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
                  <ModalBody>
                
                  <center>
                    <Col md="7">
   
                   <Input type="text" name="contar"  value={"¿Desea Entregar Producto?"}   ></Input>
                    </Col>
                    </center>
                    <strong>{"Codigo :  "} </strong>
                    {this.props.codP+" "}
                    <strong>{"Nombre :  "}</strong>
                    {this.props.nomP}
                    <br/>
                    <strong>{"Numero Lote:  "}</strong>
                    {this.state.lote}
                    <br/>
                    <strong>{"Cantidad Solicitada:  "}</strong>
                    {this.props.cantidadP}
                    <br/>
                    <strong>{"Cantidad a Entregar:  "}</strong>
                    {this.state.cantidadEntrega}
                    <br/>
                    <center>
                    
                    <Col xs="12" md="10">
                   
                    <Alert color="success">
                  
                    <strong>{"Cantidad Existente en Almacen: "}</strong>{this.state.cantidadAlmacen} <strong>{" Saldo: "}</strong>{ this.state.numSaldo}
                    
                    </Alert>
                    </Col>
                    </center>
                    <hr/>

                    <strong>{"Detalle en Farmacia  "}</strong>
                  
                    <br/>
                    <center>

                    <Col xs="12" md="12">

                    <Alert color="success">

                    <strong>{"Cantidad Existente en Farmacia: "}</strong>{this.state.cantidadFarmacia} <strong>{"Nuevo Saldo: "}</strong>{ this.state.numSaldoF}

                    </Alert>
                    </Col>
                    </center>
                    
                    
                  </ModalBody>
                  <ModalFooter>           
                    <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
                    <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
                  </ModalFooter>
                  </form>
              </Modal>


                <Modal isOpen={this.state.primaryRegistra } toggle={this.toggleRegistraProd}
                       className={'modal-success ' + this.props.className}>
                  <ModalHeader toggle={this.toggleRegistraProd}>Modal title</ModalHeader>
                  <ModalBody>
                  <center>
                   <strong>¿Desea Registrar Producto en Farmacia?</strong>
                    </center>
                    <strong>{"Codigo :  "} </strong>
                    {this.props.codP+" "}
                    <strong>{"Nombre :  "}</strong>
                    {this.props.nomP}
                    <br/>
                    <strong>{"Numero Lote:  "}</strong>
                    {this.state.lote}
                    <br/>

                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" onClick={this.onRegistraProducto}>Si</Button>{' '}
                    <Button color="secondary" onClick={this.toggleRegistraProd}>No</Button>
                  </ModalFooter>
                </Modal>
         </Table>
  
         </Form>  
        </div>
      )
    
    }
  }
  export default EntregaProducto;








