import React, { Component } from 'react';
import { 
  Button, 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  FormGroup, 
  Input,
  Alert,
} from 'reactstrap';

import Search from 'react-search-box'
import ListaEntregadoAF from '../ListaEntregadoAF'

class EntregadosAF extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, 
      lista_pedido: [],
      pedidos:[],
      lista_pedido_fecha:[],
      lista_afiliado:[],
      lista_pedido_hoy:[],
      lista_pedido_todo:[],
      estadoR:'',
      fecha:'',
      fecha_hoy:'',
      showB:false,
      showA:false,
      showC:false,
      showD:false,
      showE:false,
      showNo:false,
      codigoPedido:0,
      message:'',
    }
    this.BuscaFecha = this.BuscaFecha.bind(this)
    this.BuscaHoy = this.BuscaHoy.bind(this)
    this.BuscaTodo = this.BuscaTodo.bind(this)
  }

async componentDidMount() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd

  const respuesta1 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPedidosEntregadoAF/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  });
  const pedidos = await respuesta1.json();
  const respuesta2 = await fetch('http://localhost:8000/farmacia/pedir/v1/PedidosEntregadoFechaAF/'+hoy+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  });
  const lista_pedido = await respuesta2.json();
  
  if(lista_pedido.length>0){
    this.setState({
      showA:true,
    })
  }else{
    this.setState({
      showNo:true,
      message:"No existen Pedidos Entregados de Almacen a Farmacia Hoy (" +hoy+")",
    })
  }
  this.setState({
    lista_pedido,
    pedidos,
    fecha_hoy:hoy,
    fecha:hoy,
  });
}

BuscaPedido(value) {
  this.state.pedidos.map((item)=>{
  if (parseInt(value,10) === item.id_pedido){    
  fetch('http://localhost:8000/farmacia/pedir/v1/PedidosEntregadoCodigoAF/'+item.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_afiliado:findresponse,
        codigoPedido:item.id_pedido,
        showB:false,
        showA:false,
        showD:false,
        showC:true,
        showE:false,
        showNo:false,
        message:'',
      })
    }
    else{
      this.setState({
        showNo:true,
        showB:false,
        showA:false,
        showD:false,
        showC:false,
        showE:false,
        codigoPedido:value,
        message: value+" No existe",
      })
    }
  });
  }
  return 0
  })
}

BuscaFecha(){
  fetch('http://localhost:8000/farmacia/pedir/v1/PedidosEntregadoFechaAF/'+this.state.fecha+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_pedido_fecha:findresponse,
        showB:true,
        showA:false,
        showC:false,
        showD:false,
        showE:false,
        showNo:false,
      })
    }
    else{
      this.setState({
        showNo:true,
        showB:false,
        showA:false,
        showD:false,
        showC:false,
        showE:false,
        message:"En fecha "+this.state.fecha+" No existen Pedidos Entregados de Almacen a Farmacia",
      })
    }
  })
}

BuscaHoy(){
  fetch('http://localhost:8000/farmacia/pedir/v1/PedidosEntregadoFechaAF/'+this.state.fecha_hoy+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_pedido_hoy:findresponse,
        showB:false,
        showA:false,
        showC:false,
        showD:true,
        showE:false,
        showNo:false,
      })
    }else{
      this.setState({
        showNo:true,
        showB:false,
        showA:false,
        showC:false,
        showD:false,
        showE:false,
        message:"No existen Pedidos Entregados de Almacen a Farmacia Hoy ("+this.state.fecha_hoy+")",
      })
    }
  })
}

BuscaTodo(){
  fetch('http://localhost:8000/farmacia/pedir/v1/PedidosEntregadoAF/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_pedido_todo:findresponse,
        showB:false,
        showA:false,
        showC:false,
        showD:false,
        showE:true,
        showNo:false,
      })
    }else{
      this.setState({
        lista_pedido_todo:findresponse,
        showB:false,
        showA:false,
        showC:false,
        showD:false,
        showE:false,
        showNo:true,
        message:"No existe ningun Pedido Entregado de Almacen a Farmacia"
      })
    }
  })
}

render() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardHeader>
            <h3><i className="fa fa-th-list"></i> <strong>Pedidos Entregados de Almacen a Farmacia</strong></h3>
            </CardHeader>
            <CardBody>            
              <form>
                <FormGroup row>
                  <Col  md="3">
                  <Search                       
                  data={this.state.pedidos}
                  onChange={ this.BuscaPedido.bind(this) }
                  placeholder="Ingrese Codigo de Pedido"
                  class="search-class"
                  
                  searchKey="id_pedido"
                  />
                  </Col>
                  <Col xs="12" md="3">
                  <Input onChange={e => { this.setState({ fecha: e.target.value }) }} defaultValue={hoy} type="date" id="date-input" name="date-input" placeholder="date" />
                  </Col>
                  <Col xs="12" md="2">
                  <h3>
                  <Button onClick={ this.BuscaFecha}  size="sm" className="btn-twitter btn-brand mr-1 mb-1"><i className=" 	fa fa-search"></i><span>Buscar por Fecha</span></Button>
                  </h3>
                  </Col>                  
                  <Col  md="2">
                  <h3>
                  <Button onClick={ this.BuscaHoy}  size="sm" className="btn-facebook btn-brand mr-1 mb-1"><i className="fa fa-calendar-check-o"></i><span>Pedidos del día de Hoy</span></Button>
                  </h3>
                  </Col>
                  <Col  md="2">
                  <h3>
                  <Button onClick={ this.BuscaTodo} size="sm" className="btn-xing btn-brand mr-1 mb-1"><i className="fa fa-copy"></i><span>Ver Todos los Pedidos</span></Button>
                  </h3>
                  </Col>
                </FormGroup>
              </form>
                {this.state.showA && <ListaEntregadoAF lista_pedido={this.state.lista_pedido} titulo={'Pedidos Entregados de Almacen a Farmacia el dia de Hoy: '+this.state.fecha_hoy} />}
                {this.state.showB && <ListaEntregadoAF lista_pedido={this.state.lista_pedido_fecha} titulo={'Pedidos Entregados de Almacen a Farmacia en fecha: '+this.state.fecha} />}
                {this.state.showC && <ListaEntregadoAF lista_pedido={this.state.lista_afiliado} titulo={'Pedido Entregado de Almacen a Farmacia con Codigo: '+this.state.codigoPedido} />}
                {this.state.showD && <ListaEntregadoAF lista_pedido={this.state.lista_pedido_hoy} titulo={'Pedidos Entregados de Almacen a Farmacia el dia de Hoy: '+this.state.fecha_hoy} />}
                {this.state.showE && <ListaEntregadoAF lista_pedido={this.state.lista_pedido_todo} titulo={'Pedidos Entregados de Almacen a Farmacia'} key={5}/>}
                {this.state.showNo && 
                  <Row>
                    <Col md="3"></Col>
                    <Col md="5">
                      <Alert color="danger">
                      <center>
                        <strong>{this.state.message}</strong>
                        </center>
                      </Alert>
                    </Col>
                  </Row>
                }
            </CardBody>
          </Card>
        </Col>
      </Row>            
    </div>
  );
}
}

export default EntregadosAF;
