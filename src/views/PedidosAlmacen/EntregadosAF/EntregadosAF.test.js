import React from 'react';
import ReactDOM from 'react-dom';
import EntregadosAF from './EntregadosAF';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EntregadosAF />, div);
  ReactDOM.unmountComponentAtNode(div);
});
