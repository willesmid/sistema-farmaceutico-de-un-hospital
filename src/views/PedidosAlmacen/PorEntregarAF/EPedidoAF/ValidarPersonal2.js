import React, { Component } from 'react';

import { 
    Input,  
  } from 'reactstrap';

class ValidarPersonal2 extends Component {
  constructor(props) {
      super(props);
      this.state = {
          personal:'',
      };
  }

  render() {
    const mensaje = (this.props.idUnidad === '2') ? "Lic. ":"Dr. "
    return(
        
        <Input type="text" value={mensaje +" "+ this.props.nombreP+" "+ this.props.apellidosP} disabled/>
    )
  }
}
export default ValidarPersonal2;