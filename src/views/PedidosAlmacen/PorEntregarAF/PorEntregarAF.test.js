import React from 'react';
import ReactDOM from 'react-dom';
import PedidosPorEntregarA from './PedidosPorEntregarA';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PedidosPorEntregarA />, div);
  ReactDOM.unmountComponentAtNode(div);
});
