import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPedidoA from './ElegirPedidoA';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPedidoA />, div);
  ReactDOM.unmountComponentAtNode(div);
});

