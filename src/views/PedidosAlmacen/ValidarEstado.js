import React, { Component } from 'react';
import { Badge } from 'reactstrap';

class ValidacionEstado extends Component {
  constructor(props) {
    super(props);

    this.state = {
        
    };
  }



render() {
  const mensaje = (this.props.estadoR === "Por Entregar") ? "POR ENTREGAR":((this.props.estadoR === "Entregado") ? "ENTREGADO":(this.props.estadoR === "Anulada") ? "ANULADA":"")
  const tipo = (this.props.estadoR === "Por Entregar")?"warning":(this.props.estadoR === "Entregado")?"success":(this.props.estadoR === "Anulada")?"danger":""
  console.log(tipo)
  return(
    <td>
      <Badge color={tipo}>{mensaje}</Badge>
    </td>
  )
}
}
export default ValidacionEstado;