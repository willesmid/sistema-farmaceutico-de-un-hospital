import React from 'react';
import ReactDOM from 'react-dom';
import NuevoPedido from './NuevoPedido';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevoPedido />, div);
  ReactDOM.unmountComponentAtNode(div);
});
