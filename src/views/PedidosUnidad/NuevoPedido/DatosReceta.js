import React from 'react';
import { 
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  
 } from 'reactstrap';
//import AntecedentesG from './Antecedentes/AntecedentesG';


const DatosReceta = (props) => (
  

    
  <div>
  {
    props.listR.map(item => (
     
<Row>
  <Col xs="12" md="12">

      <CardBody>
        <Form >
        <FormGroup row>
          <Col md="9">
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">Fecha</Label>
          </Col>
          <Col  md="2">
            <Input 
            type="date" 
            id="date-input" 
            name="fecha" 
            placeholder="Fecha"
            value={item.fecha} 

            />
          </Col>


        </FormGroup>
        <FormGroup row>
          
          <Col md="1">
            
          </Col>
          <Col xs="12" md="2">
            <Label htmlFor="text-input">CODIGO AFILIADO</Label>
          </Col> 
            <Col xs="12" md="5" >
            <Input 
            type="text" 
            id="text-input" 
            name="codigoAlifiacion" 
            value={item.consulta.afiliado.codAfiliado }   

            />
          </Col> 
          <Col xs="12" md="1">
          
          </Col>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">id Receta</Label>
          </Col>
          <Col xs="12" md="2">
            <Input type="text" id="text-input" name="id_recetaA" value={item.id_receta} disabled />
          </Col>  
        </FormGroup>
        
        <FormGroup row>
        
          <Col xs="12" md="5">
          
          </Col>
        

          <Col md="1">
          
          </Col>
          <Col  md="3">
            
          </Col>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">id consulta</Label>                     
          </Col>
          <Col xs="12" md="2">
            <Input type="text" id="text-input" name="id_consulta" value={item.consulta.id_consulta} disabled />
          </Col>
        </FormGroup>

        <FormGroup row>
        
        
      

        <Col md="2">
          <Label htmlFor="text-input">Nombre y Apellido</Label>
        </Col>
        <Col  md="3">
          <Input 
          type="text" 
          id="text-input" 
          name="nombreCompleto" 
          value={item.consulta.afiliado.ci.nombres+"   "+ item.consulta.afiliado.ci.apellido_p+"   "+ item.consulta.afiliado.ci.apellido_m }   disabled

          />
        </Col>
        <Col xs="12" md="1">
          <Label htmlFor="text-input">Matricula</Label>                     
        </Col>
        <Col xs="12" md="2">
          <Input type="text" id="text-input" name="cedulaIdentidad" value={item.consulta.afiliado.ci.matricula} disabled />
        </Col>
        <Col md="1">
          <Label htmlFor="text-input">Medico</Label>
          </Col>
        <Col xs="12" md="3">
          <Input type="text" id="disabled-input" name="nombreMedico" disabled  value={"Dr. "+item.consulta.medico.nombres+" "+item.consulta.medico.apellidoPaterno } align="center" />
        </Col>
      </FormGroup>
      
        </Form>
      </CardBody>

  </Col>
</Row>
  ))
  
  }
  </div>

  
)


export default DatosReceta;
