import React from 'react';
import ReactDOM from 'react-dom';
import PedidosEmitidos from './PedidosEmitidos';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PedidosEmitidos />, div);
  ReactDOM.unmountComponentAtNode(div);
});
