import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPedido from './ElegirPedido';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPedido />, div);
  ReactDOM.unmountComponentAtNode(div);
});

