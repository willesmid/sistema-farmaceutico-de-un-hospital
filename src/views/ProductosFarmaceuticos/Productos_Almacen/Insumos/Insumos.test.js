import React from 'react';
import ReactDOM from 'react-dom';
import Insumos from './Insumos';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Insumos />, div);
  ReactDOM.unmountComponentAtNode(div);
});