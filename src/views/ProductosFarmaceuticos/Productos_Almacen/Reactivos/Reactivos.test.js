import React from 'react';
import ReactDOM from 'react-dom';
import Reactivos from './Reactivos';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Reactivos />, div);
  ReactDOM.unmountComponentAtNode(div);
});