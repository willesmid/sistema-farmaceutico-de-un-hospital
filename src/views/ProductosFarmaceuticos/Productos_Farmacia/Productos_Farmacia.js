import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import Validacion from '../Validacion'
const tdStyle = {
  fontSize: '0.9em'
}

class Productos_Farmacia extends Component {
  constructor(props) {
    super(props)

    this.state = {
        Insumos: [],
        primary: false,
    }

    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)

  }

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/v1/EstaUbicadoBusca/1/');
      const Insumos = await respuesta.json();



      this.setState({
        Insumos
      });
      console.log(Insumos)

    } catch (e) {
      console.log(e);
    }
  }

  handleChange(e) {
  
      let {value, name, id} = e.target
      console.log(value, name, id)

      let reporte_ego_copia = this.state.reporte_ego

      if(name === "egoDescripcion"){
          reporte_ego_copia[id].egoDescripcion = value
      }else if(name === "egoValorReferencia"){
          reporte_ego_copia[id].egoValorReferencia = value
      }

      this.setState({
          reporte_ego : reporte_ego_copia
      });

      console.log(reporte_ego_copia[id].egoDescripcion+"=>"+value)
      console.log(this.state.reporte_ego)
  }

  handleOnSubmit(e) {
      e.preventDefault()

      console.log(this.state.reporte_ego);
      let url = ''
      let data = {}

      this.state.reporte_ego.map((item, indice)=>{
          
          url = 'http://127.0.0.1:8000/v1/actualizaEgo/'+item.id+'/'
          data = {
              egoDescripcion: item.egoDescripcion,
              egoValorReferencia: item.egoValorReferencia,
              egoTipoExamen: item.egoTipoExamen
          }

          try {

            fetch(url, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json'
            }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

          } catch (e) {
              console.log(e);
          }
      })

      this.setState({
        primary: !this.state.primary,
      });
      
  }

  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }

  render() {

    const ListaTodos = this.state.Insumos.map((item, indice)=>{
      return(

        <tr key={item.indice}>
          
          <td id={indice} style={tdStyle}>
            {indice+1}
          </td>

          <td id={indice} style={tdStyle}>
            {item.producto_farmaceutico.codigo}
          </td>

          <td id={indice} style={tdStyle}>
            {item.producto_farmaceutico.nombre} 
          </td>

          <td id={indice} style={tdStyle}>
            {item.lote.id_lote}
          </td>

          <td id={indice} style={tdStyle}>
            {item.lote.fecha_expiracion}
          </td>


          <td id={indice} style={tdStyle}>
            {item.almacen.descripcion}
          </td>

          <td id={indice} style={tdStyle}>
            {item.stockAlmacen}
          </td>

          <td id={indice} style={tdStyle}>
          <Validacion id={item.stockAlmacen} fv={item.lote.fecha_expiracion}/>
          </td>
          
          
     
        </tr>
      )
    })

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> PRODUCTOS EN FARMACIA
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>Numero</th>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Lote</th>
                    <th>Fecha de expiracion</th>
                    <th>Localización</th>
                    <th>Stock Almacen</th>
                   
                  </tr>
                  </thead>
                  <tbody>
                    {ListaTodos}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
      </div>

    );
  }
}

export default Productos_Farmacia;

    


