import React, { Component } from 'react';
import { 
  CardHeader,
  Table,
  PaginationItem,
  PaginationLink,
  Pagination, 
  Button,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import Validacion from '../Validacion'
//import AntecedentesG from './Antecedentes/AntecedentesG';

const trNavLink = {
  padding: '0',
  margin: '0'
}

class DatosProductos extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,

      page: 1,
      eachPage: 10, 
      
    };
    this.print = this.print.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

handleClick(event) {
    this.setState({
        page: Number(event.target.id)
    });
}

print(){
  /*Actual time and date */
  var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
  }
  var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
  }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('l', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();

  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 10, 7, 20,20);

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 18, "\"PROMES\"", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(248, 15, "INVENTARIO EN FARMACIA", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(12);
  doc.text(140, 23, this.props.titulo, null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.text(140,27, showDate +' '+ showTime, null, null, 'center');
  doc.setFontSize(12);

  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {},
    headerStyles: {fontSize: 8},
    bodyStyles: {fontSize: 8},
    alternateRowStyles: {},
    columnStyles: {},
    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: {top: 30, left: 15}, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number, 
    showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,
  });
  window.open(doc.output('bloburl'), '_blank');
}

render() {
  const { page, eachPage} = this.state;
        
  const last = page * eachPage;
  const first = last - eachPage;
 
  const listaProd = this.props.Productos.slice(first, last);



  const productos = listaProd.map((item, indice)=>{
    
    return(
  
      <tr key={item.indice}>
        <td>{indice+1}</td>
        <td>{item.producto_farmaceutico.codigo}</td>
        <td>{item.producto_farmaceutico.nombre} </td>
        <td>{item.lote.id_lote}</td>
        <td>{item.lote.fecha_expiracion}</td>
        <td>{item.almacen.descripcion}</td>
        <td>{item.stockAlmacen}</td>
      
      <Validacion fv={item.lote.fecha_expiracion}/>
      <Validacion id={item.stockAlmacen} />
      </tr>
    )
  })
  const todoProductos = this.props.Productos.map((item, indice)=>{    
    return(
      <tr key={item.indice}>
        <td>{indice+1}</td>
        <td>{item.producto_farmaceutico.codigo}</td>
        <td>{item.producto_farmaceutico.nombre} </td>
        <td>{item.lote.id_lote}</td>
        <td>{item.lote.fecha_expiracion}</td>
        <td>{item.almacen.descripcion}</td>
        <td>{item.stockAlmacen}</td>
      
      <Validacion fv={item.lote.fecha_expiracion}/>
      <Validacion id={item.stockAlmacen} />
      </tr>
    )
  })


  const pages = [];
  for (let i = 1; i <= Math.ceil(this.props.Productos.length / eachPage); i++) {
      pages.push(i);
  }

  const renderpages = pages.map(number => {
      if(number % 2 === 1){
          return (
              <PaginationItem key={number} active>
                  <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
              </PaginationItem>
          );
      }
      else{
          return (
              <PaginationItem key={number}>
                  <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
              </PaginationItem>
          );
      }
  });

  return (
  
    <div className="animated fadeIn">
            <CardHeader>
              <i className="fa fa-table"></i> {this.props.titulo}
              <table style={trNavLink} align="right" >
                <tr>
                  <td>
                  <Button onClick={this.print} size="sm"  color="secondary" className="btn-brand mr-1 mb-1"><i className="fa fa-print"></i><span>Imprimir</span></Button>
                  </td>
                </tr>
              </table>  
            </CardHeader>

              <Table hover bordered striped responsive size="sm">
                <thead>
                <tr>
                    <th>Numero</th>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Lote</th>
                    <th>Fecha de expiracion</th>
                    <th>Localización</th>
                    <th>Stock Almacen</th>
                    <th>Estado Fecha</th>
                    <th>Estado Cantidad</th>
                 
                </tr>
                </thead>
                <tbody>
                  {productos}                  
               <tr>
                  <th colspan="8"></th>
                  <td colspan="1" bgcolor="#424949" ><font color="#ffffff">{"Total: "+this.props.Productos.length}</font>
                  </td>
                  </tr>  
      
              </tbody>  
                     
              </Table>
              {/* <nav>
              <Pagination>
                        {renderpages}
              </Pagination>
              </nav> */}
              <Table  style={trNavLink} hover bordered striped responsive size="sm">
              <Pagination>
                {renderpages}
              </Pagination>
              </Table>
  <Table responsive striped hidden id="tabla">
  <thead>
    <tr>
      <th>No.</th>
      <th>CODIGO</th>
      <th>NOMBRE</th>
      <th>LOTE</th>
      <th>FECHA EXPIRACION</th>
      <th>LOCALIZACION</th>
      <th>STOCK </th>
      <th>ESTADO FECHA</th>
      <th>ESTADO CANTIDAD</th>
    </tr>
  </thead>
  <tbody>
  {todoProductos}
  </tbody>
  </Table>       
    </div>

  )

}
}
  export default DatosProductos;
