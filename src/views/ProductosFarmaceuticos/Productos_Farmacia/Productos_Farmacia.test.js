import React from 'react';
import ReactDOM from 'react-dom';
import Productos_Farmacia from './Productos_Farmacia';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Productos_Farmacia />, div);
  ReactDOM.unmountComponentAtNode(div);
});