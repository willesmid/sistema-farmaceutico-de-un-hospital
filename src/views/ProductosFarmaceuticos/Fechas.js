import React, { Component } from 'react';
import { Badge } from 'reactstrap';

class Validacion extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            data: 0,
            data2: 0,
            Productos:[],
        };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState }});
    }



    async componentDidMount() {
        try {
          const respuesta = await fetch('http://localhost:8000/v1/LoteMedicamentoLista/');
          const Productos = await respuesta.json();
    
    
    
          this.setState({
            Productos
          });
          console.log(Productos)
    
        } catch (e) {
          console.log(e);
        }
      }
    

    calcDays(fv){
        /*calcula los dias que hay entre dos fechas */
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date(Date.parse(fv));
        var secondDate = new Date();  //hoy
        var diffDays = Math.round((firstDate.getTime() - secondDate.getTime())/(oneDay));

        return diffDays
    }
    render() {

        const ListaTodos = this.props.Productos.map((item, indice)=>{

            const dias = this.calcDays(item.lote.fecha_expiracion)
            if((dias >= 31 && dias <= 90)){
       
            return(
          
              <tr key={item.indice}>
                
                <td id={indice} style={tdStyle}>
                  {indice+1}
                </td>
          
                <td id={indice} style={tdStyle}>
                  {item.producto_farmaceutico.codigo}
                </td>
          
                <td id={indice} style={tdStyle}>
                  {item.producto_farmaceutico.nombre} 
                </td>
          
                <td id={indice} style={tdStyle}>
                  {item.lote.id_lote}
                </td>
          
                <td id={indice} style={tdStyle}>
                  {item.lote.fecha_expiracion}
                </td>
          
        
                <td id={indice} style={tdStyle}>
                  {item.cantidad}
                </td>
          
                <td id={indice} style={tdStyle}>
                <Validacion id={item.cantidad} fv={item.lote.fecha_expiracion}/>
                </td>
                
                
           
              </tr>
            )
        }
          })



        /*num-> calcula el stock de un determinado medicamento/insumo */
       
        return (
            <div className="animated fadeIn">
            <Row>
              <Col>
                <Card>
                  <CardHeader>
                    <i className="fa fa-align-justify"></i> {this.props.titulo}
                  </CardHeader>
                  <CardBody>
                    <Table responsive>
                      <thead>
                      <tr>
                        <th>Numero</th>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Lote</th>
                        <th>Fecha de expiracion</th>
                        <th>Cantidad</th>
                        <th>Estado del Producto</th>
                       
                      </tr>
                      </thead>
                      <tbody>
                        {ListaTodos}
                      </tbody>
                    </Table>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            
          </div>
      
        )

        
    }
}
export default Validacion;



