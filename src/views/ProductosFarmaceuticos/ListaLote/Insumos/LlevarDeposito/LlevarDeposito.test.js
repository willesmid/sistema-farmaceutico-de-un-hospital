import React from 'react';
import ReactDOM from 'react-dom';
import LlevarDeposito from './LlevarDeposito';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<LlevarDeposito />, div);
  ReactDOM.unmountComponentAtNode(div);
});