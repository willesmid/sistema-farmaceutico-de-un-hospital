import React from 'react';
import ReactDOM from 'react-dom';
import Productos_Almacen from './Productos_Almacen';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Productos_Almacen />, div);
  ReactDOM.unmountComponentAtNode(div);
});