import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,

  ListGroupItem,
} from 'reactstrap';

// const tdStyle = 
//   paddingLeft: '32em'
// }
class LlevarDeposito extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url_medicamento :'#/Inventario/InventarioPorLote/Medicamentos/LlevarDepositoMedicamentos',
      url_insumo :'#/Inventario/InventarioPorLote/Insumos/LlevarDepositoInsumos',
      url_reactivo :'#/Inventario/InventarioPorLote/Reactivos/LlevarDepositoReactivos',
    }
    this.onCancelar = this.onCancelar.bind(this)
  }

  onCancelar(){
    this.props.history.push('/Inventario/InventarioPorLote/Productos') 
  }

render() {
return (
  <div className="animated fadeIn">
  <Row>
    <Col xs="12" sm="12">
      <Card> 	
        <CardHeader>
          <h3><i className="fa fa-archive"></i><i className="fa fa-hand-lizard-o"></i><strong> Llevar Productos Farmaceuticos a Deposito</strong></h3>
        </CardHeader>
        <CardBody>         
          <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
          <ListGroupItem action color="warning">Seleccionar un Tipo de Producto</ListGroupItem>
          </Col>
          <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
          <Button block color="primary" size="lg" outline href = {this.state.url_medicamento} ><i className="fa fa-archive"></i> <i className="fa fa-hand-lizard-o "></i><span> Llevar a Deposito Medicamentos</span></Button>
          </Col>
          <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
          <Button block color="warning" size="lg" outline href = {this.state.url_insumo}><i className="fa fa-archive"></i> <i className="fa fa-hand-lizard-o "></i><span> Llevar a Deposito Insumos</span></Button>
          </Col>
          <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
          <Button block color="secondary" size="lg" outline href = {this.state.url_reactivo}><i className="fa fa-archive"></i> <i className="fa fa-hand-lizard-o "></i><span> Llevar a Deposito Reactivos</span></Button>
          </Col>
        </CardBody>
        <CardFooter>
          <h3 align="right">
          <Button type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
        </CardFooter>
      </Card>  
    </Col>
  </Row>

</div>
)}
}

export default LlevarDeposito;

    


