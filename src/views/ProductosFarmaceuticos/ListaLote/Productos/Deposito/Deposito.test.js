import React from 'react';
import ReactDOM from 'react-dom';
import Deposito from './Deposito';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Deposito />, div);
  ReactDOM.unmountComponentAtNode(div);
});