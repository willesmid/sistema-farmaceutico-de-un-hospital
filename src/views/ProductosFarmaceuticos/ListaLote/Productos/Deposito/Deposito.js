import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,
  Table,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import Validacion from '../../.././Validacion.js'
const tdStyle = {
  fontSize: '0.9em'
}

class Deposito extends Component {
  constructor(props) {
    super(props)
    this.activitiesRefs = [];
    this.activitiesRefs2 = true;
    this.state = {
      listapre: [],
    }
    this.print = this.print.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
  }

  onCancelar(){
    this.props.history.push('/Inventario/InventarioPorLote/Productos') 
  }

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/DepositoTodos/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const listapre = await respuesta.json();
      this.setState({
        listapre
      });
    } catch (e) {
      console.log(e);
    }
  }

  print(){
    /*Actual time and date */
    var options2 = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
    }
    var options3 = {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }
    var d = new Date();
    var showDate = d.toLocaleString('es-bo', options2) 
    var showTime = d.toLocaleString('es-bo', options3) 
    /*Actual time and date */
    var doc = new jsPDF('l', 'mm', 'legal'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
    var logo = new Image();
  
    logo.src = 'assets/img/dashboard/icono_promes.png';
    doc.addImage(logo, 'JPEG', 10, 7, 20,20);
  
    doc.setFont("helvetica");   
    doc.setFontSize(9);
    doc.text(57, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
    
    doc.setFont("helvetica");
    doc.setFontSize(9);
    doc.text(57, 19, "\"PROMES\"", null, null, 'center');
    
    doc.setFont("helvetica");
    doc.setFontType("bold");   //italic
    doc.setFontSize(13);
    doc.text(180, 23, "DEPOSITO DE PRODUCTOS FARMACEUTICOS", null, null, 'center');
    
    doc.setFontType("normal");
    doc.setFontSize(9);
    doc.text(180,27, showDate +' '+ showTime, null, null, 'center');
   
    var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
    doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {fontSize: 8},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 30, left: 15}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
    });
    window.open(doc.output('bloburl'), '_blank');
  }

render() {

return (
  <div className="animated fadeIn">
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className="fa fa-archive"></i><strong> Deposito Productos Farmaceuticos</strong>
            <Button size="sm" onClick={this.print} color="link" className="btn  btn-nfo float-right">
              <i className="fa fa-print"></i> Imprimir Deposito
            </Button> 
          </h3>
        </CardHeader>
        <CardBody>         
            <Table hover responsive size="sm" id="tabla">
              <thead>
                <tr>
                  <th>Numero</th>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Lote</th>
                  <th>F/Expiración</th>
                  <th>Cantidad</th>
                  <th>F/Deposito</th>
                  <th>H/Deposito</th>
                  <th>Tipo producto</th>
                  <th>Observación</th> 
                  <th>Estado</th>
                  <th>Cantidad</th>              
                </tr>
              </thead>
              <tbody>
                {this.state.listapre.map((item, indice) => (
                  <tr key={item.indice}>
                    <td style={tdStyle}>{indice+1}</td>
                    <td style={tdStyle}>{item.codigo}</td>
                    <td style={tdStyle}>{item.nombre}</td>
                    <td style={tdStyle}>{item.lote} </td>
                    <td style={tdStyle}>{item.fecha_expiracion}</td>
                    <td style={tdStyle}>{item.cantidad}</td>
                    <td style={tdStyle}>{item.fecha_deposito}</td>
                    <td style={tdStyle}>{item.hora_deposito}</td>
                    <td style={tdStyle}>{item.tipo_producto}</td>
                    <td style={tdStyle}>{item.observacion}</td>
                    <Validacion fv={item.fecha_expiracion}/>
                    <Validacion id={item.cantidad} />
                  </tr>
                ))}
              </tbody>
            </Table>   
        </CardBody>
        <CardFooter>
          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
        </CardFooter>
      </Card>  
    </Col>
  </Row>
</div>
);
}
}

export default Deposito;