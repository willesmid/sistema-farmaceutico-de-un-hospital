import React from 'react';
import ReactDOM from 'react-dom';
import DatosProductos from './DatosProductos';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DatosProductos />, div);
  ReactDOM.unmountComponentAtNode(div);
});