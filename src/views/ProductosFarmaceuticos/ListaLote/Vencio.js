import React, { Component } from 'react';
import DatosProductos from './DatosProductos'

class Vencio extends Component {
  constructor(props) {
      super(props);
      this.toggle = this.toggle.bind(this);
      this.toggleFade = this.toggleFade.bind(this);
      this.state = {
        collapse: true,
        fadeIn: true,
        timeout: 300,
        TodosProductos: [],       
      };
  }

  toggle() {
      this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
      this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  
  calcDays(fv){
      /*calcula los dias que hay entre dos fechas */
      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date(Date.parse(fv));
      var secondDate = new Date();  //hoy
      var diffDays = Math.round((firstDate.getTime() - secondDate.getTime())/(oneDay));

      return diffDays
  }

render() {
  var TodosProductos=[]
  this.props.Productos.map((item, indice)=>{
    const dias = this.calcDays(item.lote.fecha_expiracion)

    if(dias <= 0){
        
      TodosProductos=TodosProductos.concat(item)
    }
    return null
  })

  return (
    <div className="animated fadeIn">
      { <DatosProductos Productos={TodosProductos} titulo={this.props.titulo4} url_deposito={this.props.url_deposito} url_deposito2={this.props.url_deposito2} />}

    </div>
  )     
}
}
export default Vencio;



