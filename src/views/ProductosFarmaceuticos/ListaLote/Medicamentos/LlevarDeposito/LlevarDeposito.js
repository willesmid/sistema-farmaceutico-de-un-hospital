import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,
  Input,
  FormGroup, 
  InputGroup,
  InputGroupAddon,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  FormText,
} from 'reactstrap';

import Validacion from '../../.././Validacion.js'
const styles2 = { color: 'white', backgroundColor: 'white' }
const styles3 = { color: '#f33c29' }
const tdStyle = {
  fontSize: '0.9em'
}

class LlevarDeposito extends Component {
  constructor(props) {
    super(props)
    this.activitiesRefs = [];
    this.activitiesRefs2 = true;
    this.state = {
      showCancelar:true,
      listapre: [],
      valor: '',
      checked2:false,
      checked:false,
      lista:'',
      showA:false,
      showB:true,
      showAD:true,
      showBD:true,
      medicamentos:[],
      primary:false,
      primary2:false,
      listaVencidos:[],
      listaOtros:[],
      algo:'',
      observacion:'',
      showObservacion:false,
      listaNinguna:[],
    }
    this.onCancelar = this.onCancelar.bind(this)
    this.buscarValor = this.buscarValor.bind(this) 
    this.Depositar = this.Depositar.bind(this)
    this.Listar=this.Listar.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
  }
  togglePrimary() {
    this.setState({ 
      primary: !this.state.primary,
      showObservacion:false,
      observacion:'',
    });
  }

  togglePrimary2() {
    this.setState({ primary2: !this.state.primary2});
  }
  

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/LoteMedicamentoLista/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const listapre = await respuesta.json();

      this.setState({
        listapre
      });
    } catch (e) {
      console.log(e);
    }
  }

  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  onCancelar(){
    this.props.history.push('/Inventario/InventarioPorLote/Medicamentos') 
  }

onMarcarA(){
  var check = this.activitiesRefs2.checked
  console.log("por que no dara " + this.activitiesRefs2.checked)
  console.log("por que no daraasds " + check)
  if(this.activitiesRefs2.checked){
    
    for (let i = 0; i < this.activitiesRefs.length; i++) {
      if(this.activitiesRefs[i]!==null){
      this.activitiesRefs[i].checked=true
      console.log("y que es" + check)
    }else{
      
    }
  }
  
  }else{

    for (let i = 0; i < this.activitiesRefs.length; i++) {
      if(this.activitiesRefs[i]!==null){
      this.activitiesRefs[i].checked=false
      console.log("y que es" + check)
    }
    }
  }


}
onCheck(name, val) {
  const checkboxes = Object.assign({},this.state.checkboxes, {});
  for (let key in checkboxes) {
    checkboxes[key] = false;
  }
  checkboxes[name] = true;
  checkboxes.selected = val;
  this.setState({ checkboxes });
}

Depositar(){
  if(this.state.observacion!==''){
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options)

  var productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
    if (check) productos = productos.concat([this.activitiesRefs[i].value])
    }
  }

  for (let i = 0; i < productos.length; i++) {
    let urlDeposito=''
    let dataDeposito={}
    urlDeposito='http://localhost:8000/farmacia/almacena/v1/DepositoGuardar/'

    fetch('http://localhost:8000/farmacia/almacena/v1/Lote_ProductoMedicamentoBuscaLote/'+productos[i]+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      const stockGeneral=findresponse[0].producto_farmaceutico.stockGeneral
      dataDeposito={
        codigo: findresponse[0].producto_farmaceutico.codigo,
        nombre: findresponse[0].producto_farmaceutico.nombre,
        lote: findresponse[0].lote.id_lote,
        fecha_expiracion: findresponse[0].lote.fecha_expiracion,
        cantidad: findresponse[0].cantidad,
        fecha_deposito:hoy,
        hora_deposito:hora,
        tipo_producto:'medicamento',
        observacion:this.state.observacion
  
      }
      console.log(stockGeneral)
      try {
        fetch(urlDeposito, {
        method: 'POST',
        body: JSON.stringify(dataDeposito),
        headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
          console.log('Success:', response)
          if(i===productos.length-1){
          }   
      }); 
      } catch (e) {
          console.log(e);
      }
      console.log(findresponse[0].producto_farmaceutico.codigo,)
      console.log("Espero que si de:" + dataDeposito.codigo+ ' '+ dataDeposito.nombre+ ' '+ dataDeposito.lote+ ' '+dataDeposito.fecha_expiracion+ ' aqui' +dataDeposito.fecha_deposito + ' '+ dataDeposito.observacion )
      //EDITA CANTIDAD GENERAL
        let urlGeneral = ''
        let dataGeneral = {}
        urlGeneral = 'http://localhost:8000/farmacia/almacena/v1/ProductoEdita/'+findresponse[0].producto_farmaceutico.codigo+'/'
        const numGeneral= parseInt(stockGeneral,10) - parseInt(findresponse[0].cantidad,10)
        console.log('nueva cantidad RESTADA : '+ numGeneral)
        dataGeneral = {
          stockGeneral : numGeneral
        }
        try {
          fetch(urlGeneral, {
          method: 'PUT',
          body: JSON.stringify(dataGeneral),
          headers:{
            'Content-Type': 'application/json',
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
          }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

        } catch (e) {
          console.log(e);
        } 
    })

    let dataElimina = {}
    let urlElimina = ''
    urlElimina = 'http://localhost:8000/farmacia/almacena/v1/EliminarLote/'+productos[i]+'/'
    dataElimina = {
    codigo: productos[i]
    }

    try {
      fetch(urlElimina, {
      method: 'DELETE',
      body: JSON.stringify(dataElimina),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        console.log('Success:', response)
        if(i===productos.length-1){
          window.location.reload()
        }   
    }); //UTILIZAR UN SNACKBAR 
    } catch (e) {
        console.log(e);
    }
  }
  this.setState({
    primary:false,
  }) 
}

if(this.state.observacion===''){
  this.setState({
    showObservacion:true,
  })
}
this.activitiesRefs2.checked=false
}

Listar(){
  var listaVencidos=[]
  var listaOtros=[]
  var listaNinguna=[]
  
  if(this.state.lista==="vencidos"){
    fetch('http://localhost:8000/farmacia/almacena/v1/LoteMedicamentoLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      findresponse.map((item, indice)=>{
        const dias = this.calcDays(item.lote.fecha_expiracion)
        if(dias <= 0){    
          listaVencidos = listaVencidos.concat(item)
        }
        return null
      })
      this.setState({
        listapre:listaVencidos,
        showA:true,
        showAD:false,
        listaOtros:[],
        listaNinguna:[],
      });
    })

  }else{
    if(this.state.lista==="no vencidos"){
    fetch('http://localhost:8000/farmacia/almacena/v1/LoteMedicamentoLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      
      findresponse.map((item, indice)=>{
        const dias = this.calcDays(item.lote.fecha_expiracion)
        if(dias > 0){    
          listaOtros = listaOtros.concat(item)
        }
        return null
      })
     //   console.log("FINDRESPONSE=",findresponse)
      this.setState({
        listapre: listaOtros,
        showA:true,
        showAD:false,
        listaVencidos:[],
        listaNinguna:[],
      })
    })
  }else{  
    if(this.state.lista==="ninguna"){

      fetch('http://localhost:8000/farmacia/almacena/v1/LoteMedicamentoLista/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
        findresponse.map((item, indice)=>{
     
        if(item.cantidad === 0){
          listaNinguna = listaNinguna.concat(item)
        }
          return null
        })
   
        this.setState({
          listapre:listaNinguna,
          showA:true,
          showAD:false,
          listaOtros:[],
          listaVencidos:[],
        });
      })

    }else{
      fetch('http://localhost:8000/farmacia/almacena/v1/LoteMedicamentoLista/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
       //   console.log("FINDRESPONSE=",findresponse)
        this.setState({
          listapre: findresponse,
          showA:false,
          showAD:true,
          listaOtros:[],
          listaVencidos:[],
          listaNinguna:[],
        })
      })
    }
  }
}
}

calcDays(fv){
  /*calcula los dias que hay entre dos fechas */
  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  var firstDate = new Date(Date.parse(fv));
  var secondDate = new Date();  //hoy
  var diffDays = Math.round((firstDate.getTime() - secondDate.getTime())/(oneDay));

  return diffDays
}

render() {

  let items_filtrados = this.state.listapre.filter(
    (item) => {
      return (item.producto_farmaceutico.codigo + ' ' + item.producto_farmaceutico.nombre + ' ' + item.lote.id_lote)
        .toUpperCase().indexOf(
        this.state.valor.toUpperCase()) !== -1;
    }
  );

  let productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
      if (check) {
      productos = productos.concat([this.activitiesRefs[i].value])
      }
    }
  }

return (
  <div className="animated fadeIn">
  <Row>
    <Col xs="12" sm="12">
      <Card> 	
        <CardHeader>
          <h3><i className="fa fa-archive"></i><i className="fa fa-hand-lizard-o"></i><strong> Llevar Medicamentos a Deposito</strong></h3>
        </CardHeader>
        <CardBody>         
          <Col xs="12" md="12">
            <FormGroup row>
              <Col xs="12" md="5">
                <InputGroup>
                <InputGroupAddon addonType="prepend">
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" placeholder="Ingrese Codigo o Nombre" value={this.state.valor} onChange={this.buscarValor}/>
                <InputGroupAddon addonType="append">
                <Button type="button" color="primary" disabled><i className="fa fa-search"></i></Button>
                </InputGroupAddon>
                </InputGroup>
              </Col>
              <Col xs="12" md="5">
              <Input 
              type="select" 
              name="unidadProd" 
              id="unidadProd" 
              value={this.state.lista}
              onChange={e => { this.setState({ lista: e.target.value }) }}
              onClick={this.Listar}
              >
              <option value="" >Estado de Medicamento...</option>
              <option value="vencidos">VENCIDOS</option>
              <option value="no vencidos">NO VENCIDOS</option>
              <option value="ninguna">NINGUNA</option>
              </Input> 
              </Col>
              <Col xs="12" md="2">
              {this.state.showAD &&
              <h3>
              <Button size="sm" className="btn-youtube btn-brand mr-1 mb-1" disabled><i className="fa fa-hand-lizard-o"></i><span>Llevar a Deposito </span></Button>                 
              </h3>
              }
              {this.state.showA &&
              <h3>
              <Button onClick={this.togglePrimary} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-hand-lizard-o"></i><span>Llevar a Deposito </span></Button>                 
              </h3> 
              }
              </Col>
            </FormGroup>
          </Col>
         
            <Table hover responsive size="sm">
              <thead>
                <tr>
                  <th>Numero</th>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Lote</th>
                  <th>Fecha de expiracion</th>
                  <th>Cantidad</th>
                  <th>Estado Fecha</th>  
                  <th>Estado</th>                
                  <th style={styles2}>    
                  <center>
                  <input className="form-check-input" type="checkbox" value={0} ref={ref => this.activitiesRefs2 = ref} onClick={this.onMarcarA.bind(this)} />                         
                  .
                  </center>
                  </th>
                </tr>
              </thead>
              <tbody>
                {items_filtrados.map((item, indice) => (
                  <tr key={item.indice}>
                    <td style={tdStyle}>{indice+1}</td>
                    <td style={tdStyle}>{item.producto_farmaceutico.codigo}</td>
                    <td style={tdStyle}>{item.producto_farmaceutico.nombre}</td>
                    <td style={tdStyle}>{item.lote.id_lote} </td>
                    <td style={tdStyle}>{item.lote.fecha_expiracion}</td>
                    <td style={tdStyle}>{item.cantidad}</td>
                    <Validacion fv={item.lote.fecha_expiracion}/>
                    <Validacion id={item.cantidad} />
                    <td>
                    <FormGroup check inline key={indice}>
                    <input className="form-check-input" type="checkbox" value={item.lote.id_lote} ref={ref => this.activitiesRefs[indice] = ref} />
                    </FormGroup>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>  
        </CardBody>

        <CardFooter>
          <h3 align="right">
          <Button type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
        </CardFooter>
      </Card>  
    </Col>
  </Row>

  {/* MODAL DEPOSITAR */}
  <Modal isOpen={this.state.primary} className={'modal-warning ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
    <ModalBody>
    <center>
    ¿Desea llevar a Deposito el/los medicamentos?    
      <FormGroup row>
        <Col xs="12" md="2">
        <Label htmlFor="textarea-input">Observación</Label>
        </Col>
          <Col xs="12" md="10">
            <Input type="textarea" rows="1" placeholder="Observación" className="form-control" maxlength="100"
              onChange={e => this.setState({ observacion: e.target.value })} 
            />
            {
            this.state.showObservacion &&
            <FormText className="help-block" color= "#ca1125" style={styles3}>Escriba Observación</FormText>
            }
          </Col>            
      </FormGroup>       
      <Col sm="6">
      <Table  responsive size="sm">
      <thead>
        <tr>
          <th>Nro.</th>
          <th>Código</th>
        </tr>
      </thead>
      <tbody>
      {productos.map((item, indice) => ( 
        <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item}</td>
        </tr>
      ))}
      </tbody>
      </Table>
      </Col>
      </center>
    </ModalBody>
    <ModalFooter>
      <Button onClick={this.Depositar} type="submit" color="warning">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimary}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL DEPOSITAR */}
</div>
)}
}

export default LlevarDeposito;

    


