import React, { Component } from 'react';
import { Badge } from 'reactstrap';

class Validacion extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            data: 0,
            data2: 0
        };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState }});
    }

    calcDays(fv){
        /*calcula los dias que hay entre dos fechas */
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date(Date.parse(fv));
        var secondDate = new Date();  //hoy
        var diffDays = Math.round((firstDate.getTime() - secondDate.getTime())/(oneDay));

        return diffDays
    }
    render() {
        /*num-> calcula el stock de un determinado medicamento/insumo */
        const num = this.props.id
        const message = (num <= 10 && num > 0)?"POCAS UNIDADES":((num === 0)?"NO HAY MEDICAMENTO":"")
        const type = (num <= 10 && num > 0)?"secondary":((num === 0)?"danger":"")
        const dias = this.calcDays(this.props.fv)
        const cad = (dias >= 31 && dias <= 90)?"VENCIMIENTO CERCANO":((dias > 0 && dias <= 30)?"VENCERÁ":(dias <= 0?"VENCIÓ":""))
        const type2 = (dias >= 31 && dias <= 90)?"warning":((dias > 0 && dias <= 30)?"warning":(dias <= 0?"danger":""))
        
        return (
            <td>
                <Badge color={type}>{message}</Badge>
                <Badge color={type2}>{cad}</Badge>
            </td>

        );
    }
}
export default Validacion;
