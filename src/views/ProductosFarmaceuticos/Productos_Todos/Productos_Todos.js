import React, { Component } from 'react';
import { 
  Card,
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form, 
  FormGroup, 
  Button,
  Table,   
} from 'reactstrap';
//import DatosProductos2 from '../././DatosProd
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import Validacion from '../Validacion'
import DatosProductos from './DatosProductos'
import VencimientoCercano from './VencimientoCercano'
import Vencera from './Vencera'
import Vencio from './Vencio'
import PocasUnidades from './PocasUnidades'
import Ninguna from './Ninguna'

import Search from 'react-search-box'

class Todos_Productos extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1,
      data: [],  
      Productos: [],
      Productos2: [],
      ProductosT: [],

      show: false,
      titulo:'',
      nom_producto:'',

      show2:false,
      titulo2:'',

      show3:false,
      titulo3:'',

      show4:false,
      titulo4:'',

      showCant:false,
      tituloCant:'', 

      showCant2:false,
      tituloCant2:'', 

      showAAAA:false,
      select:"0",
      
    };
    this.print = this.print.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.toggleTable = this.toggleTable.bind(this)
    this.toggleTable2 = this.toggleTable2.bind(this)
  }

toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
}




async componentDidMount(){
    try{
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/productos/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const data = await respuesta.json();
      const respuesta1 = await fetch('http://localhost:8000/farmacia/almacena/v1/EstaUbicadoLista/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const Productos = await respuesta1.json();
      //Para los Estados Fecha Vencimiento y Cantidades
      const respuesta2 = await fetch('http://localhost:8000/farmacia/almacena/v1/EstaUbicadoLista/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const Productos2 = await respuesta2.json();
      const ProductosT = Productos2;
      this.setState({
        data,
        Productos,
        Productos2,
        ProductosT,
        show:true,
        titulo:'INVENTARIO DE PRODUCTOS',
        nom_producto:'',
      });
    }catch(e){
      console.log(e);
    }
}

onFormSubmit() {
    fetch('http://localhost:8000/farmacia/almacena/v1/EstaUbicadoLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      this.setState({
        Productos:findresponse,
        show: true,
        titulo:'INVENTARIO DE PRODUCTOS',
        nom_producto:'',
        show2:false,
        show3:false,
        show4:false,
        showCant:false,
        showCant2:false,
      
      })
    }); 
    return 0;
}

handleChange(value){
    this.state.data.map((item)=>{
      if (value===item.codigo){
        /*Para mostrar la descripcion del lote_medicamento con id x */
        
        fetch('http://localhost:8000/farmacia/almacena/v1/EstaUbicadoListaBuscaCod/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              Productos: findresponse,
              show: true,
              show2: false,
              show3:false,
              show4:false,
              showCant:false,
              showCant2:false,
              titulo:'DETALLE DEL PRODUCTO: ',
              nom_producto:item.nombre,
            
            })
        });  
      }
      return 0;
    })
    return 0;
}

toggleTable(){

  try{
    

    if(this.selectOptions.value==="1"){
      this.setState({
        show:false,
        showCant:false,
        showCant2:false,
        show4:false,
        show3:false,
        show2:true,  
        
             
        titulo2:'INVENTARIO DE PRODUCTOS CON VENCIMIENTO CERCANO',     
      })
    //  this.selectOptions.value="0" 
    }
   
    if(this.selectOptions.value==="2"){
      this.setState({
        show:false,
        showCant:false,
        showCant2:false,
        show4:false,
        show2:false,
        show3:true,

  
        titulo3:'INVENTARIO DE PRODUCTOS QUE VENCERAN',
    
        
      })

    }
    if(this.selectOptions.value==="3"){
      this.setState({
        show:false,
        showCant:false,
        showCant2:false,
        show2:false,
        show3:false,
        show4:true,

        titulo4:'INVENTARIO DE PRODUCTOS QUE VENCIERON',
      })
      //this.selectOptions.value="0"
    } 

    
  
  }
  catch(e){
    console.log(e);
  }

 this.selectOptions.value="0" 
  
 
}

toggleTable3(){
  try{
  this.setState({
    Productos2:[],
  })
}catch(e){



}

}


toggleTable2(){

  try{
    
    if(this.selectOptions2.value==="6"){        
      this.setState({
        show:false,
        show4:false,
        show3:false,
        show2:false,
        showCant:true,
        showCant2:false,
        
        tituloCant:'INVENTARIO DE PRODUCTOS CON POCAS UNIDADES',
      })
    }
    if(this.selectOptions2.value==="7"){
      this.setState({
        show:false,
        show4:false,
        show3:false,
        show2:false,
        showCant:false,
        showCant2:true,
        tituloCant2:'INVENTARIO DE PRODUCTOS FALTANTES',     
      })
    }
  }
  catch(e){
    console.log(e);
  }
   this.selectOptions2.value="5" 
  
}



handleFocus(event) {
  event.target.select(); // it did not work!!
}

print(){
  /*Actual time and date */
  var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
  }
  var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
  }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('l', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();

  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 10, 7, 20,20);

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 18, "\"PROMES\"", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(12);
  doc.text(140, 23, "INVENTARIO DE PRODUCTOS EN FARMACIA Y ALMACEN", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.text(140,27, showDate +' '+ showTime, null, null, 'center');
  doc.setFontSize(12);

  var res = doc.autoTableHtmlToJson(document.getElementById("tabla2"));
  doc.autoTable(res.columns, res.data, {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {},
    headerStyles: {fontSize: 8},
    bodyStyles: {fontSize: 8},
    alternateRowStyles: {},
    columnStyles: {},
    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: {top: 30, left: 15}, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number, 
    showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,
  });
  window.open(doc.output('bloburl'), '_blank');
}

render() {  
  const todoProductos = this.state.ProductosT.map((item, indice)=>{    
    return(
      <tr key={item.indice}>
        <td>{indice+1}</td>
        <td>{item.producto_farmaceutico.codigo}</td>
        <td>{item.producto_farmaceutico.nombre} </td>
        <td>{item.lote.id_lote}</td>
        <td>{item.lote.fecha_expiracion}</td>
        <td>{item.almacen.descripcion}</td>
        <td>{item.stockAlmacen}</td>
        <Validacion fv={item.lote.fecha_expiracion}/>
        <Validacion id={item.stockAlmacen} />
      </tr>
    )
  })   
  return (
  
    <div className="animated fadeIn">
    <Form>
      <Row>
        {/* Buscar por codigo de medicamento */}
        <Col xs="12" md="12">
          <Card>
            <CardHeader>
            <h3><i className="fa fa-table" ></i> <strong>Inventario en Farmacia y Almacen: Productos</strong>
            <Button size="sm" onClick={this.print} color="link" className="btn  btn-nfo float-right">
              <i className="fa fa-print"></i> Imprimir Todo
            </Button>
            </h3>
            </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12">
                    <form>
                      <FormGroup row>
                        <Col  md="3">
                          <Search
                            data = {this.state.data}
                            onChange={ this.handleChange.bind(this) }
                            placeholder="Ingrese Codigo Producto"
                            class="search-class"
                            searchKey="codigo"
                          />
                        </Col>
                        <br/>
                        <Col  md="1">
                          <Button color="success" className="mr-1"  onClick={this.onFormSubmit}  >Ver Todo</Button>
                        </Col>
                        <Col  md="3">
                        <select id="options" name="options" className="form-control" onClick={ this.toggleTable3}  ref= {selectOptions => this.selectOptions = selectOptions} >
                          <option value = "0">Seleccione Estado-Fecha...</option>
                          <option value = "1">Vencimiento Cercano</option>
                          <option value = "2">Vencerá</option>
                          <option value = "3">Venció</option>
                        </select>
                        </Col>
                        <Col xs="12" md="1">
                        <Button onClick={ this.toggleTable}   color="primary">Buscar</Button>
                        </Col>
                        <Col  md="3">
                        <select id="options" name="options" className="form-control" ref= {selectOptions2 => this.selectOptions2 = selectOptions2}>
                          <option value = "5">Seleccione Estado-Stock...</option>
                          <option value = "6">Pocas Unidades</option>
                          <option value = "7">Ninguna</option>

                        </select>
                        </Col>
                        <Col xs="12" md="1">
                        <Button onClick={this.toggleTable2} color="primary">Buscar</Button>
                        </Col>

                      </FormGroup>
                    </form> 
                  </Col>
                     
                </Row>
                {this.state.show && <DatosProductos Productos={this.state.Productos} titulo={this.state.titulo+' '+this.state.nom_producto}  />}
                {this.state.show2 && <VencimientoCercano Productos={this.state.Productos2}  titulo2={this.state.titulo2}/>}
                {this.state.show3 && <Vencera Productos={this.state.Productos2}  titulo3={this.state.titulo3} />}
                {this.state.show4 && <Vencio Productos={this.state.Productos2}  titulo4={this.state.titulo4}/> }
                {this.state.showCant && <PocasUnidades Productos={this.state.Productos2}  tituloCant={this.state.tituloCant}/> }
                {this.state.showCant2 && <Ninguna Productos={this.state.Productos2}  tituloCant2={this.state.tituloCant2}/> }
              </CardBody> 
          </Card>
        </Col>
      </Row>                

    </Form>
  
  <Table responsive striped hidden id="tabla2">
  <thead>
    <tr>
      <th>No.</th>
      <th>CODIGO</th>
      <th>NOMBRE</th>
      <th>LOTE</th>
      <th>FECHA EXPIRACION</th>
      <th>LOCALIZACION</th>
      <th>STOCK </th>
      <th>ESTADO FECHA</th>
      <th>ESTADO CANTIDAD</th>
    </tr>
  </thead>
  <tbody>
  {todoProductos}
  </tbody>
  </Table>
      </div>
    );
  }
}

export default Todos_Productos;














    













    


