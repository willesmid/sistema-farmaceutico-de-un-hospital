import React from 'react';
import ReactDOM from 'react-dom';
import Productos_Todos from './Productos_Todos';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Productos_Todos />, div);
  ReactDOM.unmountComponentAtNode(div);
});