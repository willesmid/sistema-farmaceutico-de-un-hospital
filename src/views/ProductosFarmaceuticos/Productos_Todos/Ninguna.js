import React, { Component } from 'react';
import DatosProductos from './DatosProductos'

class Ninguna extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            TodosProductos: [],       
        };

    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState }});
    }
  


    render() {
        var TodosProductos=[]
        this.props.Productos.map((item, indice)=>{


          if(item.stockAlmacen === 0){
              
            TodosProductos=TodosProductos.concat(item)
          }
          return null
        })

        return (
          <div className="animated fadeIn">

  { <DatosProductos Productos={TodosProductos} titulo={this.props.tituloCant2} url_deposito={this.props.url_deposito} url_deposito2={this.props.url_deposito2}  />}
          </div>
        )     
    }
}
export default Ninguna;


