import React, { Component } from 'react';
import { 

  Card,
  CardBody,

  CardHeader,
  Col,

  Row,

  Table,
  
 } from 'reactstrap';
import Validacion from '../Validacion'
//import AntecedentesG from './Antecedentes/AntecedentesG';

const tdStyle = {
  fontSize: '0.9em'
}

class DatosProducto extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1,
      collapse: true,
      fadeIn: true,
  
      timeout: 300
    };
  }


  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }


  toggle(tab) {
    if (this.state.activeTab !== tab) {
    this.setState({
        activeTab: tab
    });
    }
}



render() {
  const ListaTodos = this.props.Productos.map((item, indice)=>{
    return(
  
      <tr key={item.indice}>
        
        <td id={indice} style={tdStyle}>
          {indice+1}
        </td>
  
        <td id={indice} style={tdStyle}>
          {item.producto_farmaceutico.codigo}
        </td>
  
        <td id={indice} style={tdStyle}>
          {item.producto_farmaceutico.nombre} 
        </td>
  
        <td id={indice} style={tdStyle}>
          {item.lote.id_lote}
        </td>
  
        <td id={indice} style={tdStyle}>
          {item.lote.fecha_expiracion}
        </td>
  

        <td id={indice} style={tdStyle}>
          {item.cantidad}
        </td>
  
        <td id={indice} style={tdStyle}>
        <Validacion id={item.cantidad} fv={item.lote.fecha_expiracion}/>
        </td>
        
        
   
      </tr>
    )
  })





  return (
  

    <div className="animated fadeIn">
      <Row>
        <Col >
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> {this.props.titulo}
            </CardHeader>
            <CardBody>
              <Table responsive>
                <thead>
                <tr>
                  <th>Numero</th>
                  <th>Codigo</th>
                  <th>Nombre</th>
                  <th>Lote</th>
                  <th>Fecha de expiracion</th>
                  <th>Cantidad</th>
                  <th>Estado del Producto</th>
                 
                </tr>
                </thead>
                <tbody>
                  {ListaTodos}
                </tbody>
             
              </Table>
      
            </CardBody>
          </Card>
        </Col>
      </Row>
      
    </div>

  )

}
}
  export default DatosProducto;
