import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';


import DatosProductos from '../DatosProductos'

const tdStyle = {
  fontSize: '0.9em'
}

class Productos extends Component {
  constructor(props) {
    super(props)

    this.state = {
        Productos: [],
        primary: false,
        titulo:'LISTA TODOS LOS PRODUCTOS',
    }



  }

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/v1/LoteProductoLista/');
      const Productos = await respuesta.json();



      this.setState({
        Productos
      });
      console.log(Productos)

    } catch (e) {
      console.log(e);
    }
  }




  render() {



    return (
      <div className="animated fadeIn">
       <DatosProductos Productos={this.state.Productos} titulo={this.state.titulo}/>
      </div>

    );
  }
}

export default Productos;

    


