import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';


import DatosProductos from '../DatosProductos'


class Reactivos extends Component {
  constructor(props) {
    super(props)

    this.state = {
        Productos: [],
        primary: false,
        titulo:'LISTA REACTIVOS',
    }



  }

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/v1/LoteReactivosLista/');
      const Productos = await respuesta.json();



      this.setState({
        Productos
      });
      console.log(Productos)

    } catch (e) {
      console.log(e);
    }
  }




  render() {



    return (
      <div className="animated fadeIn">
       <DatosProductos Productos={this.state.Productos} titulo={this.state.titulo}/>
      </div>

    );
  }
}

export default Reactivos;

    


