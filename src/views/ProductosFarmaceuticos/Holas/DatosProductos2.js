import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, ListGroup, ListGroupItem, Row, Pagination, PaginationItem, PaginationLink, Table} from 'reactstrap';

class DatosProductos2 extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: 1,
            data: [],
            data2: []
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
        this.setState({
            activeTab: tab
        });
        }
    }

    render() {
        const des = this.props.Productos.map((item, indice)=>{
            return(
                <ListGroup key={indice}>
                    <ListGroupItem className="justify-content-between">Código medicamento/insumo <strong className="float-right">{item.codigo}</strong></ListGroupItem>
                    <ListGroupItem className="justify-content-between">Nombre medicamento/insumo <strong className="float-right">{item.nombre}</strong> </ListGroupItem>
                    <ListGroupItem className="justify-content-between">Stock <strong className="float-right">{this.props.stock} unidades</strong ></ListGroupItem>
                    <ListGroupItem className="justify-content-between">Dosis <strong className="float-right">{item.dosis}</strong ></ListGroupItem>
                    <ListGroupItem className="justify-content-between">Presentación <strong className="float-right">{item.presentacion}</strong ></ListGroupItem>
                    <ListGroupItem className="justify-content-between">Fecha de vencimiento <strong className="float-right">{item.fecha_vencimiento}</strong></ListGroupItem>
                </ListGroup>
            )
        })
        const retira = this.props.data3.map((item, indice) =>{
            return(
                <tr key={indice}>
                    <td>{item.fecha}</td>
                    <td>{item.hora}</td>
                    <td>{item.enfermera_que_retira.profesion} {item.enfermera_que_retira.nombres} {item.enfermera_que_retira.apellido_paterno} {item.enfermera_que_retira.apellido_materno}</td>
                    <td>{item.cantidad}</td>
                </tr>
            )
        })
        const repone = this.props.data2.map((item, indice) =>{
            return(
                <tr key={indice}>
                    <td>{item.fecha}</td>
                    <td>{item.hora}</td>
                    <td>{item.enfermera_que_repone.profesion} {item.  enfermera_que_repone.nombres} {item.  enfermera_que_repone.apellido_paterno} {item. enfermera_que_repone.apellido_materno}</td>
                    <td>{item.cantidad_entregada}</td>
                </tr>
            )
        })
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col sm="12" md="4">
                        <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i><strong>DESCRIPCION</strong>
                        </CardHeader>
                        <CardBody>
                            {des}
                        </CardBody>
                        </Card>
                    </Col>
                    <Col sm="12" md="4">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>SALIDAS</strong>
                            </CardHeader>
                            <CardBody>
                                <Table responsive size="sm">
                                <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Enfermera de turno</th>
                                    <th>Cantidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {retira}
                                </tbody>
                                </Table>
                                <Pagination>
                                <PaginationItem disabled><PaginationLink previous tag="button">Anterior</PaginationLink></PaginationItem>
                                <PaginationItem active>
                                    <PaginationLink tag="button">1</PaginationLink>
                                </PaginationItem>
                                <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                                <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                                <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                                <PaginationItem><PaginationLink next tag="button">Siguiente</PaginationLink></PaginationItem>
                                </Pagination>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col sm="12" md="4">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i><strong>ENTRADAS</strong>
                            </CardHeader>
                            <CardBody>
                                <Table responsive size="sm">
                                <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Enfermera de turno</th>
                                    <th>Cantidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {repone}
                                </tbody>
                                </Table>
                                <Pagination>
                                <PaginationItem disabled><PaginationLink previous tag="button">Anterior</PaginationLink></PaginationItem>
                                <PaginationItem active>
                                    <PaginationLink tag="button">1</PaginationLink>
                                </PaginationItem>
                                <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                                <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                                <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                                <PaginationItem><PaginationLink next tag="button">Siguiente</PaginationLink></PaginationItem>
                                </Pagination>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default DatosProductos2;
