import React from 'react';
import ReactDOM from 'react-dom';
import Hola from './Hola';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Hola />, div);
  ReactDOM.unmountComponentAtNode(div);
});