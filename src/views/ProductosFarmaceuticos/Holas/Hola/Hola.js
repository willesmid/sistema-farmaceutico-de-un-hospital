import React, { Component } from 'react';
import { Card,
   CardBody, 
   CardHeader, 
   Col, 
   Row, 
   Alert, 
   Form, 
   FormGroup, 
   Label,
   Button,
   
  } from 'reactstrap';
//import DatosProductos2 from '../././DatosProd
import DatosProductos from '../DatosProductos'
import VencimientoCercano from '../VencimientoCercano'
import Vencera from '../Vencera'
import Vencio from '../Vencio'
import PocasUnidades from '../PocasUnidades'
import Ninguna from '../Ninguna'

import Search from 'react-search-box'

class Hola extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1,
      data: [],
      data2 : [{name:"Vencimiento Cercano"},{name:"Vencerá"},{name:"Venció"}],
      Productos: [],
      Productos2: [],

      show: false,
      titulo:'',
      nom_producto:'',

      show2:false,
      titulo2:'',

      show3:false,
      titulo3:'',

      show4:false,
      titulo4:'',

      data3 : [{name:"Ninguno"},{name:"Pocos"}],
      showCant:false,
      tituloCant:'', 

      showCant2:false,
      tituloCant2:'', 
    };
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.toggleTable = this.toggleTable.bind(this)
  }

toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
}

async componentDidMount(){
    try{
      const respuesta = await fetch('http://localhost:8000/v1/productos/');
      const data = await respuesta.json();
      const respuesta1 = await fetch('http://localhost:8000/v1/LoteMedicamentoLista/');
      const Productos = await respuesta1.json();
      //Para los Estados Fecha Vencimiento y Cantidades
      const respuesta2 = await fetch('http://localhost:8000/v1/LoteMedicamentoLista/');
      const Productos2 = await respuesta2.json();
      this.setState({
        data,
        Productos,
        Productos2,
        show:true,
        titulo:'LISTA MEDICAMENTOS',
        nom_producto:'',
      });
    }catch(e){
      console.log(e);
    }
}

async onFormSubmit() {
    try {
      const respuesta = await fetch('http://localhost:8000/v1/LoteMedicamentoLista/');
      const Productos = await respuesta.json();

      this.setState({
        Productos,
        show: true,
        titulo:'LISTA MEDICAMENTOS',
        nom_producto:'',
        show2:false,
        show3:false,
        show4:false,
        showCant:false,
        showCant2:false,
      });
      console.log(Productos)
    } catch (e) {
      console.log(e);
    }
}

handleChange(value){
    this.state.data.map((item, indice)=>{
      if (value===item.codigo){
        /*Para mostrar la descripcion del lote_medicamento con id x */
        fetch('http://localhost:8000/v1/LoteMedicamentoBusca/'+item.codigo+'/')
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              Productos: findresponse,
              show: true,
              show2: false,
              show3:false,
              show4:false,
              showCant:false,
              showCant2:false,
              titulo:'DETALLE DEL PRODUCTO: ',
              nom_producto:item.nombre
            })
        });  
      }
      return 0
    })
    return 0;
}





handleChange2(value){
  try{
    if(value==="Vencimiento Cercano"){
      this.setState({
        show:false,
        showCant:false,
        showCant2:false,
        show4:false,
        show3:false,
        show2:true,
        titulo2:'LISTA DE PRODUCTOS CON VENCIMIENTO CERCANO',     
      })
    } 
    if(value==="Vencerá"){        
      this.setState({
        show:false,
        showCant:false,
        showCant2:false,
        show4:false,
        show2:false,
        show3:true,
        titulo3:'LISTA DE PRODUCTOS QUE VENCERAN',
      })
    }
    if(value==="Venció"){        
      this.setState({
        show:false,
        showCant:false,
        showCant2:false,
        show2:false,
        show3:false,
        show4:true,
        titulo4:'LISTA DE PRODUCTOS QUE VENCIERON',
      })
    }
  }
  catch(e){
      console.log(e);
  }  
}

handleChange3(value){
  try{
    if(value==="Ninguno"){
      this.setState({
        show:false,
        show4:false,
        show3:false,
        show2:false,
        showCant:false,
        showCant2:true,
        titulo2:'LISTA DE PRODUCTOS FALTANTES',     
      })
    } 
    if(value==="Pocos"){        
      this.setState({
        show:false,
        show4:false,
        show3:false,
        show2:false,
        showCant:true,
        showCant2:false,
        titulo3:'LISTA DE PRODUCTOS CON POCAS UNIDADES',
      })
    }
  }
  catch(e){
    console.log(e);
  }
}


toggleTable(){

  try{
    if(this.selectOptions.value==="2"){
      this.setState({
        show:false,
        show4:false,
        show3:false,
        show2:false,
        showCant:false,
        showCant2:true,
        titulo2:'LISTA DE PRODUCTOS FALTANTES',     
      })
    } 
    if(this.selectOptions.value==="1"){        
      this.setState({
        show:false,
        show4:false,
        show3:false,
        show2:false,
        showCant:true,
        showCant2:false,
        titulo3:'LISTA DE PRODUCTOS CON POCAS UNIDADES',
      })
    }
  }
  catch(e){
    console.log(e);
  }
}








render() {  
  
  return (
  
    <div className="animated fadeIn">
    <Form>
      <Row>
        {/* Buscar por codigo de medicamento */}
        <Col xs="12" md="4">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i><strong>Buscar medicamento </strong> 
            </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12">
                    <form>
                      <FormGroup row>
                        <Col md="12">
                          <Label htmlFor="codigo"><strong>Ingrese el código del medicamento</strong></Label>
                        </Col>
                        <Col xs="12" md="8">
                          <Search
                            data = {this.state.data}
                            onChange={ this.handleChange.bind(this) }
                            placeholder="Buscar..."
                            class="search-class"
                            searchKey="codigo"
                          />
                        </Col>
                        <br/>
                        <Col  md="2">
                        
                        <Button type="submit" color="success" className="mr-1"  onClick={this.onFormSubmit}>Ver Todo</Button>
                      </Col>
                      </FormGroup>
                    </form> 
                  </Col>
                
                      
                    
                </Row>
              </CardBody> 
          </Card>
        </Col>

        {/* Buscar por Estado */}
        <Col xs="12" md="4">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i><strong>Buscar por Estado </strong> 
            </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12">
                    <Form>
                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="codigo"><strong>Ingrese el Estado</strong></Label>
                        </Col>
                        <Col xs="10" md="6">
                          <Search
                            data = {this.state.data2}
                            onChange={ this.handleChange2.bind(this) }
                            placeholder="Buscar..."
                            class="search-class"
                            searchKey="name"
                          />
                        </Col>
                      </FormGroup>
                    </Form>
                  </Col>
                    <Col xs="12" md="12">               
                      <Alert color="success">
                          Vencimiento Cercano, Vencerá, Venció
                      </Alert>
                    </Col>
                </Row>
              </CardBody> 
          </Card> 
        </Col> 

        {/* Buscar por Stock */}
        {/* <Col xs="12" md="4">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i><strong>Buscar por Stock </strong> 
            </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12">
                    <Form>
                      <FormGroup row>
                        <Col md="6">
                          <Label htmlFor="codigo"><strong>Ingrese el Estado</strong></Label>
                        </Col>
                        <Col xs="10" md="6">
                          <Search
                            data = {this.state.data3}
                            onChange={ this.handleChange3.bind(this) }
                            placeholder="Buscar..."
                            class="search-class"
                            searchKey="name"
                          />
                        </Col>
                      </FormGroup>
                    </Form>
                  </Col>
                    <Col xs="12" md="12">               
                      <Alert color="success">
                          Pocas Unidades, Ninguno
                      </Alert>
                    </Col>
                </Row>


              </CardBody> 
          </Card> 
        </Col> */}


        <Col xs="12" md="4">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i><strong>Buscar por Stock </strong> 
            </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12">
                    <Form>
                      <FormGroup row>
                        <Col md="12">
                          <Label htmlFor="codigo"><strong>Ingrese el Estado</strong></Label>
                        </Col>
                        <Col  md="8">
                        <select id="options" name="options" className="form-control" ref= {selectOptions => this.selectOptions = selectOptions}>
                          <option value = "0">Seleccione...</option>
                          <option value = "1">Pocas Unidades</option>
                          <option value = "2">Ninguna</option>

                        </select>
                        </Col>
                        <Col xs="12" md="2">
                        <Button onClick={ this.toggleTable } color="primary">Aceptar</Button>
                        </Col>
                      </FormGroup>
                    </Form>
                  </Col>
                    <Col xs="12" md="12">               
                      {/* <Alert color="success">
                          Pocas Unidades, Ninguno
                      </Alert> */}
                    </Col>
                </Row>


              </CardBody> 
          </Card> 
        </Col>



                        {/* <Row>
                  
                  <Col xs="12" md="3">
                  </Col>
                  <Col xs="12" md="1">
                    <strong>Mostrar por:</strong>
                  </Col>
                  <Col xs="12" md="3">
                    <select id="options" name="options" className="form-control" ref= {selectOptions => this.selectOptions = selectOptions}>
                      <option value = "0">Seleccione...</option>
                      <option value = "1">Fecha de vencimiento cercana</option>
                      <option value = "2">Vencerá</option>
                      <option value = "3">Venció</option>
                      <option value = "4">Pocas unidades</option>
                      <option value = "5">No hay medicamento</option>
                      <option value = "6">Todo</option>
                    </select>
                  </Col>
                  <Col xs="12" md="2">
                    <Button onClick={ this.toggleTable } color="primary">Aceptar</Button>
                  </Col>
                
              </Row> */}


      </Row>                
        {this.state.show && <DatosProductos Productos={this.state.Productos} titulo={this.state.titulo+' '+this.state.nom_producto}  />}

        {this.state.show2 && <VencimientoCercano Productos={this.state.Productos2}  titulo2={this.state.titulo2}/>}
        {this.state.show3 && <Vencera Productos={this.state.Productos2}  titulo3={this.state.titulo3}/>}
        {this.state.show4 && <Vencio Productos={this.state.Productos2}  titulo4={this.state.titulo4}/> }

        {this.state.showCant && <PocasUnidades Productos={this.state.Productos2}  tituloCant={this.state.tituloCant}/> }
        {this.state.showCant2 && <Ninguna Productos={this.state.Productos2}  tituloCant2={this.state.tituloCant2}/> }
    </Form>
      
      </div>
    );
  }
}

export default Hola;










