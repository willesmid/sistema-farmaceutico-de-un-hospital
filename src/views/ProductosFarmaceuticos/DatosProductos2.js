import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, ListGroup, ListGroupItem, Row, Pagination, PaginationItem, PaginationLink, Table} from 'reactstrap';
import Validacion from './Validacion'
class DatosProductos2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeTab: 1,
            data: [],
            data2: []
        };
    }

    render(){
    const productos = this.props.Productos.map((item, indice)=>{
    
        return(
      
          <tr key={item.indice}>
            <td>{indice+1}</td>
            <td name="codigo">{item.producto_farmaceutico.nombre}</td>

            <Validacion />
    
          </tr>
        )
      })
      return (
  

        <div className="animated fadeIn">
          <Row>
              <Form onSubmit={this.props.onAddReceta}>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> {this.props.titulo}
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                    <tr>
                      <th>Numero</th>
                      <th>Codigo</th>
                      <th>Nombre</th>
                      <th>Lote</th>
                      <th>Fecha de expiracion</th>
                      <th>Cantidad</th>
                      <th>Estado del Producto</th>
                     
                    </tr>
                    </thead>
                    <tbody>
                      {productos}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            </Form>
          </Row>
          
        </div>
    
      )
    
    }
    
    
}
export default DatosProductos2;
