import React from 'react';
import ReactDOM from 'react-dom';
import EstadoProducto from './EstadoProducto';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EstadoProducto />, div);
  ReactDOM.unmountComponentAtNode(div);
});