import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,
  ListGroupItem,
} from 'reactstrap';

class EstadoProducto extends Component {
  constructor(props) {
    super(props)
    this.activitiesRefs = [];
    this.activitiesRefs2 = true;
    this.state = {
      url_medicamento :'#/Inventario/InventarioGeneral/Medicamentos/EstadoMedicamento',
      url_insumo :'#/Inventario/InventarioGeneral/Insumos/EstadoInsumo',
      url_reactivo :'#/Inventario/InventarioGeneral/Reactivos/EstadoReactivo',
    }
    this.onCancelar = this.onCancelar.bind(this)
  }

  onCancelar(){
    this.props.history.push('/Inventario/InventarioGeneral/Productos') 
  }

render() {

return (
  <div className="animated fadeIn">
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className=" 	fa fa-toggle-on"></i><strong> Habilitar - <i className="fa fa-toggle-off"></i> Inhabilitar Producto Farmaceutico</strong></h3>
        </CardHeader>
        <CardBody>
          <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <ListGroupItem action color="warning">Seleccionar un Tipo de Producto</ListGroupItem>
            </Col>
            <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <Button block color="primary" size="lg" outline href = {this.state.url_medicamento}><i className="fa fa-toggle-on"></i> Habilitar - <i className="fa fa-toggle-off"></i> Inhabilitar Medicamento</Button>
            </Col>
            <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <Button block color="warning" size="lg" outline href = {this.state.url_insumo}><i className="fa fa-toggle-on"></i> Habilitar - <i className="fa fa-toggle-off"></i> Inhabilitar Insumo</Button>
            </Col>
            <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <Button block color="secondary" size="lg" outline href = {this.state.url_reactivo}><i className="fa fa-toggle-on"></i> Habilitar - <i className="fa fa-toggle-off"></i> Inhabilitar Reactivo</Button>
          </Col>         
        </CardBody>
        <CardFooter>
          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>

        </CardFooter>
      </Card>  
    </Col>
  </Row>

 
</div>
);
}
}

export default EstadoProducto;

    


