import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,

  Row,
  ListGroupItem

} from 'reactstrap';


class NuevoProducto extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url_medicamento :'#/Inventario/InventarioGeneral/Medicamentos/NuevoMedicamento',
      url_insumo :'#/Inventario/InventarioGeneral/Insumos/NuevoInsumo',
      url_reactivo :'#/Inventario/InventarioGeneral/Reactivos/NuevoReactivo',
      
    }
    this.onCancelar = this.onCancelar.bind(this)   
  }

  onCancelar(){
    this.props.history.push('/Inventario/InventarioGeneral/Productos')
    
  }


render() {
  return (
  <div className="animated fadeIn">
  
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className="fa fa-pencil"></i> <strong>Nuevo Producto Farmaceutico</strong></h3>
        </CardHeader>
        <CardBody>
          <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <ListGroupItem action color="warning">Seleccionar un Tipo de Producto</ListGroupItem>
            </Col>
            <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <Button block color="primary" size="lg" outline href = {this.state.url_medicamento}><i className="fa fa-medkit"></i> <i className="fa fa-pencil"></i><span> Registrar Nuevo Medicamento</span></Button>
            </Col>
            <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <Button block color="warning" size="lg" outline href = {this.state.url_insumo}><i className="fa fa-medkit"></i> <i className="fa fa-pencil"></i><span> Registrar Nuevo Insumo</span></Button>
            </Col>
            <Col col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
            <Button block color="secondary" size="lg" outline href = {this.state.url_reactivo}><i className="fa fa-medkit"></i> <i className="fa fa-pencil"></i><span>Registrar Nuevo Reactivo</span></Button>
          </Col>
        </CardBody>
        <CardFooter>
          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
        </CardFooter>
      </Card>  
    </Col>

  </Row>

  </div>
  );
}
}

export default NuevoProducto;

    


