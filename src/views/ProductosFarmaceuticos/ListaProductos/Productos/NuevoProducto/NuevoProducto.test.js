import React from 'react';
import ReactDOM from 'react-dom';
import NuevoProducto from './NuevoProducto';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevoProducto />, div);
  ReactDOM.unmountComponentAtNode(div);
});