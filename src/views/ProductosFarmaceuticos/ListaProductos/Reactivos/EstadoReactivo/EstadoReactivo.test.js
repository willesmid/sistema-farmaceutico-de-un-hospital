import React from 'react';
import ReactDOM from 'react-dom';
import EstadoReactivo from './EstadoReactivo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EstadoReactivo />, div);
  ReactDOM.unmountComponentAtNode(div);
});