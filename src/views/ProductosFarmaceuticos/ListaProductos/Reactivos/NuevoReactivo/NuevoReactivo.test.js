import React from 'react';
import ReactDOM from 'react-dom';
import NuevoReactivo from './NuevoReactivo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevoReactivo />, div);
  ReactDOM.unmountComponentAtNode(div);
});