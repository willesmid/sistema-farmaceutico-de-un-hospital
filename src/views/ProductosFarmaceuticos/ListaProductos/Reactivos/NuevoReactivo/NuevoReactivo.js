import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,  
  Collapse,

} from 'reactstrap';
var urlProducto='http://localhost:8000/farmacia/almacena/v1/ProductoGuardar/'
var urlForma='http://localhost:8000/farmacia/almacena/v1/FormaFarmaceuticaGuardar/'
var urlReactivo='http://localhost:8000/farmacia/almacena/v1/ReactivoGuardar/'

const styles2 = { color: '#f33c29' }

class NuevoReactivo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showCodigo:false,
      showExiste:false,
      codigo:'',
      showNombre:false,
      nombre:'',
      showSaldo:false,
      saldo_minimo:'',
      showCant:false,
      cantidad_prescripcion:'',
      showPrimero:true,
      showSegundo:false,
      primary:false,
      showRegistra:true,
      collapse:false,
      list_forma2:[],

      primaryForma:false,
      showExisteForma:false,
      descripcion:'',
      showNuevaForma:false,
      showForma:false,

      t_forma_farmaceutica:'',
      showRegistraReactivo:true,

      showConcentracion:false,
      clasificacion:'',
      showClasificacion:false,

      primaryReactivo:false,
      showCancelarN:false,
      showCancelar:true,
      showRegistraReactivo2:true,
      opcion1:false,
      primaryReactivo2:false,
      habilitado:'',
      existeReactivo:'',
      showInsertarProducto:false,
      nombre2:'',
      saldo_minimo2:'',
      cantidad_prescripcion2:'',
      showDeshacer:true,
      
    }
    this.onValidacion = this.onValidacion.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onRegistraForma = this.onRegistraForma.bind(this)

    this.toggleForma = this.toggleForma.bind(this)
    this.recargar = this.recargar.bind(this)

    this.onValidacion2 = this.onValidacion2.bind(this)
    this.onValidacion3 = this.onValidacion3.bind(this)
    this.toggleReactivo =this.toggleReactivo.bind(this)
    this.toggleReactivo2 =this.toggleReactivo2.bind(this)
    
    this.onRegistraReactivo = this.onRegistraReactivo.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
    this.onRegistraReactivo2 = this.onRegistraReactivo2.bind(this)

    this.insertarReactivo = this.insertarReactivo.bind(this)
    this.Deshacer = this.Deshacer.bind(this)
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }

  toggleForma() {

    this.setState({
      primaryForma: !this.state.primaryForma,
      showExisteForma:false,
      showForma:false,
    })
  }
  toggleReactivo(){
    this.setState({ primaryReactivo: !this.state.primaryReactivo});
  }
  toggleReactivo2(){
    this.setState({ primaryReactivo2: !this.state.primaryReactivo2});
  }
  onCancelar(){
    this.props.history.push('/Inventario/InventarioGeneral/Reactivos')
    
  }
async componentDidMount() {

    }
onValidacion(){
  if(this.state.codigo!==''){
    fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.state.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if(findresponse.length>0){
        this.setState({
        showExiste:true,
        nombre2: findresponse[0].nombre,
        saldo_minimo2: findresponse[0].saldo_minimo,
        cantidad_prescripcion2:findresponse[0].cantidad_prescripcion,
        })
        console.log("estado" + findresponse[0].habilitado)
        if(findresponse[0].habilitado !== true){
          this.setState({
          habilitado:' - esta deshabilitado',
          })
        }else{
          this.setState({
            habilitado:'',
          })
        }

        fetch('http://localhost:8000/farmacia/almacena/v1/DetalleReactivoBusca/'+this.state.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
          if(findresponse.length>0){
            this.setState({
            showExiste:true,
            existeReactivo:'',
            })
          }else{
            this.setState({
              existeReactivo:'- No existe producto en Reactivos',
              showInsertarProducto:true,
              })
          }
        
        })
      }else{
        
        this.setState({
        showExiste:false,
        })
        if(this.state.codigo!=='' && this.state.nombre!=='' && this.state.saldo_minimo!=='' && this.state.cantidad_prescripcion!==''){
          this.setState({
          primary:true,
          })
        }     
      }
    });
  } 
  if(this.state.codigo===''){
    this.setState({
      showCodigo:true,
    })
  }else{
    this.setState({
      showCodigo:false,
    })
  }
  if(this.state.nombre===''){
    this.setState({
      showNombre:true,
    })
  }else{
    this.setState({
      showNombre:false,
    })
  }
  if(this.state.saldo_minimo===''){
    this.setState({
      showSaldo:true,
    })
  }else{
    this.setState({
      showSaldo:false,
    })
  }
  if(this.state.cantidad_prescripcion===''){
    this.setState({
      showCant:true,
    })
  }else{
    this.setState({
      showCant:false,
    })
  }
  this.setState({
    showExiste:false,
    showInsertarProducto:false,
  })
}
onFormSubmit(){
  // GUARDA PRODUCTO
  let data={
    codigo:this.state.codigo,
    nombre:this.state.nombre,
    stockGeneral:0,
    costo:0,
    saldo_minimo:this.state.saldo_minimo,
    cantidad_prescripcion:this.state.cantidad_prescripcion,
  }
  try{
    fetch(urlProducto, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }

  this.setState({
    primary:false,
    collapse:true,
    showRegistra:false,
    showPrimero:false,
    showSegundo:true,
    showCancelarN:true,
    showCancelar:false,
    showDeshacer:false,
    nombre2:'',
    saldo_minimo2:'',
    cantidad_prescripcion2:'',
  })
}
onRegistraForma(){
  
  if(this.state.descripcion!==''){
    fetch('http://localhost:8000/farmacia/almacena/v1/FormaFarmaceuticaBuscar/'+this.state.descripcion+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if(findresponse.length>0){
        this.setState({
        showExisteForma:true,
        showForma:false,
        })
      }else{
   
        if(this.state.descripcion!==''){

        let data={
          descripcion:this.state.descripcion
        }
        try{
          fetch(urlForma, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be string or {object}!
            headers:{
              'Content-Type': 'application/json',
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response));              
        } catch (e) {
          console.log(e);
        }
        this.setState({
          showExisteForma:false,
          showForma:false,
          primaryForma:false,
          descripcion:'',
        
        })
        
        }


      }
    });


  }
  if(this.state.descripcion===''){
    this.setState({
      showForma:true,
    })
  }
  this.setState({
    showExisteForma:false,
   
    
  })


    // GUARDA FORMA FARMACEUTICA
  }

recargar(){

}
onValidacion2(){


  if(this.state.clasificacion===''){
    this.setState({
      showClasificacion:true
    })
    
  }else{
    this.setState({
      showClasificacion:false
    })
  }
  if(this.state.clasificacion!==''){
    this.setState({
      primaryReactivo:true,
    })
  }
}

onValidacion3(){

  if(this.state.clasificacion===''){
    this.setState({
      showClasificacion:true
    })
    
  }else{
    this.setState({
      showClasificacion:false
    })
  }
  if(this.state.clasificacion!==''){
    this.setState({
      primaryReactivo2:true,
    })
  }

}

onRegistraReactivo(){
    let data={
    producto_farmaceutico:this.state.codigo,
    concentracion:this.state.concentracion,
    clasificacion:this.state.clasificacion,
    t_forma_farmaceutica:this.state.t_forma_farmaceutica,
  }
  try{
    fetch(urlReactivo, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        console.log('Success:', response)
        window.location.reload()
    });              
  } catch (e) {
    console.log(e);
  }
  
  this.setState({
    primaryReactivo:false,
  })
  
}

onRegistraReactivo2(){
  let data={
  producto_farmaceutico:this.state.codigo,
  concentracion:this.state.concentracion,
  clasificacion:this.state.clasificacion,
  t_forma_farmaceutica:this.state.t_forma_farmaceutica,
}
try{
  fetch(urlReactivo, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(data), // data can be string or {object}!
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
  }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)

      this.props.history.push('/Inventario/InventarioGeneral/Reactivos')
      window.location.reload()

  });              
} catch (e) {
  console.log(e);
}
this.setState({
  primaryReactivo2:false,
})

}

insertarReactivo(){
  this.setState({
    collapse:true,
    showSegundo:true,
    showPrimero:false,
    showRegistra:false,
    showDeshacer:true,
    nombre:'',
    saldo_minimo:'',
    cantidad_prescripcion:'',
    showForma:false,
    t_forma_farmaceutica:'',
    showConcentracion:false,
    showClasificacion:false,
    clasificacion:'',

  })
}


Deshacer(){
  this.setState({
    collapse:false,
    showSegundo:false,
    showPrimero:true,
    showRegistra:true,
    showDeshacer:false,
    codigo:'',
    showCodigo:false,
    nombre:'',
    showNombre:false,
    saldo_minimo:'',
    showSaldo:false,
    cantidad_prescripcion:'',
    showCant:false,
    showExiste:false,
    showInsertarProducto:false,
    clasificacion:'',
    

  })
  window.location.reload()

}

render() {
  return (
  <div className="animated fadeIn">
  
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className="fa fa-pencil"></i> <strong>Nuevo Reactivo</strong></h3>
        </CardHeader>
        <CardBody>
          
          <Form >
          {this.state.showPrimero &&
          <Row>
          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Codigo</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="username3" name="username3" maxLength="50" required onChange={e => this.setState({ codigo: e.target.value })}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-hashtag"></i></InputGroupText>
                  {this.state.showInsertarProducto &&
                  <Button type="button" color="warning" onClick={this.insertarReactivo}>Añadir a Reactivos</Button>
                  }
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showCodigo &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba codigo de producto</FormText>
              }
              {
              this.state.showExiste &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>{"Existe el Codigo" + this.state.habilitado + " " + this.state.existeReactivo}</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Nombre</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" autoComplete="username" maxLength="100" onChange={e => this.setState({ nombre: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showNombre &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Nombre de producto</FormText>
              }
            </FormGroup>
          </Col>

          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Saldo Minimo</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number2" name="numer2" onChange={e => this.setState({ saldo_minimo: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className=" 	fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showSaldo &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Saldo Minimo</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Cantidad Maxima de dispensación</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number" name="number" onChange={e => this.setState({ cantidad_prescripcion: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showCant &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Cantidad Maxima de dispensación</FormText>
              }
            </FormGroup>  
            </Col>
          </Row>
          }
          {this.state.showSegundo &&
          <Row>
          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Codigo</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="username3" name="username3" maxLength="50"  value={this.state.codigo} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-hashtag"></i></InputGroupText>
                  {this.state.showDeshacer &&
                  <Button type="button" color="warning" onClick={this.Deshacer}>Deshacer</Button>
                  }
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Nombre</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" autoComplete="username" maxLength="100" value={this.state.nombre + this.state.nombre2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
          </Col>

          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Saldo Minimo</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number2" name="numer2" value={this.state.saldo_minimo + this.state.saldo_minimo2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className=" 	fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Cantidad Maxima de dispensación</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number" name="number" value={this.state.cantidad_prescripcion + this.state.cantidad_prescripcion2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>  
            </Col>
          </Row>
          }
           <h3 align="right">
            {
            this.state.showRegistra &&
            <Button  size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion}><i className="fa fa-edit"></i><span>Registrar Producto</span></Button>
            } 
          </h3>
          <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Row>
            <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Clasificación</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="clasificacion" name="clasificacion" onChange={e => this.setState({ clasificacion: e.target.value })} required/>   
                <InputGroupAddon addonType="append"> 
                <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showClasificacion &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Clasificación</FormText>
              }
            </FormGroup> 
            </Col>
          
            <Col xs="6">

            </Col>
          </Row>
          <h3 align="right">
          {
            this.state.showRegistraReactivo &&
            <Button  size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion2}><i className="fa fa-edit"></i><span>Registrar Reactivo y añadir otro</span></Button>
          }
          {
            this.state.showRegistraReactivo2 &&
            <Button size="sm" className="btn-facebook btn-brand mr-1 mb-1" onClick={this.onValidacion3}><i className="fa fa-edit"></i><span>Registrar Reactivo</span></Button>
          }
          </h3>   
          </Collapse>
          </Form>

        </CardBody>
        <CardFooter>
          {this.state.showCancelar &&
          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
          }
          {this.state.showCancelarN &&
          <h3 align="right">
          <Button type="reset" size="sm" color="danger" disabled><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
          }
        </CardFooter>
      </Card>  
    </Col>
  </Row>

{/* MODAL REGISTRAR PRODUCTO  */}
  <Form>
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
    <form onSubmit={this.props.onAddEntrega}>
    <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
      <ModalFooter>           
        <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO PRODUCTO */}

  {/* MODAL REGISTRA FORMA FARMACEUTICA */}
  <Form>
  <Modal isOpen={this.state.primaryForma }  className={'modal-success ' + this.props.className}>
    <ModalHeader toggle={this.toggleForma}>Registra Forma Farmaceutica</ModalHeader>
      <ModalBody>
      <center>
        <strong>¿Desea Registrar nueva Forma Farmaceutica?</strong>
      </center>
        <br/>
          <Form className="was-validated">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Descripción</InputGroupText>
                </InputGroupAddon>
                <Input type="text" className="form-control-success" id="inputSuccess2i" onChange={e => this.setState({ descripcion: e.target.value } )}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-medkit"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showExisteForma &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Existe Forma Farmaceutica</FormText>
              }
              {
              this.state.showForma &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Forma Farmaceutica</FormText>
              }
            </FormGroup>
          </Form>
      </ModalBody>
    <ModalFooter>

      <Button color="success" onClick={this.onRegistraForma}>Si</Button>{' '}
      <Button color="secondary" onClick={this.toggleForma}>No</Button>
    </ModalFooter>
  </Modal>
  </Form>
  {/* FIN MODAL REGISTRA FORMA FARMACEUTICA */}

  {/* MODAL REGISTRAR MEDICAMENTO */}
  <Form>
  <Modal isOpen={this.state.primaryReactivo} toggle={this.toggleReactivo} className={'modal-primary ' + this.props.className}>
    <form >
    <ModalHeader toggle={this.toggleReactivo}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos de Reactivo y Registrar otro?
      </ModalBody>
      <ModalFooter>    

        <Button type="submit" onClick={this.onRegistraReactivo}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.toggleReactivo}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO MEDICAMENTO */}

 {/* MODAL REGISTRAR MEDICAMENTO 2*/}
 <Form>
  <Modal isOpen={this.state.primaryReactivo2} toggle={this.toggleReactivo2} className={'modal-primary ' + this.props.className}>
    <form >
    <ModalHeader toggle={this.toggleReactivo2}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos de Reactivo y Salir?
      </ModalBody>
      <ModalFooter>    

        <Button type="submit" onClick={this.onRegistraReactivo2}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.toggleReactivo2}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO MEDICAMENTO 2*/}
    </div>
  );
}
}

export default NuevoReactivo;

    


