import React from 'react';
import ReactDOM from 'react-dom';
import NuevoInsumo from './NuevoInsumo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevoInsumo />, div);
  ReactDOM.unmountComponentAtNode(div);
});