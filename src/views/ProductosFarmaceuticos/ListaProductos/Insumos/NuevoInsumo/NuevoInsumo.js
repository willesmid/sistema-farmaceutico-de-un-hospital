import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,  
  Collapse,

} from 'reactstrap';
var urlProducto='http://localhost:8000/farmacia/almacena/v1/ProductoGuardar/'
var urlPres='http://localhost:8000/farmacia/almacena/v1/PresentacionGuardar/'

var urlUnidad='http://localhost:8000/farmacia/almacena/v1/UnidadGuardar/'
var urlInsumo='http://localhost:8000/farmacia/almacena/v1/InsumoGuardar/'

const styles2 = { color: '#f33c29' }

class NuevoInsumo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showCodigo:false,
      showExiste:false,
      codigo:'',
      showNombre:false,
      nombre:'',
      showSaldo:false,
      saldo_minimo:'',
      showCant:false,
      cantidad_prescripcion:'',
      showPrimero:true,
      showSegundo:false,
      primary:false,
      showRegistra:true,
      collapse:false,
      list_pres:[],
    

      primaryForma:false,
      showExistePres:false,
      descripcion:'',
      showNuevaForma:false,
      showPresentacion:false,

      t_presentacion:'',
      show_presentacion:false, 
      showRegistraMedicamento:true,
      t_unidad:'',
      showConcentracion:false,
      clasificacion:'',
      showClasificacion:false,

      primaryMedicamento:false,
      showCancelarN:false,
      showCancelar:true,
      showRegistraMedicamento2:true,
      opcion1:false,
      primaryMedicamento2:false,
      habilitado:'',
      show_unidad:false,
      list_unidad:[],

      primaryUnidad:false,
      descripcionUnidad:'',
      existeInsumo:'',
      showInsertarProducto:false,
      showDeshacer:false,
      codigo2:'',
      nombre2: '',
      saldo_minimo2: '',
      cantidad_prescripcion2:'',

    }
    this.onValidacion = this.onValidacion.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onRegistraPres = this.onRegistraPres.bind(this)
    

    this.toggleForma = this.toggleForma.bind(this)
    this.recargar = this.recargar.bind(this)

    this.onValidacion2 = this.onValidacion2.bind(this)
    this.onValidacion3 = this.onValidacion3.bind(this)
    this.toggleMedicamento =this.toggleMedicamento.bind(this)
    this.toggleMedicamento2 =this.toggleMedicamento2.bind(this)
    
    this.onRegistraInsumo = this.onRegistraInsumo.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
    this.onRegistraMedicamento2 = this.onRegistraMedicamento2.bind(this)

    this.toggleUnidad = this.toggleUnidad.bind(this)
    this.recargar2 = this.recargar2.bind(this)
    this.onRegistraUnidad = this.onRegistraUnidad.bind(this)
    
    this.insertarInsumo = this.insertarInsumo.bind(this)
    this.Deshacer = this.Deshacer.bind(this)

  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }

  toggleForma() {

    this.setState({
      primaryForma: !this.state.primaryForma,
      showExistePres:false,
      showPresentacion:false,
      descripcion:'',
    })
  }

  toggleUnidad() {

    this.setState({
      primaryUnidad: !this.state.primaryUnidad,
      showExisteUnidad:false,
      showUnidad:false,
      descripcionUnidad:'',
    })
  }
  toggleMedicamento(){
    this.setState({ primaryMedicamento: !this.state.primaryMedicamento});
  }
  toggleMedicamento2(){
    this.setState({ primaryMedicamento2: !this.state.primaryMedicamento2});
  }
  onCancelar(){
    this.props.history.push('/Inventario/InventarioGeneral/Insumos')
    
  }
async componentDidMount() {
  fetch('http://localhost:8000/farmacia/almacena/v1/PresentacionLista/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    this.setState({
      list_pres: findresponse,
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/UnidadLista/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    this.setState({
      list_unidad: findresponse,
    })
  });


}

onValidacion(){
  if(this.state.codigo!==''){
    fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.state.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if(findresponse.length>0){
        this.setState({
        showExiste:true,
        nombre2: findresponse[0].nombre,
        saldo_minimo2: findresponse[0].saldo_minimo,
        cantidad_prescripcion2:findresponse[0].cantidad_prescripcion,

        })
        console.log("estado" + findresponse[0].habilitado)
        if(findresponse[0].habilitado !== true){
          this.setState({
          habilitado:' - esta deshabilitado',
          })
        }else{
          this.setState({
            habilitado:'',
          })
        }

        fetch('http://localhost:8000/farmacia/almacena/v1/DetalleInsumoBusca/'+this.state.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
          if(findresponse.length>0){
            this.setState({
            showExiste:true,
            existeInsumo:'',
            })
          }else{
            this.setState({
              existeInsumo:'- No existe producto en Insumos',
              showInsertarProducto:true,
              })
          }
        
        })
      }else{
        
        this.setState({
        showExiste:false,
        
        })
        if(this.state.codigo!=='' && this.state.nombre!=='' && this.state.saldo_minimo!=='' && this.state.cantidad_prescripcion!==''){
          this.setState({
          primary:true,

          })
        }     
      }
    });
  } 



  if(this.state.codigo===''){
    this.setState({
      showCodigo:true,
    })
  }else{
    this.setState({
      showCodigo:false,
    })
  }
  if(this.state.nombre===''){
    this.setState({
      showNombre:true,
    })
  }else{
    this.setState({
      showNombre:false,
    })
  }
  if(this.state.saldo_minimo===''){
    this.setState({
      showSaldo:true,
    })
  }else{
    this.setState({
      showSaldo:false,
    })
  }
  if(this.state.cantidad_prescripcion===''){
    this.setState({
      showCant:true,
    })
  }else{
    this.setState({
      showCant:false,
    })
  }

  this.setState({
    showExiste:false,
    showInsertarProducto:false,

})

}
onFormSubmit(){
  // GUARDA PRODUCTO
  let data={
    codigo:this.state.codigo,
    nombre:this.state.nombre,
    stockGeneral:0,
    costo:0,
    saldo_minimo:this.state.saldo_minimo,
    cantidad_prescripcion:this.state.cantidad_prescripcion,
  }
  try{
    fetch(urlProducto, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }

  this.setState({
    primary:false,
    collapse:true,
    showRegistra:false,
    showPrimero:false,
    showSegundo:true,
    showCancelarN:true,
    showCancelar:false,
    showDeshacer:false,
    showInsertarProducto:false,
    nombre2:'',
    saldo_minimo2:'',
    cantidad_prescripcion2:'',
  })
}
onRegistraPres(){
  var b=false
  if(this.state.descripcion!==''){
    try{
    fetch('http://localhost:8000/farmacia/almacena/v1/PresentacionLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {

      findresponse.map((item)=>{
        if(item.descripcion===this.state.descripcion){
        b = true
        }
        return null
        })

      
      if(b){
        this.setState({
        showExistePres:true,
        showPresentacion:false,
        })
      }else{
        if(this.state.descripcion!==''){

        let data={
          descripcion:this.state.descripcion
        }
        try{
          fetch(urlPres, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be string or {object}!
            headers:{
              'Content-Type': 'application/json',
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response));              
        } catch (e) {
          console.log(e);
        }
        this.setState({
          showExistePres:false,
          showPresentacion:false,
          primaryForma:false,
          descripcion:'',
        })
        }
      }
    }
    
    ); 
  }
   
  catch (e) {
    console.log(e+"no entiendo");
  }
  }

  if(this.state.descripcion===''){
    this.setState({
      showPresentacion:true,
    })
  }
  this.setState({
    showExistePres:false,
   
    
  })


    // GUARDA FORMA FARMACEUTICA
  }

onRegistraUnidad(){
  var b=false
  if(this.state.descripcionUnidad!==''){
    try{
    fetch('http://localhost:8000/farmacia/almacena/v1/UnidadLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {

      findresponse.map((item)=>{
        if(item.descripcion===this.state.descripcionUnidad){
        b = true
        }
        return null
      })


      if(b){
        this.setState({
        showExisteUnidad:true,
        showUnidad:false,
        })
      }else{
        if(this.state.descripcionUnidad!==''){

        let data={
          descripcion:this.state.descripcionUnidad
        }
        try{
          fetch(urlUnidad, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be string or {object}!
            headers:{
              'Content-Type': 'application/json',
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response));              
        } catch (e) {
          console.log(e);
        }
        this.setState({
          showExisteUnidad:false,
          showUnidad:false,
          primaryUnidad:false,
          descripcionUnidad:'',
        })
        }
      }
    });
  } 
  catch (e) {
    console.log(e+"no entiendo");
  }
  }

  if(this.state.descripcionUnidad===''){
    this.setState({
      showUnidad:true,
    })
  }
  this.setState({
    showExisteUnidad:false,
    
  })
    // GUARDA UNIDAD
  }

recargar(){
  fetch('http://localhost:8000/farmacia/almacena/v1/PresentacionLista/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
   //   console.log("FINDRESPONSE=",findresponse)
    this.setState({
      list_pres: findresponse,

    })
  })

}
recargar2(){
  fetch('http://localhost:8000/farmacia/almacena/v1/UnidadLista/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
   //   console.log("FINDRESPONSE=",findresponse)
    this.setState({
      list_unidad: findresponse,

    })
  })

}
onValidacion2(){
  if(this.state.t_presentacion===''){
    this.setState({
      show_presentacion:true
    })
    
  }else{
    this.setState({
      show_presentacion:false
    })
  }
  if(this.state.t_unidad===''){
    this.setState({
      show_unidad:true
    })
    
  }else{
    this.setState({
      show_unidad:false
    })
  }

  if(this.state.t_presentacion!=='' && this.state.t_unidad!==''){
    this.setState({
      primaryMedicamento:true,
    })
  }
}

onValidacion3(){
  if(this.state.t_presentacion===''){
    this.setState({
      show_presentacion:true
    })
    
  }else{
    this.setState({
      show_presentacion:false
    })
  }
  if(this.state.t_unidad===''){
    this.setState({
      show_unidad:true
    })
    
  }else{
    this.setState({
      show_unidad:false
    })
  }

  if(this.state.t_presentacion!=='' && this.state.t_unidad!==''){
    this.setState({
      primaryMedicamento2:true,
    })
  }

}

onRegistraInsumo(){
  let data={
    producto_farmaceutico:this.state.codigo,
    t_presentacion:this.state.t_presentacion,
    t_unidad:this.state.t_unidad,
  }
  try{
    fetch(urlInsumo, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        console.log('Success:', response)
        window.location.reload()
    });              
  } catch (e) {
    console.log(e);
  }
  
  this.setState({
    primaryMedicamento:false,
  })
  
}

onRegistraMedicamento2(){
  let data={
  producto_farmaceutico:this.state.codigo,
  t_presentacion:this.state.t_presentacion,
  t_unidad:this.state.t_unidad,
}
try{
  fetch(urlInsumo, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(data), // data can be string or {object}!
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
  }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)

      this.props.history.push('/Inventario/InventarioGeneral/Insumos')
      window.location.reload()

  });              
} catch (e) {
  console.log(e);
}
this.setState({
  primaryMedicamento2:false,
})

}

insertarInsumo(){
  this.setState({
    collapse:true,
    showSegundo:true,
    showPrimero:false,
    showRegistra:false,
    showDeshacer:true,
    nombre:'',
    saldo_minimo:'',
    cantidad_prescripcion:'',
  })
}

Deshacer(){
  this.setState({
    collapse:false,
    showSegundo:false,
    showPrimero:true,
    showRegistra:true,
    showDeshacer:false,
    codigo:'',
    showCodigo:false,
    nombre:'',
    showNombre:false,
    saldo_minimo:'',
    showSaldo:false,
    cantidad_prescripcion:'',
    showCant:false,
    showExiste:false,
    showInsertarProducto:false,

  })
  window.location.reload()

}
render() {
  return (
  <div className="animated fadeIn">
  
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className="fa fa-pencil"></i> <strong>Nuevo Insumo</strong></h3>
        </CardHeader>
        <CardBody>
          
          <Form >
          {this.state.showPrimero &&
          <Row>
          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Codigo</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="username3" name="username3" maxLength="50" required onChange={e => this.setState({ codigo: e.target.value })}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-hashtag"></i></InputGroupText>
                  {this.state.showInsertarProducto &&
                  <Button type="button" color="warning" onClick={this.insertarInsumo}>Añadir a Insumos</Button>
                  }
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showCodigo &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba codigo de producto</FormText>
              }
              {
              this.state.showExiste &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>{"Existe el Codigo" + this.state.habilitado + " " + this.state.existeInsumo}</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Nombre</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" autoComplete="username" maxLength="100" onChange={e => this.setState({ nombre: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showNombre &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Nombre de producto</FormText>
              }
            </FormGroup>
          </Col>

          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Saldo Minimo</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number2" name="numer2" onChange={e => this.setState({ saldo_minimo: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className=" 	fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showSaldo &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Saldo Minimo</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Cantidad Maxima de dispensación</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number" name="number" onChange={e => this.setState({ cantidad_prescripcion: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showCant &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Cantidad Maxima de dispensación</FormText>
              }
            </FormGroup>  
            </Col>
          </Row>
          }
          {this.state.showSegundo &&
          <Row>
          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Codigo</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="username3" name="username3" maxLength="50"  value={this.state.codigo} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-hashtag"></i></InputGroupText>
                </InputGroupAddon>
                {this.state.showDeshacer &&
                  <Button type="button" color="warning" onClick={this.Deshacer}>Deshacer</Button>
                }
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Nombre</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" autoComplete="username" maxLength="100" value={this.state.nombre+this.state.nombre2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
          </Col>

          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Saldo Minimo</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number2" name="numer2" value={this.state.saldo_minimo+this.state.saldo_minimo2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className=" 	fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Cantidad Maxima de dispensación</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number" name="number" value={this.state.cantidad_prescripcion+this.state.cantidad_prescripcion2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>  
            </Col>
          </Row>
          }
           <h3 align="right">
            {
            this.state.showRegistra &&
            <Button  size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion}><i className="fa fa-edit"></i><span>Registrar Producto</span></Button>
            } 
          </h3>
          <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Row>
            <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Presentación</InputGroupText>
                </InputGroupAddon>
                <Input type="select" name="medico" id="select" required
                  value={this.state.t_presentacion}
                  onChange={e => this.setState({ t_presentacion: e.target.value })}>                                         
                  <option value="">presentacion...</option>
                    {this.state.list_pres.map(item1 => (
                      <option value={item1.id}>{item1.descripcion}</option>
                    ))}
                </Input>    
                <InputGroupAddon addonType="append">
                <Button type="button" color="success" onClick={this.toggleForma}>+</Button>
                <Button type="button" color="light" onClick={this.recargar}><i className="fa fa-refresh"></i></Button>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.show_presentacion &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Seleccione Presentación</FormText>
              }
            </FormGroup>
            </Col>
          
            <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Unidad de Manejo</InputGroupText>
                </InputGroupAddon>
                <Input type="select" name="medico" id="select" required
                  value={this.state.t_unidad}
                  onChange={e => this.setState({ t_unidad: e.target.value })}>                                         
                  <option value="">presentacion...</option>
                    {this.state.list_unidad.map(item1 => (
                      <option value={item1.id}>{item1.descripcion}</option>
                    ))}
                </Input>    
                <InputGroupAddon addonType="append">
                <Button type="button" color="success" onClick={this.toggleUnidad}>+</Button>
                <Button type="button" color="light" onClick={this.recargar2}><i className="fa fa-refresh"></i></Button>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.show_unidad &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Seleccione Unidad de Manejo</FormText>
              }
            </FormGroup>  
            </Col>
          </Row>
          <h3 align="right">
          {
            this.state.showRegistraMedicamento &&
            <Button  size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion2}><i className="fa fa-edit"></i><span>Registrar Insumo y añadir otro</span></Button>
            }
      
        {
          this.state.showRegistraMedicamento2 &&
        <Button size="sm" className="btn-facebook btn-brand mr-1 mb-1" onClick={this.onValidacion3}><i className="fa fa-edit"></i><span>Registrar Insumo</span></Button>
        }
        </h3>
          </Collapse>
          </Form>

        </CardBody>
        <CardFooter>

          {this.state.showCancelar &&

          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
          }
          {this.state.showCancelarN &&
          <h3 align="right">
          <Button type="reset" size="sm" color="danger" disabled><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
          }
        </CardFooter>
      </Card>  
    </Col>

  </Row>


{/* MODAL REGISTRAR PRODUCTO  */}
  <Form>
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
    <form onSubmit={this.props.onAddEntrega}>
    <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
      <ModalFooter>           
        <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO PRODUCTO */}

  {/* MODAL REGISTRA PRESENTACION */}
  <Form>
  <Modal isOpen={this.state.primaryForma }  className={'modal-success ' + this.props.className}>
    <ModalHeader toggle={this.toggleForma}>Registra Presentación</ModalHeader>
      <ModalBody>
      <center>
        <strong>¿Desea Registrar nueva Presentación?</strong>
      </center>
        <br/>
          <Form className="was-validated">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Descripción</InputGroupText>
                </InputGroupAddon>
                <Input type="text" className="form-control-success" id="inputSuccess2i" maxlength="30" onChange={e => this.setState({ descripcion: e.target.value } )}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-medkit"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showExistePres &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Existe Presentación</FormText>
              }
              {
              this.state.showPresentacion &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Presentación</FormText>
              }
            </FormGroup>
          </Form>
      </ModalBody>
    <ModalFooter>

      <Button color="success" onClick={this.onRegistraPres}>Si</Button>{' '}
      <Button color="secondary" onClick={this.toggleForma}>No</Button>
    </ModalFooter>
  </Modal>
  </Form>
  {/* FIN MODAL REGISTRA PRESENTACIÓN */}

  {/* MODAL REGISTRA UNIDAD DE MANEJO */}
  <Form>
  <Modal isOpen={this.state.primaryUnidad}  className={'modal-success ' + this.props.className}>
    <ModalHeader toggle={this.toggleUnidad}>Registra Unidad de Manejo</ModalHeader>
      <ModalBody>
      <center>
        <strong>¿Desea Registrar nueva Unidad de Manejo?</strong>
      </center>
        <br/>
          <Form className="was-validated">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Descripción</InputGroupText>
                </InputGroupAddon>
                <Input type="text" className="form-control-success" id="inputSuccess2i" maxlength="30" onChange={e => this.setState({ descripcionUnidad: e.target.value } )}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-medkit"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showExisteUnidad &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Existe Unidad de Manejo</FormText>
              }
              {
              this.state.showUnidad &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Unidad de Manejo</FormText>
              }
            </FormGroup>
          </Form>
      </ModalBody>
    <ModalFooter>

      <Button color="success" onClick={this.onRegistraUnidad}>Si</Button>{' '}
      <Button color="secondary" onClick={this.toggleUnidad}>No</Button>
    </ModalFooter>
  </Modal>
  </Form>
  {/* FIN MODAL REGISTRA UNIDAD DE MANEJO */}


  {/* MODAL REGISTRAR MEDICAMENTO */}
  <Form>
  <Modal isOpen={this.state.primaryMedicamento} toggle={this.toggleMedicamento} className={'modal-primary ' + this.props.className}>
    <form >
    <ModalHeader toggle={this.toggleMedicamento}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos de Insumo y Registrar otro?
      </ModalBody>
      <ModalFooter>    

        <Button type="submit" onClick={this.onRegistraInsumo}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.toggleMedicamento}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO MEDICAMENTO */}

 {/* MODAL REGISTRAR MEDICAMENTO 2*/}
 <Form>
  <Modal isOpen={this.state.primaryMedicamento2} toggle={this.toggleMedicamento2} className={'modal-primary ' + this.props.className}>
    <form >
    <ModalHeader toggle={this.toggleMedicamento2}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos de Insumo y Salir?
      </ModalBody>
      <ModalFooter>    

        <Button type="submit" onClick={this.onRegistraMedicamento2}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.toggleMedicamento2}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO MEDICAMENTO 2*/}
    </div>
  );
}
}

export default NuevoInsumo;

    


