import React from 'react';
import ReactDOM from 'react-dom';
import EstadoInsumo from './EstadoInsumo';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EstadoInsumo />, div);
  ReactDOM.unmountComponentAtNode(div);
});