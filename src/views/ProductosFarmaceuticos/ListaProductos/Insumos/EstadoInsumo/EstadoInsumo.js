import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,
  Input,
  FormGroup, 
  InputGroup,
  InputGroupAddon,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,

} from 'reactstrap';
import ValidarEstado from './ValidarEstado'
const styles2 = { color: 'white', backgroundColor: 'white' }

class EstadoInsumo extends Component {
  constructor(props) {
    super(props)
    this.activitiesRefs = [];
    this.activitiesRefs2 = true;
    this.state = {
      showCancelar:true,
      listapre: [],
      valor: '',
      checked2:false,
      checked:false,
      lista:'',
      showA:false,
      showB:false,
      showAD:true,
      showBD:true,
      medicamentos:[],
      primary:false,
      primary2:false,
      
    }
    this.onCancelar = this.onCancelar.bind(this)
    this.buscarValor = this.buscarValor.bind(this) 
  
    this.Habilitar = this.Habilitar.bind(this)
    this.Deshabilitar = this.Deshabilitar.bind(this)
    this.Listar=this.Listar.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
  }
  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }

  togglePrimary2() {
    this.setState({ primary2: !this.state.primary2});
  }
  

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/InsumosTodo/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const listapre = await respuesta.json();
      this.setState({
        listapre
      });
    } catch (e) {
      console.log(e);
    }
  }

  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  onCancelar(){
    this.props.history.push('/Inventario/InventarioGeneral/Insumos') 
  }

onMarcarA(){
  var check = this.activitiesRefs2.checked
  console.log("por que no dara " + this.activitiesRefs2.checked)
  console.log("por que no daraasds " + check)
  if(this.activitiesRefs2.checked){
    
    for (let i = 0; i < this.activitiesRefs.length; i++) {
      if(this.activitiesRefs[i]!==null){
      this.activitiesRefs[i].checked=true
      console.log("y que es" + check)
    }else{
      
    }
  }
  
  }else{

    for (let i = 0; i < this.activitiesRefs.length; i++) {
      if(this.activitiesRefs[i]!==null){
      this.activitiesRefs[i].checked=false
      console.log("y que es" + check)
    }
    }
  }


}
onCheck(name, val) {
  const checkboxes = Object.assign({},this.state.checkboxes, {});
  for (let key in checkboxes) {
    checkboxes[key] = false;
  }
  checkboxes[name] = true;
  checkboxes.selected = val;
  this.setState({ checkboxes });
}

Habilitar(){
  var productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
    if (check) productos = productos.concat([this.activitiesRefs[i].value])
    }
  }
  this.setState({
    medicamentos: this.state.medicamentos.concat([productos]),
  }); 
  for (let i = 0; i < productos.length; i++) {
  let url = ''
  let data = {}
  url = 'http://localhost:8000/farmacia/almacena/v1/EditaProducto/'+productos[i]+'/'
  data = {
    habilitado:true
  }
  try {
    fetch(url, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)
      if(i===productos.length-1){
        window.location.reload()
      }   
  }); 
  } catch (e) {
      console.log(e);
  } 
  }  
  this.setState({
    primary:false,
  })
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    this.activitiesRefs[i].checked=false
    console.log("y que es" + check)
  }
}
this.activitiesRefs2.checked=false
}

Deshabilitar(){
  var productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
    if (check) productos = productos.concat([this.activitiesRefs[i].value])
    }
  }
  this.setState({
    medicamentos: this.state.medicamentos.concat([productos]),
  }); 
  for (let i = 0; i < productos.length; i++) {
  let url = ''
  let data = {}
  url = 'http://localhost:8000/farmacia/almacena/v1/EditaProducto/'+productos[i]+'/'
  data = {
    habilitado:false
  }
  try {
    fetch(url, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)
      if(i===productos.length-1){
        window.location.reload()
      }   
  }); 
  } catch (e) {
      console.log(e);
  } 
  }  
  this.setState({
    primary2:false,
  })
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    this.activitiesRefs[i].checked=false
    console.log("y que es" + check)
  }
}
this.activitiesRefs2.checked=false
  
}

Listar(){
  if(this.state.lista==="habilitados"){
    fetch('http://localhost:8000/farmacia/almacena/v1/InsumoHospitalario/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
     //   console.log("FINDRESPONSE=",findresponse)
      this.setState({
        listapre: findresponse,
        showA:false,
        showAD:true,
        showB:true,
        showBD:false,
      })
    })

  }else{
    if(this.state.lista==="inhabilitados"){
    fetch('http://localhost:8000/farmacia/almacena/v1/InsumosInhabilitados/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
     //   console.log("FINDRESPONSE=",findresponse)
      this.setState({
        listapre: findresponse,
        showA:true,
        showAD:false,
        showB:false,
        showBD: true,
        
      })
    })
  }else{                                
      fetch('http://localhost:8000/farmacia/almacena/v1/InsumosTodo/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
       //   console.log("FINDRESPONSE=",findresponse)
        this.setState({
          listapre: findresponse,
          showA:false,
          showAD:true,
          showB:false,
          showBD: true,
        })
      })
    
  }
  }

}

render() {
  let items_filtrados = this.state.listapre.filter(
    (item) => {
      return (item.producto_farmaceutico.codigo + ' ' + item.producto_farmaceutico.nombre + ' ' + item.lista_costo)
        .toUpperCase().indexOf(
          this.state.valor.toUpperCase()) !== -1;
    }
  );
  
  var productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
    if (check) productos = productos.concat([this.activitiesRefs[i].value])
    }
  }

return (
  <div className="animated fadeIn">
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className=" 	fa fa-toggle-on"></i><strong> Habilitar - <i className="fa fa-toggle-off"></i> Inhabilitar Insumo</strong></h3>
        </CardHeader>
        <CardBody>         
          <Col xs="12" md="12">
            <FormGroup row>
              <Col xs="12" md="4">
                <InputGroup>
                <InputGroupAddon addonType="prepend">
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" placeholder="Ingrese Codigo o Nombre" value={this.state.valor} onChange={this.buscarValor}/>
                <InputGroupAddon addonType="append">
                <Button type="button" color="primary" disabled><i className="fa fa-search"></i></Button>
                </InputGroupAddon>
                </InputGroup>
              </Col>
              <Col xs="12" md="4">
              <Input 
              type="select" 
              name="unidadProd" 
              id="unidadProd" 
              value={this.state.lista}
             
              onChange={e => { this.setState({ lista: e.target.value }) }}
              onClick={this.Listar}
              >
              <option value="" required>Estado de Medicamento...</option>
                <option value="habilitados">HABILITADOS</option>
                <option value="inhabilitados">INHABILITADOS</option>
     
            </Input> 
              </Col>
              <Col xs="12" md="2">
              {this.state.showAD &&
              <h3>
              <Button size="sm"  className="btn-spotify btn-brand mr-1 mb-1" disabled><i className="fa fa-pencil"></i><span> Habilitar </span></Button>
              </h3> }
              {this.state.showA &&
              <h3>
              <Button onClick={this.togglePrimary} size="sm" className="btn-spotify btn-brand mr-1 mb-1"><i className="fa fa-pencil"></i><span> Habilitar </span></Button>
              </h3> 
              }
              </Col>
              <Col xs="12" md="2">
              {this.state.showBD &&
              <h3>
              <Button size="sm" className="btn-youtube btn-brand mr-1 mb-1" disabled><i className="fa fa-trash-o"></i><span>Deshabilitar </span></Button>                 
              </h3>
              }
              {this.state.showB &&
              <h3>
              <Button onClick={this.togglePrimary2} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Deshabilitar </span></Button>                 
              </h3>
              }
              </Col>
         
            </FormGroup>
          </Col>
         
            <Table hover responsive size="sm">
              <thead>
                <tr>
                  <th>Nro.</th>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Presentación</th>
                  <th>Unidad de Manejo</th>
                  <th>StockGeneral</th>
                  <th>Habilitado</th>
                  <th style={styles2}>    
                  <center>
                  <input className="form-check-input" type="checkbox" value={0} ref={ref => this.activitiesRefs2 = ref} onClick={this.onMarcarA.bind(this)} />                         
                  .
                  </center>
                  </th>
                </tr>
              </thead>
              <tbody>
                {items_filtrados.map((item, indice) => (
                
                <tr key={indice}>
                  <td>{indice + 1}</td>
                  <td>{item.producto_farmaceutico.codigo}</td>
                  <td>{item.producto_farmaceutico.nombre}</td>
                  <td>{item.t_presentacion.descripcion}</td>
                  <td>{item.t_unidad.descripcion}</td>
                  <td>{item.producto_farmaceutico.stockGeneral}
                  </td>

                <ValidarEstado estadoR={item.producto_farmaceutico.habilitado}/>
                  
                
                  <td>
                    <FormGroup check inline key={indice}>
                    <input className="form-check-input" type="checkbox" value={item.producto_farmaceutico.codigo} ref={ref => this.activitiesRefs[indice] = ref} />
                    </FormGroup>
                  </td>
                </tr>
                ))}
              </tbody>
            </Table>  
            
        </CardBody>

        <CardFooter>
          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>

        </CardFooter>
      </Card>  
    </Col>
  </Row>

  {/* MODAL HABILITAR */}
  <Modal isOpen={this.state.primary} className={'modal-primary ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
    <ModalBody>
    ¿Desea habilitar el/los medicamentos?
      <center>
      <Col sm="6">
      <Table  responsive size="sm">
      <thead>
        <tr>
          <th>Nro.</th>
          <th>Código</th>
        </tr>
      </thead>
      <tbody>
      {productos.map((item, indice) => ( 
        <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item}</td>
        </tr>
      ))}
      </tbody>
      </Table>
      </Col>
      </center>
    </ModalBody>
    <ModalFooter>
      <Button onClick={this.Habilitar} type="submit" color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimary}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL HABILITAR */}

  {/* MODAL DESHABILITAR */}
  <Modal isOpen={this.state.primary2} className={'modal-primary ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
    <ModalBody>
      ¿Desea deshabilitar el/los medicamentos?
      <center>
      <Col sm="6">
      <Table  responsive size="sm">
      <thead>
        <tr>
          <th>Nro.</th>
          <th>Código</th>
        </tr>
      </thead>
      <tbody>
      {productos.map((item, indice) => ( 
        <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item}</td>
        </tr>
      ))}
        </tbody>
      </Table>
      </Col>
      </center>
    </ModalBody>
    <ModalFooter>
      <Button onClick={this.Deshabilitar} type="submit" color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL DESHABILITAR*/}
</div>
);
}
}

export default EstadoInsumo;

    


