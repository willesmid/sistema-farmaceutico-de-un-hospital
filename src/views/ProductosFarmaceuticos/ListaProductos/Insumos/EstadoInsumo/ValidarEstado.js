import React, { Component } from 'react';
import { Badge } from 'reactstrap';

class ValidacionEstado extends Component {
  constructor(props) {
    super(props);
    this.state = {    
    };
  }

  render() {
    const mensaje = (this.props.estadoR) ? "✓":"×"
    const tipo = (this.props.estadoR)?"success":"danger"
    console.log(tipo)
    return(
      <td>
        <Badge color={tipo}>{mensaje}</Badge>
      </td>
    )
  }
}
export default ValidacionEstado;