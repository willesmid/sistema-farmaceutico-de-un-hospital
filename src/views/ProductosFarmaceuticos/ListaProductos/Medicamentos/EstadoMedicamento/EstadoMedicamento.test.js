import React from 'react';
import ReactDOM from 'react-dom';
import EstadoMedicamento from './EstadoMedicamento';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EstadoMedicamento />, div);
  ReactDOM.unmountComponentAtNode(div);
});