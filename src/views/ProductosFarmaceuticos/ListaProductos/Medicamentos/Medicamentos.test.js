import React from 'react';
import ReactDOM from 'react-dom';
import Medicamentos from './Medicamentos';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Medicamentos />, div);
  ReactDOM.unmountComponentAtNode(div);
});