import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,  
  Collapse,

} from 'reactstrap';
var urlProducto='http://localhost:8000/farmacia/almacena/v1/ProductoGuardar/'
var urlForma='http://localhost:8000/farmacia/almacena/v1/FormaFarmaceuticaGuardar/'
var urlMedicamento='http://localhost:8000/farmacia/almacena/v1/MedicamentoGuardar/'
const styles2 = { color: '#f33c29' }

class NuevoMedicamento extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showCodigo:false,
      showExiste:false,
      codigo:'',
      showNombre:false,
      nombre:'',
      showSaldo:false,
      saldo_minimo:'',
      showCant:false,
      cantidad_prescripcion:'',
      showPrimero:true,
      showSegundo:false,
      primary:false,
      showRegistra:true,
      collapse:false,
      list_forma:[],
      list_forma2:[],

      primaryForma:false,
      showExisteForma:false,
      descripcion:'',
      showNuevaForma:false,
      showForma:false,

      t_forma_farmaceutica:'',
      show_forma_farmaceutica:false, 
      showRegistraMedicamento:true,
      concentracion:'',
      showConcentracion:false,
      clasificacion:'',
      showClasificacion:false,

      primaryMedicamento:false,
      showCancelarN:false,
      showCancelar:true,
      showRegistraMedicamento2:true,
      opcion1:false,
      primaryMedicamento2:false,
      habilitado:'',
      existeMedicamento:'',
      showInsertarProducto:false,
      nombre2:'',
      saldo_minimo2:'',
      cantidad_prescripcion2:'',
      showDeshacer:true,
      
      
    }
    this.onValidacion = this.onValidacion.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onRegistraForma = this.onRegistraForma.bind(this)

    this.toggleForma = this.toggleForma.bind(this)
    this.recargar = this.recargar.bind(this)

    this.onValidacion2 = this.onValidacion2.bind(this)
    this.onValidacion3 = this.onValidacion3.bind(this)
    this.toggleMedicamento =this.toggleMedicamento.bind(this)
    this.toggleMedicamento2 =this.toggleMedicamento2.bind(this)
    
    this.onRegistraMedicamento = this.onRegistraMedicamento.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
    this.onRegistraMedicamento2 = this.onRegistraMedicamento2.bind(this)

    this.insertarMedicamento = this.insertarMedicamento.bind(this)
    this.Deshacer = this.Deshacer.bind(this)
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }

  toggleForma() {

    this.setState({
      primaryForma: !this.state.primaryForma,
      showExisteForma:false,
      showForma:false,
      descripcion:'',
    })
  }
  toggleMedicamento(){
    this.setState({ primaryMedicamento: !this.state.primaryMedicamento});
  }
  toggleMedicamento2(){
    this.setState({ primaryMedicamento2: !this.state.primaryMedicamento2});
  }
  onCancelar(){
    this.props.history.push('/Inventario/InventarioGeneral/Medicamentos')
    
  }
async componentDidMount() {
    fetch('http://localhost:8000/farmacia/almacena/v1/FormaFarmaceuticaLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      this.setState({
        list_forma: findresponse,
      })
    });
    }
    
onValidacion(){
  if(this.state.codigo!==''){
    fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.state.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if(findresponse.length>0){
        this.setState({
        showExiste:true,
        nombre2: findresponse[0].nombre,
        saldo_minimo2: findresponse[0].saldo_minimo,
        cantidad_prescripcion2:findresponse[0].cantidad_prescripcion,
        })
        console.log("estado" + findresponse[0].habilitado)
        if(findresponse[0].habilitado !== true){
          this.setState({
          habilitado:' - esta deshabilitado',
          })
        }else{
          this.setState({
            habilitado:'',
          })
        }

        fetch('http://localhost:8000/farmacia/almacena/v1/detalleMedicamentoBusca/'+this.state.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
          if(findresponse.length>0){
            this.setState({
            showExiste:true,
            existeMedicamento:'',
            })
          }else{
            this.setState({
              existeMedicamento:'- No existe producto en Medicamentos',
              showInsertarProducto:true,
              })
          }
        
        })
      }else{
        
        this.setState({
        showExiste:false,
        })
        if(this.state.codigo!=='' && this.state.nombre!=='' && this.state.saldo_minimo!=='' && this.state.cantidad_prescripcion!==''){
          this.setState({
          primary:true,
          })
        }     
      }
    });
  } 
  if(this.state.codigo===''){
    this.setState({
      showCodigo:true,
    })
  }else{
    this.setState({
      showCodigo:false,
    })
  }
  if(this.state.nombre===''){
    this.setState({
      showNombre:true,
    })
  }else{
    this.setState({
      showNombre:false,
    })
  }
  if(this.state.saldo_minimo===''){
    this.setState({
      showSaldo:true,
    })
  }else{
    this.setState({
      showSaldo:false,
    })
  }
  if(this.state.cantidad_prescripcion===''){
    this.setState({
      showCant:true,
    })
  }else{
    this.setState({
      showCant:false,
    })
  }
  this.setState({
    showExiste:false,
    showInsertarProducto:false,
  })
}
onFormSubmit(){
  // GUARDA PRODUCTO
  let data={
    codigo:this.state.codigo,
    nombre:this.state.nombre,
    stockGeneral:0,
    costo:0,
    saldo_minimo:this.state.saldo_minimo,
    cantidad_prescripcion:this.state.cantidad_prescripcion,
  }
  try{
    fetch(urlProducto, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then((res) => {res.json(); console.log('respuesta 1:', res.status)})
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }

  this.setState({
    primary:false,
    collapse:true,
    showRegistra:false,
    showPrimero:false,
    showSegundo:true,
    showCancelarN:true,
    showCancelar:false,
    showDeshacer:false,
    nombre2:'',
    saldo_minimo2:'',
    cantidad_prescripcion2:'',
  })
}
onRegistraForma(){
  
  if(this.state.descripcion!==''){
    fetch('http://localhost:8000/farmacia/almacena/v1/FormaFarmaceuticaBuscar/'+this.state.descripcion+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if(findresponse.length>0){
        this.setState({
        showExisteForma:true,
        showForma:false,
        })
      }else{
   
        if(this.state.descripcion!==''){

        let data={
          descripcion:this.state.descripcion
        }
        try{
          fetch(urlForma, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be string or {object}!
            headers:{
              'Content-Type': 'application/json',
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response));              
        } catch (e) {
          console.log(e);
        }
        this.setState({
          showExisteForma:false,
          showForma:false,
          primaryForma:false,
          descripcion:'',
        
        })
        
        }


      }
    });


  }
  if(this.state.descripcion===''){
    this.setState({
      showForma:true,
    })
  }
  this.setState({
    showExisteForma:false,
   
    
  })


    // GUARDA FORMA FARMACEUTICA
  }

recargar(){
  fetch('http://localhost:8000/farmacia/almacena/v1/FormaFarmaceuticaLista/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
   //   console.log("FINDRESPONSE=",findresponse)
    this.setState({
      list_forma: findresponse,

    })
  })

}
onValidacion2(){
  if(this.state.t_forma_farmaceutica===''){
    this.setState({
      show_forma_farmaceutica:true
    })
    
  }else{
    this.setState({
      show_forma_farmaceutica:false
    })
  }
  if(this.state.concentracion===''){
    this.setState({
      showConcentracion:true
    })
    
  }else{
    this.setState({
      showConcentracion:false
    })
  }
  if(this.state.clasificacion===''){
    this.setState({
      showClasificacion:true
    })
    
  }else{
    this.setState({
      showClasificacion:false
    })
  }
  if(this.state.t_forma_farmaceutica!=='' && this.state.concentracion!=='' && this.state.clasificacion!==''){
    this.setState({
      primaryMedicamento:true,
    })
  }
}

onValidacion3(){
  if(this.state.t_forma_farmaceutica===''){
    this.setState({
      show_forma_farmaceutica:true
    })
    
  }else{
    this.setState({
      show_forma_farmaceutica:false
    })
  }
  if(this.state.concentracion===''){
    this.setState({
      showConcentracion:true
    })
    
  }else{
    this.setState({
      showConcentracion:false
    })
  }
  if(this.state.clasificacion===''){
    this.setState({
      showClasificacion:true
    })
    
  }else{
    this.setState({
      showClasificacion:false
    })
  }
  if(this.state.t_forma_farmaceutica!=='' && this.state.concentracion!=='' && this.state.clasificacion!==''){
    this.setState({
      primaryMedicamento2:true,
    })
  }

}

onRegistraMedicamento(){
    let data={
    producto_farmaceutico:this.state.codigo,
    concentracion:this.state.concentracion,
    clasificacion:this.state.clasificacion,
    t_forma_farmaceutica:this.state.t_forma_farmaceutica,
  }
  try{
    fetch(urlMedicamento, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        console.log('Success:', response)
        window.location.reload()
    });              
  } catch (e) {
    console.log(e);
  }
  
  this.setState({
    primaryMedicamento:false,
  })
  
}

onRegistraMedicamento2(){
  let data={
  producto_farmaceutico:this.state.codigo,
  concentracion:this.state.concentracion,
  clasificacion:this.state.clasificacion,
  t_forma_farmaceutica:this.state.t_forma_farmaceutica,
}
try{
  fetch(urlMedicamento, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(data), // data can be string or {object}!
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
  }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)

      this.props.history.push('/Inventario/InventarioGeneral/Medicamentos')
      window.location.reload()

  });              
} catch (e) {
  console.log(e);
}
this.setState({
  primaryMedicamento2:false,
})

}

insertarMedicamento(){
  this.setState({
    collapse:true,
    showSegundo:true,
    showPrimero:false,
    showRegistra:false,
    showDeshacer:true,
    nombre:'',
    saldo_minimo:'',
    cantidad_prescripcion:'',
    showForma:false,
    t_forma_farmaceutica:'',
    showConcentracion:false,
    concentracion:'',
    showClasificacion:false,
    clasificacion:'',

  })
}


Deshacer(){
  this.setState({
    collapse:false,
    showSegundo:false,
    showPrimero:true,
    showRegistra:true,
    showDeshacer:false,
    codigo:'',
    showCodigo:false,
    nombre:'',
    showNombre:false,
    saldo_minimo:'',
    showSaldo:false,
    cantidad_prescripcion:'',
    showCant:false,
    showExiste:false,
    showInsertarProducto:false,
    clasificacion:'',
    

  })
  window.location.reload()

}

render() {
  return (
  <div className="animated fadeIn">
  
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className="fa fa-pencil"></i> <strong>Nuevo Medicamento</strong></h3>
        </CardHeader>
        <CardBody>
          
          <Form >
          {this.state.showPrimero &&
          <Row>
          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Codigo</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="username3" name="username3" maxLength="50" required onChange={e => this.setState({ codigo: e.target.value })}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-hashtag"></i></InputGroupText>
                  {this.state.showInsertarProducto &&
                  <Button type="button" color="warning" onClick={this.insertarMedicamento}>Añadir a Medicamentos</Button>
                  }
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showCodigo &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba codigo de producto</FormText>
              }
              {
              this.state.showExiste &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>{"Existe el Codigo" + this.state.habilitado + " " + this.state.existeMedicamento}</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Nombre</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" autoComplete="username" maxLength="100" onChange={e => this.setState({ nombre: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showNombre &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Nombre de producto</FormText>
              }
            </FormGroup>
          </Col>

          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Saldo Minimo</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number2" name="numer2" onChange={e => this.setState({ saldo_minimo: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className=" 	fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showSaldo &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Saldo Minimo</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Cantidad Maxima de dispensación</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number" name="number" onChange={e => this.setState({ cantidad_prescripcion: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showCant &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Cantidad Maxima de dispensación</FormText>
              }
            </FormGroup>  
            </Col>
          </Row>
          }
          {this.state.showSegundo &&
          <Row>
          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Codigo</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="username3" name="username3" maxLength="50"  value={this.state.codigo} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-hashtag"></i></InputGroupText>
                  {this.state.showDeshacer &&
                  <Button type="button" color="warning" onClick={this.Deshacer}>Deshacer</Button>
                  }
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Nombre</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" autoComplete="username" maxLength="100" value={this.state.nombre + this.state.nombre2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
          </Col>

          <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Saldo Minimo</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number2" name="numer2" value={this.state.saldo_minimo + this.state.saldo_minimo2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className=" 	fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Cantidad Maxima de dispensación</InputGroupText>
                </InputGroupAddon>
                <Input type="number" id="number" name="number" value={this.state.cantidad_prescripcion + this.state.cantidad_prescripcion2} disabled/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-sort"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>  
            </Col>
          </Row>
          }
           <h3 align="right">
            {
            this.state.showRegistra &&
            <Button  size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion}><i className="fa fa-edit"></i><span>Registrar Producto</span></Button>
            } 
          </h3>
          <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Row>
            <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Forma Farmaceutica</InputGroupText>
                </InputGroupAddon>
                <Input type="select" name="medico" id="select" required
                  value={this.state.t_forma_farmaceutica}
                  onChange={e => this.setState({ t_forma_farmaceutica: e.target.value })}>                                         
                  <option value="">farmaceutica...</option>
                    {this.state.list_forma.map(item1 => (
                      <option value={item1.id}>{item1.descripcion}</option>
                    ))}
                </Input>    
                <InputGroupAddon addonType="append">
                <Button type="button" color="success" onClick={this.toggleForma}>+</Button>
                <Button type="button" color="light" onClick={this.recargar}><i className="fa fa-refresh"></i></Button>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.show_forma_farmaceutica &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Seleccione Forma Farmaceutica</FormText>
              }
            </FormGroup>
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Concentración</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="concentracion" name="concentracion" onChange={e => this.setState({ concentracion: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showConcentracion &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Concentración</FormText>
              }
            </FormGroup>  
            </Col>
          
            <Col xs="6">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Clasificación A.T.Q.</InputGroupText>
                </InputGroupAddon>
                <Input type="text" id="concentracion" name="concentracion" onChange={e => this.setState({ clasificacion: e.target.value })} required/>
                <InputGroupAddon addonType="append">
                <InputGroupText><i className="fa fa-font"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showClasificacion &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Clasificación A.T.Q.</FormText>
              }
            </FormGroup>  
            </Col>
          </Row>
          <h3 align="right">
          {
            this.state.showRegistraMedicamento &&
            <Button  size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion2}><i className="fa fa-edit"></i><span>Registrar Medicamento y añadir otro</span></Button>
            }
      
        {
          this.state.showRegistraMedicamento2 &&
        <Button size="sm" className="btn-facebook btn-brand mr-1 mb-1" onClick={this.onValidacion3}><i className="fa fa-edit"></i><span>Registrar Medicamento</span></Button>
        }
        </h3>
            
             
          </Collapse>

          </Form>


        </CardBody>
        <CardFooter>

          {this.state.showCancelar &&

          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
          }
          {this.state.showCancelarN &&
          <h3 align="right">
          <Button type="reset" size="sm" color="danger" disabled><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>
          }
        </CardFooter>
      </Card>  
    </Col>

  </Row>


{/* MODAL REGISTRAR PRODUCTO  */}
  <Form>
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
    <form onSubmit={this.props.onAddEntrega}>
    <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
      <ModalFooter>           
        <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO PRODUCTO */}

  {/* MODAL REGISTRA FORMA FARMACEUTICA */}
  <Form>
  <Modal isOpen={this.state.primaryForma }  className={'modal-success ' + this.props.className}>
    <ModalHeader toggle={this.toggleForma}>Registra Forma Farmaceutica</ModalHeader>
      <ModalBody>
      <center>
        <strong>¿Desea Registrar nueva Forma Farmaceutica?</strong>
      </center>
        <br/>
          <Form className="was-validated">
            <FormGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Descripción</InputGroupText>
                </InputGroupAddon>
                <Input type="text" className="form-control-success" id="inputSuccess2i" maxlength="30" onChange={e => this.setState({ descripcion: e.target.value } )}/>
                <InputGroupAddon addonType="append">
                  <InputGroupText><i className="fa fa-medkit"></i></InputGroupText>
                </InputGroupAddon>
              </InputGroup>
              {
              this.state.showExisteForma &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Existe Forma Farmaceutica</FormText>
              }
              {
              this.state.showForma &&
              <FormText className="help-block" color= "#ca1125" style={styles2}>Escriba Forma Farmaceutica</FormText>
              }
            </FormGroup>
          </Form>
      </ModalBody>
    <ModalFooter>

      <Button color="success" onClick={this.onRegistraForma}>Si</Button>{' '}
      <Button color="secondary" onClick={this.toggleForma}>No</Button>
    </ModalFooter>
  </Modal>
  </Form>
  {/* FIN MODAL REGISTRA FORMA FARMACEUTICA */}

  {/* MODAL REGISTRAR MEDICAMENTO */}
  <Form>
  <Modal isOpen={this.state.primaryMedicamento} toggle={this.toggleMedicamento} className={'modal-primary ' + this.props.className}>
    <form >
    <ModalHeader toggle={this.toggleMedicamento}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos de Medicamento y Registrar otro?
      </ModalBody>
      <ModalFooter>    

        <Button type="submit" onClick={this.onRegistraMedicamento}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.toggleMedicamento}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO MEDICAMENTO */}

 {/* MODAL REGISTRAR MEDICAMENTO 2*/}
 <Form>
  <Modal isOpen={this.state.primaryMedicamento2} toggle={this.toggleMedicamento2} className={'modal-primary ' + this.props.className}>
    <form >
    <ModalHeader toggle={this.toggleMedicamento2}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos de Medicamento y Salir?
      </ModalBody>
      <ModalFooter>    

        <Button type="submit" onClick={this.onRegistraMedicamento2}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.toggleMedicamento2}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO MEDICAMENTO 2*/}
    </div>
  );
}
}

export default NuevoMedicamento;

    


