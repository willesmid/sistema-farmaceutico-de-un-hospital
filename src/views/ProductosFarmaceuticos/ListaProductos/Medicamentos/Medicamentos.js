import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Table,
  PaginationItem,
  PaginationLink,
  Pagination, 
  Button,
  NavLink, 
  InputGroup,
  InputGroupAddon,
  Input
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
let url_nuevo ='#/Inventario/InventarioGeneral/Medicamentos/NuevoMedicamento'
let url_estado ='#/Inventario/InventarioGeneral/Medicamentos/EstadoMedicamento'
const trNavLink = {
  padding: '0',
  margin: '0'
}

class Medicamentos extends Component {
  constructor(props) {
    super(props)
    this.buscarValor = this.buscarValor.bind(this) 
    this.state = {
        Medicamentos: [],
        primary: false,
        page: 1,
        eachPage: 10, 
        valor: '',
    }
    this.handleClick = this.handleClick.bind(this)
    this.print = this.print.bind(this)
    this.print2 = this.print2.bind(this)
    this.print3 = this.print3.bind(this)
  }

  handleClick(event) {
    this.setState({
        page: Number(event.target.id)
    });
}

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/Medicamentos/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } }
      );
      const Medicamentos = await respuesta.json();
      this.setState({
        Medicamentos
      });
      console.log(Medicamentos)

    } catch (e) {
      console.log(e);
    }
  }
  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

print(){
  /*Actual time and date */
  var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
  }
  var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
  }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();

  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 10, 7, 20,20);

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 18, "\"PROMES\"", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(14);
  doc.text(115, 23, "INVENTARIO GENERAL DE MEDICAMENTOS", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.text(105,27, showDate +' '+ showTime, null, null, 'center');
  doc.setFontSize(12);

  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {},
    headerStyles: {fontSize: 8},
    bodyStyles: {fontSize: 8},
    alternateRowStyles: {},
    columnStyles: {},
    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: {top: 30, left: 15}, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number, 
    showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,
  });
  window.open(doc.output('bloburl'), '_blank');
}

print2(){
  /*Actual time and date */
  var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
  }
  var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
  }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();

  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 10, 7, 20,20);

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 18, "\"PROMES\"", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(14);
  doc.text(105, 23, "LISTA DE MEDICAMENTOS", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.text(105,27, showDate +' '+ showTime, null, null, 'center');
  doc.setFontSize(12);

  var res = doc.autoTableHtmlToJson(document.getElementById("tabla2"));
  doc.autoTable(res.columns, res.data, {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {},
    headerStyles: {fontSize: 8},
    bodyStyles: {fontSize: 8},
    alternateRowStyles: {},
    columnStyles: {},
    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: {top: 30, left: 15}, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number, 
    showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,
  });
  window.open(doc.output('bloburl'), '_blank');
}

print3(){
  /*Actual time and date */
  var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
  }
  var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
  }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();

  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 10, 7, 20,20);

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 18, "\"PROMES\"", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(14);
  doc.text(115, 23, "INVENTARIO GENERAL DE MEDICAMENTOS", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.text(105,27, showDate +' '+ showTime, null, null, 'center');
  doc.setFontSize(12);

  var res = doc.autoTableHtmlToJson(document.getElementById("tabla3"));
  doc.autoTable(res.columns, res.data, {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {},
    headerStyles: {fontSize: 8},
    bodyStyles: {fontSize: 8},
    alternateRowStyles: {},
    columnStyles: {},
    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: {top: 30, left: 15}, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number, 
    showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,
  });
  window.open(doc.output('bloburl'), '_blank');
}

  render() {
    let items_filtrados = this.state.Medicamentos.filter(
      (item) => {
        return (item.producto_farmaceutico.codigo + ' ' + item.producto_farmaceutico.nombre + ' ' + item.t_forma_farmaceutica.descripcion)
          .toUpperCase().indexOf(
            this.state.valor.toUpperCase()) !== -1;
      }
    );
    
    const { page, eachPage} = this.state;
    const last = page * eachPage;
    const first = last - eachPage;
    const items_filtrados2 = items_filtrados.slice(first, last);

    const ListaMedicamentos = items_filtrados2.map((item, indice)=>{
      return(
        <tr key={item.indice}> 
          <td>{indice+1}</td>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>{item.concentracion}</td>
          <td>{item.clasificacion}</td>
          <td>{item.producto_farmaceutico.stockGeneral}</td>
        </tr>
      )
    })

    const medicamentos_fil = items_filtrados.map((item, indice)=>{
      return(
        <tr key={item.indice}> 
          <td>{indice+1}</td>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>{item.concentracion}</td>
          <td>{item.clasificacion}</td>
          <td>{item.producto_farmaceutico.stockGeneral}</td>
        </tr>
      )
    })

    const TodoMedicamentos = this.state.Medicamentos.map((item, indice)=>{
      return(
        <tr key={item.indice}> 
          <td>{indice+1}</td>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>{item.concentracion}</td>
          <td>{item.clasificacion}</td>
          <td>{item.producto_farmaceutico.stockGeneral}</td>
        </tr>
      )
    })
    const Lista = this.state.Medicamentos.map((item, indice)=>{
      return(
        <tr key={item.indice}> 
          <td>{indice+1}</td>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>{item.concentracion}</td>
          <td>{item.clasificacion}</td>
        </tr>
      )
    })
  const pages = [];
    for (let i = 1; i <= Math.ceil(items_filtrados.length / eachPage); i++) {
        pages.push(i);
    }
  
    const renderpages = pages.map(number => {
        if(number % 2 === 1){
            return (
                <PaginationItem key={number} active>
                    <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                </PaginationItem>
            );
        }
        else{
            return (
                <PaginationItem key={number}>
                    <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
                </PaginationItem>
            );
        }
    });
    
    return (
<div className="animated fadeIn">
  <Row>
    <Col>
      <Card>
        <CardHeader>
          <h3><i className="fa fa-table"></i> <strong>Inventario General: Medicamentos</strong>
              <Button size="sm" onClick={this.print2} color="link" className="btn  btn-nfo float-right">
                <i className="fa fa-print"></i> Imprimir Lista
              </Button>
              <Button size="sm" onClick={this.print} color="link" className="btn  btn-nfo float-right">
                <i className="fa fa-print"></i> Imprimir Todo
              </Button>                
          </h3>
        </CardHeader>
        <CardBody>
          <table style={trNavLink} align="right" >
            <tr>
              <td>
                <InputGroup>
                <InputGroupAddon addonType="prepend">
                </InputGroupAddon>
                <Input  size="100" type="text" placeholder="Ingrese Codigo o Nombre de Medicamento" value={this.state.valor} onChange={this.buscarValor}/>
                <InputGroupAddon addonType="append">
                <Button type="button" color="primary" disabled><i className="fa fa-search"></i></Button>
                </InputGroupAddon>
                </InputGroup>    
              </td>
              <td>
              <NavLink style={trNavLink} href = {url_nuevo} >
              <Button size="sm" className="btn-spotify btn-brand mr-1 mb-1"><i className="fa fa-pencil"></i><span>Nuevo Medicamento</span></Button>
              </NavLink> 
              </td>
              <td> 
              <NavLink style={trNavLink} href = {url_estado} >
              <Button size="sm" className="btn-html5 btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Habilitar/Inhabilitar Medicamento</span></Button>
              </NavLink> 
              </td>
              <td>
              <Button size="sm" onClick={this.print3} className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-print"></i><span>Imprimir</span></Button>
              </td>
            </tr>
          </table> 
          <Table hover bordered striped responsive size="sm">
            <thead>
            <tr>
              <th>Numero</th>
              <th>Codigo</th>
              <th>Nombre</th>
              <th>F. Farmaceutica</th>
              <th>Concentracion</th>
            
              <th>Clasificacion</th>
              <th>Stock</th>
            </tr>
            </thead>
            <tbody>
            {ListaMedicamentos}
            <tr>
            <th colspan="6"></th>
            <td colspan="1" bgcolor="#424949" ><font color="#ffffff">{"Total: "+items_filtrados.length}</font>
            </td>
            </tr>  
          </tbody>
          </Table>

          <Table  style={trNavLink} hover bordered striped responsive size="sm">
          <Pagination>
            {renderpages}
          </Pagination>
          </Table>

        </CardBody>
      </Card>
    </Col>
  </Row>
  <Table responsive striped hidden id="tabla">
  <thead>
    <tr>
      <th>No.</th>
      <th>CODIGO</th>
      <th>NOMBRE</th>
      <th>F. FARMACEUTICA</th>
      <th>CONCENTRACION</th>
      <th>CLASIFICACION</th>
      <th>STOCK</th>
    </tr>
  </thead>
  <tbody>
  {TodoMedicamentos}
  </tbody>
  </Table>

  <Table responsive striped hidden id="tabla2">
  <thead>
    <tr>
      <th>No.</th>
      <th>CODIGO</th>
      <th>NOMBRE</th>
      <th>F. FARMACEUTICA</th>
      <th>CONCENTRACION</th>
      <th>CLASIFICACION</th>
    </tr>
  </thead>
  <tbody>
  {Lista}
  </tbody>
  </Table>

  <Table responsive striped hidden id="tabla3">
  <thead>
    <tr>
      <th>No.</th>
      <th>CODIGO</th>
      <th>NOMBRE</th>
      <th>F. FARMACEUTICA</th>
      <th>CONCENTRACION</th>
      <th>CLASIFICACION</th>
      <th>STOCK</th>
    </tr>
  </thead>
  <tbody>
  {medicamentos_fil}
  </tbody>
  </Table>
</div>
    );
  }
}

export default Medicamentos;
