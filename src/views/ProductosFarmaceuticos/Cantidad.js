import React, { Component } from 'react';
class Cantidad extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300, 
            data: 0,
            data2: 0
        };
    }
    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }
    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState }});
    }
    async componentDidMount() {
        try {

       

            const respuesta = await fetch('http://localhost:8000/enfermeria/farmacia_auxiliar/v1/listare/'+this.props.id+'/');
            const data = await respuesta.json();
            if(data.length > 0){
                this.setState({
                    data: data[0].sum
                });
            }
            const respuesta2 = await fetch('http://localhost:8000/enfermeria/farmacia_auxiliar/v1/listar/'+this.props.id+'/');
            const data2 = await respuesta2.json();
            if(data2.length > 0){
                this.setState({
                    data2: data2[0].sum
                });
            }
        } catch (e) {
            console.log(e);
        }
    }
    render() {
        return (
            <div>
                {this.state.data - this.state.data2} unidades
            </div>
        );
    }
}

export default Cantidad;
