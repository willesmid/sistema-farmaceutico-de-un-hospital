import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Table,
  Button,
  // PaginationItem,
  // PaginationLink,
  // Pagination, 
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import Saldo from '../Saldo'
import Validar from '../Validar'

const styles2 = { color: 'white', backgroundColor: '#2b2c2c', textAlign:'center' }
// const trNavLink = {
//   padding: '0',
//   margin: '0'
// }

class Reactivos extends Component {
  constructor(props) {
    super(props)
    this.buscarValor = this.buscarValor.bind(this) 
    this.state = {
        Reactivos: [],
        primary: false,
        page: 1,
        eachPage: 10, 
        valor: '',
    }
    this.print = this.print.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(event) {
    this.setState({
        page: Number(event.target.id)
    });
}

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/Reactivos/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const Reactivos = await respuesta.json();
      this.setState({
        Reactivos
      });
      console.log(Reactivos)

    } catch (e) {
      console.log(e);
    }
  }
  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  print(){
    /*Actual time and date */
    var options2 = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
    }
    var options3 = {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }
    var d = new Date();
    var showDate = d.toLocaleString('es-bo', options2) 
    var showTime = d.toLocaleString('es-bo', options3) 
    /*Actual time and date */
    var doc = new jsPDF('l', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
    var logo = new Image();
  
    logo.src = 'assets/img/dashboard/icono_promes.png';
    doc.addImage(logo, 'JPEG', 10, 7, 20,20);
  
    doc.setFont("helvetica");
    doc.setFontType("bold");   //italic
    doc.setFontSize(7);
    doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
    
    doc.setFont("helvetica");
    doc.setFontType("bold");   //italic
    doc.setFontSize(7);
    doc.text(50, 18, "\"PROMES\"", null, null, 'center');
    
    doc.setFont("helvetica");
    doc.setFontType("bold");   //italic
    doc.setFontSize(14);
    doc.text(140, 23, "SALDOS MINIMOS DE REACTIVOS", null, null, 'center');
    
    doc.setFontType("normal");
    doc.setFontSize(9);
    doc.text(140,27, showDate +' '+ showTime, null, null, 'center');
    doc.setFontSize(12);
  
    var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
    doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {fontSize: 8},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 30, left: 15}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
    });
    window.open(doc.output('bloburl'), '_blank');
  }

  render() {
    let items_filtrados = this.state.Reactivos.filter(
      (item) => {
        return (item.producto_farmaceutico.codigo + ' ' + item.producto_farmaceutico.nombre)
          .toUpperCase().indexOf(
            this.state.valor.toUpperCase()) !== -1;
      }
    );
    
    // const { page, eachPage} = this.state;
    // const last = page * eachPage;
    // const first = last - eachPage;
    // const items_filtrados2 = items_filtrados.slice(first, last);

    const ListaReactivos = this.state.Reactivos.map((item, indice)=>{
      if(item.producto_farmaceutico.stockGeneral>item.producto_farmaceutico.saldo_minimo){
      return(
        <tr key={item.indice}> 
          <td style={{backgroundColor: ' #d0ffe7 ', textAlign: 'center'}}>{indice+1}</td>
          <td style={{backgroundColor: ' #d0ffe7 '}}>{item.producto_farmaceutico.codigo}</td>
          <td style={{backgroundColor: ' #d0ffe7 '}}>{item.producto_farmaceutico.nombre}</td>
          <td style={{backgroundColor: ' #d0ffe7 '}}>{item.clasificacion}</td>
        
          <Saldo almacen={1} codigo={item.producto_farmaceutico.codigo} stock={item.producto_farmaceutico.stockGeneral} saldo={item.producto_farmaceutico.saldo_minimo} />
          <Saldo almacen={2} codigo={item.producto_farmaceutico.codigo} stock={item.producto_farmaceutico.stockGeneral} saldo={item.producto_farmaceutico.saldo_minimo} />     
          <td style={{backgroundColor: ' #a5ffd1 '}}>{item.producto_farmaceutico.stockGeneral}</td>
          <td style={{backgroundColor: '#86efb9'}}>{item.producto_farmaceutico.saldo_minimo}</td>
          <Validar stock={item.producto_farmaceutico.stockGeneral} saldo={item.producto_farmaceutico.saldo_minimo} />
        </tr>
      )
      }else{

        return(
          <tr key={item.indice}> 
            <td style={{backgroundColor: '#fee1df', textAlign: 'center'}}>{indice+1}</td>
            <td style={{backgroundColor: '#fee1df'}}>{item.producto_farmaceutico.codigo}</td>
            <td style={{backgroundColor: '#fee1df'}}>{item.producto_farmaceutico.nombre}</td>
            <td style={{backgroundColor: '#fee1df'}}>{item.clasificacion}</td>

            <Saldo almacen={1} codigo={item.producto_farmaceutico.codigo} stock={item.producto_farmaceutico.stockGeneral} saldo={item.producto_farmaceutico.saldo_minimo} />
            <Saldo almacen={2} codigo={item.producto_farmaceutico.codigo} stock={item.producto_farmaceutico.stockGeneral} saldo={item.producto_farmaceutico.saldo_minimo}/>     
            <td style={{backgroundColor: '#fecfcc'}}>{item.producto_farmaceutico.stockGeneral}</td>
            <td style={{backgroundColor: '#feb6b2'}}>{item.producto_farmaceutico.saldo_minimo}</td>
            <Validar stock={item.producto_farmaceutico.stockGeneral} saldo={item.producto_farmaceutico.saldo_minimo} />
          </tr>
        )
      }
    })
  // const pages = [];
  //   for (let i = 1; i <= Math.ceil(items_filtrados.length / eachPage); i++) {
  //       pages.push(i);
  //   }
  
  //   const renderpages = pages.map(number => {
  //       if(number % 2 === 1){
  //           return (
  //               <PaginationItem key={number} active>
  //                   <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
  //               </PaginationItem>
  //           );
  //       }
  //       else{
  //           return (
  //               <PaginationItem key={number}>
  //                   <PaginationLink tag="button" id={number} onClick={this.handleClick}>{number}</PaginationLink>
  //               </PaginationItem>
  //           );
  //       }
  //   });
    
return (
  <div className="animated fadeIn">
    <Row>
      <Col>
        <Card>
          <CardHeader>
            <h3><i className="fa fa-tasks"></i> <strong>Saldos Minimos Reactivos</strong>
                <Button size="sm" onClick={this.print} color="link" className="btn  btn-nfo float-right">
                  <i className="fa fa-print"></i> Imprimir Saldo
                </Button>             
            </h3>
          
          </CardHeader>
          <CardBody>
            <Table hover bordered striped responsive size="sm">
              <thead style={styles2}>
                <tr>
                  <th rowspan="2"><p>Nro.</p></th>
                  <th rowspan="2"><p>Codigo</p></th>
                  <th rowspan="2"><p>Nombre</p></th>
                  <th rowspan="2"><p>clasificacion</p></th>
                  
                  <th colspan="2"><center>Cantidad</center></th> 
                  <th rowspan="2"><p>Stock General</p></th>
                  <th rowspan="2"><p>Saldo Minimo</p></th>
                  <th rowspan="2"><p>Observacion</p></th>                     
                </tr>
                <tr>
                  <th>Farmacia</th>
                  <th>Almacen</th>                          
                </tr>
              </thead>
              <tbody>
              {ListaReactivos}
              <tr>
              <th colspan="8"></th>
              <td colspan="1" bgcolor="#424949" ><font color="#ffffff">{"Total: "+items_filtrados.length}</font>
              </td>
              </tr>  
            </tbody>
            </Table>
  
            {/* <Table  style={trNavLink} hover bordered striped responsive size="sm">
            <Pagination>
              {renderpages}
            </Pagination>
            </Table> */}

          </CardBody>
        </Card>
      </Col>
    </Row>

  <Table responsive striped hidden id="tabla">
  <thead>
  <tr>
    <th rowspan="2"><p>Nro.</p></th>
    <th rowspan="2"><p>Codigo</p></th>
    <th rowspan="2"><p>Nombre</p></th>
    <th rowspan="2"><p>Clasificacion</p></th>
    <th>C. Farmacia</th>
    <th>C. Almacen</th>  
    <th rowspan="2"><p>Stock General</p></th>
    <th rowspan="2"><p>Saldo Minimo</p></th>
    <th rowspan="2"><p>Observacion</p></th>                     
  </tr>
  </thead>
  <tbody>
  {ListaReactivos}
  </tbody>
  </Table>
  </div>
);
  }
}

export default Reactivos;
