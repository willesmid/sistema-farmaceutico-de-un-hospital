import React, { Component } from 'react';
class Saldo extends Component {
  constructor(props) {
    super(props);
      this.state = {
        dataCant:0,
      };
  }
  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      fetch('http://localhost:8000/farmacia/almacena/v1/LocalizacionBuscaCodsuma/'+this.props.almacen+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            dataCant: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.props.codigo+" "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    if(this.props.almacen===1){
      if(this.props.stock>this.props.saldo){
    return(
    
      <td style={{backgroundColor: '#a5ffd1'}}>
        {this.state.dataCant}  
      </td>
      
    )
      }else{
      return(
        

        <td style={{backgroundColor: '  #fecfcc '}}>
        {this.state.dataCant}  
      </td>
      )

      }
    }else{


      if(this.props.stock>this.props.saldo){
        return(
        
          <td style={{backgroundColor: ' #86efb9 '}}>
            {this.state.dataCant}  
          </td>
          
        )
          }else{
          return(
            

            <td style={{backgroundColor: '#feb6b2 '}}>
            {this.state.dataCant}  
          </td>
          )
    
          }



    }
  }
}
export default Saldo;