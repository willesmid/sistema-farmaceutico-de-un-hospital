import React, { Component } from 'react';
import { 


  Table,
  Button,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
const styles2 = { color: 'white', backgroundColor: ' #273746 ', textAlign:'center' }
class DatosAlmacen extends Component {
constructor(props) {
  super(props);

  this.state = {

  };
  this.print = this.print.bind(this)
}

print(){
  /*Actual time and date */
  var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
  }
  var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
  }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();

  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 10, 7, 20,20);

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(7);
  doc.text(50, 18, "\"PROMES\"", null, null, 'center');

  doc.setFontType("normal");
  doc.setFontSize(7);
  doc.text(190,15, showDate +' '+ showTime, null, null, 'center');
  
  if(this.props.fecha!==''){
  doc.setFontType("normal");
  doc.setFontSize(8);
  doc.text(178,19, "FECHA TRANSACCIÓN: "+this.props.fecha, null, null, 'center');
  }else{
  doc.setFontType("normal");
  doc.setFontSize(8);
  doc.text(181,19, "TODAS LAS TRANSACCIONES", null, null, 'center');
  }

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(15);
  doc.text(110, 23, "BIND CARD", null, null, 'center');

  doc.setFontType("normal");   //italic
  doc.setFontSize(9);
  doc.text(15, 30, "CODIGO PRODUCTO");

  doc.setFontType("bold");
  doc.setFontSize(9);
  doc.text(60, 30, this.props.datos[0].codigo);

  doc.setFontType("normal");   //italic
  doc.setFontSize(9);
  doc.text(130, 30, "STOCK GENERAL");
 
  doc.setFontType("bold");
  doc.setFontSize(9);
  doc.text(180, 30, ""+this.props.datos[0].stockGeneral );

 doc.setFontType("normal");   //italic
  doc.setFontSize(9);
  doc.text(15, 34, "DESCRIPCIÓN");

  doc.setFontType("bold");
  doc.setFontSize(9);
  doc.text(60, 34, this.props.datos[0].nombre);

  doc.setFontType("normal");   //italic
  doc.setFontSize(9);
  doc.text(130, 34, "LOCALIZACIÓN");
 
  doc.setFontType("bold");
  doc.setFontSize(9);
  doc.text(180, 34, "ALMACEN" );

  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {},
    headerStyles: {fontSize: 8},
    bodyStyles: {fontSize: 8},
    alternateRowStyles: {},
    columnStyles: {},
    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: {top: 36, left: 15}, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number, 
    showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,
  });
  window.open(doc.output('bloburl'), '_blank');
}

render() {

return (
<div className="animated fadeIn">
  <Button size="sm" onClick={this.print} color="link" className="btn  btn-nfo float-right">
    <i className="fa fa-print"></i> IMPRIMIR BIND CARD
  </Button>  
  <Table hover bordered striped responsive size="sm">
    <thead style={styles2}>
      <tr>
        <th rowSpan="2"><p>Fecha</p></th>
        <th colSpan="2">CODIGOS</th>
        <th colSpan="2">TRANSACCION</th>
        <th colSpan="2">NUMERO LOTE</th>
   
        <th rowSpan="2"><p>Observacion</p></th>
        <th rowSpan="2"><p>Acciones</p></th>    
      </tr>
      <tr>
        <th>Receta</th>
        <th>Pedido</th>  
        <th>Salidas</th>
        <th>Entradas</th>   
        <th>Salida</th>
        <th>Nuevo</th>                           
      </tr>
    </thead>
    <tbody>

      {this.props.salidaRecetaMedicamento.map((u, i)=> {   
        return (

          <tr key = {i} >
       
            <td style={{maxWidth: '100px' }}   >{u.id_receta.fecha_entrega} </td>
            <td style={{maxWidth: '150px' }} >{u.id_receta.id_receta} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '150px'}} >{u.cantidad} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '100px'}} >{u.lote_salida+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '100px'}}></td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '500px'}}>   
            <center>            
              <Button size="sm" className="btn-stack-overflow btn-brand mr-1 mb-1"><i className="fa fa-pencil"></i><span>Editar</span></Button>
            </center> 
            </td>
          </tr>
        )
      })}


      {this.props.salidaRecetaInsumo.map((u, i)=> {
        return (
          <tr key = {i} >
        
            <td style={{maxWidth: '100px'}} >{u.id_receta.fecha_entrega} </td>
            <td style={{maxWidth: '150px'}} >{u.id_receta.id_receta} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '150px'}} >{u.cantidad} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '100px'}} >{u.lote_salida+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '100px'}} ></td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '500px'}} > 
            <center>               
              <Button size="sm" className="btn-stack-overflow btn-brand mr-1 mb-1"><i className="fa fa-pencil"></i><span>Editar</span></Button>
            </center> 
          </td>
          </tr>
        )
      })}

      {this.props.salidaPedidoProducto.map((u, i)=> {

        return (
          <tr key = {i}>
          
            <td style={{maxWidth: '100px'}}>{u.id_pedido.fecha_entrega} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '150px'}}>{u.id_pedido.id_pedido} </td>
            <td style={{maxWidth: '150px'}}>{u.cantidad_entregada} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '100px'}}>{u.lote_salida+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '100px'}}></td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '500px'}}>  
            <center>             
              <Button size="sm" className="btn-stack-overflow btn-brand mr-1 mb-1"><i className="fa fa-pencil"></i><span>Editar</span></Button>
            </center> 
          </td>
          </tr>
        )
      })}

      {this.props.entradaPedidoProducto.map((u, i)=> {
        if(u.cantidad_farmacia>0){
        return (
          <tr key = {i}>
          
            <td style={{maxWidth: '100px'}}>{u.id_pedido.fecha_recibida} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '150px'}}>{u.id_pedido.id_pedido} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '150px'}}>{u.cantidad_farmacia} </td>
            <td style={{maxWidth: '100px'}}></td>
            <td style={{maxWidth: '100px'}}>{u.lote_entrada+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '150px'}}></td>   
            <td style={{maxWidth: '500px'}}>  
            <center>             
              <Button size="sm" className="btn-stack-overflow btn-brand mr-1 mb-1"><i className="fa fa-pencil"></i><span>Editar</span></Button>
            </center> 
          </td>
          </tr>
        )
        }else{
          return null
        }
      })}


    </tbody>            

  </Table> 

{/* INICIO IMPRIMIR */}
  <Table hover bordered striped responsive size="sm" hidden id="tabla">
    <thead style={styles2}>
      <tr>
        <th>Fecha</th>
        <th>Receta</th>
        <th>Pedido</th> 
        <th>T/Salidas</th>
        <th>T/Entradas</th> 
        <th>Lote/Salida</th>
        <th>Lote/Nuevo</th> 
        <th>Observacion</th>
      </tr>

    </thead>
    <tbody>

      {this.props.salidaRecetaMedicamento.map((u, i)=> {   
        return (

          <tr key = {i} >
       
            <td style={{maxWidth: '100px' }}   >{u.id_receta.fecha_entrega} </td>
            <td style={{maxWidth: '150px' }} >{u.id_receta.id_receta} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '150px'}} >{u.cantidad} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '100px'}} >{u.lote_salida+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '100px'}}></td>
            <td style={{maxWidth: '150px'}}></td>
          </tr>
        )
      })}


      {this.props.salidaRecetaInsumo.map((u, i)=> {
        return (
          <tr key = {i} >
        
            <td style={{maxWidth: '100px'}} >{u.id_receta.fecha_entrega} </td>
            <td style={{maxWidth: '150px'}} >{u.id_receta.id_receta} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '150px'}} >{u.cantidad} </td>
            <td style={{maxWidth: '150px'}} ></td>
            <td style={{maxWidth: '100px'}} >{u.lote_salida+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '100px'}} ></td>
            <td style={{maxWidth: '150px'}} ></td>

          </tr>
        )
      })}

      {this.props.salidaPedidoProducto.map((u, i)=> {

        return (
          <tr key = {i}>
          
            <td style={{maxWidth: '100px'}}>{u.id_pedido.fecha_entrega} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '150px'}}>{u.id_pedido.id_pedido} </td>
            <td style={{maxWidth: '150px'}}>{u.cantidad_entregada} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '100px'}}>{u.lote_salida+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '100px'}}></td>
            <td style={{maxWidth: '150px'}}></td>

          </tr>
        )
      })}

      {this.props.entradaPedidoProducto.map((u, i)=> {
        if(u.cantidad_farmacia>0){
        return (
          <tr key = {i}>
          
            <td style={{maxWidth: '100px'}}>{u.id_pedido.fecha_recibida} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '150px'}}>{u.id_pedido.id_pedido} </td>
            <td style={{maxWidth: '150px'}}></td>
            <td style={{maxWidth: '150px'}}>{u.cantidad_farmacia} </td>
            <td style={{maxWidth: '100px'}}></td>
            <td style={{maxWidth: '100px'}}>{u.lote_entrada+' - '+ u.fecha_expiracion} </td>
            <td style={{maxWidth: '150px'}}></td>   
          </tr>
        )
        }else{
          return null
        }
      })}


    </tbody>            

  </Table>

{/* FIN IMPRIMIR   */}   
  </div>
  );
}
}
export default DatosAlmacen;