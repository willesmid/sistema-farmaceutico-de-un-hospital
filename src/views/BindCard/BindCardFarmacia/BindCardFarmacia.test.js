import React from 'react';
import ReactDOM from 'react-dom';
import BindCardFarmacia from './BindCardFarmacia';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BindCardFarmacia />, div);
  ReactDOM.unmountComponentAtNode(div);
});