import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row,  
  FormGroup, 
  ListGroup,
  ListGroupItem,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,

} from 'reactstrap';


import Search from 'react-search-box'
import DatosFarmacia from './DatosFarmacia';
class BindCardFarmacia extends Component {

  constructor(props) {
    super(props);
   
    this.state = {
      activeTab: 1,
      data: [],
      data2: [],
      codigo:'',
      show: false,
      num1: 0,
      num2: 0,
      list:[],
      primary:false,
      message:'',
      fecha:'',
      fechaB:'',
      salidaRecetaMedicamento:[],
      salidaRecetaInsumo:[],
      salidaPedidoProducto:[],
      entradaPedidoProducto:[],
    };
    this.togglePrimary = this.togglePrimary.bind(this)
    this.BuscaTodo = this.BuscaTodo.bind(this)
    this.BuscaFecha = this.BuscaFecha.bind(this)
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }


  async componentDidMount(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
    this.setState({
      fecha:hoy,
    })
    
    try{
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/productos/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const data = await respuesta.json();
      this.setState({
        data
      });
    }catch(e){
      console.log(e);
    }
  }

  handleChange(value){
    this.setState({
      codigo:value,
    })  
  }

 BuscaTodo(){
  if(this.state.codigo===''){
    this.setState({     
      primary: !this.state.primary,
      message: 'Ingrese codigo',
    })
  }else{
  console.log("jaja"+this.state.codigo)
    this.state.data.map((item, indice)=>{
      if (this.state.codigo===item.codigo){
        /*Para mostrar la descripcion del producto-farmaceutico con id x */
        fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              list: findresponse,
              show: true
            })
        });
        /*Para mostrar en la tabla de SALIDAS RECETAS del medicamento x */
        fetch('http://localhost:8000/farmacia/recetar/v1/BindCardRecetaBusca/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              salidaRecetaMedicamento: findresponse,
              show: true
            })
        });
        
        /*Para mostrar en la tabla de SALIDAS RECETAS del Insumo x */
        fetch('http://localhost:8000/farmacia/recetar/v1/BindCardRecetaIBusca/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              salidaRecetaInsumo: findresponse,
              show: true
            })
        });

        /*Para mostrar en la tabla de SALIDAS PEDIDOS del producto x */
        fetch('http://localhost:8000/farmacia/pedir/v1/BindCardPedidoBusca/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              salidaPedidoProducto: findresponse,
              show: true
            })
        });
   
        /*Para mostrar en la tabla de ENTRADAS PEDIDOS del producto x */
        fetch('http://localhost:8000/farmacia/pedir/v1/BindCardPedidoEBusca/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              entradaPedidoProducto: findresponse,
              show: true
            })
        });
      } 
      return 0;
    })
    this.setState({
      fechaB:'',
    })
  }
}

BuscaFecha(){
  if(this.state.codigo===''){
    this.setState({     
      primary: !this.state.primary,
      message: 'Ingrese codigo',
    })
  }else{
  console.log("jaja"+this.state.codigo, "FECHA"+ this.state.fecha)
    this.state.data.map((item, indice)=>{
      if (this.state.codigo===item.codigo){
        /*Para mostrar la descripcion del producto-farmaceutico con id x */
        fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+item.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              list: findresponse,
              show: true
            })
        });
        /*Para mostrar en la tabla de SALIDAS RECETAS del medicamento x */
        fetch('http://localhost:8000/farmacia/recetar/v1/BindCardRecetaBuscaFecha/'+item.codigo+'/'+this.state.fecha+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              salidaRecetaMedicamento: findresponse,
              show: true
            })
        });
        
        /*Para mostrar en la tabla de SALIDAS RECETAS del Insumo x */
        fetch('http://localhost:8000/farmacia/recetar/v1/BindCardRecetaIBuscaFecha/'+item.codigo+'/'+this.state.fecha+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              salidaRecetaInsumo: findresponse,
              show: true
            })
        });

        /*Para mostrar en la tabla de SALIDAS PEDIDOS del producto x */
        fetch('http://localhost:8000/farmacia/pedir/v1/BindCardPedidoBuscaFecha/'+item.codigo+'/'+this.state.fecha+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              salidaPedidoProducto: findresponse,
              show: true
            })
        });
   
        /*Para mostrar en la tabla de ENTRADAS PEDIDOS del producto x */
        fetch('http://localhost:8000/farmacia/pedir/v1/BindCardPedidoEBuscaFecha/'+item.codigo+'/'+this.state.fecha+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
            this.setState({
              entradaPedidoProducto: findresponse,
              show: true
            })
        });
      } 
      return 0;
    })
      this.setState({
        fechaB:this.state.fecha
      })
  }

  }
  render() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
    
    return (
      <div className="animated fadeIn">
        <Row>
      
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
              <h3><i className="fa fa-th-list"></i> <strong>BIND CARD FARMACIA</strong></h3>
              </CardHeader>
              <CardBody>
              <Row>
                <Col xs="12" md="12">
                  <form>
                    <FormGroup row>
                      <Col md="4">
                        <Search
                          data = {this.state.data}
                          onChange={ this.handleChange.bind(this) }
                          placeholder="Ingrese Codigo"
                          class="search-class"
                          searchKey="codigo"
                        />
                      </Col>
                      <Col xs="12" md="3">
                      <Input onChange={e => { this.setState({ fecha: e.target.value }) }} defaultValue={hoy} type="date" id="date-input" name="date-input" placeholder="date" />
                      </Col>
                      <Col xs="12" md="2">
                      <h3>
                      <Button onClick={ this.BuscaFecha}  size="sm" className="btn-twitter btn-brand mr-1 mb-1"><i className=" 	fa fa-search"></i><span>Buscar por Fecha</span></Button>
                      </h3>
                      </Col> 
                      <Col  md="3">
                      <h3>
                      <Button onClick={ this.BuscaTodo} size="sm" className="btn-xing btn-brand mr-1 mb-1"><i className="fa fa-copy"></i><span>Ver Todas las transacciones</span></Button>
                      </h3>
                      </Col>    
                      </FormGroup>
                      {
                      this.state.list.map((item, indice)=>{
                        return (
                      <ListGroup key={indice}>
                        <FormGroup row>
                        <Col md="6">
                          <ListGroupItem className="justify-content-between">CODIGO PRODUCTO <strong className="float-right">{item.codigo}</strong></ListGroupItem>
                          <ListGroupItem className="justify-content-between">DESCRIPCION<strong className="float-right">{item.nombre}</strong> </ListGroupItem>
                          </Col>
                          <Col md="6">
                          <ListGroupItem className="justify-content-between">STOCK GENERAL <strong className="float-right">{item.stockGeneral} unidades</strong ></ListGroupItem>
                          <ListGroupItem className="justify-content-between">LOCALIZACION <strong className="float-right">{"FARMACIA"}</strong ></ListGroupItem>
                          </Col>
                          </FormGroup>
                      </ListGroup>
                    )})}
                  </form>
                </Col>

              </Row>
              {this.state.show && 
              <DatosFarmacia  data2={this.state.data2} salidaRecetaMedicamento={this.state.salidaRecetaMedicamento}
              salidaRecetaInsumo={this.state.salidaRecetaInsumo} salidaPedidoProducto={this.state.salidaPedidoProducto}
              entradaPedidoProducto={this.state.entradaPedidoProducto}
              datos={this.state.list}
              fecha={this.state.fechaB}
              />
              }
              </CardBody> 
            </Card>
          </Col>
    
        </Row>

{/* MODAL ADVERTENCIA */}
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
      <ModalBody>
        <strong>{this.state.message}</strong>
      </ModalBody>
    <ModalFooter>
      <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
    </ModalFooter>
  </Modal>
{/* FIN MODAL ADVERTENCIA */}

      </div>
    );
  }
}

export default BindCardFarmacia;
