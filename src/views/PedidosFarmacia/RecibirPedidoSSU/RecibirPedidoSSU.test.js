import React from 'react';
import ReactDOM from 'react-dom';

import RecibirPedidoSSU from './RecibirPedidoSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RecibirPedidoSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});
