import React, { Component } from 'react';
import { 
  Button, 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup,
  Collapse,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Alert,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import ValidarPersonal2 from './ValidarPersonal2'
var urlLote='http://localhost:8000/farmacia/almacena/v1/LoteGuardar/' 
var urlLoteProducto='http://localhost:8000/farmacia/almacena/v1/Lote_ProductoGuardar/'
var urlEstaUbicado='http://localhost:8000/farmacia/almacena/v1/EstaUbicadoRegistra/'

const styles2 = { color: 'white', backgroundColor: '#0b5345', textAlign:'center' }
class ElegirRSSU extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url_pedido: '#/listaPedidos/ElegirRSSU',
      productos: [],
      productos2:[],
      personalList:[],
      productosImp:[],
      idUnidad:0,
      nombreP:'',
      apellidosP:'',
      collapse2: true,
      productosE:[],
      prodE:'',
      NomE:'',
      posicion:0,
      showL:false,
      primary:false,
      message:'',
      fecha_e:'',
      lote:'',

      primaryLote:false,
      primaryLote2:false,
      showGuardaL:true,
      showYaGuardadoL:false,

      cantidad_R:0,
      cantidad_A:0,
      cantidad_F:0,
      idProd:0,
      cantidadGeneral:0,

      primaryCant:false,
      cantidadGeneralM:0,
      cantF:0,
      cantA:0,
      primaryT:false,
      primary4:false,
      recibido_por:'',
      existeLote:'',
      existeLoteF:'',
      fecha_aux:'',
      showImp:false,
      primaryPrint:false,
      pedidoFecha_entrega:'',
      showAlert:false,
    }
    console.log(this.props.match.params)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.toggleLote = this.toggleLote.bind(this)
    this.toggleLote2 = this.toggleLote2.bind(this)
    this.toggleDatos = this.toggleDatos.bind(this)
    this.togglePrint =this.togglePrint.bind(this)
    this.togglePrimary4 = this.togglePrimary4.bind(this)
  
    this.onValidacionLote = this.onValidacionLote.bind(this)
    this.onGuardarLote = this.onGuardarLote.bind(this)
    this.onValidacionCantidad = this.onValidacionCantidad.bind(this)
    this.onRecibirProd = this.onRecibirProd.bind(this)
    this.togglePrimaryCant = this.togglePrimaryCant.bind(this)

    this.onValidacionT = this.onValidacionT.bind(this) 
    this.togglePrimaryT = this.togglePrimaryT.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
    this.print = this.print.bind(this);
    this.onValidacionImp = this.onValidacionImp.bind(this);

  }
  toggleDatos() {
    this.setState({ collapse2: !this.state.collapse2 });
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }
  
  toggleLote() {
    this.setState({ primaryLote: !this.state.primaryLote});
  }

  toggleLote2() {
    this.setState({ primaryLote2: !this.state.primaryLote2});
  }
  
  togglePrimaryT() {
    this.setState({ primaryT: !this.state.primaryT});
  }
  togglePrimaryCant() {
    this.setState({ primaryCant: !this.state.primaryCant});
  }
  togglePrint() {
    this.setState({ primaryPrint: !this.state.primaryPrint});
  }
  togglePrimary4() {
    if(this.state.showImp===true){
    this.setState({
      primary4: !this.state.primary4,
    });
    this.props.history.push('/PedidosFarmacia/PedidosSSU/PedidosRecibidos')
  }else{
    this.setState({
      showAlert: true,
    });
  }
  }
  onCancelar(){
    this.props.history.push('/PedidosFarmacia/PedidosSSU/PedidosRecibidos')
  }
componentDidMount() {
  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoSSUBusca/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productosImp: findresponse,
        })
    });

  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoSSUBusca2/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productos: findresponse,
          productosE:findresponse,
          url_pedido : this.state.url_pedido +'/'+ this.props.match.params.id_pedido +'/'+ this.props.match.params.fecha +'/'+ this.props.match.params.unidadNombre +'/'+ this.props.match.params.personalNombre +'/'+this.props.match.params.personalApellidos+'/'+ this.props.match.localizacion +'/resultados',
        })
    });
    
    fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoSSUBusca3/'+this.props.match.params.id_pedido+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
      .then((Response)=>Response.json())
      .then((findresponse) => {
        console.log("FINDRESPONSE=",findresponse)
          this.setState({
            productos2: findresponse,
          
          })
      });
      
    
}

// OnValidacion //



onGuardarLote(){
  let data2={
    id_lote: this.state.lote,
    fecha_expiracion: this.state.fecha_e,
  }
  try{
    fetch(urlLote, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data2), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json() )
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }
  
  
  fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.state.prodE+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
  console.log("FINDRESPONSE=",findresponse)
    this.setState({
      cantidadGeneral: findresponse[0].stockGeneral
    })
    console.log("Cantidad General "+this.state.cantidadGeneral)
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/LocalizacionBuscaCodsuma/'+1+'/'+this.state.prodE+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse)=>{    
    this.setState({
        cantF: findresponse[0].sum
    });
  console.log("Cantidad Sumada: "+this.state.dataCant)
  })
 
  fetch('http://localhost:8000/farmacia/almacena/v1/LocalizacionBuscaCodsuma/'+2+'/'+this.state.prodE+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse)=>{    
    this.setState({
        cantA: findresponse[0].sum
    });
  console.log("Cantidad Sumada: "+this.state.dataCant)
  })
  
  this.setState({
    showL:true,
    primaryLote:false,
    primaryLote2:false,
    showGuardaL:false,
    showYaGuardadoL:true,
  })   
}

onValidacionLote(){
  if(this.state.prodE===''){
    this.setState({
      primary: !this.state.primary,
      message: 'Seleccione producto'
    })
    }else{
      if(this.state.fecha_e ==='' || this.state.lote ==='' ){
        this.setState({
          primary: !this.state.primary,
          message: 'Registre datos de Lote'
        })
      }
    }
    if(this.state.prodE!=='' && this.state.fecha_e !=='' && this.state.lote !==''){

      ////// aqui /////////
      fetch('http://localhost:8000/farmacia/almacena/v1/LoteBusca/'+this.state.lote+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
        if(findresponse.length>0){  

            this.setState({
              existeLote:findresponse[0].id_lote,
              existeLoteF:findresponse[0].fecha_expiracion,
              fecha_e:findresponse[0].fecha_expiracion,
            })
          
  
          fetch('http://localhost:8000/farmacia/almacena/v1/Lote_ProductoBuscaL/'+this.state.lote+'/',{
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            } 
          })
          .then((Response)=>Response.json())
          .then((findresponse) => {
            if(findresponse.length>0){
              this.setState({
                primary: !this.state.primary,
                message: 'Existe Lote y esta registrado con un producto ',
                
              })
            }else{
              this.setState({
                primaryLote2: !this.state.primaryLote2,
                message: 'Existe Lote y no esta registrado con un producto ',
            
              })

            }
          
          })
        }else{

      this.setState({
        primaryLote: !this.state.primaryLote,
        message: 'Seleccione producto',
        fecha_e:this.state.fecha_aux,
        
      })     
        }
      });


/////////// hasta qui ///////////////

    }  
}

onValidacionCantidad(){

if(this.state.cantidad_R>0){
if(this.state.cantidad_R===''){
  this.setState({
    primary: !this.state.primary,
    message: 'Ingrese Cantidad',
  })
}else{
  if(this.state.cantidad_A===''){
    this.setState({
      cantidadGeneralM: parseInt(this.state.cantidadGeneral,10)+parseInt(this.state.cantidad_R,10),
      primary: !this.state.primary,
      message: 'Ingrese Cantidad Almacen',
    })
  }else{

  if(parseInt(this.state.cantidad_A,10) <=parseInt(this.state.cantidad_R,10)){
    this.setState({
      primaryCant: !this.state.primaryCant,
      message: 'Esta Bien',
      cantidad_F: parseInt(this.state.cantidad_R,10)- parseInt(this.state.cantidad_A,10),
    })

  }else{
    this.setState({
      primary: !this.state.primary,
      message: 'La Cantidad en Almacen debe ser menor o igual a la Cantidad Total',
      cantidad_F: 0,
    })
}}
}
}else{
  this.setState({
    primary: !this.state.primary,
    message: 'La Cantidad a Recibir debe ser mayor a 0',
    cantidad_F: 0,
  })
}
}

onRecibirProd(e){
// GUARDA LOTE-PRODUCTO 
  let data={
    cantidad: this.state.cantidad_R,
    producto_farmaceutico: this.state.prodE,
    lote:this.state.lote
  }
  try{
    fetch(urlLoteProducto, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }
//GUARDA ESTA UBICADO FARMACIA
if(this.state.cantidad_F>0){
  let dataEF={
    stockAlmacen: this.state.cantidad_F,
    producto_farmaceutico: this.state.prodE,
    lote:this.state.lote,
    almacen:1,
  }
  try{
    fetch(urlEstaUbicado, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(dataEF), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }
}
//GUARDA ESTA UBICADO ALMACEN
if(this.state.cantidad_A>0){
let dataEA={
  stockAlmacen: this.state.cantidad_A,
  producto_farmaceutico: this.state.prodE,
  lote:this.state.lote,
  almacen:2,
}
try{
  fetch(urlEstaUbicado, {
    method: 'POST', // or 'PUT'
    body: JSON.stringify(dataEA), // data can be string or {object}!
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
  }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));              
} catch (e) {
  console.log(e);
}
}
//EDITA CANTIDAD GENERAL
let urlGeneral = ''
let dataGeneral = {}
urlGeneral = 'http://localhost:8000/farmacia/almacena/v1/ProductoEdita/'+this.state.prodE+'/'
const numGeneral= parseInt(this.state.cantidadGeneral,10) + parseInt(this.state.cantidad_R,10)
console.log('nueva cantidad SUMADA : '+ numGeneral)
dataGeneral = {
  stockGeneral : numGeneral
}
try {
  fetch(urlGeneral, {
  method: 'PUT',
  body: JSON.stringify(dataGeneral),
  headers:{
    'Content-Type': 'application/json',
    Authorization: `JWT ${localStorage.getItem('token')}`
  }
  }).then(res => res.json())
  .catch(error => console.error('Error:', error))
  .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

} catch (e) {
    console.log(e);
}
this.setState({
  showEntrega:false,
})

//EDITA PRODUCTO-PEDIDO (CANTIDAD ENTREGADA)
  let urlCantidadEntregada = ''
  let dataCantidadEntregada = {}
  urlCantidadEntregada = 'http://localhost:8000/farmacia/pedir/v1/ProductoPedidoECantidadEdita/'+this.state.idProd+'/'
  dataCantidadEntregada = {
    cantidad_recibida:this.state.cantidad_R,
    lote_entrada:this.state.lote,
    fecha_expiracion:this.state.fecha_e,
    cantidad_farmacia: this.state.cantidad_F,
    cantidad_almacen: this.state.cantidad_A,
  }
  try {
    fetch(urlCantidadEntregada, {
    method: 'PUT',
    body: JSON.stringify(dataCantidadEntregada),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
  } catch (e) {
      console.log(e);
  }
  e.preventDefault();
  this.setState({
    posicion: this.state.posicion+1,
    showL:false,
    primaryCant:false,
    showGuardaL:true,
    showYaGuardadoL:false,
    lote:0,
    fecha_e:'',
    prodE:'',
    cantidad_F:'',
    cantidad_A:'',
  })
  this.myFormRef.reset();
}
onValidacionImp(){
  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoSSUBusca/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productosImp: findresponse,
        })
    });
      this.setState({     
        primaryPrint: true,
      })      
}

onValidacionT(){
  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoSSUBusca/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productosImp: findresponse,
        })
    });
  
if(this.state.posicion!==this.state.productos.length){
  this.setState({     
    primary: !this.state.primary,
    message: 'Faltan productos por recibir',
  })
}else{
  if(this.state.recibido_por===''){
    this.setState({     
      primary: !this.state.primary,
      message: 'Asignar Personal del que recibio el Pedido', 
    })
  } 
}
if(this.state.posicion===this.state.productos.length && this.state.recibido_por!==''){
  this.setState({     
    primaryT: !this.state.primaryT,
  })
}   
}

onFormSubmit(e){
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options) 

  let urlTermina = ''
  let dataTermina = {}
  urlTermina = 'http://localhost:8000/farmacia/pedir/v1/PedidoSSUEstadoEdita/'+this.props.match.params.id_pedido+'/'
  dataTermina = {
    estado : 'Recibido',
    personal_externo : this.state.recibido_por,
    fecha_recibida: hoy,
    hora_recibida: hora,
  }
  try {
    fetch(urlTermina, {
    method: 'PUT',
    body: JSON.stringify(dataTermina),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then((res) => {res.json(); console.log('respuesta 1:', res.status);    
    if(res.status === 200){
      this.setState({
        primaryT: !this.state.primaryT,
        primary4: !this.state.primary4,
        pedidoFecha_entrega:hoy+" "+hora,
        showImp:false,
        showAlert:false,
      }); 
    }
  })
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
  } catch (e) {
      console.log(e);
  }
  // this.props.history.push('/PedidosFarmacia/PedidosSSU/PedidosRecibidos')

  // this.setState({ 
  //   primary2: !this.state.primary2,
  // })
}

print(){

  /*Actual time and date */
    /*Actual time and date */
    var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }
    var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }

  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();
  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 9, 7, 20,20);
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(53, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(51, 19, "\"PROMES\"", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(154, 15,  "NUMERO DE PEDIDO:");
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(195, 15, " "+this.props.match.params.id_pedido);
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  // doc.text(158, 19, "LOCALIZACION :");

  doc.setFontType("normal");
  doc.setFontType("bold"); 
  doc.setFontSize(8);
  doc.text(154, 19, "SEGURO SOCIAL UNIVERSITARIO")
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 25, "PEDIDOS DE MEDICAMENTOS", null, null, 'center');

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 29, "E INSUMOS MEDICOS", null, null, 'center');
  
  
  doc.setFontType("normal");
  doc.setFontSize(7);
  doc.text(189,23, showDate +' '+ showTime, null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.setFontType("bold");
  doc.text(10,36, "UNIDAD SOLICITANTE:");
  
  doc.setFontType("normal");
  doc.text(48, 36, "PROMES - FARMACIA")
  
  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
  
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 38, left: 10, right: 52}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
  });

  doc.setFontType("bold");
  doc.setFontSize(8);
  doc.text(165, 40, "Realizado Por: ");

  doc.setFontType("normal");
  doc.text(165, 51, "Dr. "+this.props.match.params.personalNombre+" "+this.props.match.params.personalApellidos);
  doc.setFontType("bold");
  doc.text(165, 55, "Nombre");

  doc.setFontType("normal");
  doc.text(165, 62, ""+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision);
  doc.setFontType("bold");
  doc.text(165, 66, "Fecha");

  doc.setFontType("normal");
  doc.text(165, 78, "................................................");
  doc.setFontType("bold");
  doc.text(165, 82, "Firma");

  doc.setFontType("bold");
  doc.text(165, 85, "________________________");

  doc.setFontType("bold");
  doc.text(165, 90, "Entregado por: ");

  if(this.state.recibido_por !==''){
    doc.setFontType("normal");
    doc.text(165, 101, ""+this.state.recibido_por);
    doc.setFontType("bold");
    doc.text(165, 105, "Nombre");
  
    doc.setFontType("normal");
    doc.text(165, 112, ""+this.state.pedidoFecha_entrega);
    doc.setFontType("bold");
    doc.text(165, 116, "Fecha");

  }else{
    doc.setFontType("normal");
    doc.text(165, 101, "................................................");
    doc.setFontType("bold");
    doc.text(165, 105, "Nombre");
  
    doc.setFontType("normal");
    doc.text(165, 112, ".............../.............../...............");
    doc.setFontType("bold");
    doc.text(165, 116, "Fecha");
  }

  doc.setFontType("normal");
  doc.text(165, 128, "................................................");
  doc.setFontType("bold");
  doc.text(165, 132, "Firma");

  doc.setFontSize(10);
  doc.text(10, 155, "_______________________");
  doc.text(13, 160, "UNIDAD SOLICITANTE");
  
  doc.text(72, 155, "__________________________________");
  doc.text(75, 160, "Vo. Bo. JEFE UNIDAD SOLICITANTE");

  doc.text(150, 155, "___________________________");
  doc.text(154, 160, "Vo. Bo. GERENTE MEDICO");
  this.setState({ showImp: true, primaryPrint:false, showAlert:false});
 
  window.open(doc.output('bloburl'), '_blank'); 
}


render() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options) 
  const table=this.state.productosImp.map((u, i)=> {
    if(u.cantidad_recibida!==null){
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.codigo.codigo} </td>
        <td>{u.codigo.nombre}</td>
        <td>{u.unidadProd}</td>
        <td>{u.cantidad_solicitada}</td>
        <td>{u.cantidad_recibida+" ("+u.lote_entrada+"  "+u.fecha_expiracion+")"}</td>
      </tr>
    )
  }else{
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.codigo.codigo} </td>
        <td>{u.codigo.nombre}</td>
        <td>{u.unidadProd}</td>
        <td>{u.cantidad_solicitada}</td>
        <td></td>
      </tr>
    )

    }
  })
  return (
  <div>
    <Table responsive striped hidden id="tabla">
    <thead>
        <tr>
          <th>No.</th>
          <th>CODIGO</th>
          <th>DETALLE</th>
          <th>UNIDAD</th>
          <th>Solicitada</th>
          <th>Entregada</th>            
        </tr>
    </thead>
    <tbody>
    {table}
    </tbody>
    </Table>
    <Row>
      <Col xs="12" md="12">
        <Card>
          <CardHeader>
          <i className="fa fa-list-alt"></i><strong>Recibir Pedido del Seguro Social Universitario</strong>
            <Button  onClick={this.onValidacionImp} color="link" className="btn btn-sm btn-nfo float-right">
              <i className="fa fa-print"></i> Imprimir Pedido
            </Button>
          </CardHeader>
          <CardBody>                
          <Collapse isOpen={this.state.collapse2} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
            <Form >
              <FormGroup row>  
                <Col md="2">
                <Label htmlFor="text-input">UNIDAD SOLICITANTE</Label>
                </Col> 
                <Col md="7" >        
                <Input type="text" id="text-input" name="codigoAlifiacion" value={"PROMES - "+this.props.match.params.localizacion}/>           
                </Col> 
                <Col md="1">
                <Label htmlFor="text-input">N°Pedido</Label>
                </Col>
                <Col md="2">
                <Input type="text"  id="text-input" name="id_recetaA" value={this.props.match.params.id_pedido}  disabled/>
                </Col>  
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input">Pedido Realizado por</Label>                     
                </Col>
                <Col md="4">
                <ValidarPersonal2 idUnidad={this.props.match.params.unidadId} nombreP={this.props.match.params.personalNombre} apellidosP={this.props.match.params.personalApellidos}/>
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">F/Emisión</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="date-input" name="fecha" placeholder="Fecha" value={this.props.match.params.fecha_emision} disabled />
                </Col>
                <Col md="1">
                <Label htmlFor="hora">H/Emisión</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control" value={this.props.match.params.hora_emision} disabled  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input"> Unidad a Solicitar</Label>
                </Col> 
                <Col md="4" >
                <Input type="text" id="text-input" name="cedulaIdentidad" value={this.props.match.params.unidad}  disabled/>
                </Col> 
                <Col md="1">
                <Label htmlFor="fecha">F/Recibida</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="date-input" name="fecha" placeholder="Fecha" value={hoy} disabled/>
                </Col>
                <Col md="1">
                <Label htmlFor="hora">H/Recibida</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control" value={hora} disabled  />
                </Col>
              </FormGroup>
            </Form>
            </Collapse>

              <FormGroup row>
                <Col xs="12" md="1">
                <Button size="sm" color="secondary"  onClick={this.toggleDatos} ><i className="icon-note"></i><span> Datos Pedido</span></Button>
                </Col> 
                <Col md="5">
                </Col>
                <Col  xs="12" md="1">
                <Label htmlFor="text-input">Recibido</Label>
                </Col> 
                <Col xs="12" md="5" >
                <Input type="text" name="recibido_por" id="recibido_por" placeholder="recibido por" onChange={e => this.setState({ recibido_por : e.target.value })}  required   />
                </Col> 
              </FormGroup>

             <form  ref={(el) => this.myFormRef = el}>
              <FormGroup row> 
                <Col xs="12" md="1">
                <Label htmlFor="text-input">Codigo</Label>
                </Col> 
                <Col xs="12" md="3" >
                <Input type="text" name="unidad" id="unidad" value={this.state.prodE}/>
                </Col> 
                <Col md="8">
                  <Table hover bordered striped responsive size="sm">
                  <tbody>
                    <tr>
                      <td bgcolor="  #a3e4d7 " style={{maxWidth: '70px'}}>
                      <Label htmlFor="text-input"><span> Codigo Lote </span><i className=" fa fa-dropbox"></i></Label>
                      </td>                            
                      <td bgcolor="#a3e4d7" style={{maxWidth: '100px'}}>
                      <Input type="text" name="lote" id="lote" required onChange={e => this.setState({ lote : e.target.value })}/>
                      </td>
                      <td bgcolor="#a3e4d7" style={{maxWidth: '90px'}} >
                      <Label htmlFor="date-input"><span> Fecha Expiración </span><i className="fa fa-calendar"></i></Label>
                      </td>
                      <td bgcolor="#a3e4d7" style={{maxWidth: '100px'}}>
                      <Input  type="date" id="fecha_e" name="fecha_e" placeholder="date" onChange={e => this.setState({ fecha_e: e.target.value, fecha_aux:e.target.value })}/>
                      </td>
                      <td bgcolor="#a3e4d7" style={{maxWidth: '70px'}}>
                      {this.state.showGuardaL && 
                      <center><Button size="sm" onClick={this.onValidacionLote} className="btn-vine btn-brand mr-1 mb-1"><i className="fa fa-save"></i><span>Guardar</span></Button></center>
                      }
                      {this.state.showYaGuardadoL &&
                      <center><Button size="sm"  className="btn-vine btn-brand mr-1 mb-1" disabled><i className="fa fa-check-square-o"></i><span>Guardado</span></Button></center>
                      }
                      </td>
                    </tr>
                  </tbody>
                  </Table>
                </Col>
              </FormGroup>
              
              {this.state.showL && 
              <FormGroup row>
                <Col xs="12" md="1">
                <Label htmlFor="text-input">Cantidad</Label>
                </Col> 
                <Col xs="12" md="3" >
                <Input type="number" id="cantidad" name="cantidad" className="form-control" onChange={e => this.setState({ cantidad_R : e.target.value })}/>
                </Col> 
                <Col md="8">
                  <Table hover bordered striped responsive size="sm">
                  <tbody>
                    <tr>
                      <td bgcolor=" #aed6f1 " style={{maxWidth: '70px'}}>
                      <Label htmlFor="text-input"><span> Almacen </span><i className="fa fa-file-text-o"></i></Label>
                      </td>                            
                      <td bgcolor=" #aed6f1 " style={{maxWidth: '100px'}}>
                      <Input type="number" id="cantidadA" name="cantidadA" className="form-control" onChange={e => this.setState({ cantidad_A : e.target.value })}/>
                      </td>
                      <td bgcolor=" #aed6f1 " style={{maxWidth: '90px'}} >
                      <Label htmlFor="date-input"><span> Farmacia </span><i className="fa fa-file-text"></i></Label>
                      </td>
                      <td bgcolor=" #aed6f1 " style={{maxWidth: '100px'}}>
                      <Input  type="number" id="cantidadF" name="cantidadF" className="form-control" value={this.state.cantidad_F} disabled />
                      </td>
                      <td bgcolor=" #aed6f1 " style={{maxWidth: '60px'}}>
                      <center><Button size="sm" onClick={this.onValidacionCantidad} className="btn-github btn-brand mr-1 mb-1"><i className="fa fa-hand-lizard-o"></i><span>Recibir</span></Button></center>
                      </td>
                    </tr>
                  </tbody>
                  </Table>
                </Col>      
              </FormGroup>
              }
              
              <Table hover bordered striped responsive size="sm">
                <thead style={styles2}>
                <tr >
                  <th rowSpan="2"><p></p><p>Numero</p></th>
                  <th rowSpan="2"><p>Codigo</p></th>
                  <th rowSpan="2"><p>Detalle</p></th>
                  <th rowSpan="2"><p>Unidad</p></th>
                  <th rowSpan="2"><p>Cantidad Solicitada</p></th> 
                  <th rowSpan="2" bgcolor="#1d8348"><p>Acciones</p></th> 
                </tr>
                </thead>
                <tbody>
                  {this.state.productos.map((u, i)=> {
                    return (
                      <tr key = {i}>
                        <td style={{maxWidth: '5px', textAlign: 'center'}}>{i+1}</td>
                        <td style={{maxWidth: '50px'}} >{u.codigo.codigo} </td>
                        <td style={{maxWidth: '200px'}}>{u.codigo.nombre}</td>
                        <td style={{maxWidth: '50px'}}>{u.unidadProd}</td>
                        <td style={{maxWidth: '10px'}}>{u.cantidad_solicitada}</td>
                        <td style={{maxWidth: '200px'}} colSpan="2">                            
                          {(this.state.posicion === i)  && 
                            <center>
                              <Button size="sm" className="btn-twitter btn-brand mr-1 mb-1"  onClick={e => { this.setState({ prodE: u.codigo.codigo, NomE:u.codigo.nombre, idProd:u.id }) }} ><i className=" 	fa fa-hand-o-left"></i><span>Elegir</span></Button>
                            </center>
                          }                       
                          {(this.state.posicion > i)&&  
                          <center>
                            <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check-square-o"></i><span>Recibido</span></Button> 
                          </center>  
                          }                       
                          {(this.state.posicion < i )  &&  
                          <center>
                            <Button size="sm" color="secondary"  disabled><i className="icon-note"></i><span> Por Recibir</span></Button>
                          </center>
                          }                       
                        </td>
                      </tr>
                    )
                  })}
                  {this.state.productos2.map((u, i)=> {                 
                    return (
                      <tr key = {i}>
                        <td style={{maxWidth: '5px', textAlign: 'center'}}>{i+1}</td>
                        <td style={{maxWidth: '50px'}} >{u.codigo.codigo} </td>
                        <td style={{maxWidth: '200px'}}>{u.codigo.nombre}</td>
                        <td style={{maxWidth: '50px'}}>{u.unidadProd}</td>
                        <td style={{maxWidth: '10px'}}>{u.cantidad_solicitada}</td>
                        <td style={{maxWidth: '200px'}} colSpan="2">                            
                     
                          {(u.cantidad_recibida!==null)&&  
                          <center>
                            <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check-square-o"></i><span>Recibido</span></Button> 
                          </center>  
                          }                       
                        </td>
                      </tr>
                    )
                  })}
                               
                </tbody>
              </Table>
            </form>
          {/* FIN DE REGISTRO */}
          <center>
          <Button onClick={this.onValidacionT} className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-check-circle"></i><span>Terminar Recepción</span></Button>
          <Button onClick={this.onCancelar} className="btn-google-plus btn-brand mr-1 mb-1"><i className="fa fa-times-circle"></i><span>Cancelar Recepción</span></Button>
          </center>
          {/* FIN DE REGISTRO */}
          </CardBody>
        </Card>
      </Col>    
    </Row>
  {/* MODAL ADVERTENCIA */}
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
      <ModalBody>
        <strong>{this.state.message}</strong> 
      </ModalBody>
    <ModalFooter>
      <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL ADVERTENCIA */}

  {/* MODAL IMPRESION*/}
  <Modal isOpen={this.state.primaryPrint} toggle={this.togglePrint} className={this.props.className}>
    <ModalHeader toggle={this.togglePrint}><i className="fa fa-print"></i> <strong> Imprimir Pedido</strong></ModalHeader>
      <ModalBody>
       Falta Terminar de Recibir 
       ¿Desea Imprimir Pedido?
      </ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={this.print}>Aceptar</Button>{' '}
      <Button color="secondary" onClick={this.togglePrint}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL IMPRESION*/}
  
  {/* MODAL REGISTRAR LOTE */}
  <Modal isOpen={this.state.primaryLote }  className={'modal-success ' + this.props.className}>
    <ModalHeader toggle={this.toggleLote}>Registra Lote</ModalHeader>
      <ModalBody>
      <center>
        <strong>¿Desea Registrar nuevo Lote?</strong>
        </center>
        <strong>{"Datos Producto "}</strong> 
        <br/>
        <strong>{" Codigo: "}</strong>
        {this.state.prodE+" "}
        <strong>{"Nombre: "}</strong>
        {this.state.NomE}
        <br/>
        <strong>{"Datos Lote "}</strong> 
        <br/>
        <strong>{" Lote :  "}</strong>
        {this.state.lote+" "}
        <strong>{"Fecha Expiracion:  "}</strong>
        {this.state.fecha_e}
        <br/>
      </ModalBody>
    <ModalFooter>
      <Button color="success" onClick={this.onGuardarLote}>Si</Button>{' '}
      <Button color="secondary" onClick={this.toggleLote}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL REGISTRAR LOTE */}

    {/* MODAL REGISTRAR LOTE 2*/}
    <Modal isOpen={this.state.primaryLote2 }  className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.toggleLote2}>Registra Lote</ModalHeader>
      <ModalBody>
      <center>
        <strong>Existe Lote Registrado ¿Desea asignar al producto?</strong>
        </center>
        <strong>{"Datos Producto "}</strong> 
        <br/>
        <strong>{" Codigo: "}</strong>
        {this.state.prodE+" "}
        <strong>{"Nombre: "}</strong>
        {this.state.NomE}
        <br/>
        <strong>{"Datos Lote "}</strong> 
        <br/>
        <strong>{" Lote :  "}</strong>
        {this.state.existeLote+" "}
        <strong>{"Fecha Expiracion:  "}</strong>
        {this.state.existeLoteF}
        <br/>
      </ModalBody>
    <ModalFooter>
      <Button color="warning" onClick={this.onGuardarLote}>Si</Button>{' '}
      <Button color="secondary" onClick={this.toggleLote2}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL REGISTRAR LOTE 2*/}

  {/* MODAL REGISTRAR DATOS */}
  <Modal isOpen={this.state.primaryCant}  className={'modal-primary ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimaryCant}>Confirmación</ModalHeader>
      <ModalBody>
        <center>
        ¿Desea registrar los datos?
        </center>
        <br/>
        <strong>{"Datos Producto "}</strong> 
        <br/>
        <strong>{" Codigo: "}</strong>
        {this.state.prodE+" "}
        <strong>{"Nombre: "}</strong>
        {this.state.NomE+" "}
        <strong>{"Codigo Lote "}</strong> 
        {this.state.lote+" "}
        <br/>
        <strong>{" Cantidad a Recibir :  "}</strong>
        {this.state.cantidad_R}
        <br/>
        <strong>{"Recibir en ALMACEN: "}</strong>
        {this.state.cantidad_A+'  ' }
        <strong>{"Recibir en FARMACIA: "}</strong>
        {this.state.cantidad_F}
        <br/>
        <center>              
        <Col xs="12" md="12">
        <Alert color="success">
        <strong>{"Cantidad Existente PROMES: "}</strong>{this.state.cantidadGeneral} <strong>{" Nuevo Saldo: "}</strong>{ parseInt(this.state.cantidadGeneral,10)+parseInt(this.state.cantidad_R,10)}
        <br/>
        <hr/>
        <strong>{"Cantidad Existente ALMACEN: "}</strong>{this.state.cantA} <strong>{" Nuevo Saldo: "}</strong>{ parseInt(this.state.cantA,10)+parseInt(this.state.cantidad_A,10)}
        <br/>
        <strong>{"Cantidad Existente FARMACIA: "}</strong>{this.state.cantF} <strong>{" Nuevo Saldo: "}</strong>{ parseInt(this.state.cantF,10)+parseInt(this.state.cantidad_F,10)}
        </Alert>
        </Col>
        </center>
      </ModalBody>
    <ModalFooter>
      <Button onClick={this.onRecibirProd} type="submit" color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimaryCant}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL REGISTRAR DATOS */}

  {/* MODAL TERMINAR RECIBIR */}
  <Modal isOpen={this.state.primaryT} toggle={this.togglePrimaryT} className={'modal-primary ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimaryT}>Confirmación</ModalHeader> 
      <ModalBody>
        ¿Desea terminar de recibir los productos?
      </ModalBody>
    <ModalFooter>           
      <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimaryT}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL TERMINAR RECIBIR */}

{/* MODAL DATOS REGISTRADOS */}
<Modal isOpen={this.state.primary4} toggle={this.togglePrimary4} className={'modal-success ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimary4}><strong>Correcto</strong></ModalHeader>
  <ModalBody>
    Los datos se registraron correctamente.
    {this.state.showAlert && <Alert color="danger"><strong>{"No realizo la impresion"}</strong></Alert>}
    <Button  onClick={this.print} color="link" className="btn btn-nfo float-right">
      <i className="fa fa-print"></i> Imprimir Pedido
    </Button>
  </ModalBody>
   <ModalFooter>
     <Button color="success" onClick={this.togglePrimary4}>Aceptar</Button>
    </ModalFooter>
 </Modal>
{/* FIN DATOS REGISTRADOS */}
  
  </div>
  )}
}

export default ElegirRSSU;
