import React from 'react';
import ReactDOM from 'react-dom';
import ElegirRSSU from './ElegirRSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirRSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});

