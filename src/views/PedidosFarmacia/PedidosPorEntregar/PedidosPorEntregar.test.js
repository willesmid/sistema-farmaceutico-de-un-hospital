import React from 'react';
import ReactDOM from 'react-dom';
import PedidosPorEntregar from './PedidosPorEntregar';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PedidosPorEntregar />, div);
  ReactDOM.unmountComponentAtNode(div);
});
