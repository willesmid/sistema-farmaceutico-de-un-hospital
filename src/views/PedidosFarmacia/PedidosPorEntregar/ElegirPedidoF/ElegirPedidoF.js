import React, { Component } from 'react';
import { 
  Button, 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup,
  Modal,
  ModalFooter,
  ModalBody,
  ModalHeader,
  Collapse,
  Alert,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import EntregaProducto from './EntregaProducto'
import ValidarPersonal2 from './ValidarPersonal2'
const styles2 = { color: 'white', backgroundColor: '#1d1e1e', textAlign:'center' }
class ElegirPedidoF extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url_pedido: '#/listaPedidos/ElegirPedidoF',
      productos: [],
      personalList:[],
      contProducto_Noentregado:[],
      productosImp:[],
      idUnidad:0,
      nombreP:'',
      apellidosP:'',
      codP:'',
      cantidadP:0,

      primary:false,
      primary2:false,
      primary4:false,
      contarItems:0,
      personal:'',

      idProd:0,
      fecha_entrega:'',
      collapse:true,
      pedidoFecha_entrega:'',
      showImp:false,
      showAlert:false,
      //DESDE AQUI LOGIN
      logged_in: localStorage.getItem('token') ? true : false,
      nombre:'',
      paterno:'',
      materno:'',
      ci:0,
      prf:'',
    }
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
    this.togglePrimary4 = this.togglePrimary4.bind(this)
    this.toggleDatos = this.toggleDatos.bind(this)
    this.onValidacion = this.onValidacion.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
    this.print = this.print.bind(this)
  }
  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }
  togglePrimary2() {
    this.setState({ primary2: !this.state.primary2});
  }
  toggleDatos() {
    this.setState({ collapse: !this.state.collapse });
  }
  togglePrimary4() {
    if(this.state.showImp===true){
    this.setState({
      primary4: !this.state.primary4,
    });
    this.props.history.push('/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar')
  }else{
    this.setState({
      showAlert: true,
    });
  }
  }

  onCancelar(){
    this.props.history.push('/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar')
  }

async componentDidMount() {
    // DESDE AQUI CON EL LOGIN
    if(this.state.logged_in){
      if(typeof(localStorage.getItem('token')) === 'undefined'){
        window.location.href = '#/Login'
      }else{
        const respuesta7 = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
          })
          const datosUsuario = await respuesta7.json();
          this.setState({
            username:datosUsuario.username
          });
  
          try{
            // alert(this.state.username)
            const respuesta7 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.username+'/', {
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              }
            })
              const datosUsuario = await respuesta7.json();
              this.setState({
                nombre:datosUsuario[0].nombres,
                paterno: datosUsuario[0].primer_apellido,
                materno: datosUsuario[0].segundo_apellido,
                ci: datosUsuario[0].ci,
              });
              // alert(this.state.ci)
          } catch (e) {
              console.log(e);
          }
      }
    }
    else{
      window.location.href = '#/Login'
    }
  // HASTA AQUI LOGIN 
  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoBusca/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productos: findresponse,
          productosImp:findresponse,
          url_pedido : this.state.url_pedido +'/'+ this.props.match.params.id_pedido +'/'+ this.props.match.params.fecha +'/'+ this.props.match.params.unidadNombre +'/'+ this.props.match.params.personalNombre +'/'+this.props.match.params.personalApellidos+'/'+ this.props.match.localizacion +'/resultados',
        })
    });

    fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoBuscaNoEntregado/'+this.props.match.params.id_pedido+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          contProducto_Noentregado: findresponse,
        })
    });

    fetch('http://localhost:8000/farmacia/pedir/v1/PersonalBusca/'+1+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())  
    .then((findresponse) => {
      this.setState({
        personalList: findresponse
      })
    });

    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      
      if(dd<10) {
          dd='0'+dd
      } 

      if(mm<10) {
          mm='0'+mm
      } 

    hoy = yyyy+"-"+mm+"-"+dd
    this.setState({
      fecha_entrega: hoy,
    });
    if(this.props.match.params.unidadId!== '2' ){
      this.setState({prf:'Dr.'})
    }else{
      this.setState({prf:'Lic.'})
    }
}
// { path: '/listaPedidos/ElegirPedido/:id_pedido/:fecha/:unidadId/:unidadNombre/:personalNombre/:personalApellidos/:localizacion', exact: true, name: 'Datos Pedido', component: ElegirPedido },

handleOnAddEntrega (e) {   
  e.preventDefault();
  if(e.target.contar.value !==' '){
    this.setState({
      contarItems:this.state.contarItems+1
    })
  }
  console.log("ESTA ENTREGANDO : "+ e.target.contar.value)
  console.log("Cantidad de Recetas : "+ this.state.productos.length)
  console.log("Contando Items : "+ this.state.contarItems)
  if(this.state.contarItems===this.state.productos.length){
    console.log("AHORA SI")
  }
  fetch('http://localhost:8000/farmacia/pedir/v1/ProductoPedidoBusca/'+this.props.match.params.id_pedido+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      console.log("FINDRESPONSE=",findresponse)
        this.setState({
          productosImp: findresponse,
        })
    });
}

onValidacion(e){
  console.log("Cantidad de Recetas : "+ this.state.productos.length)
  console.log("Cantidad de Recetas no entregados : "+ this.state.contProducto_Noentregado.length)
  console.log("Contando Items : "+ this.state.contarItems)

  const num=this.state.contProducto_Noentregado.length
  console.log("total insumo y medicamentos: "+num)
  if(this.state.contarItems!==num){
    this.setState({     
      primary: !this.state.primary,
      message: 'Faltan Productos por entregar',
    })
  }
  if(this.state.contarItems===num ){
    this.setState({     
      primary2: !this.state.primary2,
    })
  } 
}

onFormSubmit(e){
 
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options) 

  let urlTermina = ''
  let dataTermina = {}
  urlTermina = 'http://localhost:8000/farmacia/pedir/v1/PedidoEstadoEdita/'+this.props.match.params.id_pedido+'/'
  dataTermina = {
    estado : 'Entregado',
    farmaceutica : this.state.ci,
    fecha_entrega : hoy,
    hora_entrega : hora,
  }

    try {
      fetch(urlTermina, {
      method: 'PUT',
      body: JSON.stringify(dataTermina),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then((res) => {res.json(); console.log('respuesta 1:', res.status);    
        if(res.status === 200){
          this.setState({
            primary2: !this.state.primary2,
            primary4: !this.state.primary4,
            pedidoFecha_entrega:hoy+" "+hora,
            showImp:false,
            showAlert:false,
          }); 
        }
      }
      )
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success verificando:', response)); //UTILIZAR UN SNACKBAR 
    } catch (e) {
        console.log(e);
    }
    // this.props.history.push('/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar')
    // this.setState({
    //   primary2: !this.state.primary2,
    // })
}

print(){
  /*Actual time and date */
    /*Actual time and date */
    var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }
    var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }
    var unidad = this.props.match.params.unidadNombre;
    unidad = unidad.toUpperCase(unidad);
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();
  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 9, 7, 20,20);
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(53, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(51, 19, "\"PROMES\"", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(158, 15,  "NUMERO DE PEDIDO:");
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(195, 15, " "+this.props.match.params.id_pedido);
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(158, 19, "LOCALIZACION :");

  doc.setFontType("normal");
  doc.setFontSize(8);
  doc.text(185, 19, " "+this.props.match.params.localizacion)
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 25, "PEDIDOS DE MEDICAMENTOS", null, null, 'center');

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 29, "E INSUMOS MEDICOS", null, null, 'center');
  
  
  doc.setFontType("normal");
  doc.setFontSize(7);
  doc.text(189,23, showDate +' '+ showTime, null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.setFontType("bold");
  doc.text(10,36, "UNIDAD SOLICITANTE:");
  
  doc.setFontType("normal");
  doc.text(48, 36, "PROMES - "+unidad)
  
  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
  
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 38, left: 10, right: 52}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
  });

  doc.setFontType("bold");
  doc.setFontSize(8);
  doc.text(165, 40, "Realizado Por: ");

  doc.setFontType("normal");
  doc.text(165, 51, this.state.prf+" "+this.props.match.params.personalNombre+" "+this.props.match.params.personalApellidos);
  doc.setFontType("bold");
  doc.text(165, 55, "Nombre");

  doc.setFontType("normal");
  doc.text(165, 62, ""+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision);
  doc.setFontType("bold");
  doc.text(165, 66, "Fecha");

  doc.setFontType("normal");
  doc.text(165, 78, "................................................");
  doc.setFontType("bold");
  doc.text(165, 82, "Firma");

  doc.setFontType("bold");
  doc.text(165, 85, "________________________");

  doc.setFontType("bold");
  doc.text(165, 90, "Entregado por: ");

  if(this.state.pedidoFecha_entrega!==''){
    doc.setFontType("normal");
    doc.text(165, 101, "Dr. "+this.state.nombre+" "+this.state.paterno+" "+this.state.materno);
    doc.setFontType("bold");
    doc.text(165, 105, "Nombre");

    doc.setFontType("normal");
    doc.text(165, 112, ""+this.state.pedidoFecha_entrega);
    doc.setFontType("bold");
    doc.text(165, 116, "Fecha");

  }else{
    doc.setFontType("normal");
    doc.text(165, 101, "................................................");
    doc.setFontType("bold");
    doc.text(165, 105, "Nombre");

    doc.setFontType("normal");
    doc.text(165, 112, ".............../.............../...............");
    doc.setFontType("bold");
    doc.text(165, 116, "Fecha");
  }

  doc.setFontType("normal");
  doc.text(165, 128, "................................................");
  doc.setFontType("bold");
  doc.text(165, 132, "Firma");

  doc.setFontSize(10);
  doc.text(10, 155, "_______________________");
  doc.text(13, 160, "UNIDAD SOLICITANTE");
  
  doc.text(72, 155, "__________________________________");
  doc.text(75, 160, "Vo. Bo. JEFE UNIDAD SOLICITANTE");

  doc.text(150, 155, "___________________________");
  doc.text(154, 160, "Vo. Bo. GERENTE MEDICO");
  this.setState({ showImp: true, showAlert:false});
  window.open(doc.output('bloburl'), '_blank'); 
}

render() {
  var unidad = this.props.match.params.unidadNombre;
  unidad = unidad.toUpperCase(unidad);
  
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
  
  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options)  /*Time to show in input*/

  const table=this.state.productosImp.map((u, i)=> {
    if(u.cantidad_entregada!==null){
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.codigo.codigo} </td>
        <td>{u.codigo.nombre}</td>
        <td>{u.unidadProd}</td>
        <td>{u.cantidad_solicitada}</td>
        <td>{u.cantidad_entregada+" ("+u.lote_salida+"  "+u.fecha_expiracion+")"}</td>
      </tr>
    )
  }else{
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.codigo.codigo} </td>
        <td>{u.codigo.nombre}</td>
        <td>{u.unidadProd}</td>
        <td>{u.cantidad_solicitada}</td>
        <td></td>
      </tr>
    )
    }
  })  
  return (   
  <div>
    <Table responsive striped hidden id="tabla">
    <thead>
        <tr>
          <th>No.</th>
          <th>CODIGO</th>
          <th>DETALLE</th>
          <th>UNIDAD</th>
          <th>Solicitada</th>
          <th>Entregada</th>            
        </tr>
    </thead>
    <tbody>
    {table}
    </tbody>
    </Table>   
    <Row>
      <Col xs="12" md="12">
        <Card>
          <CardHeader>
            <i className="fa fa-list-alt"></i><strong>Detalle Pedido Por Entregar</strong>
            <Button  onClick={this.print} color="link" className="btn btn-sm btn-nfo float-right">
              <i className="fa fa-print"></i> Imprimir Pedido
            </Button>            
          </CardHeader>
          <CardBody>
          <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>

            <Form >
              <FormGroup row>  
                <Col md="2">
                <Label htmlFor="text-input">UNIDAD SOLICITANTE</Label>
                </Col> 
                <Col md="4" >        
                <Input type="text" id="text-input" name="codigoAlifiacion" value={"PROMES - "+unidad}/>           
                </Col> 
                <Col md="1">
                <Label htmlFor="text-input">Localización</Label>                     
                </Col>
                <Col md="2">
                <Input type="text" id="text-input" name="cedulaIdentidad" value={this.props.match.params.localizacion}  disabled/>
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">N°Pedido</Label>
                </Col>
                <Col md="2">
                <Input type="text"  id="text-input" name="id_recetaA" value={this.props.match.params.id_pedido}  disabled/>
                </Col>  
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input">Pedido Realizado por</Label>                     
                </Col>
                <Col md="4">
                <ValidarPersonal2 idUnidad={this.props.match.params.unidadId} nombreP={this.props.match.params.personalNombre} apellidosP={this.props.match.params.personalApellidos}/>
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">F/Emisión</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="date-input" name="fecha" placeholder="Fecha" value={this.props.match.params.fecha_emision} disabled />
                </Col>
                <Col md="1">
                <Label htmlFor="hora">H/Emisión</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control" value={this.props.match.params.hora_emision} disabled  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input"> Pedido Entregado por</Label>
                </Col> 
                <Col md="4" >
                <Input type="text" name="medico" id="text" required
                  value={"Dr. "+this.state.nombre+" "+this.state.paterno+" "+this.state.materno}
                  onChange={e => this.setState({ personal: e.target.value })}>                                         
                  {/* <option value="">Personal...</option>
                    {this.state.personalList.map(item1 => (
                      <option value={item1.ci}>{item1.nombre+" "+item1.apellidos}</option>
                  ))} */}
                  </Input>                
                </Col> 
                <Col md="1">
                <Label htmlFor="fecha">F/Entrega</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="fecha" name="fecha" className="form-control" defaultValue={hoy} disabled ref={fecha => this.fecha = fecha}/>
                </Col>
                <Col md="1">
                <Label htmlFor="hora">H/Entrega</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control"  defaultValue={hora} disabled ref= {hora => this.hora = hora}/>
                </Col>
              </FormGroup>
            </Form>
            </Collapse>
            <Button size="sm" color="secondary" onClick={this.toggleDatos} style={{ marginBottom: '1rem' }}><i className="icon-note"></i><span> Datos Pedido</span></Button>

            
                <Table hover bordered striped responsive size="sm">
                <thead style={styles2}>
                  <tr >
                    <th rowspan="2"><p></p><p>Nro.</p></th>
                    <th rowspan="2"><p>Codigo</p></th>
                    <th rowspan="2"><p>Detalle</p></th>
                    <th rowspan="2"><p>Unidad</p></th>
                    <th rowspan="2"><p>Cantidad</p></th> 
                    <th rowspan="2" bgcolor="#424949"><p>Acciones</p></th> 
                  </tr>
                  </thead>
                  <tbody>
                    {this.state.productos.map((u, i)=> {
                      return (
                        <tr key = {i} >
                          <td style={{maxWidth: '2px', textAlign: 'center'}}>{i+1}</td>
                          <td style={{maxWidth: '50px'}} >{u.codigo.codigo} </td>
                          <td style={{maxWidth: '100px'}}>{u.codigo.nombre}</td>
                          <td style={{maxWidth: '50px'}}>{u.unidadProd}</td>
                          <td style={{maxWidth: '10px', textAlign: 'center'}}>{u.cantidad_solicitada}</td>                    
                          <td style={{maxWidth: '450px'}}>
                            <EntregaProducto codP={u.codigo.codigo} cantidadP={u.cantidad_solicitada} nomP={u.codigo.nombre} idProd={u.id} cantidadE={u.cantidad_entregada} onAddEntrega={this.handleOnAddEntrega.bind(this)}/> 
                          </td>
                        </tr>
                      )
                    })}             
                  </tbody>
                </Table>
          {/* FIN DE REGISTRO */}
            <center>
            <Button onClick={this.onValidacion} className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-check-circle"></i><span>Terminar Entrega</span></Button>
            <Button onClick={this.onCancelar} className="btn-google-plus btn-brand mr-1 mb-1"><i className="fa fa-times-circle"></i><span>Cancelar Entrega</span></Button>
            </center>
          {/* FIN DE REGISTRO */}
          </CardBody>
        </Card>
      </Col>
    </Row>

{/* MODAL ADVERTENCIA */}
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
      <ModalBody>
        <strong>{this.state.message}</strong>
      </ModalBody>
    <ModalFooter>
      <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
    </ModalFooter>
  </Modal>
{/* FIN MODAL ADVERTENCIA */}

{/* MODAL REGISTRO  */}
  <Form>
  <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
    <form onSubmit={this.props.onAddEntrega}>
    <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
      <ModalFooter>           
        <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
{/* FIN MODAL REGISTRO */}

{/* MODAL DATOS REGISTRADOS */}
<Modal isOpen={this.state.primary4} toggle={this.togglePrimary4} className={'modal-success ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimary4}><strong>Correcto</strong></ModalHeader>
  <ModalBody>
    Los datos se registraron correctamente.
    {this.state.showAlert && <Alert color="danger"><strong>{"No realizo la impresion"}</strong></Alert>}
    <Button  onClick={this.print} color="link" className="btn btn-nfo float-right">
      <i className="fa fa-print"></i> Imprimir Pedido
    </Button>
  </ModalBody>
   <ModalFooter>
     <Button color="success" onClick={this.togglePrimary4}>Aceptar</Button>
    </ModalFooter>
 </Modal>
{/* FIN DATOS REGISTRADOS */}
</div>
)
}
}

export default ElegirPedidoF;
