import React, { Component } from 'react';
import { 
  Form,
  Col,
  Input,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Alert,

} from 'reactstrap';

class EntregaProducto extends Component {
  constructor(props) {
    super(props)
    this.state = {     
    list_lote: [],
    cantidadLote: 0,
    cantidadFarmacia:0,
    cantidadGeneral:0,
    lote:'',
    fecha_expiracion:'',
    codP1:'',
    lote1:'',
    numSaldo:0,
    showEntrega:true,   
    primary:false,
    primary2:false,
    menssage:'',
    showYaEntregado:false,
    showCantFarmacia: false,
    cantidadEntrega:0,
    };
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onValidacion = this.onValidacion.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)  
}

togglePrimary() {
  this.setState({ primary: !this.state.primary});
}

togglePrimary2() {
  this.setState({ primary2: !this.state.primary2});
}

componentDidMount(){
  //console.log(value); CardBody
  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodP/'+this.props.codP+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_lote: findresponse,
        lote:findresponse[0].lote.id_lote
      })  
  // AQUI SACAMOS LOS DATOS DEL LOTE [0] //
      fetch('http://localhost:8000/farmacia/almacena/v1/LoteProductoBusca2/'+this.props.codP+'/'+findresponse[0].lote.id_lote+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())  
      .then((findresponse) => {
        this.setState({
          cantidadLote: findresponse[0].cantidad
        })
      });
    
      fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBusca2/'+this.props.codP+'/'+findresponse[0].lote.id_lote+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())  
      .then((findresponse) => {
        this.setState({
          cantidadFarmacia: findresponse[0].stockAlmacen
        })
      });
      fetch('http://localhost:8000/farmacia/almacena/v1/LoteBusca/'+findresponse[0].lote.id_lote+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())  
      .then((findresponse) => {
        this.setState({
          fecha_expiracion: findresponse[0].fecha_expiracion
        })
      });      
  // FIN AQUI SACAMOS LOS DATOS DEL LOTE [0] //    
  })
  //Primera vista Cantidad del codigo//
  fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.props.codP+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadGeneral: findresponse[0].stockGeneral
    })
  });
  // FIN Primera vista Cantidad del codigo///
}

handleChange(){
  fetch('http://localhost:8000/farmacia/almacena/v1/LoteProductoBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadLote: findresponse[0].cantidad
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadFarmacia: findresponse[0].stockAlmacen
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.props.codP+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      cantidadGeneral: findresponse[0].stockGeneral
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/LoteBusca/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      fecha_expiracion: findresponse[0].fecha_expiracion
    })
  }); 
}

onValidacion(e){
  const numT=this.state.cantidadFarmacia - this.state.cantidadEntrega
  console.log("Validar cantidad en Farmacia: "+numT )

  if(this.state.lote === '' ){
    this.setState({     
      primary: !this.state.primary,
      message: 'Ingrese Lote ',
    })
  }
  else{
    if(this.state.cantidadEntrega<0){
      this.setState({     
        primary: !this.state.primary,
        message: 'Cantidad a entregar debe ser mayor a 0',
      })

    }else{
      if(this.state.cantidadEntrega>this.props.cantidadP){
        this.setState({     
          primary: !this.state.primary,
          message: 'Cantidad a Entregar debe ser menor o igual a la Cantidad Solicitada',
        })

      }else{
        if(numT<0){
          this.setState({     
            primary: !this.state.primary,
            message: 'Cantidad Insuficiente ',
            showCantFarmacia: true,          
          })
        }else{
          this.setState({
          numSaldo: this.state.cantidadFarmacia - this.state.cantidadEntrega,
          primary2: !this.state.primary2, 
        })
        }
      }
    }      
  }
}


onFormSubmit(e) {
  let urlLote = ''
  let urlFarmacia = ''
  let urlGeneral = ''
  let dataLote = {}
  let dataFarmacia = {}
  let dataGeneral = {}

  urlLote = 'http://localhost:8000/farmacia/almacena/v1/LoteCantidadEdita/'+this.state.lote+'/'
  urlFarmacia = 'http://localhost:8000/farmacia/almacena/v1/EstaFarmaciaEdita/'+this.state.lote+'/'  
  urlGeneral = 'http://localhost:8000/farmacia/almacena/v1/ProductoEdita/'+this.props.codP+'/'

  const numLote = this.state.cantidadLote - this.state.cantidadEntrega
  const numFarmacia = this.state.cantidadFarmacia - this.state.cantidadEntrega
  const numGeneral= this.state.cantidadGeneral - this.state.cantidadEntrega

  console.log('nueva cantidadLOTE jijiji: '+ this.state.cantidadLote)
  console.log('nueva cantidadENTREGAR jijiji: '+ this.state.cantidadEntrega)
  console.log('nueva cantidad RESTA : '+ numLote)
  console.log('nueva cantidadGENERAL jijiji: '+ this.state.cantidadGeneral)
  console.log('nueva cantidadPRESCRIPCION jijiji: '+ this.state.cantidadEntrega)
  console.log('nueva cantidad RESTA : '+ numGeneral)

  dataLote = {
    cantidad : numLote
  }
  dataFarmacia = {
    stockAlmacen : numFarmacia
  }
  dataGeneral = {
    stockGeneral : numGeneral
  }
  // dataProducto_pedido= {
  //   lote_salida : this.state.lote
  // }
    try {
      fetch(urlLote, {
      method: 'PUT',
      body: JSON.stringify(dataLote),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
    } catch (e) {
        console.log(e);
    }
    try {

      fetch(urlFarmacia, {
      method: 'PUT',
      body: JSON.stringify(dataFarmacia),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }
    try {
      fetch(urlGeneral, {
      method: 'PUT',
      body: JSON.stringify(dataGeneral),
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
    } catch (e) {
        console.log(e);
    }
    this.setState({
      showEntrega:false,
    })

//EDITA PRODUCTO-PEDIDO (CANTIDAD ENTREGADA)
let urlCantidadEntregada = ''
let dataCantidadEntregada = {}
urlCantidadEntregada = 'http://localhost:8000/farmacia/pedir/v1/ProductoPedidoCantidadEdita/'+this.props.idProd+'/'
dataCantidadEntregada = {
  cantidad_entregada:this.state.cantidadEntrega,
  lote_salida:this.state.lote,
  fecha_expiracion:this.state.fecha_expiracion,
}

  try {
    fetch(urlCantidadEntregada, {
    method: 'PUT',
    body: JSON.stringify(dataCantidadEntregada),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
  } catch (e) {
      console.log(e);
  }

//FIN CANTIDAD ENTREGADA
    this.setState({
      primary2: !this.state.primary2,
      showYaEntregado: true
    })
// VALIDANDO UN PRODUCTO ///
  this.setState({
    cantidadFarmacia: numFarmacia
  })
// FIN VALIDANDO UN PRODUCTO ///
  }

render() {  
  if(this.props.cantidadE !== null){
    return(
    <div >
    <Form onSubmit={this.props.onAddEntrega}>
    <center>
    <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check"></i><span>Producto Entregado</span></Button>
    </center>
    </Form>  
    </div>
    )
  }
    else{
      return (
        <div >
           <Form onSubmit={this.props.onAddEntrega}>
          <Table hover bordered striped responsive size="sm"> 
          <tr>
          <td style={{maxWidth: '200px', backgroundColor: '#ffffff'}}>
          {this.state.list_lote.length>1 && 
          <Input 
            type="select" 
            name="lote" 
            id="lote" 
            value={this.state.lote}
            required
            onChange={e => { this.setState({ lote: e.target.value }) }}
            onClick={ this.handleChange.bind(this)}>
            {this.state.list_lote.map(item1 => (
                <option value={item1.lote.id_lote} >{item1.lote.fecha_expiracion+' ('+item1.lote.id_lote+')'}</option>
                ))}
          </Input>
          }
          { this.state.list_lote.length===1 &&
            <Input 
            type="text  " 
            name="lote" 
            id="lote" 
            value={this.state.list_lote[0].lote.fecha_expiracion+' ('+this.state.list_lote[0].lote.id_lote+')'}>
            </Input>
          }
          </td>
          <td style={{maxWidth: '80px', backgroundColor: '#ffffff'}}>
          <Input  type="text"  value ={this.state.cantidadFarmacia} name="cantidad"  id="text-input" onChange={ this.handleChange.bind(this)}></Input>
          </td>
          <td bgcolor="#ffffff" style={{maxWidth: '80px'}}>
          <Input  type="number"  name="cantidadEntrega" onChange={e => this.setState({ cantidadEntrega: e.target.value })} id="text-input" placeholder="0"></Input>
          </td>
          <td bgcolor="#ffffff" style={{maxWidth: '80px'}}>   
         {this.state.showEntrega && 
        //  <Button color="primary" className="mr-1" size="sm"   onClick={this.onValidacion} >Confirmar</Button>
        <Button size="sm" className="btn-twitter btn-brand mr-1 mb-1"  onClick={this.onValidacion}><i className="fa fa-hand-lizard-o"></i><span>Confirmar</span></Button>
         }
         {this.state.showYaEntregado &&
          <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check-square-o"></i><span>Entregado</span></Button>
        }
         </td>
         </tr>
        </Table>
      <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
        <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
          <ModalBody>
            <strong>{this.state.message}</strong>
            { this.state.showCantFarmacia && <Alert color="success">            
                <strong>{"REALICE PEDIDO DE ALMACEN O REALICE PEDIDO DE SSU"}</strong>                  
            </Alert>}   
          </ModalBody>
        <ModalFooter>
          <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
        <form onSubmit={this.props.onAddEntrega}>
        <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
          <ModalBody>
          <center>
            <Col md="7">
            <Input type="text" name="contar"  value={"¿Desea Entregar Medicamento?"}   ></Input>
            </Col>
            </center>
            <strong>{"Codigo :  "} </strong>
            {this.props.codP+" "}
            <strong>{"Nombre :  "}</strong>
            {this.props.nomP}
            <br/>
            <strong>{"Numero Lote:  "}</strong>
            {this.state.lote}
            <br/>
            <strong>{"Cantidad Solicitada:  "}</strong>
            {this.props.cantidadP}
            <br/>
            <strong>{"Cantidad a Entregar:  "}</strong>
            {this.state.cantidadEntrega}
            <br/>
            <center>
            <Col xs="12" md="10">
            <Alert color="success">
            <strong>{"Cantidad Existente en Farmacia: "}</strong>{this.state.cantidadFarmacia} <strong>{" Saldo: "}</strong>{ this.state.numSaldo}
            </Alert>
            </Col>
            </center>
          </ModalBody>
          <ModalFooter>           
            <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
            <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
          </ModalFooter>
          </form>
      </Modal>
    </Form>  
  </div> 
  )}
  }
}
export default EntregaProducto;








