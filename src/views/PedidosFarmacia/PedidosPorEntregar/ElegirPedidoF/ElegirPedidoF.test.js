import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPedidoF from './ElegirPedidoF';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPedidoF />, div);
  ReactDOM.unmountComponentAtNode(div);
});

