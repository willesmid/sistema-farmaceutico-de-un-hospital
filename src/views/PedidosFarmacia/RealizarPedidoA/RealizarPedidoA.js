import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalBody, 
  Alert,
  Label, 
  Input,
  Collapse,
  ListGroupItem,
  Card, CardBody, CardHeader, Col, Row, Form, Table, FormGroup,} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import DatosProducto from './DatosProducto'
import Search from 'react-search-box';
const styles2 = { color: 'white', backgroundColor: '#2b2c2c', textAlign:'center' }
var url='http://localhost:8000/farmacia/pedir/v1/ProductoPedidoGuardar/'
var url_p='http://localhost:8000/farmacia/pedir/v1/PedidoGuardar/' 

class RealizarPedidoA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataN:[],
      data3: 0,
      dataCant: 0,
      list:[],
      idA:0,
      swA:false,
    
      loading: false,
      Id_recetaA:'',

      id_recetaAA:'',
      recetas:[],
      CodigoAlifiacion: '',
      fecha:'',

      Id_consulta:'',
      NombreCompleto:'',
      CedulaIdentidad:'',
      NombreMedico:'',
      listR:[],
      productos:[],
      Codigo:'',
      Nombre:'',
      Forma:'',
      Cantidad:'',
      Prescripcion:'',
      prescripcion:'',
      cantidad_solicitada:0,
      primary:false,
      primary2:false,
      primaryPrint:false,

      codigo:'',
      showAA:false,
      showCant:false,
      num:0,
      message2:'',
      menssage:'',
      contarItem:0,
      collapse: false,
      fadeIn: true,
      medicamentos2:[],
      nombre2:'',
      cantidad2:0,
      forma2:'',
      prescripcion2:'',

      // PEDIDO
      unidad:1,
      unidadList:[],
      farmaceuticaList:[], 
      farmaceutica:'',
      almacen:2,
      almacenList:[], 
      primary3:false,

      showPedido:true,
      showLlenarP:false,
      unidadProd:'',
      showSelect:true,
      nombreAlmacen:'',

      list_lote:[],
      codigoProducto:'',
      lote:'',
      cantidadAlmacen:0,
      showDatosPedido:false,
      collapse2:true,
      codigoPedido:0,
      fechaPedido:'',
      horaPedido:'',
      primaryProdNombre:false,
      listaProdNombre:[],
      //DESDE AQUI LOGIN
      logged_in: localStorage.getItem('token') ? true : false,
      nombre:'',
      paterno:'',
      materno:'',
      ci:0,
      unidadId:0,
      unidadNombre:'',
      showImp:false,
    };    
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.toggle = this.toggle.bind(this);
    this.togglePrint =this.togglePrint.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    //PEDIDO
    this.onGeneraPedido = this.onGeneraPedido.bind(this);
    this.togglePrimary3 = this.togglePrimary3.bind(this)
    this.muestraPedido2 =this.muestraPedido2.bind(this);
    this.onCancelar = this.onCancelar.bind(this)
    this.togglePrimaryProdNom = this.togglePrimaryProdNom.bind(this)
    this.print = this.print.bind(this)
    this.onValidacion= this.onValidacion.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }
  toggleFade() {
    this.setState({ fadeIn: !this.state.fadeIn });
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary,});
  }
  
  togglePrimary2() {
    this.setState({ primary2: !this.state.primary2,});
  }

  togglePrimaryProdNom() {
    this.setState({ primaryProdNombre: !this.state.primaryProdNombre});
  }

  onCancelar(){
    this.props.history.push('/PedidosFarmacia/PedidosAlmacen/PedidosEmitidosAlmacen')
  }
  
async componentDidMount() { //ciclo de vida de react de forma asincrona
  // DESDE AQUI CON EL LOGIN
  if(this.state.logged_in){
    if(typeof(localStorage.getItem('token')) === 'undefined'){
      window.location.href = '#/Login'
    }else{
      const respuesta7 = await fetch('http://localhost:8000/core/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
        })
        const datosUsuario = await respuesta7.json();
        this.setState({
          username:datosUsuario.username
        });

        try{
          // alert(this.state.username)
          const respuesta7 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.username+'/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
            const datosUsuario = await respuesta7.json();
            var cadena = datosUsuario[0].unidad.nombre_unidad;
            cadena = cadena.toUpperCase(cadena);
            this.setState({
              unidadNombre: cadena,
              unidadId:datosUsuario[0].unidad.id,
              nombre:datosUsuario[0].nombres,
              paterno: datosUsuario[0].primer_apellido,
              materno: datosUsuario[0].segundo_apellido,
              ci: datosUsuario[0].ci,
            });
            // alert(this.state.ci+" "+this.state.unidadId+" "+this.state.unidadNombre)
        } catch (e) {
            console.log(e);
        }
    }

  }
  else{
    window.location.href = '#/Login'
  }
// HASTA AQUI LOGIN 
  try {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd

    this.setState({
      fecha: hoy,
    });
    const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/productos/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    });
    const data = await respuesta.json(); 
    const respuestaN = await fetch('http://localhost:8000/farmacia/almacena/v1/ListaProductosNombres/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    });
    const dataN = await respuestaN.json(); 
    const unidades = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaUnidades/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    const unidadList = await unidades.json()
    const almacenes = await fetch('http://localhost:8000/farmacia/almacena/v1/ListaAlmacen/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    const almacenList = await almacenes.json()     
    const farmaceutica = await fetch('http://localhost:8000/farmacia/pedir/v1/PersonalBusca/'+this.state.unidad+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })   
    const farmaceuticaList = await farmaceutica.json()
    this.setState({
      data, 
      dataN,
      unidadList,
      almacenList,
      farmaceuticaList,
    });
  } catch (e) {
    console.log(e);
  }
}

fEliminar(i){
  const nuevo=this.state.productos.filter(productos=>{
    return productos !== i
  })
  if(this.state.productos.length!==1){
    this.setState({
    productos:[...nuevo]
    })
  }
  else{
    this.setState({
      productos:[...nuevo],
      bandera:false
    })
  }
  this.setState({
    contarItem:this.state.contarItem-1,
    showImp: false
  })
}

togglePrimary3(e) {
  e.preventDefault()
  if(this.state.almacen!=='' ){

  this.setState({
    primary3: !this.state.primary3,
  });
}else{
  this.setState({
    primary: !this.state.primary,
    message: 'Seleccione los campos faltantes'
  });
}
}

togglePrint() {
  this.setState({ primaryPrint: !this.state.primaryPrint});
}

muestraPedido2(e){
  this.setState({
    collapse2: !this.state.collapse2,
  })
}

onGeneraPedido(e) {
  e.preventDefault()
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options) 
    
  let datos_p= {
    fecha_emision: hoy,
    hora_emision: hora,
    localizacion: this.state.almacen,
    unidad: this.state.unidadId,
    personal: this.state.ci,
    farmaceutica: 1,  
  }
  try {  
    fetch(url_p, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(datos_p), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => {
        console.log('Success:', response)
        this.setState({
          codigoPedido:response.id_pedido,
          fechaPedido:response.fecha_emision,
          horaPedido:response.hora_emision,
          collapse:true,
          showDatosPedido:true,
        })
    });

  } catch (e) {
    console.log(e);
  }

  this.setState({
    collapse:true,
    primary3:false,
    showPedido:false,
  })
  
} 

handleOnAddPedido (event) {       
  event.preventDefault();
  const num = this.state.dataCant - this.state.cantidad_solicitada
  console.log("Cantidad calculada: "+num)
  if(this.state.cantidad_solicitada !== '' && this.state.unidadProd !== '' ){
    if(this.state.cantidad_solicitada  > 0){
      this.setState({
        message: '',
      })
    }
    else{
      this.setState({
        codigo: event.target.codigo.value,
        cantidad_solicitada:this.state.cantidad_solicitada,
        primary: !this.state.primary,
        message: 'La cantidad a retirar debe ser mayor a 0.'
      })
    }
    if(num<0){
      this.setState({
        primary: !this.state.primary,
        showCant:true,
        menssage: "nada",
        message2: "Cantidad insuficiente",
      })
    }
    else{
      this.setState({
        showCant: false,
        menssage: "nada",
        message2: '',
      })
    }
  }
  else{ 
    this.setState({
      codigo: event.target.codigo.value,
      cantidad_solicitada:this.state.cantidad_solicitada,
      showAA : true,
      primary: !this.state.primary,
      showCant: false,
      message: 'Complete los espacios en blanco.',
      message2: '',
    });
  }
  if(this.state.cantidad_solicitada !== '' && this.state.unidadProd!== '' && this.state.cantidad_solicitada > 0 && event.target.codigo.value !=='' && num>=0 && this.state.contarItem<20){
    
    var a=0
    this.state.productos.map((item) =>{
      if(item.Codigo===event.target.codigo.value){
        a=1
      }
      return 0
    });

  if(a===0){
  
    let obj = {
      Codigo : event.target.codigo.value,
      Nombre : event.target.nombre.value,
      Cantidad_Solicitada : this.state.cantidad_solicitada, 
      UnidadProd :this.state.unidadProd,
      Id_Pedido : this.state.codigoPedido,
      //arreglar se remplaza al guardar con el id del Formulario
    };
    this.setState({
      codigo: event.target.codigo.value,
      cantidad_solicitada:this.state.cantidad_solicitada,
      showAA : true,
      productos: this.state.productos.concat([obj]),
      contarItem: this.state.contarItem + 1,
      showImp: false
    }); 
  }else{
    this.setState({
      primary: !this.state.primary,
      message: 'El producto farmaceutico ya fue adicionado',
      showCant:false,
      menssage2: '',
    })

  }      
  }else{   
    if(event.target.codigo.value === '' ){
      this.setState({
        codigo: event.target.codigo.value,
        cantidad_solicitada:this.state.cantidad_solicitada,
        showAA : true,
        primary: !this.state.primary,
        message: 'Busque Producto',
      })
    }
  } 
  if(this.state.contarItem>19){
    this.setState({  
      primary: !this.state.primary,
      showCant:true,
      menssage: "Termino de recetar",
      message2: "Cantidad de item completos",
    })
  }
  console.log("cantidad item: "+this.state.contarItem);  
}

handleChange2(value) {
  //console.log(value); 

  this.state.data.map((item, indice)=>{
    if (value === item.codigo){          
     fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
     //   console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list: findresponse,
        codigoProducto: item.codigo,
        primaryProdNombre:false,
      })
       // console.log('LIST=',this.state.list.length)   
      if (this.state.list.length===0) {
        this.setState({
          show: false,
          show2: true
        })
         // document.write("No hay " + this.state.list.length);
      }
      else{
        this.setState({
          show: true,
          show2: false
        })
        } 
    });

    fetch('http://localhost:8000/farmacia/almacena/v1/LocalizacionBuscaCodsuma/'+this.state.almacen+'/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse)=>{    

    if(findresponse.length>0){ 
      this.setState({
        dataCant: findresponse[0].sum
      });
      console.log("Cantidad Sumada primer evento existe: "+findresponse[0].sum)

      }else{
      this.setState({
        dataCant: 0
      });
      console.log("Cantidad Sumada primer evento no existe: "+0)

      }
    })

    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoAlmacenBuscaCodP/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      // console.log("FINDRESPONSE=",findresponse)
        this.setState({
          list_lote: findresponse
        })  
    })
    
    }

    return 0
  })

}

handleChange3(value) {
  //console.log(value); 
  this.state.data.map((item, indice)=>{
    if (value === item.nombre){          
     fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBuscaNombre/'+item.nombre+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      // console.log("NOMBREEEE: "+item.nombre + "CODIGOOO: "+item.codigo)
      if (findresponse.length>1) {
        this.setState({
          show: false,
          primaryProdNombre: true,
          listaProdNombre:findresponse,
        })
      }
      else{
        this.setState({
          list:findresponse,
          codigoProducto: item.codigo,
        })

        fetch('http://localhost:8000/farmacia/almacena/v1/LocalizacionBuscaCodsuma/'+this.state.almacen+'/'+findresponse[0].codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse)=>{
          if(findresponse.length>0){    
          this.setState({
              dataCant: findresponse[0].sum
          });
          console.log("primer evento existe: "+findresponse[0].sum)
        }else{
          this.setState({
            dataCant: 0
        });
        console.log("primer evento no existe: "+0)
        }
       
        })
    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoAlmacenBuscaCodP/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      // console.log("FINDRESPONSE=",findresponse)
        this.setState({
          list_lote: findresponse
        })  
    })



      }
 
    });
  }
    return 0
  })
}


onFormSubmit(e) {
  e.preventDefault()
  this.state.productos.map((data,i)=>{
  let data2={
    cantidad_solicitada: data.Cantidad_Solicitada,
    unidadProd: data.UnidadProd,
    id_pedido: data.Id_Pedido,
    codigo: data.Codigo,
  }
  try {  
    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data2), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));
      // alert("SE GUARDO SATISFACTORIAMENTE EL MEDICAMENTO")      
  } catch (e) {
    console.log(e);
  }
    return 0
  })

this.props.history.push('/PedidosFarmacia/PedidoAlmacen/PedidosEmitidosAlmacen')
  this.setState({
      primary2: !this.state.primary2,
  }); 
} 

handleCantidadLote(){
  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoAlmacenBusca2/'+this.state.codigoProducto+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
  // console.log("FINDRESPONSE=",findresponse)
    this.setState({
      cantidadAlmacen: findresponse[0].stockAlmacen
    })
  });
}

print(){
  /*Actual time and date */
    /*Actual time and date */
    var options2 = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric'
    }
    var options3 = {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit'
    }
  var d = new Date();
  var showDate = d.toLocaleString('es-bo', options2) 
  var showTime = d.toLocaleString('es-bo', options3) 
  /*Actual time and date */
  var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
  var logo = new Image();
  logo.src = 'assets/img/dashboard/icono_promes.png';
  doc.addImage(logo, 'JPEG', 9, 7, 20,20);
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(53, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(8);
  doc.text(51, 19, "\"PROMES\"", null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(158, 15,  "NUMERO DE PEDIDO:");
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(195, 15, " "+this.state.codigoPedido);
  
  doc.setFontType("normal");
  doc.setFontType("bold");  
  doc.setFontSize(8);
  doc.text(158, 19, "LOCALIZACION :");

  doc.setFontType("normal");
  doc.setFontSize(8);
  doc.text(185, 19, "  ALMACEN")
  
  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 25, "PEDIDOS DE MEDICAMENTOS", null, null, 'center');

  doc.setFont("helvetica");
  doc.setFontType("bold");   //italic
  doc.setFontSize(11);
  doc.text(105, 29, "E INSUMOS MEDICOS", null, null, 'center');
  
  
  doc.setFontType("normal");
  doc.setFontSize(7);
  doc.text(189,23, showDate +' '+ showTime, null, null, 'center');
  
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.setFontType("bold");
  doc.text(10,36, "UNIDAD SOLICITANTE:");
  
  doc.setFontType("normal");
  doc.text(48, 36, "PROMES - "+this.state.unidadNombre)
  
  var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
  doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
  
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 38, left: 10, right: 52}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
  });

  doc.setFontType("bold");
  doc.setFontSize(8);
  doc.text(165, 40, "Realizado Por: ");

  doc.setFontType("normal");
  doc.text(165, 51, "Dr. "+this.state.nombre+" "+this.state.paterno+" "+this.state.materno);
  doc.setFontType("bold");
  doc.text(165, 55, "Nombre");

  doc.setFontType("normal");
  doc.text(165, 62, ""+this.state.fechaPedido+" "+this.state.horaPedido);
  doc.setFontType("bold");
  doc.text(165, 66, "Fecha");

  doc.setFontType("normal");
  doc.text(165, 78, "................................................");
  doc.setFontType("bold");
  doc.text(165, 82, "Firma");

  doc.setFontType("bold");
  doc.text(165, 85, "________________________");

  doc.setFontType("bold");
  doc.text(165, 90, "Entregado por: ");

  doc.setFontType("normal");
  doc.text(165, 101, "................................................");
  doc.setFontType("bold");
  doc.text(165, 105, "Nombre");

  doc.setFontType("normal");
  doc.text(165, 112, ".............../.............../...............");
  doc.setFontType("bold");
  doc.text(165, 116, "Fecha");

  doc.setFontType("normal");
  doc.text(165, 128, "................................................");
  doc.setFontType("bold");
  doc.text(165, 132, "Firma");

  doc.setFontSize(10);
  doc.text(10, 155, "_______________________");
  doc.text(13, 160, "UNIDAD SOLICITANTE");
  
  doc.text(72, 155, "__________________________________");
  doc.text(75, 160, "Vo. Bo. JEFE UNIDAD SOLICITANTE");

  doc.text(150, 155, "___________________________");
  doc.text(154, 160, "Vo. Bo. GERENTE MEDICO");
  this.setState({ showImp: true, primaryPrint:false});
  window.open(doc.output('bloburl'), '_blank'); 
}

onValidacion(){
  if(this.state.showImp===false && this.state.productos.length>0){
    this.setState({ 
      showCant: false,
      message2:'',
      primaryPrint: !this.state.primaryPrint,
      message: 'No realizó la impresión del Pedido',
    })
  }else{
    if(this.state.productos.length===0){
      this.setState({ 
        showCant: false,
        message2:'',
        primary: !this.state.primary,
        message: 'No registró ningún producto',
      })}
  }
  if(this.state.showImp===true && this.state.productos.length>0){
  this.setState({
    primary2:!this.state.primary2
  })
}}

render() {

  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options)  /*Time to show in input*/

  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }
  const table=this.state.productos.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.Codigo} </td>
        <td>{u.Nombre}</td>
        <td>{u.UnidadProd}</td>
        <td>{u.Cantidad_Solicitada}</td>
      </tr>
    )
  })
return (
<div className="animated fadeIn">
<Table responsive striped hidden id="tabla">
    <thead>
      <tr>
        <th>No.</th>
        <th>CODIGO</th>
        <th>DETALLE</th>
        <th>UNIDAD</th>
        <th>Solicitada</th>
        <th>Entregada</th>            
      </tr>
    </thead>
    <tbody>
    {table}
    </tbody>
</Table>    
<Row>
  <Col xs="12">          
    <Card>
      <CardHeader>
        <h3><i className="fa fa-pencil-square-o"></i> <strong>Realizar Pedido a Almacen</strong>
          <Button  onClick={this.print} color="link" className="btn  btn-nfo float-right">
            <i className="fa fa-print"></i> Imprimir Pedido
          </Button>
        </h3>
      </CardHeader>
      <CardBody>
        <Collapse isOpen={this.state.collapse2} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Card>
          <ListGroupItem action color="info">Informacion Pedido </ListGroupItem>
          <CardBody>
            <Form>
              <FormGroup row>
                <Col xs="12" md="1">
                <Label htmlFor="text-input">Solicitar</Label>
                </Col>
                <Col md="2">
                <Input type="text" name="unidad" id="text" value={"ALMACEN"}/>
                </Col> 
                <Col md="1">
                <Label htmlFor="fecha">Fecha</Label>
                </Col>
                <Col md="2">
                <Input type="date" id="fecha" name="fecha" className="form-control" defaultValue={hoy} disabled ref={fecha => this.fecha = fecha}/>
                </Col>
                <Col md="1">
                <Label htmlFor="fecha">Hora</Label>
                </Col>
                <Col md="2">
                <Input type="time" id="hora" name="hora" className="form-control"  defaultValue={hora} disabled ref= {hora => this.hora = hora} />
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">N°Pedido</Label>
                </Col>
                <Col md="2">
                <Input type="text" id="text-input" value={this.state.codigoPedido} name="id_recetaA"  disabled/>
                </Col> 
              </FormGroup>
              <FormGroup row> 
                <Col md="2">
                <Label htmlFor="text-input">Unidad Solicitante</Label>
                </Col> 
                <Col md="5" >
                <Input type="text" name="unidad" id="text" value={"PROMES - FARMACIA"}/>
                </Col>
                <Col xs="12" md="2">
                <Label htmlFor="text-input">Realizado por</Label>
                </Col> 
                <Col xs="12" md="3" >
                <Input type="text" name="medico" id="text" required
                  value={"Dr. "+this.state.nombre+" "+this.state.paterno+" "+this.state.materno}
                  onChange={e => this.setState({ farmaceutica: e.target.value })}>                                         
                  {/* <option value="">farmaceutica...</option>
                    {this.state.farmaceuticaList.map(item1 => (
                      <option value={item1.ci}>{item1.nombre+" "+item1.apellidos}</option>
                    ))} */}
                </Input>
                </Col>  
              </FormGroup>
            </Form>
          </CardBody>
          </Card>
        </Collapse>

{this.state.showPedido && <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" onClick={this.togglePrimary3} style={{ marginBottom: '1rem' }}><i className="fa fa-sign-in"></i><span>Generar Pedido</span></Button>    }
{this.state.showDatosPedido && <Button color="secondary" size="sm"  onClick={this.muestraPedido2} style={{ marginBottom: '1rem' }}><i className="icon-note"></i><span> Datos Pedido</span></Button>}
        
        <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Card>
            <ListGroupItem action color="info">Elija Medicamento - Insumo </ListGroupItem>
            <Col xs="12" md="12">
              <br/>
              <FormGroup row>
                <Col xs="12" md="1">
                </Col>
                <Col xs="12" md="2">
                <Label htmlFor="codigo"><strong>Ingrese Codigo : </strong></Label>
                </Col>
                <Col xs="12" md="3">
                <Search                       
                  data={this.state.data}
                  onChange={ this.handleChange2.bind(this)}
                  placeholder="Codigo-Medicamento"
                  class="search-class"
                  searchKey="codigo"
                  className="form-control"
                />
                </Col> 
                <Col xs="12" md="2">
                <Label htmlFor="codigo"><strong>Ingrese Nombre: </strong></Label>
                </Col>
                <Col xs="12" md="3">
                <Search                       
                  data={this.state.dataN}
                  onChange={ this.handleChange3.bind(this)}
                  placeholder="Nombre-Medicamento"
                  class="search-class"
                  searchKey="nombre"
                  className="form-control"
                />
                </Col>
                <Col xs="12" md="1">
                </Col>                          
              </FormGroup>
              <hr/>
            </Col>
              
              <DatosProducto list={this.state.list} onAddPedido={this.handleOnAddPedido.bind(this)} />                      
            
            <Col  xs="12" md="12">
              <FormGroup row>
                <Col xs="12" md="6">
                <Table hover bordered striped responsive size="sm">
                  <tr>
                    <td bgcolor="#d5f5e3" style={{maxWidth: '70px'}}>
                    <Label htmlFor="text-input">Lote</Label>
                    </td>                            
                    <td bgcolor="#d5f5e3">
                    <Input type="select" name="lote" id="lote" value={this.state.lote} required
                      onChange={e => { this.setState({ lote: e.target.value }) }}
                      onClick={ this.handleCantidadLote.bind(this)}>
                      <option value="" >Lote..   </option>
                      {this.state.list_lote.map(item1 => (
                        <option value={item1.lote.id_lote} >{item1.lote.id_lote}</option>
                      ))}
                    </Input>
                    </td>
                    <td bgcolor="#d5f5e3" style={{maxWidth: '100px'}} >
                    <Label htmlFor="text-input">Cantidad Existente</Label>
                    </td>
                    {/* "#eaeded" */}
                    <td bgcolor="#d5f5e3" style={{maxWidth: '70px'}}>
                    <Input type="text" id="cantidad" name="cantidad" className="form-control" value={this.state.cantidadAlmacen}   />
                    </td>
                  </tr>
                </Table>
                </Col>
                <Col xs="12" md="1">
                <Label htmlFor="text-input">Cantidad</Label>
                </Col>
                <Col xs="12" md="2">
                <Input type="number" id="cantidad" name="cantidad" className="form-control" onChange={e => this.setState({ cantidad_solicitada: e.target.value })}   />
                </Col> 
                <Col xs="12" md="1">
                <Label htmlFor="text-input">Unidad P</Label>
                </Col>
                <Col xs="12" md="2">
                <Input type="select" name="unidadProd" id="unidadProd" value={this.state.unidadProd} required
                  onChange={e => { this.setState({ unidadProd: e.target.value }) }}>
                  <option value="" required>Unidad...</option>
                  <option value="PIEZA">PIEZA</option>
                  <option value="CAJA">CAJA</option>
                  <option value="FRASCO">FRASCO</option>
                </Input>   
                </Col>
              </FormGroup>
            </Col>
            
            <Form>
              <Col>
                <Table hover bordered striped responsive size="sm">
                  <thead style={styles2}>
                  <tr >
                    <th rowSpan="2"><p>Numero</p></th>
                    <th rowSpan="2"><p>Codigo</p></th>
                    <th rowSpan="2"><p>Detalle</p></th>
                    <th rowSpan="2"><p>Unidad</p></th>
                    <th colSpan="2"><center>Cantidad</center></th> 
                    <th rowSpan="2"><p>Acciones</p></th>                  
                  </tr>
                  <tr>
                    <th >Solicitada</th>
                    <th>Entregada</th>                          
                  </tr>
                  </thead>
                  <tbody>
                    {this.state.productos.map((u, i)=> {
                      return (
                        <tr key = {i} >
                          <td style={{maxWidth: '5px', textAlign: 'center'}}>{i+1}</td>
                          <td style={{maxWidth: '50px'}} >{u.Codigo} </td>
                          <td style={{maxWidth: '150px'}}>{u.Nombre}</td>
                          <td style={{maxWidth: '50px'}}>{u.UnidadProd}</td>
                          <td style={{maxWidth: '30px'}}>{u.Cantidad_Solicitada}</td>
                          <td style={{maxWidth: '30px'}}>{}</td>
                          <td style={{maxWidth: '50px'}}>
                            <center>
                            <Button onClick={(e)=>this.fEliminar(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>    
                            </center>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
              </Col>                   
            </Form>
          </Card>  
          <center>
          <Button onClick={this.onValidacion} className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-save"></i><span>Registrar</span></Button>
          <Button onClick={this.onCancelar} className="btn-google-plus btn-brand mr-1 mb-1"><i className=" 	fa fa-times-circle"></i><span>Cancelar</span></Button>
          </center>      
        </Collapse>
      </CardBody> 
    </Card>  
  </Col>
</Row>
        
  {/* MODAL GENERAR PEDIDO */}
  <Modal isOpen={this.state.primary3} toggle={this.togglePrimary3} className={'modal-primary ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary3}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea Generar un Nuevo Pedido?
      </ModalBody>
    <ModalFooter>
      <Button onClick={this.onGeneraPedido} type="submit" color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimary3}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL GENERAR PEDIDO */}

  {/* MODAL ADVERTENCIA */} 
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
      <ModalBody>
        <strong>{this.state.message}</strong>
        {this.state.showCant && <Alert color="danger"><strong>{this.state.message2}</strong></Alert>}
      </ModalBody>
    <ModalFooter>
      <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL ADVERTENCIA */}

  {/* MODAL IMPRESION*/}
  <Modal isOpen={this.state.primaryPrint} toggle={this.togglePrint} className={this.props.className}>
    <ModalHeader toggle={this.togglePrint}><i className="fa fa-print"></i> <strong> Imprimir Pedido</strong></ModalHeader>
      <ModalBody>
      {this.state.message}
      {!this.state.showImp && <Button  onClick={this.print} color="link" className="btn btn-nfo float-right">
      <i className="fa fa-print"></i> Imprimir Pedido
      </Button>}
      </ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={this.togglePrint}>Aceptar</Button>{' '}
    </ModalFooter>
  </Modal>
  {/* FIN MODAL IMPRESION*/}
  
  {/* MODAL REGISTRAR DATOS */} 
  <Form>
  <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
    <ModalFooter>
      <Button onClick={this.onFormSubmit} type="submit" color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
    </ModalFooter>
  </Modal>
  </Form>
  {/* FIN MODAL REGISTRAR DATOS */} 

  {/* MODAL MEDICAMENTO NOMBRE*/}
  <Modal isOpen={this.state.primaryProdNombre} toggle={this.togglePrimaryProdNom} className={'modal-info ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimaryMedNom}>Elija Medicamento</ModalHeader>
      <ModalBody>
        <Table responsive size="sm">
        <tbody>
        {this.state.listaProdNombre.map((item, indice) => ( 
          <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item.codigo}</td>
          <td>{item.nombre}</td>
          <td>
          <Button color="info" onClick={(e)=>this.handleChange2(item.codigo)}>{"✓"}</Button>
          </td>
          </tr>
        ))}
        </tbody>
        </Table>
      </ModalBody>
  </Modal>
  {/* FIN MODAL MEDICAMNETO NOMBRE*/}
</div>
);
} 
}
export default RealizarPedidoA;

