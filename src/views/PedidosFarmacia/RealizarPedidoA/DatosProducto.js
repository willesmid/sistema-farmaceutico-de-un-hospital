import React, { Component } from 'react';
import {

  Col,  
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from 'reactstrap';
//import AntecedentesG from './Antecedentes/AntecedentesG';
class DatosProducto extends Component {
 

  constructor(props) {
    super(props);

    this.state = {
     
      primary: false,
      message: '',
      prescripcion:'',
      data: 0,
      
    };
    // this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)

  }


  
togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }

  
  render() {

if(this.props.list.length>0){
  return (
    <div className="animated fadeIn">
      {this.props.list.map(item => (
        <Form onSubmit={this.props.onAddPedido}>    
          <Col xs="12" md="12">
                    <FormGroup row>
                    
                      <Col xs="12" md="1">
                        <Label htmlFor="text-input">Codigo</Label>
                      </Col>
                      <Col xs="12" md="3">
                        <Input type="text" id="text-input" name="codigo" value={item.codigo} disabled />
                      </Col>
                      <Col xs="12" md="2">
                        <Label htmlFor="text-input">Nombre Producto</Label>
                      </Col>
                      <Col xs="12" md="4">
                        <Input type="text" id="text-input" name="nombre" value={item.nombre} disabled />
                      </Col>   
                      <Col xs="12" md="2">
                      <h2>
                      <Button onClick={this.onHandleSubmit} size="sm" className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-arrow-circle-right"></i><span>Adicionar Producto</span></Button>                     
                      </h2>
                      </Col>
                      
                  </FormGroup>
          </Col>
        </Form>
       
      ))} 

    </div>
   );
 }
else{
  return(
    <div className="animated fadeIn">

      <Form onSubmit={this.props.onAddPedido}>
        <Col xs="12" md="12">
          <FormGroup row>
            <Col xs="12" md="1">
              <Label htmlFor="text-input">Codigo</Label>
            </Col>
            <Col xs="12" md="3">
              <Input type="text" id="text-input" name="codigo" disabled />
            </Col>
            <Col xs="12" md="2">
              <Label htmlFor="text-input">Nombre Producto</Label>
            </Col>
            <Col xs="12" md="4">
              <Input type="text" id="text-input" name="nombre" disabled />
            </Col>
            <Col xs="12" md="2">
            <h2>
            <Button onClick={this.onHandleSubmit} size="sm" className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-arrow-circle-right"></i><span>Adicionar Producto</span></Button>            
            </h2>
            </Col>
          </FormGroup>
        </Col>
      </Form>
  </div>  
  )
    }
  }
}



export default DatosProducto;
