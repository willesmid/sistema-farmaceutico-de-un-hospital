import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPA from './ElegirPA';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPA />, div);
  ReactDOM.unmountComponentAtNode(div);
});

