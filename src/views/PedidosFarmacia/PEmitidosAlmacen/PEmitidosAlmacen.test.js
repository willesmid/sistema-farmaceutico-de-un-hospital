import React from 'react';
import ReactDOM from 'react-dom';
import PEmitidosAlmacen from './PEmitidosAlmacen';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PEmitidosAlmacen />, div);
  ReactDOM.unmountComponentAtNode(div);
});
