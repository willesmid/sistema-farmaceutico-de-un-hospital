import React, { Component } from 'react';
import { 
  Button, 
  NavLink, 
  Table,
  Badge,
} from 'reactstrap';

import ValidarEstado2 from './ValidarEstado2'
import ValidarPersonal from './ValidarPersonal'
let url_pedido = '#/PedidosFarmacia/PedidosSSU/RecibirPedido/ElegirPedidoSSU'

const tdStyle = {
  fontSize: '0.9em'   
}

const trNavLink = {
  padding: '0',
  margin: '0'
}

class ListaPorRecibirSSU extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, 
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }

    const pedidos = this.props.lista_pedido.map((item, indice)=>{
      return(
        <tr key={item.indice}>
        <td id={indice} style={tdStyle}>
          {indice+1}
        </td>
        <td id={indice} style={tdStyle}>
          {item.id_pedido}
        </td>
        <td id={indice} style={tdStyle}>
          {item.localizacion.descripcion}
        </td>
        <td id={indice} style={tdStyle}>
          <ValidarPersonal idUnidad={item.unidad.id} />
          {item.personal.nombres+" "+item.personal.primer_apellido+" "+item.personal.segundo_apellido}
        </td>
        <td id={indice} style={tdStyle}>
          {item.fecha_emision}
        </td>
        <td id={indice} style={tdStyle}>
          {item.fecha_recibida}
        </td>
        <td id={indice} style={tdStyle} >
          {item.unidad}
        </td>
        
          <ValidarEstado2 estadoR={item.estado}/>
        <td id={indice}>
          <NavLink style={trNavLink} href = {url_pedido +'/'+ item.id_pedido +'/'+ item.unidad.id+'/'+ item.unidad +'/'+ item.personal.nombres+'/'+ (item.personal.primer_apellido+" "+item.personal.segundo_apellido)+'/'+item.localizacion.descripcion+'/'+item.fecha_emision+'/'+item.hora_emision+'/'+item.personal_externo+'/'+item.fecha_recibida+'/'+item.hora_recibida} >
           <Button size="sm" className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-hand-lizard-o"></i><span>Recibir</span></Button>
          </NavLink>
        </td>

        </tr>
      )
    })
  
  return (
    <div className="animated fadeIn">
      <Badge className="mr-1" color="light"><h6><i className="fa fa-align-justify"></i>{' '+this.props.titulo}</h6></Badge>      
      <Table responsive>
        <thead>
          <tr>
            <th>Nro.</th>
            <th>N°Pedido</th>
            <th>Unidad</th>
            <th>Realizado por</th>
            <th>F/Emision</th>
            <th>F/Recibida</th>
            <th >Pedido Externo SSU</th>
            <th>Estado</th>
            <th colspan="2" > <center>Acciones</center></th>
          </tr>
        </thead>
        <tbody>
          {pedidos}
        </tbody>
      </Table>
    </div>
  );
  }
}
export default ListaPorRecibirSSU;
