import React, { Component } from 'react';
import { Badge } from 'reactstrap';

class ValidarEstado2 extends Component {
  constructor(props) {
    super(props);
    this.state = {   
    };
  }

render() {
  const mensaje = (this.props.estadoR === "Por Recibir") ? "POR RECIBIR":((this.props.estadoR === "Recibido") ? "RECIBIDO":(this.props.estadoR === "Anulada") ? "ANULADA":"")
  const tipo = (this.props.estadoR === "Por Recibir")?"warning":(this.props.estadoR === "Recibido")?"success":(this.props.estadoR === "Anulada")?"danger":""
  console.log(tipo)
  return(
    <td>
      <Badge color={tipo}>{mensaje}</Badge>
    </td>
  )
  }
}
export default ValidarEstado2;