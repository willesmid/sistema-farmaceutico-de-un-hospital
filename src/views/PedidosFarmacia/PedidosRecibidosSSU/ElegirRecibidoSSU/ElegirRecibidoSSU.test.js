import React from 'react';
import ReactDOM from 'react-dom';
import ElegirRecibidoSSU from './ElegirRecibidoSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirRecibidoSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});

