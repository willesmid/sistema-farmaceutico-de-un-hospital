import React from 'react';
import ReactDOM from 'react-dom';

import PedidosRecibidosSSU from './PedidosRecibidosSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PedidosRecibidosSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});
