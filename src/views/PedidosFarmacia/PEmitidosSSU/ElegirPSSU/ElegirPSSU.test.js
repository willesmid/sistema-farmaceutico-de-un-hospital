import React from 'react';
import ReactDOM from 'react-dom';
import ElegirSSU from './ElegirPSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});

