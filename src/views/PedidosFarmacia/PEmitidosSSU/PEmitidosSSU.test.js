import React from 'react';
import ReactDOM from 'react-dom';

import PEmitidosSSU from './PEmitidosSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PEmitidosSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});
