import React from 'react';
import ReactDOM from 'react-dom';
import RealizarPedidoSSU from './RealizarPedidoSSU';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RealizarPedidoSSU />, div);
  ReactDOM.unmountComponentAtNode(div);
});
