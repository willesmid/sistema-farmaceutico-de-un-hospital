import React from 'react';
import ReactDOM from 'react-dom';
import ElegirPedidoE from './ElegirPedidoE';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirPedidoE />, div);
  ReactDOM.unmountComponentAtNode(div);
});

