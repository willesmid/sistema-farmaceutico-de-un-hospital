
import React, { Component } from 'react';
import Search from 'react-search-box';






import {
  Button,
  Card,
  CardBody,
 
  CardHeader,
  Col,
  Form,
  FormGroup,

  Input,
  Label,
  Row,

  
} from 'reactstrap';

class Buscador extends Component {
  constructor() {
    super();

    this.state = {
      data: [],
      loading: false,
      muestrBoton: false,
    };

   
  }
  


  componentDidMount() {
    this.setState({
      loading: true
    });

    fetch('http://127.0.0.1:8000/v1/productos/')
    .then(res => res.json())
    .then(data => {
      this.setState({
        data: data,
        loading: false
      });


    })
    fetch('https://api.github.com/search/repositories?q=topic:ruby+topic:rails')
    .then((res) => {
             console.log(res.json());
        })
        fetch('http://127.0.0.1:8000/v1/productos/')
    .then((res) => {
             console.log(res.json());
        })
  }

  handleChange(value) {
    console.log(value);
    this.setState({ muestrBoton: true });

  }


  render() {

    

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
           <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i>Elija Medicamento
              </CardHeader>
              <CardBody>
              <Form onSubmit={this.props.onAddObjetivo}>
              <FormGroup row>
                
                <Col md="2">
                 <Label htmlFor="text-input">Buscar por codigo</Label>
                </Col>
                <Col  md="4">
                  <Search
                    data={ this.state.data }
                    onChange={ this.handleChange.bind(this) }
                    placeholder="Ingresa el codigo..."
                    class="search-class"
                    searchKey="codigo"
                    ref="codigo"
                    
                    className="codigo"
                    
                     

                  />

           
           
                </Col>

               


                <Col md="2">
                 <Label htmlFor="text-input">Buscar por Nombre</Label>
                </Col>
                <Col  md="4">
                
                  <Search 
                    data={ this.state.data }
                    onChange={ this.handleChange.bind(this) }
                    placeholder="Ingresa el nombre..."
                    class="search-class"
                    searchKey="nombre"
                    ref="nombre"
                    name="nombre"
                    
                     

                  />
            

                </Col>
              </FormGroup>

              <FormGroup row>
                
           
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Cantidad</Label>
                </Col>
                <Col xs="12" md="2">
                  <Input type="number" id="number-input" 
                   ref="cantidad"
                   name="cantidad" 

                  />
                </Col> 

                <Col xs="12" md="3">
               
                </Col> 


                <Col xs="12" md="2">
                      <Label htmlFor="textarea-input">Prescripcion</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Input 
                        type="textarea"
                        id="textarea-input"  
                        name="prescripcion"
                    
                        rows="4"
                        placeholder="Contenido"
                        ref="prescripcion"
                      />
                    </Col>
              </FormGroup>

           
                    <Button type="submit" color="success" className="mr-1">Adicionar</Button>
                    <Button type="reset" size="md" color="danger" className="mr-1" onClick = {this.handleClose}>Cancelar</Button>         
                </Form>
              </CardBody>
            </Card>
          </Col>
          
        </Row>
        


      </div>

    );
  }
}
export default Buscador;




