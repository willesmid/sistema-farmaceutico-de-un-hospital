import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row, Button} from 'reactstrap';
//import AntecedentesG from './Antecedentes/AntecedentesG';
class DatosMedicamento extends Component {
 

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);

    this.state = {
      collapse: false,
      fadeIn: true,
      timeout: 300,
      estudiante: [],
      lista: [],
      idA:'a'
    };
  }
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  /*
  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch('http://localhost:8000/odontologia/persona/v1/estudiantes/'+4914509+'/');
      const estudiante = await respuesta.json();
      this.setState({
        estudiante
      });
    } catch (e) {
        console.log(e);
    }
  }*/






  
  render() {

    console.log(this.props.list);
    const lista = this.props.list.map((item,indice)=>{
      return(
        <tr key={item.producto_farmaceutico.codigo}>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.producto_farmaceutico.stockGeneral}</td>
          <td>{item.t_forma_farmaceutica.id_forma_farmaceutica}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>{item.concentracion}</td>
          <td>{item.clasificacion}</td>

        </tr>
        
      )
      
    }

  )
  
    return (

    <div className="animated fadeIn">
    <Row>
          <Col xs="12">
            <Card>
              
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Datos Medicamento</strong>
              </CardHeader>
    
              <CardBody>
          
                  <div>
                  <table class="table">
                        <thead>
                        <tr>
                          
                          <th>Codigo</th>
                          <th>Nombre</th> 
                          <th>asasas</th>
                          <th>asasas</th>                         
                          <th>asasas</th>                         
                          <th>asasas</th>
                          <th>asasas</th>                         
                                                                  
                                                                  
                                                                 
                                         
                    
                        </tr>
                        
                          </thead>
                          <tbody>
                                        {lista}
                                        {/*this.setState({idA:lista[0].key})*/}
                                         {/*console.log(this.state.idA)*/}
                                         
                        </tbody>
                  </table>
                  </div>
          
                  <Button className="btn btn-outline-primary" href="#/Tratamiento">Tratamiento</Button>   
                  <Button className="btn btn-outline-primary" onClick={this.toggle} style={{ marginLeft: '1rem' }}>Primera Consulta</Button>
               
                  
              </CardBody>
            </Card>
          </Col>
        </Row>

                  {/* <Collapse isOpen={this.state.collapse}>

                       <AntecedentesG idA={lista[0].key}/>
      
                  </Collapse> */}
    </div>
    );
  }
}

export default DatosMedicamento;
