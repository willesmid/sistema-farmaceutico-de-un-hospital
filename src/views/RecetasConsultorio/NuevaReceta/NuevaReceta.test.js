import React from 'react';
import ReactDOM from 'react-dom';
import NuevaReceta from './NuevaReceta';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevaReceta />, div);
  ReactDOM.unmountComponentAtNode(div);
});
