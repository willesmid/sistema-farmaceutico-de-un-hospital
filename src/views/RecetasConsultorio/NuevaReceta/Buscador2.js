
import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import  Alerta from '../Alerta/Alerta';
import DatosMedicamento from './DatosMedicamento';
import Boton from './Boton';



import Search from 'react-search-box';


class Buscador2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,

      data: [],
      list:[],
      idA:0,
      swA:false,
      
      show: false,
      show2: false,

      loading: false,
      muestrBoton: false,
    };
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona

    this.setState({
      loading: true
    });

    try {
      const respuesta = await fetch('http://localhost:8000/v1/productos/');
      const data = await respuesta.json();

      if(this.state.data.length===0){
        console.log("no hay nada")
      }

      this.setState({
        data,
        loading: false
        
      });
    } catch (e) {
        console.log(e);
    }
  }



  
handleChange(value) {
  //console.log(value); 


  console.log("data=",this.state.data)
  //const listAl = this.state.data.map((item, indice)=>{

  this.state.data.map((item, indice)=>{

    if (value === item.codigo){    
      
     fetch('http://localhost:8000/v1/detalleMedicamentoBusca/'+item.codigo+'/')
    .then((Response)=>Response.json())
    .then((findresponse) => {
     //   console.log("FINDRESPONSE=",findresponse)
        this.setState({
          list: findresponse
        })
       // console.log('LIST=',this.state.list.length)   

        if (this.state.list.length===0) {
          this.setState({
            show: false,
            show2: true
          })
          
         // document.write("No hay " + this.state.list.length);
        }
        else{
          this.setState({
            show: true,
            show2: false
          })
          //document.write("SI hay "+ this.state.list.length);
        } 
    });

    }
    return 0
}


)

  this.setState({ muestrBoton: true});

}





  render() {


    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }

    return (
      
      <div className="animated fadeIn">
        
        <Row>
          <Col xs="12">
            <Card>
              
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Buscar Paciente</strong>
              </CardHeader>
    
              <CardBody>
                <form>
                <div className="form-group">
  
                        <Search                       
                        data={this.state.data}
                        onChange={ this.handleChange.bind(this) }
                        placeholder="Matricula PROMES"
                        class="search-class"
                        searchKey="codigo"
                        />
                    
                    {/*console.log(Object.keys(this.state.list))*/}
                    {/*console.log(this.state.list)*/}
                    {/*console.log(Object.values(this.state.list))*/}
                    
                    
                </div>
              </form>
              
              { this.state.muestrBoton ? <Boton /> : null }
              </CardBody>
            </Card>
          </Col>
          
        </Row>

        {this.state.show && <DatosMedicamento list={this.state.list}  />}
        {this.state.show2 && <Alerta/>}
    </div>

    );
  }
  
}


export default Buscador2;

