import React, { Component } from 'react';

//import Tables from '../Tables/Tables';
//import Pagination from '../ObjetivosGestion/Pagination';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Col,
  Form,
  Row,
  Table,
  FormGroup,


} from 'reactstrap';


import Cabezera from '../NuevaReceta/Cabezera';
import Buscador from '../NuevaReceta/Buscador';
import Buscador2 from '../NuevaReceta/Buscador2';






class NuevaReceta extends Component {
  constructor(props) {
    super(props);
    this.state = {
      objetivos:[],
      Cabezera: [],
      Personal:[],
      Cargos:[],
      formulario:1,
     

      
      Codigo:'',
      Nombre:'',
      Cantidad:'',
      Prescripcion:'',
      ResponsableInformacion:'',
      Cargo:'',
      warning: false,
      bandera:false,
      pageOfItems: []
     }; 
     this.onFormSubmit = this.onFormSubmit.bind(this)
     this.fEditar = this.fEditar.bind(this);
     this.toggleWarning = this.toggleWarning.bind(this);
     this.onChangePage = this.onChangePage.bind(this);
  }
  handleOnAddObjetivo (event) {
    event.preventDefault();
    let obj = {
      Codigo: event.target.cantidad.value,
      Nombre:  event.target.cantidad.value,
      Cantidad: event.target.cantidad.value,
      Prescripcion:event.target.prescripcion.value,
      
      
    
    };
    this.setState({
      objetivos: this.state.objetivos.concat([obj]),
      bandera: true,
      
    });
    event.target.reset();



    
  }
 toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }

 

async componentDidMount() {
  try {
    const respuesta = await fetch('http://localhost:8000/v1/DatosFormulario/');
    const Cabezera = await respuesta.json();
    const respuesta1 = await fetch('http://localhost:8000/v1/ListaPersonal/');
    const Personal = await respuesta1.json();
    const respuesta2 = await fetch('http://localhost:8000/v1/ListaCargos/');
    const Cargos = await respuesta2.json();
    setTimeout(()=> this.setState({
      Cabezera, //la cabezera de formulario que recibimos de la ruta se muestra aqui,
      Personal,
      Cargos,
    }),1000);
  } catch (e) {
    console.log(e);
  }
}


onChangePage(pageOfItems) {
  // update state with new page of items
  this.setState({ pageOfItems: pageOfItems });
}


render() {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col xs="12" md="12">
          <Card>
          
             <Form>
          
      
                    <CardBody>
                    <Cabezera/>
                    
                      {/* <Cabezera/>  */}
                      <Buscador  onAddObjetivo={this.handleOnAddObjetivo.bind(this)} />
                    <Buscador2 />
                        <Card>
                          <Table responsive>
                          <thead>
                            <tr>

                              <th>Numero</th>
                              <th>Codigo</th>
                              <th>Nombre</th>
                              <th>Cantidad</th>
                              <th>Prescripcion</th>
                              <th>Acciones</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.objetivos.map((u, i)=> {
                              return (
                                <tr key = {i}>
                                  <td style={{maxWidth: '5px', textAlign: 'center'}}>{i+1}</td>
                                  <td style={{maxWidth: '50px'}} >{u.Codigo} </td>
                                  <td style={{maxWidth: '50px'}}>{u.Nombre}</td>
                                  <td style={{maxWidth: '50px'}}>{u.Cantidad}</td>
                                  <td style={{maxWidth: '50px'}}>{u.Prescripcion}</td>
                                  <td style={{maxWidth: '50px'}}>
                                    {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                                    <Button onClick={(e)=>this.fEliminar(u)} type="button">Eliminar</Button></td>
                                    
                                </tr>
                                     )
                            })}
                          
                          </tbody>
                          </Table>
                          {/* <Pagination items={this.state.objetivos} onChangePage={this.onChangePage} /> */}


                        </Card>
                     
                    </CardBody> 
                      <CardFooter>
                        <hr/>
                          <FormGroup row>
                            <Col xs="12" md="4">
                            </Col>
                            <Col xs="12" md="4">
                            <Button type="submit" color="success" className="mr-1" disabled={!this.state.bandera} onClick={this.onFormSubmit}>Registrar</Button>
                              {/* <Button type="submit" color="success" className="mr-1" disabled={!this.state.bandera} onClick={this.state.bandera==='' ? this.onFormSubmit:this.toggleWarning}>Registrar</Button> */}
                            <Button type="reset" size="md" color="danger" className="mr-1" onClick = {this.handleClose}>Cancelar</Button>
                            </Col>
                            <Col xs="12" md="4">
                            </Col>
                          </FormGroup>
                     </CardFooter>
                </Form>
              </Card>  
            </Col>
          </Row>
         

        </div>
     );
   
  
  }
  
}

export default NuevaReceta;



