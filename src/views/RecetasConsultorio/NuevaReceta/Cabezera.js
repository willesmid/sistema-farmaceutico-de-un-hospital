import React, { Component } from 'react';

import {

  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';
class Cabezera extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Cabezera: [],
    };
  }
// para datos de la cabezera
  async componentDidMount() {
    try {
      
      const respuesta = await fetch('http://localhost:8000/v2/ListaReceta/');
      const Cabezera = await respuesta.json();
      this.setState({
        Cabezera //la cabezera de formulario que recibimos de la ruta se muestra aqui
      });
    } catch (e) {
      console.log(e);
    }
  }
//para datos de la cabezera
  render() {
    return (
      <div className="animated fadeIn">
          {this.state.Cabezera.map(item => (
        <Row>
          <Col xs="12" md="12">
           <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Recetar
              </CardHeader>
              <CardBody>
                <Form>
                <FormGroup row>
                  <Col md="9">
                  </Col>
                  <Col md="1">
                  <Label htmlFor="text-input">Fecha</Label>
                  </Col>
                  <Col  md="2">
                    <Input 
                    type="date" 
                    id="date-input" 
                    name="date-input" 
                    placeholder="Fecha"
                    value={item.fecha} 

                    />
                  </Col>


                </FormGroup>
                <FormGroup row>
                  
                  <Col md="1">
                    
                  </Col>
                  <Col xs="12" md="2">
                    <Label htmlFor="text-input">CODIGO AFILIADO</Label>
                  </Col> 
                    <Col xs="12" md="5" >
                    <Input 
                    type="text" 
                    id="text-input" 
                    name="text-input" 
                    value={item.consulta.afiliado.codigoAfiliacion }   

                    />
                  </Col> 
                  <Col xs="12" md="1">
                  
                  </Col>
                  <Col xs="12" md="1">
                    <Label htmlFor="text-input">id Receta</Label>
                  </Col>
                  <Col xs="12" md="2">
                    <Input type="text" id="text-input" name="text-input" value={item.id_receta} disabled />
                  </Col>  
                </FormGroup>
                
                <FormGroup row>
                
                  <Col xs="12" md="5">
                  
                  </Col>
                

                  <Col md="1">
                  
                  </Col>
                  <Col  md="3">
                    
                  </Col>
                  <Col xs="12" md="1">
                    <Label htmlFor="text-input">id consulta</Label>                     
                  </Col>
                  <Col xs="12" md="2">
                    <Input type="text" id="text-input" name="text-input" value={item.consulta.id_consulta} disabled />
                  </Col>
                </FormGroup>

                <FormGroup row>
                
                
              

                <Col md="2">
                  <Label htmlFor="text-input">Nombre y Apellido</Label>
                </Col>
                <Col  md="3">
                  <Input 
                  type="text" 
                  id="text-input" 
                  name="text-input" 
                  value={item.consulta.afiliado.nombres+"   "+ item.consulta.afiliado.apellidoPaterno+"   "+ item.consulta.afiliado.apellidoMaterno  }   disabled

                  />
                </Col>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Matricula</Label>                     
                </Col>
                <Col xs="12" md="2">
                  <Input type="text" id="text-input" name="text-input" value={item.consulta.afiliado.cedulaIdentidad} disabled />
                </Col>
                <Col md="1">
                  <Label htmlFor="text-input">Medico</Label>
                  </Col>
                <Col xs="12" md="3">
                  <Input type="text" id="disabled-input" name="disabled-input" disabled  value={"Dr. "+item.consulta.medico.nombres+" "+item.consulta.medico.apellidoPaterno } align="center" />
                </Col>
              </FormGroup>
                <hr/>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
          ))} 
      </div>
     );
   }
 }
export default Cabezera;