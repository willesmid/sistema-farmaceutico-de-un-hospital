import React from 'react';
import ReactDOM from 'react-dom';
import RecetasEmitidas from './RecetasEmitidas';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RecetasEmitidas />, div);
  ReactDOM.unmountComponentAtNode(div);
});
