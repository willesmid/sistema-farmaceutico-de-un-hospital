import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup,
  Button
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
const styles2 = { color: 'white', backgroundColor: '#154360', textAlign:'center' }

class ElegirReceta extends Component {
  constructor(props) {
    super(props)
    this.state = {
        url_receta: '#/RecetasEnConsultorio/RecetasEmitidas/ElegirReceta',
        list: [],
        list_insumo:[],
        list_otro:[],
        entregado:'',    
    }
    this.print = this.print.bind(this)
    this.print2 = this.print2.bind(this)
    this.print3 = this.print3.bind(this)
    console.log(this.props.match.params)
  }

componentDidMount() {
  fetch('http://localhost:8000/farmacia/recetar/v1/detalleMedicamentoRecetaBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list: findresponse,
          url_receta : this.state.url_receta +'/'+ this.props.match.params.id_receta +'/'+ this.props.match.params.id_consulta +'/'+ this.props.match.params.codigoAfiliacion +'/'+this.props.match.params.cedulaIdentidadA+'/'+ this.props.match.nombresA +'/'+ this.props.match.params.apellidoPaternoA +'/'+ this.props.match.params.apellidoMaternoA+'/'+this.props.match.params.nombresM+'/'+this.props.match.params.apellidoPaternoM+'/' +this.props.match.params.fecha +'/resultados',
      })
      console.log("que muestra aqui"+this.props.match.params.farmaceuticaN)
    if(this.props.match.params.farmaceuticaN !=='Ninguno'){
      this.setState({
        entregado:'Dr. '+this.props.match.params.farmaceuticaN+' '+ this.props.match.params.farmaceuticaA
      })
    }
    if(this.props.match.params.fecha_entrega !=='null'){
      this.setState({
        fecha_entrega:this.props.match.params.fecha_entrega
      })
    }

  });

  
  fetch('http://localhost:8000/farmacia/recetar/v1/DetalleInsumoRecetaBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_insumo: findresponse
      })  
  });

  fetch('http://localhost:8000/farmacia/recetar/v1/OtroMedicamentoBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_otro: findresponse
      })
      console.log(this.state.list_otro)
      console.log("holasss")    
      // console.log('LIST=',this.state.list.length)   
      if (this.state.list.length===0) {
        this.setState({
          show: false,
          show2: true
        })
        // document.write("No hay " + this.state.list.length);
      }
      else{
        this.setState({
          show: true,
          show2: false
        })
        //document.write("SI hay "+ this.state.list.length);
      }
  });
  
}

print(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(155, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.props.match.params.id_receta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(155, 19, "VALIDEZ DE ESTA RECETA 48 HORAS");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombresA +" "+ this.props.match.params.apellidoPaternoA+" "+ this.props.match.params.apellidoMaternoA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoAfiliacion)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision)

var res = doc.autoTableHtmlToJson(document.getElementById("tablaT"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaTP"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp: true});

window.open(doc.output('bloburl'), '_blank');
}

print2(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(155, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.props.match.params.id_receta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(155, 19, "VALIDEZ DE ESTA RECETA 48 HORAS");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombresA +" "+ this.props.match.params.apellidoPaternoA+" "+ this.props.match.params.apellidoMaternoA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoAfiliacion)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision)

var res = doc.autoTableHtmlToJson(document.getElementById("tablaP"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaPP"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp: true});

window.open(doc.output('bloburl'), '_blank');
}

print3(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(158, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.props.match.params.id_receta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(158, 19, "ADQUIRIR EN FARMACIA EXTERNA");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombresA +" "+ this.props.match.params.apellidoPaternoA+" "+ this.props.match.params.apellidoMaternoA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoAfiliacion)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+ this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision)


var res = doc.autoTableHtmlToJson(document.getElementById("tablaO"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaOP"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp1: true});
window.open(doc.output('bloburl'), '_blank'); 
 
}

render() {
  const tableM=this.state.list.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"medicamento"}</td>
        <td>{u.codigo.producto_farmaceutico.codigo} </td>
        <td>{u.codigo.producto_farmaceutico.nombre}</td>
        <td>{u.codigo.t_forma_farmaceutica.descripcion}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableMP=this.state.list.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- medicamento: "+u.prescripcion}</td>
      </tr>
    )
  })
  const tableI=this.state.list_insumo.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"insumo"}</td>
        <td>{u.codigo.producto_farmaceutico.codigo} </td>
        <td>{u.codigo.producto_farmaceutico.nombre}</td>
        <td>{u.codigo.t_presentacion.descripcion}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableIP=this.state.list_insumo.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- insumo: "+u.prescripcion}</td>
      </tr>
    )
  })

  const tableO=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"producto externo"}</td>
        <td>{"SN"} </td>
        <td>{u.nombre}</td>
        <td>{u.forma_farmaceutica}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableOP=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- producto externo: "+u.prescripcion}</td>
      </tr>
    )
  })

  const tableO2=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.nombre}</td>
        <td>{u.forma_farmaceutica}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableOP2=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- "+u.prescripcion}</td>
      </tr>
    )
  })
return (
<div>
<Table responsive striped hidden id="tablaT">
    <thead>
      <tr>
        <th>No.</th>
        <th>TIPO</th>
        <th>CODIGO</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableM}
    {tableI}
    {tableO}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaTP">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableMP}
    {tableIP}
    {tableOP}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaP">
    <thead>
      <tr>
        <th>No.</th>
        <th>TIPO</th>
        <th>CODIGO</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableM}
    {tableI}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaPP">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableMP}
    {tableIP}
    </tbody>
  </Table>
  <Table responsive striped hidden id="tablaO">
    <thead>
      <tr>
        <th>No.</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableO2}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaOP">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableOP2}
    </tbody>
  </Table>

<Row>
  <Col xs="12" md="12">
   <Card>
      <CardHeader>
      <i className=" 	fa fa-drivers-license-o"></i><strong>Detalle Receta</strong>
        <Button onClick={this.print3} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Externa
        </Button>
        <Button  onClick={this.print2} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Promes
        </Button>
        <Button  onClick={this.print} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta
        </Button>
      </CardHeader>
      <CardBody>
        <Form >
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">NOMBRES - APELLIDOS</Label>
          </Col>
          <Col  md="4">
            <Input type="text" id="text-input" name="nombreCompleto" value={this.props.match.params.nombresA +"   "+ this.props.match.params.apellidoPaternoA+"   "+ this.props.match.params.apellidoMaternoA} />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Consulta</Label>                     
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_consulta" value={this.props.match.params.id_consulta} disabled />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Receta</Label>
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_recetaA" value={this.props.match.params.id_receta} disabled />
          </Col>  
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Codigo de Afiliado</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="codigo" name="codigoAlifiacion" value={this.props.match.params.codigoAfiliacion} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="date" id="fecha_e" name="fecha_e" value={this.props.match.params.fecha_emision} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora_e" name="hora_e" value={this.props.match.params.hora_emision} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Prescrito por  Medico</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="medico"  value={"Dr. "+this.props.match.params.nombresM+" "+this.props.match.params.apellidoPaternoM} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Entrega</Label>
          </Col>
          <Col md="2">
          <Input type="date" id="fecha" name="fecha" value={this.props.match.params.fecha_entrega} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Entrega</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora" name="hora" value={this.props.match.params.hora_entrega} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>        
          <Col md="2">
          <Label htmlFor="text-input">Receta Entregada Por</Label>
          </Col> 
          <Col md="4">
          <Input type="text" id="farmaceutica" name="farmaceutica"   value={this.state.entregado} disabled/>
          </Col>
        </FormGroup>
        </Form>
           
        <Table hover bordered striped responsive size="sm" >
        <thead  style={styles2} >
        <tr>
          <th><p></p>Numero<p></p></th>
          <th><p></p>Codigo<p></p></th>
          <th><p></p>Nombre Producto<p></p></th> 
          <th><p></p>Forma/Presentacion<p></p></th>
          <th><p></p>Cantidad<p></p></th>                         
          <th ><p></p>Prescripción (Indicaciones)<p></p></th> 
          <th><p></p>Tipo de producto<p></p></th>                                 
        </tr>
        </thead>

         <tbody>
           {/* parte que hace el llenado de un estado  */}
           {this.state.list.map((u, i)=> {
            return (
              <tr key = {i} bgcolor="#ebf5fb">
                <td style={{maxWidth: '5px', backgroundColor: '#ebf5fb', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                <td style={{maxWidth: '200px', backgroundColor: '#ebf5fb'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.codigo.t_forma_farmaceutica.descripcion} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb', textAlign: 'center'}} >{u.cantidad} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.prescripcion} </td> 
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb', textAlign: 'center'}} >{"medicamento"} </td> 
              </tr>
            )
           })}
          
          {this.state.list_insumo.map((u, i)=> {
             return (
              <tr key = {i} bgcolor="#f2f4f4">
                <td style={{maxWidth: '5px', backgroundColor: '#f2f4f4', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#f2f4f4'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.codigo.t_presentacion.descripcion} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4', textAlign: 'center'}} >{u.cantidad} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.prescripcion} </td> 
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4', textAlign: 'center'}} >{"insumo"}</td> 
              </tr>
            )
           })}

           {/* parte que hace el llenado de un estado  */}
           {this.state.list_otro.map((u, i)=> {
             return (
              <tr key = {i} bgcolor="#ffffff">
                <td style={{maxWidth: '5px', backgroundColor: '#ffffff', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{"SN"} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#ffffff'}} >{u.nombre} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.forma_farmaceutica} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}} >{u.cantidad} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.prescripcion} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}} >{"producto externo"}</td> 
              </tr>
                    )
           })}
         </tbody>
         </Table> 
      </CardBody>
    </Card>
  </Col>
  <CardBody> 
</CardBody>
</Row>
      </div>
    )
  }
}

export default ElegirReceta;
