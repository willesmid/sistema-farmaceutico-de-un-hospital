import React from 'react';
import ReactDOM from 'react-dom';
import ElegirReceta from './ElegirReceta';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirReceta />, div);
  ReactDOM.unmountComponentAtNode(div);
});

