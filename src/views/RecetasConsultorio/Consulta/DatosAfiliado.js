import React, { Component } from 'react';
import { 
  Col, 
  FormGroup, 
  Form, 
  Label, 
  Input,
} from 'reactstrap';
//import AntecedentesG from './Antecedentes/AntecedentesG';
class DatosAfiliado extends Component {
 
  constructor(props) {
    super(props);

    this.state = {
    
      estudiante: [],
      lista: [],
      idA:'a'
    };
  }
  
  render() {
    return (
    <div className="animated fadeIn">
    {
        this.props.listAfiliado.map(item => (    

          <Col xs="12" md="12">
            <Form  >
            <FormGroup row>
              {/* <Col xs="12" md="1">
                <Label htmlFor="text-input">CODIGO AFILIADO</Label>
              </Col> 
                <Col xs="12" md="3" >
                <Input 
                type="text" 
                id="afiliado" 
                name="afiliado" 
                value={item.codAfiliado} 
              
                />
              </Col>     */}
            <Col md="2">
              <Label htmlFor="text-input">Nombre y Apellido</Label>
            </Col>
            <Col  md="5">
              <Input 
              type="text" 
              id="text-input" 
              name="nombreCompleto" 
              value={item.ci.nombres+"   "+ item.ci.primer_apellido+"   "+ item.ci.segundo_apellido  }   disabled
      
              />
            </Col>
            <Col xs="12" md="1">
              <Label htmlFor="text-input">Matricula</Label>                     
            </Col>
            <Col xs="12" md="4">
              <Input type="text" id="text-input" name="cedulaIdentidad" value={item.ci.ci} disabled />
            </Col> 
          </FormGroup>
            </Form>
            </Col>
      ))
      
      }
      
                 
    </div>
    );
  }
}

export default DatosAfiliado;

