import React from 'react';
import ReactDOM from 'react-dom';
import Consulta from './Consulta';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Consulta />, div);
  ReactDOM.unmountComponentAtNode(div);
});
