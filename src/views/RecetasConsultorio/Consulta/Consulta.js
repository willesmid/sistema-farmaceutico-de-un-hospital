import React, { Component } from 'react';
import {
   Button,
   Row,
   Col,
   Card,
   CardHeader,
   CardBody,
   ListGroupItem,
   FormGroup,
   Label,
   Form,
   Input,
   NavLink, 
   
} from 'reactstrap';

import Search from 'react-search-box';
import DatosAfiliado from './DatosAfiliado'

var url='http://localhost:8000/farmacia/recetar/v1/ConsultaGuardar/' 
let url_receta ='#/RecetasEnConsultorio/Consulta/Recetar'

const trNavLink = {
  padding: '0',
  margin: '0'
}
class Consulta extends Component { 
    constructor(props) {
      super(props);
  
      this.state = {
        primary: false,
        data:[],
        listAfiliado:[],
        loading: false, 
        fecha:'',

        medico:'',
        afiliado:'',
        medicosList:[],

        consultaList:[],
        codigoConsulta:0,
        showReceta:false,
    
        showLlenarReceta:false,

        showConsulta:true,
        Id_Receta:0,
        id_consulta:0,
        nombreM:'',
        apellido_pM:'',
        apellido_mM:'',

        //DESDE AQUI LOGIN
        logged_in: localStorage.getItem('token') ? true : false,
        nombre:'',
        paterno:'',
        materno:'',
        ci:0,
        
      };
      this.onFormSubmit = this.onFormSubmit.bind(this)  
    }

async componentDidMount() { //ciclo de vida de react de forma asincrona
  try {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd
    this.setState({
      fecha: hoy,
    });
    const respuesta = await fetch('http://localhost:8000/farmacia/recetar/v1/detalleafiliados/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    });
    const data = await respuesta.json();
    const respuesta2 = await fetch('http://localhost:8000/farmacia/recetar/v1/MedicoLista/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    });
    const medicosList = await respuesta2.json();
    this.setState({
      data, 
      medicosList,
    });

// DESDE AQUI CON EL LOGIN
    if(this.state.logged_in){
      if(typeof(localStorage.getItem('token')) === 'undefined'){
        window.location.href = '#/Login'
      }else{
        const respuesta7 = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
          })
          const datosUsuario = await respuesta7.json();
          this.setState({
            username:datosUsuario.username
          });

          try{
            // alert(this.state.username)
            const respuesta7 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.username+'/', {
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              }
            })
              const datosUsuario = await respuesta7.json();
              this.setState({
                nombre:datosUsuario[0].nombres,
                paterno: datosUsuario[0].primer_apellido,
                materno: datosUsuario[0].segundo_apellido,
                ci: datosUsuario[0].ci,
              });
              // alert(this.state.ci)
          } catch (e) {
              console.log(e);
          }
      }

    }
    else{
      window.location.href = '#/Login'
    }
// HASTA AQUI LOGIN 
  } catch (e) {
    console.log(e)
  }
}

handleOnAddAfiliado (event) {   
  this.setState({
    afiliado: event.target.afiliado.value,  
  });
  event.target.reset();
}

handleChange(value) {
  this.state.data.map((item)=>{
    if (value === item.codAfiliado){    
     fetch('http://localhost:8000/farmacia/recetar/v1/detalleafiliado/'+item.codAfiliado+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
        this.setState({
          listAfiliado: findresponse,
          codigoA: item.codAfiliado,
          nombreA:item.ci.nombres,
          apellido_pA:item.ci.primer_apellido,
          apellido_mA:item.ci.segundo_apellido,
        })
    });
    }
    return 0
}
)
}

onFormSubmit(e) {
  let datos= {
    afiliado: this.state.codigoA,
    medico: this.state.ci,
  }
  try {  
   console.log("datos ", datos)
    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(datos), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)
      console.log('codigoAfiliado:', response.id_consulta)
      this.setState({
        id_consulta:response.id_consulta,
      })
    });
  } catch (e) {
    console.log(e);
  }
this.setState({
showReceta:true,
showConsulta:false,
})

fetch('http://localhost:8000/farmacia/recetar/v1/MedicoBusca/'+this.state.medico+'/',{
  headers: {
    Authorization: `JWT ${localStorage.getItem('token')}`
  } 
})
.then((Response)=>Response.json())
.then((findresponse)=>{
  this.setState({
    nombreM:findresponse[0].nombres,
    apellido_pM:findresponse[0].apellidoPaterno,
    apellido_mM:findresponse[0].apellidoMaterno, 

  })
})
}

render() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }
      
return (
  
  <div className="animated fadeIn">  
      <Row>     
        <Col xs="12">          
          <Card>
            <CardHeader>
            <h3><i className="fa fa-clipboard"></i> <strong>Nueva Consulta</strong></h3>
            </CardHeader>
            <CardBody>
            <Form>
            <Card>
                <ListGroupItem action color="info">Datos de la Consulta </ListGroupItem>
                  <Col xs="12" md="12">
                  <br/>
                  <form>
                    <FormGroup row>
                      <Col  md="2">
                      <Label htmlFor="codigo"><strong>Ingrese Codigo del Afiliado: </strong></Label>
                      </Col>
                      <Col  md="3">
                        <Search                       
                          data={this.state.data}
                          onChange={ this.handleChange.bind(this) }
                          placeholder="Afiliado"
                          class="search-class"
                          type="string"
                          searchKey="codAfiliado"
                          className="form-control"
                          />
                      </Col>
                      <Col md="1">
                        <Label htmlFor="text-input">Medico</Label>
                      </Col>  
                      <Col md="3" style={{textAlign:'center'}}>      
                        <Input 
                          type="text" 
                          name="medico" 
                          id="text" 
                          required
                          value={'Dr. '+this.state.nombre+' ' + this.state.paterno + ' ' +this.state.materno}
                          onChange={e => this.setState({ medico: e.target.value })}>   
                          {/* <option value="">Seleccionar</option>
                            {this.state.medicosList.map(item1 => (
                                <option value={item1.cedulaIdentidad} >{"Dr. "+item1.nombres+" "+item1.apellidoPaterno }</option>
                                ))} */}
                        </Input>
                      </Col>
                      <Col md="1">
                        <Label htmlFor="fecha">Fecha</Label>
                        </Col>
                        <Col xs="12" md="2">
                        <Input type="date" id="fecha" name="fecha" className="form-control" defaultValue={hoy} disabled ref={fecha => this.fecha = fecha}/>
                      </Col>
                      
                    </FormGroup>
                  </form> 
              </Col>
              <DatosAfiliado listAfiliado={this.state.listAfiliado}/>
        </Card>
          


             <Card>
          <ListGroupItem action color="info">Diagnostico </ListGroupItem> 
          <CardBody>
          <Col xs="12" md="12">
              <FormGroup row>
             
                <Col xs="12" md="3">
                  <Label htmlFor="textarea-input">Detalle de Diagnostico  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    type="textarea"
                    id="aaa"  
                
                    rows="3"
                    placeholder="Contenido"
                    className="form-control"
                    onChange={e => this.setState({ prescripcion: e.target.value })} 
                    />
                </Col>

               
              </FormGroup>
            </Col>
            </CardBody>  
            
            </Card>
                              
        <center>
        {this.state.showConsulta && <Button color="primary" onClick={this.onFormSubmit} type="submit" className="mr-1">Guardar Consulta</Button>}
        </center>
        </Form>  
        <NavLink style={trNavLink} href = {url_receta +'/'+ this.state.id_consulta +'/'+ this.state.codigoA +'/'+ this.state.nombreA+'/'+ this.state.apellido_pA+'/'+ this.state.apellido_mA +'/'+ this.state.nombre+'/'+this.state.paterno+'/'+this.state.materno+'/'+this.state.fecha } >
<Button color="primary" type="submit" className="mr-1">Recetar</Button>
        </NavLink>    
   </CardBody>
   
          </Card>
        </Col>
      </Row>
  


    </div>
  )
}
            
}

export default Consulta;







