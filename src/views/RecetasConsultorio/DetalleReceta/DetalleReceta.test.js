import React from 'react';
import ReactDOM from 'react-dom';
import DetalleReceta from './DetalleReceta';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DetalleReceta/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
