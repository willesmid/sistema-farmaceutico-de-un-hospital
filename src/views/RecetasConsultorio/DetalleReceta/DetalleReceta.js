import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row, Pagination, PaginationItem, PaginationLink, Table
} from 'reactstrap';

class DetalleReceta extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <strong>Pedido de medicamentos e insumos medicos/Pedido de materiales</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                  <FormGroup row>
                    <Col md="1">
                      <Label htmlFor="unidad">Unidad solicitante</Label>
                    </Col>
                    <Col xs="12" md="8">
                      <Input type="text" id="unidad" name="unidad" placeholder="Enfermería" value="Enfermería" disabled />
                    </Col>
                    <Col md="1">
                      <Label htmlFor="pedido">No. pedido</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="text" id="pedido" name="pedido" value="25" disabled/>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="1">
                      <Label htmlFor="nombre">Nombre</Label>
                    </Col>
                    <Col xs="12" md="8">
                      <Input type="text" id="nombre" name="nombre" disabled/>
                    </Col>
                    <Col md="1">
                      <Label htmlFor="fecha">Fecha</Label>
                    </Col>
                    <Col xs="12" md="2">
                      <Input type="text" id="fecha" name="fecha" value="21/05/18" disabled/>
                    </Col>
                  </FormGroup>
                  <hr />
                  <Row>
                    <Col xs="12" lg="12">
                      <Table responsive striped>
                        <thead>
                        <tr>
                          <th>Código</th>
                          <th>Unidad</th>
                          <th>Detalle</th>
                          <th>Cantidad solicitada</th>
                          <th>Cantidad entregada</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td>376467</td>
                          <td>Ibuprofeno</td>
                          <td>400mg</td>
                          <td>15</td>
                          <td>
                            <Input type="number" id="cantidad" name="cantidad" placeholder="" />
                          </td>
                        </tr>
                        <tr>
                          <td>376467</td>
                          <td>Ibuprofeno</td>
                          <td>400mg</td>
                          <td>15</td>
                          <td>
                            <Input type="number" id="cantidad" name="cantidad" placeholder="" />
                          </td>
                        </tr>
                        <tr>
                          <td>376467</td>
                          <td>Ibuprofeno</td>
                          <td>400mg</td>
                          <td>15</td>
                          <td>
                            <Input type="number" id="cantidad" name="cantidad" placeholder="" />
                          </td>
                        </tr>
                        <tr>
                          <td>376467</td>
                          <td>Ibuprofeno</td>
                          <td>400mg</td>
                          <td>15</td>
                          <td>
                            <Input type="number" id="cantidad" name="cantidad" placeholder="" />
                          </td>
                        </tr>
                        <tr>
                          <td>376467</td>
                          <td>Ibuprofeno</td>
                          <td>400mg</td>
                          <td>15</td>
                          <td>
                            <Input type="number" id="cantidad" name="cantidad" placeholder="" />
                          </td>
                        </tr>
                        </tbody>
                      </Table>
                        <Button type="submit" color="primary">Registrar</Button>
                      <Pagination>
                        <PaginationItem disabled><PaginationLink previous tag="button">Anterior</PaginationLink></PaginationItem>
                        <PaginationItem active>
                          <PaginationLink tag="button">1</PaginationLink>
                        </PaginationItem>
                        <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
                        <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
                        <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
                        <PaginationItem><PaginationLink next tag="button">Siguiente</PaginationLink></PaginationItem>
                      </Pagination>
                    </Col>
                    
                  </Row>
                  
                  
                      
                    
                
                  
                  
                </Form>
              </CardBody>
            </Card>
            
          </Col>
        </Row>
       

        
      
      
      </div>
    );
  }
}

export default DetalleReceta;
