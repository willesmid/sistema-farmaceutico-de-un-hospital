import React from 'react';
import { 
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,  
 } from 'reactstrap';

const DatosReceta = (props) => (    
  <div>
  {
    props.listR.map(item => (
     
<Row>
 
  <Col xs="12" md="12">
      <Form >
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Nombres y Apellidos</Label>
          </Col>
          <Col  md="4">
            <Input type="text" id="text-input" name="nombreCompleto" value={item.consulta.afiliado.ci.nombres +"   "+ item.consulta.afiliado.ci.primer_apellido +"   "+ item.consulta.afiliado.ci.segundo_apellido} />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Consulta</Label>                     
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_consulta" value={item.consulta.id_consulta} disabled />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Receta</Label>
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_recetaA" value={item.id_receta} disabled />
          </Col>  
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Codigo de Afiliado</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="codigo" name="codigoAlifiacion" value={item.consulta.afiliado.codAfiliado} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="date" id="fecha_e" name="fecha_e" value={item.fecha_emision} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora_e" name="hora_e" value={item.hora_emision} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Prescrito por  Medico</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="medico"  value={"Dr. "+item.consulta.medico.nombres+" "+item.consulta.medico.primer_apellido + " " + item.consulta.medico.segundo_apellido} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Entrega</Label>
          </Col>
          <Col md="2">
          <Input type="date" id="fecha" name="fecha" value={item.fecha_entrega} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Entrega</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora" name="hora" value={item.hora_entrega} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>        
          <Col md="2">
          <Label htmlFor="text-input">Receta Entregada Por</Label>
          </Col> 
          <Col md="4">
          <Input type="text" id="farmaceutica" name="farmaceutica" disabled/>
          </Col>
        </FormGroup>
        </Form>

  </Col>
</Row>
  ))
  
  }
  </div>

  
)


export default DatosReceta;
