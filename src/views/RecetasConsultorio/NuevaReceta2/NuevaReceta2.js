import React, { Component } from 'react';
import {
   Button,
   Modal,
   ModalFooter,
   ModalHeader,
   ModalBody, 
   Alert,
   Label, 
   Input,
   Collapse,
   ListGroupItem,   
   Card, 
   CardBody, 
   CardHeader, 
   Col, 
   Row, 
   Form, 
   Table, 
   FormGroup,
  } from 'reactstrap';

import DatosMedicamento from './DatosMedicamento'

import DatosInsumo from './DatosInsumo'

import DatosReceta from './DatosReceta';
import Search from 'react-search-box';
const styles2 = { color: 'white', backgroundColor: '#154360', textAlign:'center' }
var url='http://localhost:8000/farmacia/recetar/v1/ListaMedicamentoRecetaGuardar/' 
var urlI='http://localhost:8000/farmacia/recetar/v1/InsumoRecetaGuardar/'

var url1='http://localhost:8000/farmacia/recetar/v1/OtroMedicamentoLista/'

class NuevaReceta2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      data1: [],
      dataN:[],
      dataIN:[],
      data3: 0,
      dataCant: 0,
      list:[],
      idA:0,
      swA:false,
      loading: false,
      Id_recetaA:'',
      id_recetaAA:'',
      recetas:[],
      CodigoAlifiacion: '',
      Fecha:'',
      Id_receta:'',
      Id_consulta:'',
      NombreCompleto:'',
      CedulaIdentidad:'',
      NombreMedico:'',
      listR:[],
      medicamentos:[],
      Codigo:'',
      Nombre:'',
      Forma:'',
      Cantidad:'',
      Prescripcion:'',
      prescripcion:'',
      cantidad:0,
      primary:false,
      primary2:false,
      codigo:'',
      showAA:false,
      showCant:false,
      num:0,
      message2:'',
      menssage:'',
      contarItem:0,
      collapse: false,
      collapse2: false,
      fadeIn: true,
      medicamentos2:[],
      nombre2:'',
      cantidad2:0,
      forma2:'',
      prescripcion2:'',
      listReceta:[],
      codigoReceta:0,
      showRecetaPrimero:true,
      showReceta:false,

      listInsumo:[],

      showInsumo:false,
      showMedicamento:true,
      dataI:[],
      insumos:[],

      list_medicamento:[],
      list_insumo:[],
      list_otro:[],
      dataCantidad:0,
      numeroTodo:0,

      showMedicamentoNombre:false,
      listaMedNombre:[],
      listaInsNombre:[],
      
      primaryMedNombre:false,
      primaryInsNombre:false,
      cantidad_prescripcion:0,
      //DESDE AQUI LOGIN
      logged_in: localStorage.getItem('token') ? true : false,
      ci:0,
    };
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.toggleDatos = this.toggleDatos.bind(this)
    this.toggle = this.toggle.bind(this)
    this.toggleFade = this.toggleFade.bind(this)
    this.onValidacion = this.onValidacion.bind(this)
    this.onCancelar = this.onCancelar.bind(this)

    this.togglePrimaryMedNom = this.togglePrimaryMedNom.bind(this)
    this.togglePrimaryInsNom = this.togglePrimaryInsNom.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }
  toggleDatos() {
    this.setState({ collapse2: !this.state.collapse2 });
  }

  toggleFade() {
    this.setState({ fadeIn: !this.state.fadeIn });
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }
  
togglePrimary2() {
    this.setState({ primary2: !this.state.primary2});
  }

togglePrimaryMedNom() {
  this.setState({ primaryMedNombre: !this.state.primaryMedNombre});
  }

togglePrimaryInsNom() {
  this.setState({ primaryInsNombre: !this.state.primaryInsNombre});
  }


async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      // DESDE AQUI CON EL LOGIN
    if(this.state.logged_in){
      if(typeof(localStorage.getItem('token')) === 'undefined'){
        window.location.href = '#/Login'
      }else{
        const respuesta7 = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
          })
          const datosUsuario = await respuesta7.json();
          this.setState({
            username:datosUsuario.username
          });

          try{
            // alert(this.state.username)
            const respuesta7 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.username+'/', {
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              }
            })
              const datosUsuario = await respuesta7.json();
              this.setState({
                ci: datosUsuario[0].ci,
              });
              //alert(this.state.ci)
          } catch (e) {
              console.log(e);
          }
      }

    }
    else{
      window.location.href = '#/Login'
    }
// HASTA AQUI LOGIN 
      
      const respuesta1 = await fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregarMedico/'+this.state.ci+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const data1 = await respuesta1.json();

      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/MedicamentosBusca/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const data = await respuesta.json();
      const respuestaN = await fetch('http://localhost:8000/farmacia/almacena/v1/MedicamentosBuscaNombre/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataN = await respuestaN.json();
      const respuestaI = await fetch('http://localhost:8000/farmacia/almacena/v1/InsumosBusca/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataI = await respuestaI.json();
      const respuestaIN = await fetch('http://localhost:8000/farmacia/almacena/v1/InsumosBuscaNombre/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataIN = await respuestaIN.json();
      const respuesta2 = await fetch('http://localhost:8000/farmacia/recetar/v1/RecetaLista/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataReceta = await respuesta2.json();
      this.setState({
        data1,
        data,
        dataI,
        dataN,
        dataIN,
        listReceta: dataReceta,
      });
    } catch (e) {
        console.log(e);
    }
}

fEliminar(i){
  const nuevo=this.state.medicamentos.filter(medicamentos=>{
    return medicamentos !== i
  })
  if(this.state.medicamentos.length!==1){
      this.setState({
        medicamentos:[...nuevo]
      })
  }
  else{
    this.setState({
      medicamentos:[...nuevo],
      bandera:false
    })
  }
  this.setState({
    contarItem:this.state.contarItem-1,
  })
}

fEliminarI(i){
  const nuevo=this.state.insumos.filter(insumos=>{
    return insumos !== i
  })
  if(this.state.insumos.length!==1){
      this.setState({
        insumos:[...nuevo]
      })
  }
  else{
    this.setState({
      insumos:[...nuevo],
      bandera:false
    })
  }
  this.setState({
    contarItem:this.state.contarItem-1,
  })
}

fEliminar2(i){
  const nuevo=this.state.medicamentos2.filter(medicamentos2=>{
    return medicamentos2 !== i
  })
  if(this.state.medicamentos2.length!==1){
      this.setState({
        medicamentos2:[...nuevo]
      })
  }
  else{
    this.setState({
      medicamentos2:[...nuevo],
      bandera:false
    })
  }
}


handleOnAddReceta (event) {
  const num = this.state.dataCant - this.state.cantidad
  console.log("Cantidad total: "+this.state.dataCant)
  console.log("Cantidad a restar: "+this.state.cantidad)
  console.log("Cantidad calculada: "+num)
  if(this.state.cantidad !== '' && this.state.prescripcion !== '' ){
      if(this.state.cantidad  > 0){
        this.setState({
          message: '',
        })
      }
      else{
        this.setState({
          codigo: event.target.codigo.value,
          cantidad:this.state.cantidad,
          primary: !this.state.primary,
          message: 'La cantidad a retirar debe ser mayor a 0.',
          message2:'',
          showCant:false,
        })
      }
      if(num<0){
        this.setState({
          primary: !this.state.primary,
          showCant:true,
          menssage: "nada",
          message2: "Cantidad insuficiente",
        })
      }else{
        if(this.state.cantidad>this.state.cantidad_prescripcion){
        this.setState({
          showCant: false,
          primary: !this.state.primary,
          message: "La cantidad prescrita debe ser menor o igual a lo establecido:  "+this.state.cantidad_prescripcion,
          message2:'',
        })
      }
      }
    }
    else{ 
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        showCant: false,
        message: 'Complete los espacios en blanco.',
        message2: '',
      });
    }
    if(this.state.cantidad !== '' && this.state.prescripcion!== '' && this.state.cantidad > 0 && event.target.codigo.value !=='' && num>=0 && this.state.cantidad<=this.state.cantidad_prescripcion && this.state.contarItem<3){  
      var a=0
      this.state.medicamentos.map((item) =>{
        if(item.Codigo===event.target.codigo.value){
          a=1
        }
        return 0
      });

    if(a===0){
    let obj = {
      Codigo : event.target.codigo.value,
      Nombre : event.target.nombre.value,
      Forma : event.target.forma.value, 
      Cantidad : this.state.cantidad, 
      Prescripcion :this.state.prescripcion,
      Id_recetaA : this.state.codigoReceta,
      //arreglar se remplaza al guardar con el id del Formulario
    };

    this.setState({
      codigo: event.target.codigo.value,
      cantidad:this.state.cantidad,
      showAA : true,
      medicamentos: this.state.medicamentos.concat([obj]),
      contarItem: this.state.contarItem + 1
    });  
    console.log("no entender"+this.state.medicamentos)
    }else{
      this.setState({
        primary: !this.state.primary,
        message: 'El medicamento ya fue adicionado',
        showCant:false,
        menssage2: '',
      })
    }

  }else{   
    if(event.target.codigo.value=== '' ){
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        message: 'Busque medicamento',
      })
    }
  } 

  if(this.state.contarItem>2){
    this.setState({  
      primary: !this.state.primary,
      showCant:true,
      menssage: "Termino de recetar",
      message2: "Cantidad de item completos",
    })
  }

  console.log("cantidad item: "+this.state.contarItem);  
  }

handleOnAddRecetaI (event) {       
  
  const num = this.state.dataCant - this.state.cantidad
  console.log("Cantidad calculada: "+num)
  if(this.state.cantidad !== '' && this.state.prescripcion !== '' ){
      if(this.state.cantidad  > 0){
        this.setState({
          message: '',
        })
      }
      else{
        this.setState({
          codigo: event.target.codigo.value,
          cantidad:this.state.cantidad,
          primary: !this.state.primary,
          message: 'La cantidad a retirar debe ser mayor a 0.'
        })
      }
      if(num<0){
        this.setState({
          primary: !this.state.primary,
          showCant:true,
          menssage: "nada",
          message2: "Cantidad insuficiente",
        })
      }else{
        if(this.state.cantidad>this.state.cantidad_prescripcion){
          this.setState({
            showCant: false,
            primary: !this.state.primary,
            message: "La cantidad prescrita debe ser menor o igual a lo establecido:  "+this.state.cantidad_prescripcion,
            message2:'',
          })
        }
      }
    }
    else{ 
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        showCant: false,
        message: 'Complete los espacios en blanco.',
        message2: '',
      });
    }
    if(this.state.cantidad !== '' && this.state.prescripcion!== '' && this.state.cantidad > 0 && event.target.codigo.value !=='' && num>=0 && this.state.cantidad<=this.state.cantidad_prescripcion && this.state.contarItem<3){
      var a=0
      this.state.insumos.map((item) =>{
        if(item.Codigo===event.target.codigo.value){
          a=1
        }
        return 0
      });

    if(a===0){
    let obj = {
      Codigo : event.target.codigo.value,
      Nombre : event.target.nombre.value,
      Presentacion : event.target.presentacion.value, 
      Cantidad : this.state.cantidad, 
      Prescripcion :this.state.prescripcion,
      Id_recetaA : this.state.codigoReceta,
      //arreglar se remplaza al guardar con el id del Formulario
    };

    this.setState({
      codigo: event.target.codigo.value,
      cantidad:this.state.cantidad,
      showAA : true,
      insumos: this.state.insumos.concat([obj]),
      contarItem: this.state.contarItem + 1
    });
  }else{
    this.setState({
      primary: !this.state.primary,
      message: 'El insumo ya fue adicionado',
      showCant:false,
      menssage2: '',
    })

  }    

  }else{   
    if(event.target.codigo.value=== '' ){
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        message: 'Busque medicamento',
      })
    }
  } 

  if(this.state.contarItem>2){
    this.setState({  
      primary: !this.state.primary,
      showCant:true,
      menssage: "Termino de recetar",
      message2: "Cantidad de item completos",
    })
  }
  console.log("cantidad item: "+this.state.contarItem);  
}


handleSubmit(e){    
    e.preventDefault();
    if(e.target.nombre2.value !== '' && e.target.cantidad2.value !== '' && e.target.prescripcion2.value !== '' && e.target.forma2.value !== '' ){
      if(e.target.cantidad2.value > 0){
        this.setState({
          message: '',
        })
      }
      else{
        this.setState({ 
          primary: !this.state.primary,
          message: 'La cantidad a retirar debe ser mayor a 0.'
        })
      }
    } else{ 
      this.setState({
        primary: !this.state.primary,
        message: 'Complete los espacios en blanco.',
      });
    }
    if(e.target.nombre2.value !== '' && e.target.cantidad2.value !== '' && e.target.prescripcion2.value !== '' && e.target.forma2.value !== '' && e.target.cantidad2.value > 0)
    {
    let obj = {
      Codigo2 : 'SN',
      Nombre2 : e.target.nombre2.value,
      Forma2 : e.target.forma2.value,
      Cantidad2 : e.target.cantidad2.value,
      Prescripcion2 : e.target.prescripcion2.value,
      Id_recetaA : this.state.codigoReceta
      //arreglar se remplaza al guardar con el id del Formulario
    };
    this.setState({
      medicamentos2: this.state.medicamentos2.concat([obj]),
    });
    e.target.reset();
  }
}

handleChange2(value) {

  this.state.data.map((item, indice)=>{
    if (value === item.codigo){          
     fetch('http://localhost:8000/farmacia/almacena/v1/detalleMedicamentoBusca/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
        this.setState({
          list: findresponse,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showInsumo:false,
          showMedicamento:true,
          showMedicamentoNombre:false,
          primaryMedNombre:false,
        })
        console.log("cantidad_prescripcion: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)
    });

    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse)=>{ 
      if(findresponse.length>0){ 
      this.setState({
        dataCant: findresponse[0].sum
      });
      console.log("Cantidad Sumada primer evento existe: "+findresponse[0].sum)

      }else{
      this.setState({
        dataCant: 0
      });
      console.log("Cantidad Sumada primer evento no existe: "+0)

      }
    })
    }
    return 0}
  )

}

handleChange3(value) {
  this.state.data.map((item, indice)=>{
    if (value === item.nombre){          
     fetch('http://localhost:8000/farmacia/almacena/v1/DetalleMedicamentobBuscaNombre/'+item.nombre+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if (findresponse.length>1) {
        this.setState({
          show: false,
          primaryMedNombre: true,
          listaMedNombre:findresponse,
        })
      }
      else{
        this.setState({
          show: true,
          show2: false,
          list: findresponse,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showInsumo:false,
          showMedicamento:true,
        })
        console.log("cantidad_prescripcion Nombre: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)

        fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+findresponse[0].producto_farmaceutico.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse)=>{
          if(findresponse.length>0){    
          this.setState({
              dataCant: findresponse[0].sum
          });
          console.log("primer evento existe: "+findresponse[0].sum)
        }else{
          this.setState({
            dataCant: 0
        });
        console.log("primer evento no existe: "+0)
        }
       
        })
      } 
    });

  }
  return 0
})
}




handleInsumo(value) {
  console.log("data=",this.state.dataI)
  this.state.dataI.map((item, indice)=>{
    if (value === item.codigo){          
     
    fetch('http://localhost:8000/farmacia/almacena/v1/DetalleInsumoBusca/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
        this.setState({
          listInsumo: findresponse,
          showInsumo:true,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showMedicamento:false,
          primaryInsNombre:false,
        })
        console.log("cantidad_prescripcion Insumo: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)

      });

    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse)=>{    
      if(findresponse.length>0){    
        this.setState({
            dataCant: findresponse[0].sum
        });
        console.log("Insumo primer evento existe: "+findresponse[0].sum)
      }else{
        this.setState({
          dataCant: 0
      });
      console.log("Insumo primer evento no existe: "+0)
      }
    })
    }
    return 0
})
}



handleInsumoNombre(value) {
  console.log("data=",this.state.dataI)
  this.state.dataI.map((item, indice)=>{
    if (value === item.nombre){          
     
    fetch('http://localhost:8000/farmacia/almacena/v1/DetalleInsumoBuscaNombre/'+item.nombre+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {

      if (findresponse.length>1) {
        this.setState({
          show: false,
          primaryInsNombre: true,
          listaInsNombre:findresponse,
        })
      }
      else{
        this.setState({
          show: true,
          show2: false,
          listInsumo: findresponse,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showInsumo:true,
          showMedicamento:false,
        })
        console.log("cantidad_prescripcion Insumo Nombre: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)

        fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+findresponse[0].producto_farmaceutico.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse)=>{
          if(findresponse.length>0){    
          this.setState({
              dataCant: findresponse[0].sum
          });
          console.log("primer evento existe: "+findresponse[0].sum)
        }else{
          this.setState({
            dataCant: 0
        });
        console.log("primer evento no existe: "+0)
        }
       
        })
      } 
    });
    }
    return 0
})
}

onFormSubmit(e) {
  this.state.medicamentos.map((data,i)=>{
    let data2={
      cantidad: data.Cantidad,
      prescripcion: data.Prescripcion,
      id_receta: data.Id_recetaA,
      codigo: data.Codigo,
    }
    try{
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data2), // data can be string or {object}!
        headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));              
    } catch (e) {
      console.log(e);
    }
  return 0
})

this.state.insumos.map((data,i)=>{
  let dataI={
    cantidad: data.Cantidad,
    prescripcion: data.Prescripcion,
    id_receta: data.Id_recetaA,
    codigo: data.Codigo,
  }
  try{
    fetch(urlI, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(dataI), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }
return 0
})

this.state.medicamentos2.map((data3,i)=>{
  let data4={
    nombre: data3.Nombre2,
    forma_farmaceutica: data3.Forma2,
    cantidad: data3.Cantidad2,
    prescripcion: data3.Prescripcion2,
    id_receta: data3.Id_recetaA
  }
  try {        
    fetch(url1, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data4), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));                  
  } catch (e) {
  console.log(e);
  }
  return 0
})

this.props.history.push('/RecetasEnConsultorio/RecetasEmitidas')
this.setState({
  primary2: !this.state.primary2,
});

}

handleRecetaNombre(u){

}

handleReceta(value) {
  //console.log(value); 
  console.log("data=",this.state.data1)
  this.state.data1.map((item)=>{
    if (parseInt(value,10) === item.id_receta){    
     fetch('http://localhost:8000/farmacia/recetar/v1/detalleRecetaBusca/'+item.id_receta+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
     //   console.log("FINDRESPONSE=",findresponse)
        this.setState({
          listR: findresponse,
          id_recetaA: item.id_receta,
          collapse2:true,
          codigoReceta:item.id_receta,
        })
       // console.log('LIST=',this.state.list.length)   
    });

    fetch('http://localhost:8000/farmacia/recetar/v1/detalleMedicamentoRecetaBusca/'+item.id_receta+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_medicamento: findresponse,
      })

  });

  
  fetch('http://localhost:8000/farmacia/recetar/v1/DetalleInsumoRecetaBusca/'+item.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_insumo: findresponse
      })  
  });

  fetch('http://localhost:8000/farmacia/recetar/v1/OtroMedicamentoBusca/'+item.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_otro: findresponse
      })

  });
    }
    return 0
}
)
}

onValidacion(){
  if(this.state.codigoReceta===0){
    this.setState({ 
      primary: !this.state.primary,
      message:'Ingrese Codigo de Receta',
      showCant:false,
    })

  }else{
    this.setState({ 
      primary2: !this.state.primary2
    })
  }
}
onCancelar(){
  this.props.history.push('/RecetasEnConsultorio/RecetasEmitidas')
}

render() {
  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }
return (
<div className="animated fadeIn">    
<Row>
  <Col xs="12">          
    <Card>
      <CardHeader>      
      <h3><i className="fa fa-edit"></i> <strong>Nueva Receta</strong></h3>
      </CardHeader>
        <CardBody>
        <Col xs="12" md="12">
            <form>
              <FormGroup row>
                <Col xs="12" md="2">
                <Label htmlFor="codigo"><strong>Ingrese Receta : </strong></Label>
                </Col>

                <Col xs="12" md="3">
                  <Search                       
                    data={this.state.data1}
                    onChange={ this.handleReceta.bind(this) }
                    placeholder="Receta"
                    class="search-class"
                    type="string"
                    searchKey="id_receta"
                   
                    />
                </Col>
                </FormGroup>
            </form>
          </Col>
        <Collapse isOpen={this.state.collapse2} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Card>

          <ListGroupItem action color="info">Datos del Afiliado </ListGroupItem>            
            <Col xs="12" md="12">
            <form>
            <br/> 
              <Row>           
                <Col>
                  <DatosReceta listR={this.state.listR} />
                </Col>
              </Row>     
            </form> 
            </Col>

            </Card>
            </Collapse>
         <Button size="sm" color="secondary" onClick={this.toggleDatos} style={{ marginBottom: '1rem' }}><i className="icon-note"></i><span> Datos Afiliado</span></Button>
      <Card>
        <ListGroupItem action color="info">Elija Medicamento/Insumo</ListGroupItem>
          
            <Col xs="12" md="12">
                <br/>
                <FormGroup row>
                <Col xs="12" md="3">
                {/* <strong>Medicamento: </strong> */}
                  <Search                       
                    data={this.state.data}
                    onChange={ this.handleChange2.bind(this)}
                    placeholder="Codigo-Medicamento"
                    class="search-class"
                    searchKey="codigo"
                    className="form-control"
                    />
                </Col>
                <Col xs="12" md="3">
                  <Search                       
                    data={this.state.dataN}
                    onChange={ this.handleChange3.bind(this)}
                    placeholder="Nombre-Medicamento"
                    class="search-class"
                    searchKey="nombre"
                    className="form-control"
                    />
                </Col>
                <Col xs="12" md="3">
                  <Search                       
                    data={this.state.dataI}
                    onChange={ this.handleInsumo.bind(this)}
                    placeholder="Codigo-Insumo"
                    class="search-class"
                    searchKey="codigo"
                    className="form-control"
                    />
                </Col>
                <Col xs="12" md="3">
                  <Search                       
                    data={this.state.dataIN}
                    onChange={ this.handleInsumoNombre.bind(this)}
                    placeholder="Nombre-Insumo"
                    class="search-class"
                    searchKey="nombre"
                    className="form-control"
                    />
                </Col>                          
                </FormGroup>
                <hr/>
              </Col>
             

                {this.state.showMedicamento &&<DatosMedicamento list={this.state.list} onAddReceta={this.handleOnAddReceta.bind(this)} key={1}/> }  
                {this.state.showInsumo && <DatosInsumo listInsumo={this.state.listInsumo} onAddRecetaI={this.handleOnAddRecetaI.bind(this)} key={2}/>}

            <Col xs="12" md="12">
              <FormGroup row>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Cantidad</Label>
                </Col>
                <Col xs="12" md="2">
                  <Input type="number" id="cantidad" name="cantidad" className="form-control"
                    onChange={e => this.setState({ cantidad: e.target.value })}   />
                </Col> 
                <Col xs="12" md="2">
                  <Label htmlFor="textarea-input">Prescripcion</Label>
                </Col>
                <Col xs="12" md="5">
                  <Input
                    type="textarea"
                    id="aaa"  
                    name="prescripcion"
                    rows="3"
                    placeholder="Contenido"
                    className="form-control"
                    maxLength="100"
                    onChange={e => this.setState({ prescripcion: e.target.value })} 
                    />
                </Col>
              </FormGroup>
            </Col>
        </Card>
        
  <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>

    <Card>           
        <ListGroupItem action color="success"> Otro Medicamento</ListGroupItem>
          <CardBody>
            <form onSubmit={this.handleSubmit.bind(this)}>
              <Col xs="12" md="12">
                <FormGroup color="success" row>
                  <Col xs="12" md="1">
                    <Label htmlFor="text-input">Nombre</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input type="text"  valid id="inputIsValid" name="nombre2" maxLength="50"
                    onChange={e => this.setState({ nombre2: e.target.value })} 
                    />
                  </Col>
                  <Col xs="12" md="1">
                    <Label htmlFor="text-input">F. Forma </Label>
                  </Col>
                  <Col xs="12" md="2">
                    <Input type="text" valid id="inputIsValid" name="forma2" maxLength="50"
                    onChange={e => this.setState({ forma2: e.target.value })}  />
                  </Col>
                  <Col xs="12" md="1">
                    <Label htmlFor="text-input" id="inputIsValid" >Cantidad</Label>
                  </Col>
                  <Col xs="12" md="2">
                    <Input type="number" valid id="inputIsValid" name="cantidad2" className="form-control"
                    onChange={e => this.setState({ cantidad2: e.target.value })}   />
                  </Col> 
                  <Col xs="12" md="2">
                    <h3>
                    <Button size="sm" type="submit" color="success"><i className="fa fa-arrow-circle-right"></i><span> Adicionar</span></Button>
                    </h3>
                  </Col>
                </FormGroup>
              </Col>

              <Col xs="12" md="12">
                <FormGroup row>
                  <Col xs="12" md="1">
                      <Label htmlFor="textarea-input" id="inputIsValid">Prescripcion </Label>
                  </Col>
                  <Col xs="12" md="5">
                    <Input 
                      valid id="inputIsValid"
                      type="textarea"
                      name="prescripcion2"
                      rows="2"
                      placeholder="Contenido"
                      className="form-control"
                      maxLength="100"
                      onChange={e => this.setState({ prescripcion2: e.target.value })} 
                    />
                  </Col>
                </FormGroup>
              </Col>
              </form>
          </CardBody>
   
    </Card>
    </Collapse>
 


  <Button onClick={this.toggle}  size="sm" className="btn-vine btn-brand mr-1 mb-1"><i className="icon-note"></i><span>Otro Medicamento</span></Button>



  <Form>

    
      <Table hover bordered striped responsive size="sm">
        <thead  style={styles2} >
        <tr>
          <th><p></p>Numero<p></p></th>
          <th><p></p>Codigo<p></p></th>
          <th><p></p>Nombre Producto<p></p></th> 
          <th><p></p>Forma/Presentacion<p></p></th>
          <th><p></p>Cantidad<p></p></th>                         
          <th><p></p>Prescripción(Indicación)<p></p></th>                         
          <th><p></p>Acciones<p></p></th>       
        </tr>
        </thead>

        <tbody>
        {this.state.list_medicamento.map((u, i)=> {
             return (
               <tr key = {i} bgcolor="#ebf5fb">
                 <td style={{maxWidth: '5px', backgroundColor: '#ebf5fb', textAlign: 'center'}}>{i+1}</td>
                 <td style={{maxWidth: '50px',backgroundColor: '#ebf5fb'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                 <td style={{maxWidth: '200px', backgroundColor: '#ebf5fb'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.codigo.t_forma_farmaceutica.descripcion} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.cantidad} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.prescripcion} </td> 
                 <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}></td>
                 </tr>
                    )
           })}
          
          {this.state.medicamentos.map((u, i)=> {
            return (
              <tr key = {i} bgcolor=" #ebf5fb">
                <td style={{maxWidth: '5px', backgroundColor: '#ebf5fb', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Codigo} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#ebf5fb'}}>{u.Nombre}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Forma}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Cantidad}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Prescripcion}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb', textAlign: 'center'}}>

                  <Button onClick={(e)=>this.fEliminar(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>                 

                </td>
              </tr>
              )
          })}
          {this.state.list_insumo.map((u, i)=> {
             return (
               <tr key = {i} bgcolor="#f2f4f4">
                 <td style={{maxWidth: '5px', backgroundColor: '#f2f4f4', textAlign: 'center'}}>{i+1}</td>
                 <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                 <td style={{maxWidth: '100px', backgroundColor: '#f2f4f4'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.codigo.t_presentacion.descripcion} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.cantidad} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.prescripcion} </td> 
                 <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>
                 </td>
                 </tr>
                    )
           })}
          {this.state.insumos.map((u, i)=> {
            return (
              <tr key = {i} bgcolor="#f2f4f4">
                <td style={{maxWidth: '5px', backgroundColor: '#f2f4f4', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.Codigo} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#f2f4f4'}}>{u.Nombre}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>{u.Presentacion}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>{u.Cantidad}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>{u.Prescripcion}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4',textAlign: 'center'}}>
                  <Button onClick={(e)=>this.fEliminarI(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>                 
                </td>
              </tr>
              )
          })}
          {this.state.list_otro.map((u, i)=> {
             return (
               <tr key = {i} bgcolor="#ffffff">

                 <td style={{maxWidth: '5px', backgroundColor: '#ffffff', textAlign: 'center'}}>{i+1}</td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{"SN"} </td>
                 <td style={{maxWidth: '100px', backgroundColor: '#ffffff'}} >{u.nombre} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.forma_farmaceutica} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.cantidad} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.prescripcion} </td>
                 <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>
                 </td>
                 </tr>
                    )
           })}
          
          {this.state.medicamentos2.map((u, i)=> {
            return (
              <tr key = {i} bgcolor="#ffffff">
                <td style={{maxWidth: '5px', backgroundColor: '#ffffff', textAlign: 'center'}} >{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.Codigo2} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#ffffff'}}>{u.Nombre2}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>{u.Forma2}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>{u.Cantidad2}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>{u.Prescripcion2}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}}>
      
                  <Button onClick={(e)=>this.fEliminar2(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>                                 
                </td>
              </tr>
                )
          })}
        </tbody>
      </Table>
   


    {/* FIN DE REGISTRO */}
    <center>
    <Button onClick={this.onValidacion} className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-save"></i><span>Registrar</span></Button>
    <Button onClick={this.onCancelar} className="btn-google-plus btn-brand mr-1 mb-1"><i className=" 	fa fa-times-circle"></i><span>Cancelar</span></Button>
    </center>
    {/* FIN DE REGISTRO */}

    {/* MODAL ADVERTENCIA */}
      <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
        <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
          <ModalBody>
            <strong>{this.state.message}</strong>
              {this.state.showCant && <Alert color="danger"><strong>{this.state.message2}</strong></Alert>}
               
          </ModalBody>
        <ModalFooter>
          <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
        <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
          <ModalBody>
                    ¿Desea registrar los datos?
          </ModalBody>
          <ModalFooter>
            <Button onClick={this.onFormSubmit} type="submit" color="primary">Si</Button>{' '}
            <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
          </ModalFooter>
      </Modal>
      {/* FIN MODAL ADVERTENCIA */}

      {/* MODAL ADVERTENCIA 2*/}
        <Modal isOpen={this.state.primaryMedNombre} toggle={this.togglePrimaryMedNom} className={'modal-info ' + this.props.className}>
        <ModalHeader toggle={this.togglePrimaryMedNom}>Elija Medicamento</ModalHeader>
          <ModalBody>
          <Table responsive size="sm">
          {this.state.listaMedNombre.map((item, indice) => ( 
        
        <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>
          <Button color="info" onClick={(e)=>this.handleChange2(item.producto_farmaceutico.codigo)}>{"✓"}</Button>


          </td>

        </tr>
        
     
      ))}
      </Table>
          </ModalBody>

      </Modal>

      {/* FIN MODAL ADVERTENCIA 3*/}

{/* MODAL ADVERTENCIA 2*/}
<Modal isOpen={this.state.primaryInsNombre} toggle={this.togglePrimaryInsNom} className={'modal-info ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimaryInsNom}>Elija Insumo </ModalHeader>
    <ModalBody>
    <Table responsive size="sm">
    {this.state.listaInsNombre.map((item, indice) => ( 
      <tr key={indice}>
      <td>{indice + 1}</td>
      <td>{item.producto_farmaceutico.codigo}</td>
      <td>{item.producto_farmaceutico.nombre}</td>
      <td>{item.t_presentacion.descripcion}</td>
      <td>
      <Button color="info" onClick={(e)=>this.handleInsumo(item.producto_farmaceutico.codigo)}>{"✓"}</Button>
      </td>
      </tr>
      ))}
    </Table>
  </ModalBody>
</Modal>

{/* FIN MODAL ADVERTENCIA 2*/}
                         
                         
    </Form>
  </CardBody> 
</Card>  
</Col>
</Row>

</div>     
    );
  } 
}
export default NuevaReceta2;

