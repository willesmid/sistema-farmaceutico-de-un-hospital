import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row 
} from 'reactstrap';

import  Alerta from '../Alerta/Alerta';
import DatosReceta from './DatosReceta';
import NuevaReceta2 from './NuevaReceta2';

//import Boton from './Boton';
import Search from 'react-search-box';


class BuscarReceta extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
   

      data: [],
      listR:[],
      idA:0,
      swA:false,
      
      show: false,
      show2: false,

      loading: false,
      muestrBoton: false,
      //recetas
      recetas:[],
      CodigoAlifiacion: '',
      Fecha:'',
      Id_receta:'',
      Id_consulta:'',
      NombreCompleto:'',
      CedulaIdentidad:'',
      NombreMedico:'',



    };
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona

    this.setState({
      loading: true
    });

    try {
      const respuesta = await fetch('http://localhost:8000/v2/ListaReceta/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const data = await respuesta.json();

      

      this.setState({
        data,
        loading: false
        
      });
    } catch (e) {
        console.log(e);
    }
  }



  
handleChange(value) {
  //console.log(value); 


  console.log("data=",this.state.data)

  const listAl = this.state.data.map((item, indice)=>{
   
    if (value == item.id_receta){    
      
     fetch('http://localhost:8000/v2/detalleRecetaBusca/'+item.id_receta+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
     //   console.log("FINDRESPONSE=",findresponse)
        this.setState({
          listR: findresponse
        })
       // console.log('LIST=',this.state.list.length)   

        if (this.state.list.length===0) {
          this.setState({
            show: false,
            show2: true
          })
          
         // document.write("No hay " + this.state.list.length);
        }
        else{
          this.setState({
            show: true,
            show2: false
          })
          //document.write("SI hay "+ this.state.list.length);
        } 
    });

    }
}



)

  this.setState({ muestrBoton: true});

}



handleOnAddReceta (event) {

  this.list.map(item => (
    console.log(item.id_receta)
  ));



  /*aqui voy haciendo el preguarado*/
  console.log("dato: "+event.target.codigoAlifiacion.value)
  event.preventDefault();
 
  let obj = {
    CodigoAlifiacion : event.target.codigoAlifiacion.value,
    
    Fecha : event.target.fecha.value,
    Id_receta : event.target.id_receta.value,
    Id_consulta : event.target.id_consulta.value, 
    NombreCompleto : event.target.nombreCompleto.value,
    CedulaIdentidad : event.target.cedulaIdentidad.value,
    NombreMedico : event.target.nombreMedico.value,  
     
    

    //arreglar se remplaza al guardar con el id del Formulario
  };
  this.setState({
    recetas: this.state.recetas.concat([obj]),
    bandera: true, 
    

  });
  
  event.target.reset();
}




  render() {


    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }

    return (
      
      <div className="animated fadeIn">
        
        <Row>
          <Col xs="12">
            <Card>
              
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Buscar Paciente</strong>
              </CardHeader>
    
              <CardBody>
                <form>
                <div className="form-group">
  
                        <Search                       
                        data={this.state.data}
                        onChange={ this.handleChange.bind(this) }
                        placeholder="Matricula PROMES"
                        class="search-class"
                        searchKey="id_receta"
                        />
                    
                    {/*console.log(Object.keys(this.state.list))*/}
                    {/*console.log(this.state.list)*/}
                    {/*console.log(Object.values(this.state.list))*/}
                    
                    
                </div>
              </form>
              
              
              </CardBody>
            </Card>
          </Col>
          
        </Row>

        <DatosReceta listR={this.state.listR}  />
        {this.state.show2 && <Alerta/>}
        <NuevaReceta2 />
    </div>

    );
  }
  
}


export default BuscarReceta;
