import React from 'react';
import ReactDOM from 'react-dom';
import NuevaReceta2 from './NuevaReceta2';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevaReceta2 />, div);
  ReactDOM.unmountComponentAtNode(div);
});
