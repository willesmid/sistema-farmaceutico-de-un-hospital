import React, { Component } from 'react';
import {

  Col, 

  Form,
  FormGroup,
  Label,
  Input,
  Button,

} from 'reactstrap';
//import AntecedentesG from './Antecedentes/AntecedentesG';

class DatosMedicamento extends Component {
 

  constructor(props) {
    super(props);

    this.state = {
     
      primary: false,
      message: '',
      prescripcion:'',
      data: 0,
      
    };
    // this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)

  }


  
togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }

  calcDays(fv){
    /*calcula los dias que hay entre dos fechas */
    
    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+fv+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse)=>{ 
  
        return findresponse[0].sum
      
       
  
    })
  
  
  }
  
  render() {
    




if(this.props.list.length>0){
  return (
    <div className="animated fadeIn">
      {this.props.list.map(item => (
      <Form onSubmit={this.props.onAddReceta}>
        <Col xs="12" md="12">
          <FormGroup row>
            <Col xs="12" md="1">
              <Label htmlFor="text-input">Codigo</Label>
            </Col>
            <Col xs="12" md="2">
              <Input type="text" id="text-input" name="codigo" value={item.producto_farmaceutico.codigo} disabled />
            </Col>
            <Col xs="12" md="1">
              <Label htmlFor="text-input">Nombre</Label>
            </Col>
            <Col xs="12" md="3">
              <Input type="text" id="text-input" name="nombre" value={item.producto_farmaceutico.nombre} disabled />
            </Col>
            <Col xs="12" md="1">
              <Label htmlFor="text-input">F.Farm. </Label>
            </Col>
            <Col xs="12" md="2">
              <Input type="text" id="text-input" name="forma" value={item.t_forma_farmaceutica.descripcion} disabled />
            </Col>   
         
            {/* <Input type="hidden" id="text-input" name="will" value={this.calcDays(item.producto_farmaceutico.codigo)} disabled /> */}
      
            <Col xs="12" md="2">
            <h2>
            <Button onClick={this.onHandleSubmit} size="sm" className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-arrow-circle-right"></i><span>Adicionar </span></Button>
            </h2>
            </Col>
          </FormGroup>
        </Col>
      </Form> 
      )
    )
  } 
    </div>
   );
 }
else{
  return(
    <div className="animated fadeIn">
    <Form onSubmit={this.props.onAddReceta}>
      <Col xs="12" md="12">
        <FormGroup row>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">Codigo</Label>
          </Col>
          <Col xs="12" md="2">
            <Input type="text" id="text-input" name="codigo" disabled />
          </Col>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">Nombre</Label>
          </Col>
          <Col xs="12" md="3">
            <Input type="text" id="text-input" name="nombre" disabled />
          </Col>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">F.Farm. </Label>
          </Col>
          <Col xs="12" md="2">
            <Input type="text" id="text-input" name="forma" disabled />
          </Col>
          <Col xs="12" md="2">
          <h2>
          <Button onClick={this.onHandleSubmit} size="sm" className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-arrow-circle-right"></i><span>Adicionar</span></Button>
          </h2>
          </Col>
        </FormGroup>
      </Col>
    </Form>
  </div>   
  )
}}
}

export default DatosMedicamento;