import React, { Component } from 'react';
import {
  Col, 
  Form,
  FormGroup,
  Label,
  Input,
  Button,

} from 'reactstrap';
//import AntecedentesG from './Antecedentes/AntecedentesG';
class DatosInsumo extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
      primary: false,
      message: '',
      prescripcion:'',
      data: 0,
      
    };
    this.togglePrimary = this.togglePrimary.bind(this)
  }
  
togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }
  render() {
    if(this.props.listInsumo.length>0){
      return (
        <div className="animated fadeIn">
        {this.props.listInsumo.map(item => (
        <Form onSubmit={this.props.onAddRecetaI}>
          <Col xs="12" md="12">
            <FormGroup row>          
              <Col xs="12" md="1">
              <Label htmlFor="text-input">Codigo</Label>
              </Col>
              <Col xs="12" md="2">
              <Input type="text" id="text-input" name="codigo" value={item.producto_farmaceutico.codigo} disabled />
              </Col>
              <Col xs="12" md="1">
              <Label htmlFor="text-input">Nombre</Label>
              </Col>
              <Col xs="12" md="3">
              <Input type="text" id="text-input" name="nombre" value={item.producto_farmaceutico.nombre} disabled />
              </Col>
              <Col xs="12" md="1">
              <Label htmlFor="text-input">Presentacion </Label>
              </Col>
              <Col xs="12" md="2">
              <Input type="text" id="text-input" name="presentacion" value={item.t_presentacion.descripcion} disabled />
              </Col>
              <Col xs="12" md="1">
              <h2>
              <Button onClick={this.onHandleSubmit} size="sm" className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-arrow-circle-right"></i><span>Adicionar</span></Button>
              </h2>              
              </Col>
            </FormGroup>
          </Col>
        </Form>
           
        ))} 
  </div>
  );
  }
  else{
    return(
        <div className="animated fadeIn">
        <Col xs="12" md="12">
        <Form onSubmit={this.props.onAddReceta}>
          
            <FormGroup row>
              <Col xs="12" md="1">
              <Label htmlFor="text-input">Codigo</Label>
              </Col>
              <Col xs="12" md="2">
              <Input type="text" id="text-input" name="codigo" disabled />
              </Col>
              <Col xs="12" md="1">
              <Label htmlFor="text-input">Nombre</Label>
              </Col>
              <Col xs="12" md="3">
              <Input type="text" id="text-input" name="nombre" disabled />
              </Col>
              <Col xs="12" md="1">
              <Label htmlFor="text-input">Presentacion </Label>
              </Col>
              <Col xs="12" md="2">
              <Input type="text" id="text-input" name="presentacion" disabled />
              </Col>
              <Col xs="12" md="1">
              <h2>
              <Button onClick={this.onHandleSubmit} size="sm" className="btn-css3 btn-brand mr-1 mb-1"><i className="fa fa-arrow-circle-right"></i><span>Adicionar</span></Button>
              </h2>
              </Col>
            </FormGroup>
         
        </Form>
        </Col>
      </div>  
    )
  }}
}
export default DatosInsumo;