import React, { Component } from 'react';
import {Alert, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';


const trNavLink = {
    padding: '0',
    margin: '0'
  }

class Alerta extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            url_receta: '#/cantidadMedicamento',
            activeTab: 1,
            data: 0,
            data2: 0,
            primary: false
        };
      
     

        
        
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
        this.setState({
            activeTab: tab
        });
        }
    }




async componentDidMount() {
        try {
            
            const respuesta = await fetch('http://localhost:8000/v1/UbicadoFarmaciaBuscaCodMsuma/'+this.props.codigo+'/',{
                headers: {
                  Authorization: `JWT ${localStorage.getItem('token')}`
                } 
              });
            const data = await respuesta.json();
            if(data.length > 0){
                this.setState({
                    data: data[0].sum
                });
            }
            console.log(data)
           
        } catch (e) {
            console.log(e);
            
        }
    }


    render() {

      
        /*num-> calcula el stock de un determinado medicamento/insumo */
        const num = this.state.data  - this.props.cantidad
        console.log(num)
        console.log(this.state.data)
        const message = (num<0)?"Cantidad insuficiente.":""

        const val = (num<0)?true:false   



        
        return (
            








            <Col xs="12" md="12">
            
                {val &&
                    <Alert color="danger">
                        <strong>
                            {message}
                        </strong>

          
                    </Alert>

                    
                }
                {!val &&
                    <div className="form-actions">
                        <Button color="primary" onClick={this.togglePrimary} className="mr-1">Registrar</Button>
                        <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-primary ' + this.props.className}>
                            <ModalHeader toggle={this.togglePrimary}>Confirmación</ModalHeader>
                            <ModalBody>
                                ¿Desea registrar los datos?
                            </ModalBody>
                            <ModalFooter>
                            <Button onClick={this.handleOnSubmit} type="submit" color="primary">Si</Button>{' '}
                            <Button color="secondary" onClick={this.togglePrimary}>No</Button>
                            </ModalFooter>
                        </Modal>



                    </div>
                }
            </Col>
            
        );
    }
}

export default Alerta;
