import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalBody, 
  Alert,
  Label, 
  Input,
  Collapse,
  ListGroupItem,   
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  Form, 
  Table, 
  FormGroup,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import DatosMedicamento from './DatosMedicamento'
import DatosInsumo from './DatosInsumo'
import Search from 'react-search-box';

const styles2 = { color: 'white', backgroundColor: '#154360', textAlign:'center' }

var url='http://localhost:8000/farmacia/recetar/v1/ListaMedicamentoRecetaGuardar/' 
var urlI='http://localhost:8000/farmacia/recetar/v1/InsumoRecetaGuardar/'
var url1='http://localhost:8000/farmacia/recetar/v1/OtroMedicamentoLista/'
var urlReceta='http://localhost:8000/farmacia/recetar/v1/RecetaGuardar/'

class NuevaReceta3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      data1: [],
      dataN:[],
      dataIN:[],
      data3: 0,
      dataCant: 0,
      list:[],
      idA:0,
      swA:false,
      loading: false,
      Id_recetaA:'',
      id_recetaAA:'',
      recetas:[],
      CodigoAlifiacion: '',
      Fecha:'',
      Id_receta:'',
      Id_consulta:'',
      NombreCompleto:'',
      CedulaIdentidad:'',
      NombreMedico:'',
      listR:[],
      medicamentos:[],
      Codigo:'',
      Nombre:'',
      Forma:'',
      Cantidad:'',
      Prescripcion:'',
      prescripcion:'',
      cantidad:0,
      primary:false,
      primary2:false,
      primary3:false,
      codigo:'',
      showAA:false,
      showCant:false,
      num:0,
      message2:'',
      menssage:'',
      contarItem:0,
      collapse: false,
      collapse2: true,
      collapse3: false,//aqui
      fadeIn: true,
      medicamentos2:[],
      nombre2:'',
      cantidad2:0,
      forma2:'',
      prescripcion2:'',
      listReceta:[],
      codigoReceta:0,
      fechaReceta:'',
      horaReceta:'',
      showRecetaPrimero:true,
      showReceta:false,

      listInsumo:[],

      showInsumo:false,
      showMedicamento:true,
      dataI:[],
      insumos:[],  
      showGeneraReceta:true, 
      showDatosReceta:false,
      
      cantidad_prescripcion:0,
      primaryMedNombre:false,
      listaMedNombre:[],
      listaInsNombre:[],
      primaryInsNombre:false,
      primaryPrint:false,
      showImp:false,
      showImp1:false,
    };
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
    this.onValidacion =this.onValidacion.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.toggleDatos = this.toggleDatos.bind(this)
    this.toggle = this.toggle.bind(this)
    this.toggleFade = this.toggleFade.bind(this)
    this.togglePrimary3 = this.togglePrimary3.bind(this)
    this.togglePrint =this.togglePrint.bind(this)
    this.GeneraReceta = this.GeneraReceta.bind(this)
    this.onCancelar = this.onCancelar.bind(this)

    this.togglePrimaryMedNom = this.togglePrimaryMedNom.bind(this)
    this.togglePrimaryInsNom = this.togglePrimaryInsNom.bind(this)
    this.print = this.print.bind(this)
    this.print2 = this.print2.bind(this)
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }
  toggleDatos() {
    this.setState({ collapse2: !this.state.collapse2 });
  }

  toggleFade() {
    this.setState({ fadeIn: !this.state.fadeIn });
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }
  
togglePrimary2() {
    this.setState({ primary2: !this.state.primary2});
  }
togglePrimary3() {
  this.setState({ primary3: !this.state.primary3});  
}

togglePrimaryMedNom() {
  this.setState({ primaryMedNombre: !this.state.primaryMedNombre});
}

togglePrimaryInsNom() {
  this.setState({ primaryInsNombre: !this.state.primaryInsNombre});
}

togglePrint() {
  this.setState({ primaryPrint: !this.state.primaryPrint});
}

async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/MedicamentosBusca/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const data = await respuesta.json();
      const respuestaN = await fetch('http://localhost:8000/farmacia/almacena/v1/MedicamentosBuscaNombre/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataN = await respuestaN.json();
      const respuestaI = await fetch('http://localhost:8000/farmacia/almacena/v1/InsumosBusca/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataI = await respuestaI.json();
      const respuestaIN = await fetch('http://localhost:8000/farmacia/almacena/v1/InsumosBuscaNombre/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const dataIN = await respuestaIN.json();

      this.setState({       
        data,
        dataI,
        dataN,
        dataIN,

      });
      // console.log("CodigoReceta:"+this.state.listReceta[this.state.listReceta.length-1].id_receta)
    } catch (e) {
        console.log(e);
    }
}



print(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(155, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.state.codigoReceta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(155, 19, "VALIDEZ DE ESTA RECETA 48 HORAS");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombreA+" "+ this.props.match.params.apellido_pA+" "+ this.props.match.params.apellido_mA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoA)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+ this.state.fechaReceta+" "+this.state.horaReceta)

var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tabla2"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp: true, primaryPrint:false});

window.open(doc.output('bloburl'), '_blank'); 
}

print2(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(158, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.state.codigoReceta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(158, 19, "ADQUIRIR EN FARMACIA EXTERNA");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombreA+" "+ this.props.match.params.apellido_pA+" "+ this.props.match.params.apellido_mA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoA)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+ this.state.fechaReceta+" "+this.state.horaReceta)


var res = doc.autoTableHtmlToJson(document.getElementById("tablaOtro"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaOtro2"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp1: true, primaryPrint:false});
window.open(doc.output('bloburl'), '_blank'); 
}





fEliminar(i){
  const nuevo=this.state.medicamentos.filter(medicamentos=>{
    return medicamentos !== i
  })
  if(this.state.medicamentos.length!==1){
      this.setState({
        medicamentos:[...nuevo]
      })
  }
  else{
    this.setState({
      medicamentos:[...nuevo],
      bandera:false
    })
  }
  this.setState({
    contarItem:this.state.contarItem-1,
    showImp:false,
  })
}

fEliminarI(i){
  const nuevo=this.state.insumos.filter(insumos=>{
    return insumos !== i
  })
  if(this.state.insumos.length!==1){
      this.setState({
        insumos:[...nuevo]
      })
  }
  else{
    this.setState({
      insumos:[...nuevo],
      bandera:false
    })
  }
  this.setState({
    contarItem:this.state.contarItem-1,
    showImp:false,
  })
}

fEliminar2(i){
  const nuevo=this.state.medicamentos2.filter(medicamentos2=>{
    return medicamentos2 !== i
  })
  if(this.state.medicamentos2.length!==1){
      this.setState({
        medicamentos2:[...nuevo]
      })
  }
  else{
    this.setState({
      medicamentos2:[...nuevo],
      bandera:false
    })
  }
  this.setState({
    showImp1:false,
  })
}

handleOnAddReceta (event) {       
  const num = this.state.dataCant - this.state.cantidad
  console.log("Cantidad total: "+this.state.dataCant)
  console.log("Cantidad a restar: "+this.state.cantidad)
  console.log("Cantidad calculada: "+num)
  if(this.state.cantidad !== '' && this.state.prescripcion !== '' ){
      if(this.state.cantidad  > 0){
        this.setState({
          message: '',
        })
      }
      else{
        this.setState({
          codigo: event.target.codigo.value,
          cantidad:this.state.cantidad,
          primary: !this.state.primary,
          message: 'La cantidad a retirar debe ser mayor a 0.',
          message2:'',
          showCant:false,
        })
      }
      if(num<0){
        this.setState({
          primary: !this.state.primary,
          showCant:true,
          menssage: "nada",
          message2: "Cantidad insuficiente",
        })
      }else{
        if(this.state.cantidad>this.state.cantidad_prescripcion){
        this.setState({
          showCant: false,
          primary: !this.state.primary,
          message: "La cantidad prescrita debe ser menor o igual a lo establecido:  "+this.state.cantidad_prescripcion,
          message2:'',
        })
      }
      }
    }
    else{ 
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        showCant: false,
        message: 'Complete los espacios en blanco.',
        message2: '',
      });
    }
    if(this.state.cantidad !== '' && this.state.prescripcion!== '' && this.state.cantidad > 0 && event.target.codigo.value !=='' && num>=0 && this.state.cantidad<=this.state.cantidad_prescripcion && this.state.contarItem<3){  
      var a=0
      this.state.medicamentos.map((item) =>{
        if(item.Codigo===event.target.codigo.value){
          a=1
        }
        return 0
      });

    if(a===0){
    let obj = {
      Codigo : event.target.codigo.value,
      Nombre : event.target.nombre.value,
      Forma : event.target.forma.value, 
      Cantidad : this.state.cantidad, 
      Prescripcion :this.state.prescripcion,
      Id_recetaA : this.state.codigoReceta,
      //arreglar se remplaza al guardar con el id del Formulario
    };

    this.setState({
      codigo: event.target.codigo.value,
      cantidad:this.state.cantidad,
      showAA : true,
      medicamentos: this.state.medicamentos.concat([obj]),
      contarItem: this.state.contarItem + 1,
      showImp: false,
    });  
    console.log("no entender"+this.state.medicamentos)
    }else{
      this.setState({
        primary: !this.state.primary,
        message: 'El medicamento ya fue adicionado',
        showCant:false,
        menssage2: '',
      })
    }

  }else{   
    if(event.target.codigo.value=== '' ){
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        message: 'Busque medicamento',
      })
    }
  } 

  if(this.state.contarItem>2){
    this.setState({  
      primary: !this.state.primary,
      showCant:true,
      menssage: "Termino de recetar",
      message2: "Cantidad de item completos",
    })
  }

  console.log("cantidad item: "+this.state.contarItem);  
  }

handleOnAddRecetaI (event) {       
  const num = this.state.dataCant - this.state.cantidad
  console.log("Cantidad calculada: "+num)
  if(this.state.cantidad !== '' && this.state.prescripcion !== '' ){
      if(this.state.cantidad  > 0){
        this.setState({
          message: '',
        })
      }
      else{
        this.setState({
          codigo: event.target.codigo.value,
          cantidad:this.state.cantidad,
          primary: !this.state.primary,
          message: 'La cantidad a retirar debe ser mayor a 0.'
        })
      }
      if(num<0){
        this.setState({
          primary: !this.state.primary,
          showCant:true,
          menssage: "nada",
          message2: "Cantidad insuficiente",
        })
      }else{
        if(this.state.cantidad>this.state.cantidad_prescripcion){
          this.setState({
            showCant: false,
            primary: !this.state.primary,
            message: "La cantidad prescrita debe ser menor o igual a lo establecido:  "+this.state.cantidad_prescripcion,
            message2:'',
          })
        }
      }
    }
    else{ 
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        showCant: false,
        message: 'Complete los espacios en blanco.',
        message2: '',
      });
    }
    if(this.state.cantidad !== '' && this.state.prescripcion!== '' && this.state.cantidad > 0 && event.target.codigo.value !=='' && num>=0 && this.state.cantidad<=this.state.cantidad_prescripcion && this.state.contarItem<3){
      var a=0
      this.state.insumos.map((item) =>{
        if(item.Codigo===event.target.codigo.value){
          a=1
        }
        return 0
      });

    if(a===0){
    let obj = {
      Codigo : event.target.codigo.value,
      Nombre : event.target.nombre.value,
      Presentacion : event.target.presentacion.value, 
      Cantidad : this.state.cantidad, 
      Prescripcion :this.state.prescripcion,
      Id_recetaA : this.state.codigoReceta,
      //arreglar se remplaza al guardar con el id del Formulario
    };

    this.setState({
      codigo: event.target.codigo.value,
      cantidad:this.state.cantidad,
      showAA : true,
      insumos: this.state.insumos.concat([obj]),
      contarItem: this.state.contarItem + 1,
      showImp: false,
    });
  }else{
    this.setState({
      primary: !this.state.primary,
      message: 'El insumo ya fue adicionado',
      showCant:false,
      menssage2: '',
    })

  }    

  }else{   
    if(event.target.codigo.value=== '' ){
      this.setState({
        codigo: event.target.codigo.value,
        cantidad:this.state.cantidad,
        showAA : true,
        primary: !this.state.primary,
        message: 'Busque medicamento',
      })
    }
  } 

  if(this.state.contarItem>2){
    this.setState({  
      primary: !this.state.primary,
      showCant:true,
      menssage: "Termino de recetar",
      message2: "Cantidad de item completos",
    })
  }
  console.log("cantidad item: "+this.state.contarItem);  

  }

GeneraReceta(e) {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options) 

  let datosReceta= {
      fecha_emision: hoy,
      hora_emision: hora,
      consulta: this.props.match.params.id_consulta,
      estado: 'Por Entregar',
      farmaceutica: 1,
  }
  try {
    console.log("datos ", datosReceta)
      fetch(urlReceta, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(datosReceta), // data can be string or {object}!
        headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
          console.log('Success:', response)
          this.setState({
            codigoReceta:response.id_receta,
            fechaReceta:response.fecha_emision,
            horaReceta:response.hora_emision,
          })
      });
      } catch (e) {
        console.log(e);
      }
this.setState({ 
  primary3: 
  !this.state.primary3,
  collapse3:true,
  showDatosReceta:true,
  showGeneraReceta:false,
}); 
}

handleSubmit(e){    
    e.preventDefault();
    if(e.target.nombre2.value !== '' && e.target.cantidad2.value !== '' && e.target.prescripcion2.value !== '' && e.target.forma2.value !== '' ){
      if(e.target.cantidad2.value > 0){
        this.setState({
          message: '',
        })
      }
      else{
        this.setState({ 
          primary: !this.state.primary,
          message: 'La cantidad a retirar debe ser mayor a 0.'
        })
      }
    } else{ 
      this.setState({
        primary: !this.state.primary,
        message: 'Complete los espacios en blanco.',
      });
    }
    if(e.target.nombre2.value !== '' && e.target.cantidad2.value !== '' && e.target.prescripcion2.value !== '' && e.target.forma2.value !== '' && e.target.cantidad2.value > 0)
    {
    let obj = {
      Codigo2 : 'SN',
      Nombre2 : e.target.nombre2.value,
      Forma2 : e.target.forma2.value,
      Cantidad2 : e.target.cantidad2.value,
      Prescripcion2 : e.target.prescripcion2.value,
      Id_recetaA : this.state.codigoReceta
      //arreglar se remplaza al guardar con el id del Formulario
    };
    this.setState({
      medicamentos2: this.state.medicamentos2.concat([obj]),
      showImp1: false,
    });
    e.target.reset();
  }
}

handleChange2(value) {
  this.state.data.map((item, indice)=>{
    if (value === item.codigo){          
     fetch('http://localhost:8000/farmacia/almacena/v1/detalleMedicamentoBusca/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
        this.setState({
          list: findresponse,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showInsumo:false,
          showMedicamento:true,
          showMedicamentoNombre:false,
          primaryMedNombre:false,
        })
        console.log("cantidad_prescripcion: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)
    });

    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse)=>{ 
      if(findresponse.length>0){ 
      this.setState({
        dataCant: findresponse[0].sum
      });
      console.log("Cantidad Sumada primer evento existe: "+findresponse[0].sum)

      }else{
      this.setState({
        dataCant: 0
      });
      console.log("Cantidad Sumada primer evento no existe: "+0)

      }
    })
    }
    return 0}
  )

}

handleChange3(value) {
  this.state.data.map((item, indice)=>{
    if (value === item.nombre){          
     fetch('http://localhost:8000/farmacia/almacena/v1/DetalleMedicamentobBuscaNombre/'+item.nombre+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
      if (findresponse.length>1) {
        this.setState({
          show: false,
          primaryMedNombre: true,
          listaMedNombre:findresponse,
        })
      }
      else{
        this.setState({
          show: true,
          show2: false,
          list: findresponse,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showInsumo:false,
          showMedicamento:true,
        })
        console.log("cantidad_prescripcion Nombre: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)

        fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+findresponse[0].producto_farmaceutico.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse)=>{
          if(findresponse.length>0){    
          this.setState({
              dataCant: findresponse[0].sum
          });
          console.log("primer evento existe: "+findresponse[0].sum)
        }else{
          this.setState({
            dataCant: 0
        });
        console.log("primer evento no existe: "+0)
        }
       
        })
      } 
    });

  }
  return 0
})
}

handleInsumo(value) {
  console.log("data=",this.state.dataI)
  this.state.dataI.map((item, indice)=>{
    if (value === item.codigo){          
     
    fetch('http://localhost:8000/farmacia/almacena/v1/DetalleInsumoBusca/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
        this.setState({
          listInsumo: findresponse,
          showInsumo:true,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showMedicamento:false,
          primaryInsNombre:false,
        })
        console.log("cantidad_prescripcion Insumo: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)

      });

    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+item.codigo+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse)=>{    
      if(findresponse.length>0){    
        this.setState({
            dataCant: findresponse[0].sum
        });
        console.log("Insumo primer evento existe: "+findresponse[0].sum)
      }else{
        this.setState({
          dataCant: 0
      });
      console.log("Insumo primer evento no existe: "+0)
      }
    })
    }
    return 0
})
}

handleInsumoNombre(value) {
  console.log("data=",this.state.dataI)
  this.state.dataI.map((item, indice)=>{
    if (value === item.nombre){          
     
    fetch('http://localhost:8000/farmacia/almacena/v1/DetalleInsumoBuscaNombre/'+item.nombre+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {

      if (findresponse.length>1) {
        this.setState({
          show: false,
          primaryInsNombre: true,
          listaInsNombre:findresponse,
        })
      }
      else{
        this.setState({
          show: true,
          show2: false,
          listInsumo: findresponse,
          cantidad_prescripcion: findresponse[0].producto_farmaceutico.cantidad_prescripcion,
          showInsumo:true,
          showMedicamento:false,
        })
        console.log("cantidad_prescripcion Insumo Nombre: "+findresponse[0].producto_farmaceutico.cantidad_prescripcion)

        fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodMsuma/'+findresponse[0].producto_farmaceutico.codigo+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse)=>{
          if(findresponse.length>0){    
          this.setState({
              dataCant: findresponse[0].sum
          });
          console.log("primer evento existe: "+findresponse[0].sum)
        }else{
          this.setState({
            dataCant: 0
        });
        console.log("primer evento no existe: "+0)
        }
        })
      } 
    });
    }
    return 0
})
}

onFormSubmit(e) {
  this.state.medicamentos.map((data,i)=>{
    let data2={
      cantidad: data.Cantidad,
      prescripcion: data.Prescripcion,
      id_receta: data.Id_recetaA,
      codigo: data.Codigo,
    }
    try{
      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data2), // data can be string or {object}!
        headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));              
    } catch (e) {
      console.log(e);
    }
  return 0
})

this.state.insumos.map((data,i)=>{
  let dataI={
    cantidad: data.Cantidad,
    prescripcion: data.Prescripcion,
    id_receta: data.Id_recetaA,
    codigo: data.Codigo,
  }
  try{
    fetch(urlI, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(dataI), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));              
  } catch (e) {
    console.log(e);
  }
return 0
})

this.state.medicamentos2.map((data3,i)=>{
  let data4={
    nombre: data3.Nombre2,
    forma_farmaceutica: data3.Forma2,
    cantidad: data3.Cantidad2,
    prescripcion: data3.Prescripcion2,
    id_receta: data3.Id_recetaA
  }
  try {        
    fetch(url1, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data4), // data can be string or {object}!
      headers:{
        'Content-Type': 'application/json',
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));                  
  } catch (e) {
  console.log(e);
  }
  return 0
})

this.props.history.push('/RecetasEnConsultorio/RecetasEmitidas')
this.setState({
  primary2: !this.state.primary2,
});
}
onCancelar(){
  this.props.history.push('/RecetasEnConsultorio/RecetasEmitidas')
}
onValidacion(){
if(this.state.showImp===false && this.state.showImp1 ===false && this.state.medicamentos2.length>0 && (this.state.medicamentos.length>0 || this.state.insumos.length>0)){
  this.setState({ 
    showCant: false,
    message2:'',
    primaryPrint: !this.state.primaryPrint,
    message: 'No se realizó impresión de (Receta Promes - Receta Externa)',

  })
}else{
  if(this.state.showImp===false && (this.state.medicamentos.length>0 || this.state.insumos.length>0)){
    this.setState({ 
      showCant: false,
      message2:'',
      primaryPrint: !this.state.primaryPrint,
      message: 'No se realizó impresión de la Receta Promes'
    })
  }else{
    if(this.state.showImp1===false && this.state.medicamentos2.length>0 ){
      this.setState({ 
        showCant: false,
        message2:'',
        primaryPrint: !this.state.primaryPrint,
        message: 'No se realizó impresión de la Receta Externa'
      })
    }
    }
  }
  if(this.state.medicamentos2.length===0 && this.state.medicamentos.length===0 && this.state.insumos.length===0){
    this.setState({ 
      showCant: false,
      message2:'',
      primary: !this.state.primary,
      message: 'No se registro ningún producto farmaceutico'
    })
  }
  if(this.state.showImp===true && this.state.showImp1 ===true && (this.state.medicamentos.length>0 || this.state.insumos.length>0) && this.state.medicamentos2.length>0){
    this.setState({ primary2: !this.state.primary2});
  }else{
    if(this.state.showImp===true && (this.state.medicamentos.length>0 || this.state.insumos.length>0) && this.state.medicamentos2.length===0){
      this.setState({ primary2: !this.state.primary2});
    }else{
      if((this.state.medicamentos.length===0 && this.state.insumos.length===0) && this.state.showImp1===true && this.state.medicamentos2.length>0){
        this.setState({ primary2: !this.state.primary2});
      }
    }
  }
}

render() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
  
  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options)  /*Time to show in input*/

  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }
  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }

  const table=this.state.medicamentos.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"medicamento"}</td>
        <td>{u.Codigo} </td>
        <td>{u.Nombre}</td>
        <td>{u.Forma}</td>
        <td>{u.Cantidad}</td>
      </tr>
    )
  })
  const tablei=this.state.insumos.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"insumo"} </td>
        <td>{u.Codigo} </td>
        <td>{u.Nombre}</td>
        <td>{u.Presentacion}</td>
        <td>{u.Cantidad}</td>
      </tr>
    )
  })

  const tableOtro=this.state.medicamentos2.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.Nombre2}</td>
        <td>{u.Forma2}</td>
        <td>{u.Cantidad2}</td>
      </tr>
    )
  })

  const table2=this.state.medicamentos.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- medicamento: "+u.Prescripcion}</td>
      </tr>
    )
  })
  const table2i=this.state.insumos.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- insumo: "+u.Prescripcion}</td>
      </tr>
    )
  })
  const tableOtro2=this.state.medicamentos2.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- "+u.Prescripcion2}</td>
      </tr>
    )
  })
return (

<div className="animated fadeIn">    
  
  <Table responsive striped hidden id="tabla">
    <thead>
      <tr>
        <th>No.</th>
        <th>TIPO</th>
        <th>CODIGO</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {table}
    {tablei}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tabla2">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {table2}
    {table2i}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaOtro">
    <thead>
      <tr>
        <th>No.</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableOtro}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaOtro2">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableOtro2}
    </tbody>
  </Table>

<Row>
  <Col xs="12">          
    <Card>
      <CardHeader> 
      <h3><i className="fa fa-pencil-square-o"></i> <strong>Recetar</strong>
        <Button onClick={this.print2} color="link" className="btn  btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Externa
        </Button>
        <Button  onClick={this.print} color="link" className="btn  btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Promes
        </Button>
      </h3>
      </CardHeader>
        <CardBody>
        <Collapse isOpen={this.state.collapse2} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
          <Card>
          <ListGroupItem action color="info">Datos del Afiliado </ListGroupItem>            
            <Col xs="12" md="12">
            <Row>     
            <CardBody>
              <Form >
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input">Nombres y Apellidos</Label>
                </Col>
                <Col  md="4">
                  <Input type="text" id="text-input" name="nombreCompleto" value={this.props.match.params.nombreA+"   "+ this.props.match.params.apellido_pA+"   "+ this.props.match.params.apellido_mA} />
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">N°Consulta</Label>                     
                </Col>
                <Col md="2">
                <Input type="text" id="text-input" name="id_consulta" value={this.props.match.params.id_consulta} disabled />
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">N°Receta</Label>
                </Col>
                <Col md="2">
                <Input type="text" id="text-input" name="id_recetaA" value={this.state.codigoReceta} disabled />
                </Col>  
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input">Codigo de Afiliado</Label>
                </Col> 
                <Col xs="12" md="4" >
                <Input type="text" id="codigo" name="codigoAlifiacion" value={this.props.match.params.codigoA} disabled/>
                </Col>     
                <Col md="1">
                <Label htmlFor="text-input">F/Emisión</Label>
                </Col>
                <Col  md="2">
                <Input type="date" id="fecha" name="fecha" className="form-control" defaultValue={hoy} disabled ref={fecha => this.fecha = fecha}/>
                </Col>
                <Col md="1">
                <Label htmlFor="text-input">H/Emisión</Label>
                </Col>
                <Col  md="2">
                <Input type="time" id="hora" name="hora" className="form-control"  defaultValue={hora} disabled ref= {hora => this.hora = hora} />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col md="2">
                <Label htmlFor="text-input">Prescrito por  Medico</Label>
                </Col> 
                <Col xs="12" md="4" >
                <Input type="text" id="medico"  value={"Dr. "+this.props.match.params.nombreM+" "+this.props.match.params.apellido_pM+" "+ this.props.match.params.apellido_mM} disabled/>
                </Col>     
              
              </FormGroup>
              </Form>
            </CardBody>
            </Row>
            </Col>
          </Card>
        </Collapse>

        {this.state.showGeneraReceta && <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" onClick={this.togglePrimary3} ><i className="fa fa-sign-in"></i><span>Generar Receta</span></Button> }
        {this.state.showDatosReceta && <Button size="sm" color="secondary" onClick={this.toggleDatos} style={{ marginBottom: '1rem' }}><i className="icon-note"></i><span> Datos Afiliado</span></Button>}
  <Collapse isOpen={this.state.collapse3} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
  
      <Card>
        <ListGroupItem action color="info">Elija Medicamento/Insumo

        </ListGroupItem>
        <Col xs="12" md="12">
          <br/>
          <FormGroup row>
            <Col md="3">
            <Search                       
              data={this.state.data}
              onChange={ this.handleChange2.bind(this)}
              placeholder="Codigo-Medicamento"
              class="search-class"
              searchKey="codigo"
              className="form-control"
            />
            </Col>
            <Col md="3">
            <Search                       
              data={this.state.dataN}
              onChange={ this.handleChange3.bind(this)}
              placeholder="Nombre-Medicamento"
              class="search-class"
              searchKey="nombre"
              className="form-control"
            />
            </Col>
            <Col md="3">
            <Search                       
              data={this.state.dataI}
              onChange={ this.handleInsumo.bind(this)}
              placeholder="Codigo-Insumo"
              class="search-class"
              searchKey="codigo"
              className="form-control"
            />
            </Col>
            <Col xs="12" md="3">
            <Search                       
              data={this.state.dataIN}
              onChange={ this.handleInsumoNombre.bind(this)}
              placeholder="Nombre-Insumo"
              class="search-class"
              searchKey="nombre"
              className="form-control"
            />
            </Col>                        
          </FormGroup>
          <hr/>
        </Col>
        
        {this.state.showMedicamento &&<DatosMedicamento list={this.state.list} onAddReceta={this.handleOnAddReceta.bind(this)} /> }  
        {this.state.showInsumo && <DatosInsumo listInsumo={this.state.listInsumo} onAddRecetaI={this.handleOnAddRecetaI.bind(this)}/>}
        
        <Col xs="12" md="12">
          <FormGroup row>
            <Col md="1">
            <Label htmlFor="text-input">Cantidad</Label>
            </Col>
            <Col md="2">
            <Input type="number" id="cantidad" name="cantidad" className="form-control" onChange={e => this.setState({ cantidad: e.target.value })}   />
            </Col> 
            <Col md="2">
            <Label htmlFor="textarea-input">Prescripcion</Label>
            </Col>
            <Col md="5">
            <Input type="textarea" id="prescripcion"  name="prescripcion" rows="3" placeholder="Contenido" className="form-control" maxLength="100" onChange={e => this.setState({ prescripcion: e.target.value })}/>
            </Col>
            <hr/>
          </FormGroup>
        </Col>
      </Card>

    <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>

      <Card>           
        <ListGroupItem action color="success">Otro Medicamento</ListGroupItem>
        <CardBody>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <Col xs="12" md="12">
            <FormGroup color="success" row>
              <Col md="1">
              <Label htmlFor="text-input">Nombre</Label>
              </Col>
              <Col xs="12" md="3">
              <Input type="text" valid id="inputIsValid" name="nombre2" maxLength="50" onChange={e => this.setState({ nombre2: e.target.value })}/>
              </Col>
              <Col md="1">
              <Label htmlFor="text-input">F. Forma </Label>
              </Col>
              <Col md="2">
              <Input type="text" valid id="inputIsValid" name="forma2" maxLength="50" onChange={e => this.setState({ forma2: e.target.value })}  />
              </Col>
              <Col md="1">
              <Label htmlFor="text-input" valid id="inputIsValid" >Cantidad</Label>
              </Col>
              <Col md="2">
              <Input type="number" valid id="inputIsValid" name="cantidad2" className="form-control" onChange={e => this.setState({ cantidad2: e.target.value })}   />
              </Col> 
              <Col md="2">
              <h3>
              <Button size="sm" type="submit" color="success"><i className="fa fa-arrow-circle-right"></i><span> Adicionar</span></Button>
              </h3>
              </Col>
            </FormGroup>
          </Col>
          <Col xs="12" md="12">
            <FormGroup row>
              <Col md="1">
              <Label htmlFor="textarea-input" valid id="inputIsValid">Prescripcion </Label>
              </Col>
              <Col xs="12" md="5">
              <Input type="textarea" valid id="inputIsValid" name="prescripcion2" rows="2" placeholder="Contenido" className="form-control" maxLength="100" onChange={e => this.setState({ prescripcion2: e.target.value })}/>
              </Col>
            </FormGroup>
          </Col>
        </form>
        </CardBody>
      </Card>
    </Collapse>
    <Button onClick={this.toggle}  size="sm" className="btn-vine btn-brand mr-1 mb-1"><i className="icon-note"></i><span>Otro Medicamento</span></Button>
  
  <Form>
    <Table hover bordered striped responsive size="sm" >
      <thead  style={styles2} >
      <tr>
        <th><p></p>Numero<p></p></th>
        <th><p></p>Codigo<p></p></th>
        <th><p></p>Nombre Producto<p></p></th> 
        <th><p></p>Forma/Presentacion<p></p></th>
        <th><p></p>Cantidad<p></p></th>                         
        <th><p></p>Prescripción(Indicación)<p></p></th>                         
        <th><p></p>Acciones<p></p></th>       
      </tr>
      </thead>
      <tbody>
        {this.state.medicamentos.map((u, i)=> {
          return (
            <tr key = {i} bgcolor=" #ebf5fb ">
              <td style={{maxWidth: '5px', backgroundColor: '#ebf5fb', textAlign: 'center'}}>{i+1}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.Codigo} </td>
              <td style={{maxWidth: '100px', backgroundColor: '#ebf5fb'}}>{u.Nombre}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Forma}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Cantidad}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}>{u.Prescripcion}</td>
              <td style={{maxWidth: '50px', textAlign: 'center', backgroundColor: '#ebf5fb'}} >
                <Button onClick={(e)=>this.fEliminar(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>                 
              </td>
            </tr>
          )
        })}
        
        {this.state.insumos.map((u, i)=> {
          return (
            <tr key = {i} bgcolor="#f2f4f4">
              <td style={{maxWidth: '5px', backgroundColor: '#f2f4f4', textAlign: 'center'}}>{i+1}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.Codigo} </td>
              <td style={{maxWidth: '100px', backgroundColor: '#f2f4f4'}}>{u.Nombre}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>{u.Presentacion}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>{u.Cantidad}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}}>{u.Prescripcion}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4', textAlign: 'center'}}>
                <Button onClick={(e)=>this.fEliminarI(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>                 
              </td>
            </tr>
          )
          })}
          
        {this.state.medicamentos2.map((u, i)=> {
          return (
            <tr key = {i} bgcolor="#ffffff">
              <td style={{maxWidth: '5px', backgroundColor: '#ffffff', textAlign: 'center'}} >{i+1}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.Codigo2} </td>
              <td style={{maxWidth: '100px', backgroundColor: '#ffffff'}}>{u.Nombre2}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>{u.Forma2}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>{u.Cantidad2}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}}>{u.Prescripcion2}</td>
              <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}}>
                <Button onClick={(e)=>this.fEliminar2(u)} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Eliminar</span></Button>                                 
              </td>
            </tr>
          )
          })}
 
      </tbody>
    </Table>

        {/* FIN DE REGISTRO */}
        <center>
        
        <Button onClick={this.onValidacion} className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-save"></i><span>Registrar</span></Button>
        <Button onClick={this.onCancelar} className="btn-google-plus btn-brand mr-1 mb-1"><i className=" 	fa fa-times-circle"></i><span>Cancelar</span></Button>
      
        </center>
        {/* FIN DE REGISTRO */}
    </Form>
  </Collapse> 
  </CardBody> 
</Card>
</Col>
</Row>

  {/* MODAL ADVERTENCIA */}
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
      <ModalBody>
        <strong>{this.state.message}</strong>
          {this.state.showCant && <Alert color="danger"><strong>{this.state.message2}</strong></Alert>}
            
      </ModalBody>
    <ModalFooter>
      <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL ADVERTENCIA */}
  
  {/* MODAL IMPRESION*/}
    <Modal isOpen={this.state.primaryPrint} toggle={this.togglePrint} className={this.props.className}>
    <ModalHeader toggle={this.togglePrint}><i className="fa fa-print"></i> <strong> Imprimir Receta</strong></ModalHeader>
      <ModalBody>
      {this.state.message}
      {!this.state.showImp1 && this.state.medicamentos2.length>0 &&
        <Button onClick={this.print2} color="link" className="btn  btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Externa
        </Button>}
      {!this.state.showImp && (this.state.medicamentos.length>0 || this.state.insumos.length>0) &&
        <Button  onClick={this.print} color="link" className="btn  btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Promes
        </Button>}
      </ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={this.togglePrint}>Aceptar</Button>{' '}
    </ModalFooter>
  </Modal>
  {/* FIN MODAL IMPRESION*/}

  {/* MODAL REGISTRAR */}
  <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
      <ModalFooter>
        <Button onClick={this.onFormSubmit} type="submit" color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
      </ModalFooter>
  </Modal>
  {/* FIN MODAL REGISTRAR */}
            
  {/* MODAL GENERAR PEDIDO */}
  <Modal isOpen={this.state.primary3}  className={'modal-primary ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary3}>Confirmación</ModalHeader>
      <ModalBody>
        ¿Desea Generar una Nuevo Receta?
      </ModalBody>
      <ModalFooter>
        <Button onClick={this.GeneraReceta} type="submit" color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary3}>No</Button>
      </ModalFooter>
  </Modal>
  {/* FIN MODAL GENERAR PEDIDO*/}

  {/* MODAL MEDICAMENTO NOMBRE*/}
  <Modal isOpen={this.state.primaryMedNombre} toggle={this.togglePrimaryMedNom} className={'modal-info ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimaryMedNom}>Elija Medicamento</ModalHeader>
      <ModalBody>
        <Table responsive size="sm">
        <tbody>
        {this.state.listaMedNombre.map((item, indice) => ( 
          <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item.producto_farmaceutico.codigo}</td>
          <td>{item.producto_farmaceutico.nombre}</td>
          <td>{item.t_forma_farmaceutica.descripcion}</td>
          <td>
          <Button color="info" onClick={(e)=>this.handleChange2(item.producto_farmaceutico.codigo)}>{"✓"}</Button>
          </td>
          </tr>
        ))}
        </tbody>
        </Table>
      </ModalBody>
  </Modal>
  {/* FIN MODAL MEDICAMNETO NOMBRE*/}
  {/* MODAL INSUMO NOMBRE 2*/}
<Modal isOpen={this.state.primaryInsNombre} toggle={this.togglePrimaryInsNom} className={'modal-info ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimaryInsNom}>Elija Insumo </ModalHeader>
    <ModalBody>
    <Table responsive size="sm">
    {this.state.listaInsNombre.map((item, indice) => ( 
      <tr key={indice}>
      <td>{indice + 1}</td>
      <td>{item.producto_farmaceutico.codigo}</td>
      <td>{item.producto_farmaceutico.nombre}</td>
      <td>{item.t_presentacion.descripcion}</td>
      <td>
      <Button color="info" onClick={(e)=>this.handleInsumo(item.producto_farmaceutico.codigo)}>{"✓"}</Button>
      </td>
      </tr>
      ))}
    </Table>
  </ModalBody>
</Modal>

{/* FIN MODAL INSUMO NOMBRE 2*/}
                         


</div>     
    );
  } 
}
export default NuevaReceta3;

