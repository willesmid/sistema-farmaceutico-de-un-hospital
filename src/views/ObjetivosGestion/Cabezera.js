import React, { Component } from 'react';
import { render } from 'react-dom';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
} from 'reactstrap';
class Cabezera extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Cabezera: [],
    };
  }
// para datos de la cabezera
  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/v1/DatosFormulario/');
      const Cabezera = await respuesta.json();
      this.setState({
        Cabezera //la cabezera de formulario que recibimos de la ruta se muestra aqui
      });
    } catch (e) {
      console.log(e);
    }
  }
//para datos de la cabezera
  render() {
    return (
      <div className="animated fadeIn">
          {this.state.Cabezera.map(item => (
        <Row>
          <Col xs="12" md="12">
           <Card>
              <CardBody>
              <Form>
              <FormGroup row>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Codigo</Label>
                </Col>
                <Col xs="12" md="2">
                  <Input type="text" id="text-input" name="text-input" value={item.codigo} disabled />
                </Col>
                <Col md="6">
                  <Label htmlFor="disabled-input">NOMBRE DE LA ENTIDAD</Label>
                </Col>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Gestion</Label>
                </Col> 
                <Col xs="12" md="2" >
                  <Input 
                  type="number" 
                  id="number-input" 
                  name="number-input" 
                  name="text"
                  placeholder="gestion"
                  required
                  />
                </Col>   
              </FormGroup>
              <FormGroup row>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Sigla</Label>                     
                </Col>
                <Col xs="12" md="2">
                  <Input type="text" id="text-input" name="text-input" value={item.sigla} disabled />
                </Col>
                <Col xs="12" md="5">
                  <Input type="text" id="disabled-input" name="disabled-input" disabled  value={item.nombre_entidad} align="center" />
                </Col>
                <Col md="1">
                  <Label htmlFor="text-input">Fecha</Label>
                </Col>
                <Col  md="3">
                  <Input 
                  type="date" 
                  id="date-input" 
                  name="date-input" 
                  placeholder="Fecha"
                  required
                  />
                </Col>
              </FormGroup>
              <hr/>
              </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
          ))} 
      </div>
     );
   }
 }
export default Cabezera;