import React, { Component } from 'react';
import { Alert, Button, Card, CardImg, Input, CardBody, CardGroup, Col, Container, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

class Login extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
        switch: false,
        displayed_form: '',
        EntradaUsuario:'',
        EntradaContraseña:'',
      /*pregunta si existe algun toker*/
        logged_in: localStorage.getItem('token') ? true : false,
        username: '',
        nombreUnidad:'',
    };
    this.changeStatus = this.changeStatus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handle_login = this.handle_login.bind(this);
}
async componentDidMount() {
  // alert(this.state.logged_in)
  if (this.state.logged_in) {
    fetch('http://localhost:8000/core/current_user/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(json => {
        this.setState({ username: json.username });
      });
  }
}
  changeStatus(){
    // this.setState({
    //   switch: false,
    //   url: "http://localhost:3000/#/dashboard"
    // })
    console.log(this.user.value, "fgg", this.password.value)
    this.setState({
      switch: true,
      url: ""
    })
    document.getElementById("user").value = ''
    document.getElementById("password").value = ''
  }

  handleChange(key) {
    return function (e) {
      var state = {};
      state[key] = e.target.value;
      this.setState(state);
    }.bind(this);
  }

  handle_login (e){
    e.preventDefault();
    let data ={
      username:this.state.EntradaUsuario,
      password:this.state.EntradaContraseña
    }
    fetch('http://localhost:8000/token-auth/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(json => {
        localStorage.setItem('token', json.token);
        // alert(json.token)
        if(typeof(json.token) === 'undefined'){
          // alert(json.token)
          this.setState({
            switch: true,
            EntradaUsuario:'',
            EntradaContraseña:'',
          });
        }else{

            fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.EntradaUsuario+'/',{
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              } 
            })
            .then((Response)=>Response.json())  
            .then((findresponse) => {
              var cadena = findresponse[0].unidad.nombre_unidad;
              cadena = cadena.toUpperCase(cadena);
              this.setState({
                logged_in: true,
                displayed_form: '',
                username: json.user.username
              });

              if(cadena==='FARMACIA'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard1/'
                // alert(cadena)
              }
              if(cadena==='MEDICINA GENERAL'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard2/'
                // alert(cadena)
                console.log("que unidad sera  "+cadena);
              }
              if(cadena==='ENFERMERIA'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard3/'
                // alert(cadena)
              }
              if(cadena==='ODONTOLOGIA'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard4/'
                // alert(cadena)
              }
              if(cadena==='LABORATORIO'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard5/'
                // alert(cadena)
              }
              if(cadena==='ADMINISTRACION'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard/'
                // alert(cadena)
              }
              if(cadena==='NINGUNO'){
                console.log("que unidad "+json.user.username);
                window.location.href = '#/dashboard/'
                // alert(cadena)
              }

            });
          
          // window.location.href = '#/dashboard/'+json.token+'/'+this.state.username
          // window.location.href='#/Formulacion/PoaGeneral/listaPoaGeneral/'+this.props.match.params.Gestion
        }
      });
  };

  render() {
    console.log(this.state.logged_in)
    // console.log(this.state.username_I)
    // console.log(this.state.password_I)
    console.log(this.state.username)
    // console.log(this.state.password)
    return (
      <div className="app flex-row align-items-center">
              <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Iniciar sesión</h1>
                    <p className="text-muted">Iniciar sesión en su cuenta</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="nombre de usuario" 
                      value={this.state.EntradaUsuario} 
                      onChange={this.handleChange('EntradaUsuario')}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="contraseña" 
                      value={this.state.EntradaContraseña} 
                      onChange={this.handleChange('EntradaContraseña')}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" onClick={this.handle_login}>Iniciar sesión</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        {/* <Button color="link" className="px-0">¿Olvidaste tu contraseña?</Button> */}
                      </Col>
                    </Row>
                    {this.state.switch && <Row>
                      <Col md="1"></Col>
                      <Col md="10">
                        <Alert color="danger"><strong>Usuario o contraseña incorrecta. Intente de nuevo.</strong></Alert>
                      </Col>
                    </Row>}
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                  <CardBody className="text-center">
                  {/*assets/img/dashboard/icono_promes.png*/}
                  <CardImg top width="100%" src="" alt="HOSPITAL" />
                    <div>
                      {/* <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p> */}
                      {/* <Button color="primary" className="mt-3" active>Regístrate ahora!</Button> */}
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;

