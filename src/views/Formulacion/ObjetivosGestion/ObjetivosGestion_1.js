import React, { Component } from 'react';
import { render } from 'react-dom';
import Tables from '../Tables/Tables';
// import { Button, Container, Form, FormGroup, Input, Label,Col} from 'reactstrap';
import {
  Badge,
  Button,
  ButtonDropdown,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from 'reactstrap';
var url = 'http://localhost:8000/v1/guardar/';
class ObjetivosGestion extends Component {
  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.state = {
      Cabezera: [],
      Gestion: '',
      fecha: '',
      Descripcion:'',
      Resultados:'',
      Beneficiarios:'',
      prueba:[]
     
    };
    this.handleClick = this.handleClick.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    
  }
  handleClick(event) {  // switch the value of the showModal state
    this.setState({
      showTable: true
    });
  }
  getComponent() {
    alert("entro")
     return <Tables/>
  
  }

  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }

 
  onFormSubmit(e) {
    let objetivos =  new Array()
    let data = {
      //gestion:this.value,
      gestion: this.state.Gestion,
      fecha: this.state.fecha,
      descripcion: this.state.Descripcion,
    }
    var prueba=JSON.stringify(data)
    alert(prueba);
    
    objetivos.push(prueba);

   alert(objetivos[0],objetivos[1]);
    //alert(JSON.stringify(data))
    // let data=JSON.stringify(this.state.Gestion,this.state.fecha)
  
    // try {
    //      //alert(JSON.stringify(this.state, null, ' '));
    //      fetch(url, {
    //        method: 'POST', // or 'PUT'
    //        body: JSON.stringify(this.state), // data can be string or {object}!
    //        headers:{
    //          'Content-Type': 'application/json'
    //        }
    //      }).then(res => res.json())
    //      .catch(error => console.error('Error:', error))
    //      .then(response => console.log('Success:', response));
    //     // alert("SE GUARDO SATISFACTORIAMENTE EL OBJETIVO")
              
    //    } catch (e) {
    //      console.log(e);
    //    }
  }
  // para envio desde el backend
  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/v1/DatosFormulario/');
      
      const Cabezera = await respuesta.json();
     
      this.setState({
        Cabezera //los medicos que recibimos de la ruta se muestra aqui
        
      });
    
    } catch (e) {
      console.log(e);
    }
  }
// para envio desde el backend

  render() {
    return (
      <div className="animated fadeIn">
          {this.state.Cabezera.map(item => (
        <Row>
          <Col xs="12" md="12">
           <Card>
              <CardHeader>
                <strong>Objetivos de Gestion</strong>
              </CardHeader>
              <CardBody>
              <Form onSubmit={this.onFormSubmit}>
              <FormGroup row>
                {/* <Col xs="12" md="9">
                  <Label></Label>
                </Col> */}
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Codigo</Label>
                </Col>
                <Col xs="12" md="2">
                  <Input type="text" id="text-input" name="text-input" value={item.codigo} disabled />
                </Col>
                <Col md="6">
                  <Label htmlFor="disabled-input">NOMBRE DE LA ENTIDAD</Label>
                </Col>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Gestion</Label>
                </Col> 
                <Col xs="12" md="2" >
                  <Input 
                  type="number" 
                  id="number-input" 
                  name="number-input" 
                  name="text"
                  placeholder="gestion"
                  value={this.state.Gestion}
                  onChange={e => this.setState({ Gestion: e.target.value })}
                  />
                </Col>   
              </FormGroup>

              <FormGroup row>
                <Col xs="12" md="1">
                  <Label htmlFor="text-input">Sigla</Label>                     
                </Col>
                <Col xs="12" md="2">
                  <Input type="text" id="text-input" name="text-input" value={item.sigla} disabled />
                </Col>
                <Col xs="12" md="5">
                  <Input type="text" id="disabled-input" name="disabled-input" disabled  value={item.nombre_entidad} align="center" />
                </Col>
                <Col md="1">
                  <Label htmlFor="text-input">Fecha</Label>
                </Col>
                <Col  md="3">
                  <Input 
                  type="date" 
                  id="date-input" 
                  name="date-input" 
                  placeholder="Fecha"  
                  value={this.state.fecha}
                  onChange={e => this.setState({ fecha: e.target.value })} 
                  />
                </Col>
              </FormGroup>
              <hr/>
              <Tables/>
              <hr/>
              <hr/>
              <FormGroup row>
                <Col md="1">
                  <Label htmlFor="textarea-input">Descripcion</Label>
                </Col>
                <Col xs="12" md="3">
                  <Input 
                    type="textarea" 
                    name="textarea-input" 
                    id="textarea-input" 
                    rows="3"
                    placeholder="Contenido"
                    value={this.state.Descripcion}
                    onChange={e => this.setState({ Descripcion: e.target.value })}
                  />
                </Col>
                <Col md="1">
                  <Label htmlFor="textarea-input">Resultados</Label>
                </Col>
                <Col xs="12" md="3">
                  <Input 
                  type="textarea" 
                  name="textarea-input" 
                  id="textarea-input" 
                  rows="3"
                  placeholder="Contenido"
                  value={this.state.Resultados}
                  onChange={e => this.setState({ Resultados: e.target.value })}
                  />
                </Col>
               
                <Col xs="12"md="1">
                  <Label htmlFor="select">Beneficiarios</Label>
                </Col>
                <Col xs="12" md="3">
                  <Input 
                  type="select" 
                  name="select" 
                  id="select" 
                  value={this.state.Beneficiarios}
                  onChange={e => this.setState({ Beneficiarios: e.target.value })}
                  >
                    <option value="0">Seleccionar</option>
                    <option value="Estudiantes General">Estudiantes General</option>
                    <option value="Estudiantes Afiliados">Afiliados</option>
                  </Input>
                </Col>
                <hr/>
              </FormGroup>
              <FormGroup>
                <Col xs="12"md="1">
                  <Label htmlFor="select"></Label>
                </Col>
              </FormGroup>
              <Button type="submit" color="success" className="mr-1">Registrar</Button>
              <Button type="reset" size="md" color="danger" className="mr-1">Cancelar</Button>         
              </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
          ))} 
      </div>
     );
   }
 }
export default ObjetivosGestion;