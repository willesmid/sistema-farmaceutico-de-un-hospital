import React from 'react';
import { Card, CardImg, Row, Col } from 'reactstrap';



const Example = (props) => {    
  return (
    <div>
      <br/>
      <br/>
      <Row>
        <Col xs= "6" md="2"></Col>
        <Col xs="6" md="2">
          <a href="#/">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/icono_promes.png" alt="SI-GESHO-PROMES" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/Inventario/InventarioGeneral/Productos">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/image9.png" alt="Inventario" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/SaldosMinimos/Productos">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/saldos.png" alt="Saldos" />
            </Card>
          </a>
        </Col> 
        <Col xs="6" md="2">
          <a href="#/RecetasEnConsultorio/Consulta">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/image10.png" alt="Recetar" />
            </Card>
          </a>
        </Col>   
      </Row>
      <br/>
      <Row>
        <Col xs= "6" md="2"></Col>
        <Col xs="6" md="2">
          <a href="#/RecetasEnFarmacia/RecetasPorEntregar">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/image11.png" alt="Recetas por entregar" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/image12.png" alt="Pedidos" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/BindCard/BindCardFarmacia">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/image13.png" alt="BindCard" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/Reportes/Medicamentos">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/reportes.png" alt="Reportes" />
            </Card>
          </a>
        </Col>    
        </Row>
    </div>
  );
};

export default Example;
