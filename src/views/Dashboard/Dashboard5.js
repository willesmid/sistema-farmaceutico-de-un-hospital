import React, { Component } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  NavLink,
  Row,
} from 'reactstrap';
import './Estilos.css';

const trNavLink = {
  padding: '0',
  margin: '0'
}

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  render() {

    return (
      <div className="animated fadeIn">

      <Row>
        <Col xs="12" sm="6" md="4">
          <NavLink style={trNavLink} href = "#/catalogo/examenes">
            <Card className="text-white bg-primary">
              <CardHeader>
                <center><strong>CATALOGO</strong></center>
              </CardHeader>
              <CardBody className="filter-green">
              <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1s-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm6 12H6v-1.4c0-2 4-3.1 6-3.1s6 1.1 6 3.1V19z"/></svg>
              </CardBody>
            </Card>
          </NavLink>
        </Col>

        <Col xs="12" sm="6" md="4">
          <NavLink style={trNavLink} href = "#/ordenesLaboratorio">
            <Card className="text-white bg-primary">
              <CardHeader>
                <center><strong>ORDENES DE LABORATORIO</strong></center>
              </CardHeader>
              <CardBody className="filter-green">
              <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M21 3H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h5v1c0 .55.45 1 1 1h6c.55 0 1-.45 1-1v-1h5c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-1 14H4c-.55 0-1-.45-1-1V6c0-.55.45-1 1-1h16c.55 0 1 .45 1 1v10c0 .55-.45 1-1 1zm-2-9H9c-.55 0-1 .45-1 1s.45 1 1 1h9c.55 0 1-.45 1-1s-.45-1-1-1zm0 4H9c-.55 0-1 .45-1 1s.45 1 1 1h9c.55 0 1-.45 1-1s-.45-1-1-1zM7 8H5v2h2V8zm0 4H5v2h2v-2z"/></svg>
              </CardBody>
            </Card>
          </NavLink>
        </Col>

        <Col xs="12" sm="6" md="4">
          <NavLink style={trNavLink} href = "#/ordenesLaboratorioCanceladas">
            <Card className="text-white bg-primary">
              <CardHeader>
                <center><strong>ORDENES DE LABORATORIO CANCELADAS</strong></center>
              </CardHeader>
              <CardBody className="filter-green">
              <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z"/><path fill="none" d="M0 0h24v24H0z"/></svg>
              </CardBody>
            </Card>
          </NavLink>
        </Col>

      </Row>
      <Row>

        <Col xs="12" sm="6" md="4">
          <NavLink style={trNavLink} href = "#/pruebasRealizadas">
            <Card className="text-white bg-primary">
              <CardHeader>
                <center><strong>PRUEBAS REALIZADAS</strong></center>
              </CardHeader>
              <CardBody className="filter-green">
              <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M11 3.18v17.64c0 .64-.59 1.12-1.21.98C5.32 20.8 2 16.79 2 12s3.32-8.8 7.79-9.8c.62-.14 1.21.34 1.21.98zm2.03 0v6.81c0 .55.45 1 1 1h6.79c.64 0 1.12-.59.98-1.22-.85-3.76-3.8-6.72-7.55-7.57-.63-.14-1.22.34-1.22.98zm0 10.83v6.81c0 .64.59 1.12 1.22.98 3.76-.85 6.71-3.82 7.56-7.58.14-.62-.35-1.22-.98-1.22h-6.79c-.56.01-1.01.46-1.01 1.01z"/></svg>
              </CardBody>
            </Card>
          </NavLink>
        </Col>

        <Col xs="12" sm="6" md="4">
          <NavLink style={trNavLink} href = "#/pacientesAtendidos">
            <Card className="text-white bg-primary">
              <CardHeader>
                <center><strong>PACIENTES ATENDIDOS</strong></center>
              </CardHeader>
              <CardBody className="filter-green">
              <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v1c0 .55.45 1 1 1h14c.55 0 1-.45 1-1v-1c0-2.66-5.33-4-8-4z"/></svg>
              </CardBody>
            </Card>
          </NavLink>
        </Col>

        <Col xs="12" sm="6" md="4">
          <NavLink style={trNavLink} href = "#/dashboard">
            <Card className="text-white bg-primary">
              <CardHeader>
                <center><strong>REACTIVOS</strong></center>
              </CardHeader>
              <CardBody className="filter-green">
              <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M16.56 8.94L7.62 0 6.21 1.41l2.38 2.38-5.15 5.15c-.59.59-.59 1.54 0 2.12l5.5 5.5c.29.29.68.44 1.06.44s.77-.15 1.06-.44l5.5-5.5c.59-.58.59-1.53 0-2.12zM5.21 10L10 5.21 14.79 10H5.21zM19 11.5s-2 2.17-2 3.5c0 1.1.9 2 2 2s2-.9 2-2c0-1.33-2-3.5-2-3.5z"/><path fill-opacity=".36" d="M0 20h24v4H0z"/></svg>
              </CardBody>
            </Card>
          </NavLink>
        </Col>
      </Row>

      </div>
    );
  }
}

export default Dashboard;
