import React from 'react';
import { Card, CardImg, Row, Col } from 'reactstrap';



const Example = (props) => {
    return (
      <div className="animated fadeIn">
      <Row>
        <Col xs="0" md="2"></Col>
        <Col xs="6" md="2">
          <a href="#/">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/icono_promes.png" alt="Index" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/PrimeraConsulta">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/primeraconsulta.png" alt="Primera consulta odontologica" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/Tratamiento">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/tratamiento.png" alt="Tratamiento odontologico" />
            </Card>
          </a>
        </Col>
      </Row>
      <br />
      <Row>
        <Col xs="0" md="2"></Col>
        <Col xs="6" md="2">
          <a href="#/Insumos/BindCard">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/insumosmedicos.png" alt="Insumos medicos" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/PedidoUnidad/NuevoPedido">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/pedidoInsumos.png" alt="Pedido de insumos" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/CampPrevencion/CampTareas">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/campaing.png" alt="Campañas de prevencion bucal" />
            </Card>
          </a>
        </Col>
      </Row>
    </div>
    );
};

export default Example;
