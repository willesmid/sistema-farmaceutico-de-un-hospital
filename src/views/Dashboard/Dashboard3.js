import React from 'react';
import { Card, CardImg, Row, Col } from 'reactstrap';



const Example = (props) => {
    
    return (
      <div>
            <br/>
            <br/>

            <Row>
                <Col xs="0" md="2"></Col>
                <Col xs="6" md="2">
                    <a href="#/">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/icono_promes.png" alt="PROMES" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/ProductosFarmaceuticos/Productos_Todos/Productos_Todos">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image3.png" alt="Tratamientos" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/ProductosFarmaceuticos/Productos_Todos/Productos_Todos">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image4.png" alt="Control de signos vitales, peso y talla" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/EventosSalud/RegistrarPlanDeCharla">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image7.png" alt="Eventos de salud" />
                        </Card>
                    </a>
                </Col>
                
            </Row>
            <br/>
            <Row>
                <Col xs="0" md="2"></Col>
                {/* <Col xs="6" md="2">
                    <a href="#/">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image8.png" alt="Internación" />
                        </Card>
                    </a>
                </Col> */}
                <Col xs="6" md="2">
                    <a href="#/farmaciaAuxiliar/BuscarMedicamentoInsumo">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image1.png" alt="Farmacia auxiliar" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/Novedades/RegistrarNovedadObservacion">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image5.png" alt="Novedades y observaciones" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/PedidoUnidad/NuevoPedido">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image2.png" alt="Pedidos" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/RotacionPersonal/RealizarRotacion">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/image6.png" alt="Rotación de personal" />
                        </Card>
                    </a>
                </Col>
            </Row>
        </div>
    );
};

export default Example;
