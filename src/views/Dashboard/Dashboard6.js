import React from 'react';
import { Card, CardImg, Row, Col } from 'reactstrap';



const Example = (props) => {    
  return (
    <div>
      <br/>
      <br/>
      <Row>
        <Col xs= "6" md="2"></Col>
        <Col xs="6" md="2">
          <a href="#/">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/icono_promes.png" alt="SI-GESHO-PROMES" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/Inventario/InventarioGeneral/Productos">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/registrar.png" alt="Registrar afiliado" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/SaldosMinimos/Productos">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/baja.png" alt="Dar baja afiliado" />
            </Card>
          </a>
        </Col> 

        <Col xs="6" md="2">
          <a href="#/RecetasEnFarmacia/RecetasPorEntregar">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/buscar.png" alt="Buscar Afiliado" />
            </Card>
          </a>
        </Col> 
      </Row>
      <br/>
      <Row>
        <Col xs= "6" md="2"></Col>
        <Col xs="6" md="2">
          <a href="#/PedidosFarmacia/PedidosUnidad/PedidosPorEntregar">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/agendar.png" alt="Agendar" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/PedidosFarmacia/PedidosSSU/RealizarPedidoSSU">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/asignar.png" alt="Asignar ficha" />
            </Card>
          </a>
        </Col>  
        <Col xs="6" md="2">
          <a href="#/BindCard/BindCardFarmacia">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/crearficha.png" alt="Crear ficha" />
            </Card>
          </a>
        </Col>
        <Col xs="6" md="2">
          <a href="#/Reportes/Medicamentos">
            <Card>
              <CardImg top width="100%" src="assets/img/dashboard/reportes.png" alt="Reportes" />
            </Card>
          </a>
        </Col>    
        </Row>
    </div>
  );
};

export default Example;
