import React from 'react';
import { Card, CardImg, Row, Col } from 'reactstrap';



const Example = (props) => {
    
    return (
        <div>
            <br/>
            <br/>
            <Row>
                <Col xs= "6" md="2"></Col>
                <Col xs="6" md="2">
                    <a href="#/">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/icono_promes.png" alt="SI-GESHO-PROMES" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/RecetasEnConsultorio/Recetar">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/recetar2.png" alt="Recetar" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/RecetasEnConsultorio/Consulta">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/consulta.png" alt="Consulta" />
                        </Card>
                    </a>
                </Col>
              
                
            </Row>
            <br/>
            <Row>
                <Col xs= "6" md="2"></Col>
                <Col xs="6" md="2">
                    <a href="#/RecetasFarmacia/RecetasPorEntregar">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/historial.png" alt="historial" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/Novedades/RegistrarNovedadObservacion">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/hoja.png" alt="hoja" />
                        </Card>
                    </a>
                </Col>
                <Col xs="6" md="2">
                    <a href="#/RotacionPersonal/RealizarRotacion">
                        <Card>
                            <CardImg top width="100%" src="assets/img/dashboard/internacion.png" alt="internacion" />
                        </Card>
                    </a>
                </Col>
         
                
            </Row>
        </div>
    );
};

export default Example;
