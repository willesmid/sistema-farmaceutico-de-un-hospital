import React from 'react';
import ReactDOM from 'react-dom';
import RecetasAnuladas from './RecetasAnuladas';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RecetasAnuladas />, div);
  ReactDOM.unmountComponentAtNode(div);
});
