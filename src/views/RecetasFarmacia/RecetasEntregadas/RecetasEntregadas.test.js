import React from 'react';
import ReactDOM from 'react-dom';
import RecetasEntregadas from './RecetasEntregadas';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RecetasEntregadas />, div);
  ReactDOM.unmountComponentAtNode(div);
});
