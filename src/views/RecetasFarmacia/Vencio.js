import React, { Component } from 'react';
import { 
Badge,
FormText
} from 'reactstrap';

class Vencio extends Component {
constructor(props) {
  super(props);
  this.toggle = this.toggle.bind(this);
  this.toggleFade = this.toggleFade.bind(this);
  this.state = {
    collapse: true,
    fadeIn: true,
    timeout: 300,
    data: 0,
    data2: 0
  };
}

toggle() {
    this.setState({ collapse: !this.state.collapse });
}

toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
}

calcDays(fv){
  /*calcula los dias que hay entre dos fechas */
  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  var firstDate = new Date(Date.parse(fv));
  var secondDate = new Date();  //hoy
  var diffDays = Math.round((firstDate.getTime() - secondDate.getTime())/(oneDay));
  var abs =Math.abs(diffDays)
  console.log(abs)

  return abs
}
render() {
  /*num-> calcula el stock de un determinado medicamento/insumo */
  const dias = this.calcDays(this.props.fv)
  const cad = (dias <= 2?"VALIDO":"NO VALIDO")
  const type2 =(dias <= 2?"success":"danger")
  const unidad = (dias > 1?" días":" día")
  
  return (
    <td>
      <Badge color={type2}>{cad}</Badge>
      <FormText className="help-block" color= {type2}> {dias }{unidad}</FormText>
    </td>
  );
}
}
export default Vencio;
