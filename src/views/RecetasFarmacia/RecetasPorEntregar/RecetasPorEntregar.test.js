import React from 'react';
import ReactDOM from 'react-dom';
import RecetasPorEntregar from './RecetasPorEntregar';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RecetasPorEntregar />, div);
  ReactDOM.unmountComponentAtNode(div);
});
