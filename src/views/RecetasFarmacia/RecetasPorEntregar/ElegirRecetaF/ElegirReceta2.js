
import React, { Component } from 'react';
import { 
  Button, 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup

} from 'reactstrap';
import BuscaLote from './BuscaLote'

class ElegirRecetaF2 extends Component {
  constructor(props) {
    super(props)

    this.state = {
     
        url_receta: '#/listaRecetasF/ElegirRecetaF',
        list:[],

        list_lote : [],
        loteA:'',
        codP:''
        
        
    };
    // this.handleChange2 = this.handleChange2.bind(this)
    console.log(this.props.match.params)
  }

  componentDidMount() {

    fetch('http://localhost:8000/v3/detalleMedicamentoRecetaBusca/'+this.props.match.params.id_receta+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
      .then((Response)=>Response.json())
      .then((findresponse) => {
        console.log("FINDRESPONSE=",findresponse)
          this.setState({
            list: findresponse,

              url_receta : this.state.url_receta +'/'+ this.props.match.params.id_receta +'/'+ this.props.match.params.id_consulta +'/'+ this.props.match.params.codigoAfiliacion +'/'+this.props.match.params.cedulaIdentidadA+'/'+ this.props.match.nombresA +'/'+ this.props.match.params.apellidoPaternoA +'/'+ this.props.match.params.apellidoMaternoA+'/'+this.props.match.params.nombresM+'/'+this.props.match.params.apellidoPaternoM+'/' +this.props.match.params.fecha +'/resultados',
            
                                            
          
            
            
          })

          console.log("holasss")
         
         // console.log('LIST=',this.state.list.length)   
  
          if (this.state.list.length===0) {
            this.setState({
              show: false,
              show2: true
            })
            
           // document.write("No hay " + this.state.list.length);
          }
          else{
            this.setState({
              show: true,
              show2: false
            })
            //document.write("SI hay "+ this.state.list.length);
          } 
      });
    
    

    
   

  
  }



 

  async handleChange(){
    //console.log(value); 
       fetch('http://localhost:8000/v2/detalleMedicamentoRecetaBusca/'+this.props.match.params.id_receta+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
        console.log("FINDRESPONSE=",findresponse)
          this.setState({
            list: findresponse
            
          })

          console.log("holasss")
         
         // console.log('LIST=',this.state.list.length)   
  
          if (this.state.list.length===0) {
            this.setState({
              show: false,
              show2: true
            })
            
           // document.write("No hay " + this.state.list.length);
          }
          else{
            this.setState({
              show: true,
              show2: false
            })
            //document.write("SI hay "+ this.state.list.length);
          } 
      });
  
      
  }
  


  handleChange2(value){

    console.log(value); 
   

           
       fetch('http://localhost:8000/v1/LoteProductoBusca/A1101/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
        console.log("FINDRESPONSE=",findresponse)
          this.setState({
            list_lote: findresponse
            
          })

          console.log("holasss")
         
         // console.log('LIST=',this.state.list.length)   
  
 
 
      });
      
  }




  handleOnAddReceta3 (event) {

    event.preventDefault();

   
    this.setState({
      
      loteA: event.target.lote.value,
      
      
    });
    console.log(this.state.loteA)
    
 
  }





  render() {
    return (
      <div>
<Row>
  <Col xs="12" md="12">
   <Card>
      <CardHeader>
        <i className="fa fa-align-justify"></i> Entregar Receta
      </CardHeader>
      <CardBody>
        <Form >
        <FormGroup row>
          <Col md="9">
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">Fecha</Label>
          </Col>
          <Col  md="2">
            <Input 
            type="date" 
            id="date-input" 
            name="fecha" 
            placeholder="Fecha"
            value={this.props.match.params.fecha}

            />
          </Col>


        </FormGroup>
        <FormGroup row>
          
          <Col md="1">
            
          </Col>
          <Col xs="12" md="2">
            <Label htmlFor="text-input">CODIGO AFILIADO</Label>
          </Col> 
            <Col xs="12" md="5" >
            <Input 
            type="text" 
            id="text-input" 
            name="codigoAlifiacion" 
            value={this.props.match.params.codigoAfiliacion }   

            />
          </Col> 
          <Col xs="12" md="1">
          
          </Col>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">id Receta</Label>
          </Col>
          <Col xs="12" md="2">
            <Input type="text" id="text-input" name="id_recetaA" value={this.props.match.params.id_receta} disabled />
          </Col>  
        </FormGroup>
        
        <FormGroup row>
        
          <Col xs="12" md="5">
          
          </Col>
        

          <Col md="1">
          
          </Col>
          <Col  md="3">
            
          </Col>
          <Col xs="12" md="1">
            <Label htmlFor="text-input">id consulta</Label>                     
          </Col>
          <Col xs="12" md="2">
            <Input type="text" id="text-input" name="id_consulta" value={this.props.match.params.id_consulta} disabled />
          </Col>
        </FormGroup>

        <FormGroup row>
        
        
      

        <Col md="2">
          <Label htmlFor="text-input">Nombre y Apellido</Label>
        </Col>
        <Col  md="3">
          <Input 
          type="text" 
          id="text-input" 
          name="nombreCompleto" 
          value={this.props.match.params.nombresA +"   "+ this.props.match.params.apellidoPaternoA+"   "+ this.props.match.params.apellidoMaternoA  }   disabled

          />
        </Col>
        <Col xs="12" md="1">
          <Label htmlFor="text-input">Matricula</Label>                     
        </Col>
        <Col xs="12" md="2">
          <Input type="text" id="text-input" name="cedulaIdentidad" value={this.props.match.params.cedulaIdentidadA}  disabled />
        </Col>
        <Col md="1">
          <Label htmlFor="text-input">Medico</Label>
          </Col>
        <Col xs="12" md="3">
          <Input type="text" id="disabled-input" name="nombreMedico" disabled  value={"Dr. "+ this.props.match.params.nombresM +" "+this.props.match.params.apellidoPaternoM } align="center" />
        </Col>
      </FormGroup>
        <hr/>
        </Form>

        <Card>
         <Table responsive>
         <thead>
           <tr>
           <th>Numero</th>
           <th>Codigo</th>
           <th>Nombre</th> 
           <th>Forma</th>
           <th>Cantidad</th>                         
           <th>Prescripcion</th>  
                             
           <th>Lote</th>  
           <th>Accciones</th> 
           <th>Accciones</th>      
               
             
           </tr>
         </thead>
         <tbody>
           {/* parte que hace el llenado de un estado  */}
           {this.state.list.map((u, i)=> {
             return (
               <tr key = {i}>

                <td style={{maxWidth: '5px', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                <td style={{maxWidth: '150px'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                <td style={{maxWidth: '150px'}} >{u.codigo.t_forma_farmaceutica.descripcion} </td>
                <td style={{maxWidth: '50px'}} >{u.cantidad} </td>
                <td style={{maxWidth: '150px'}} >{u.prescripcion} </td>                 
                <td style={{maxWidth: '200px'}}>
                
                  <BuscaLote codP={u.codigo.producto_farmaceutico.codigo}  onAddReceta2={this.handleOnAddReceta3.bind(this)}/> 
                 
                </td>
                <td style={{maxWidth: '150px'}} >{this.state.lote_A} </td>  
                <td>
                   {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                   <Button onClick={(e)=>this.fEliminar(u)} color="primary" className="mr-1" size="sm">Ver</Button>
                </td>
                
                   {/* <Button onClick={(e)=>this.fEditar(i)} type="button">Editar</Button> */}
                <td><Button onClick={(e)=>this.fEliminar(u)} color="primary" className="mr-1" size="sm">Confirmar</Button></td>
                
                 </tr>
                    )
           })}
            {/* parte que hace el llenado de un estado  */}
         </tbody>
         </Table>
       </Card>
     {/* <FormObjetivos  onAddObjetivo={this.handleOnAddObjetivo.bind(this)} /> */}
       
      </CardBody>
    </Card>
  </Col>

       <CardBody>
                      
                      {/* <Cabezera/>  */}
       
   </CardBody>

</Row>

      </div>
    )
  }
}

export default ElegirRecetaF2;