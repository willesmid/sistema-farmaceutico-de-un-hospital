import React from 'react';
import ReactDOM from 'react-dom';
import ElegirRecetaF from './ElegirRecetaF';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ElegirRecetaF />, div);
  ReactDOM.unmountComponentAtNode(div);
});

