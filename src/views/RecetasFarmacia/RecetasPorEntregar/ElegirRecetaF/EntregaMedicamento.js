import React, { Component } from 'react';
import { 
  Form,
  Col,
  Input,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Alert,
} from 'reactstrap';

class EntregaMedicamento extends Component {
  constructor(props) {
    super(props)
    this.state = {     
    list_lote: [],
    cantidadLote: 0,
    cantidadFarmacia:0,
    cantidadGeneral:0,
    lote:'',
    codP1:'',
    lote1:'',
    numSaldo:0,
    showEntrega:true,   
    
    primary:false,
    primary2:false,
    menssage:'',
    showYaEntregado:false,
    showCantFarmacia: false,
    fecha_expiracion:'',
    };
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onValidacion = this.onValidacion.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)  
}

togglePrimary() {
  this.setState({ primary: !this.state.primary});
}

togglePrimary2() {
  this.setState({ primary2: !this.state.primary2});
}

componentDidMount(){
  //console.log(value); CardBody
  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBuscaCodP/'+this.props.codP+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    // console.log("FINDRESPONSE=",findresponse)
    this.setState({
      list_lote: findresponse,
      lote:findresponse[0].lote.id_lote
    })
     // AQUI SACAMOS LOS DATOS DEL LOTE [0] //
     fetch('http://localhost:8000/farmacia/almacena/v1/LoteProductoBusca2/'+this.props.codP+'/'+findresponse[0].lote.id_lote+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())  
    .then((findresponse) => {
    // console.log("FINDRESPONSE=",findresponse)
      this.setState({
        cantidadLote: findresponse[0].cantidad
      })
    });
  
    fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBusca2/'+this.props.codP+'/'+findresponse[0].lote.id_lote+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())  
    .then((findresponse) => {
    // console.log("FINDRESPONSE=",findresponse)
      this.setState({
        cantidadFarmacia: findresponse[0].stockAlmacen
      })
    });
  
    fetch('http://localhost:8000/farmacia/almacena/v1/LoteBusca/'+findresponse[0].lote.id_lote+'/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())  
    .then((findresponse) => {
      this.setState({
        fecha_expiracion: findresponse[0].fecha_expiracion
      })
    }); 
     
     // FIN AQUI SACAMOS LOS DATOS DEL LOTE [0] // 
})
  //Primera vista Cantidad del codigo//
fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.props.codP+'/',{
  headers: {
    Authorization: `JWT ${localStorage.getItem('token')}`
  } 
})
.then((Response)=>Response.json())  
.then((findresponse) => {
// console.log("FINDRESPONSE=",findresponse)
  this.setState({
    cantidadGeneral: findresponse[0].stockGeneral
  })
});
// FIN Primera vista Cantidad del codigo///
}

handleChange(){
  fetch('http://localhost:8000/farmacia/almacena/v1/LoteProductoBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
  // console.log("FINDRESPONSE=",findresponse)
    this.setState({
      cantidadLote: findresponse[0].cantidad
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/UbicadoFarmaciaBusca2/'+this.props.codP+'/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
  // console.log("FINDRESPONSE=",findresponse)
    this.setState({
      cantidadFarmacia: findresponse[0].stockAlmacen
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/ProductoBusca2/'+this.props.codP+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
  // console.log("FINDRESPONSE=",findresponse)
    this.setState({
      cantidadGeneral: findresponse[0].stockGeneral
    })
  });

  fetch('http://localhost:8000/farmacia/almacena/v1/LoteBusca/'+this.state.lote+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      fecha_expiracion: findresponse[0].fecha_expiracion
    })
  });   
}

onValidacion(e){
  const numT=this.state.cantidadFarmacia - this.props.cantidadP
  // console.log("Validar cantidad en Farmacia: "+numT )
  if(this.state.lote === '' ){
    this.setState({     
      primary: !this.state.primary,
      message: 'Ingrese Lote ',
    })
  }else{
    if(numT<0){
      this.setState({     
        primary: !this.state.primary,
        message: 'Cantidad Insuficiente ',
        showCantFarmacia: true, 
      })}else{
    this.setState({
      numSaldo: this.state.cantidadFarmacia - this.props.cantidadP,
      primary2: !this.state.primary2,  
    })
  }}
}

onFormSubmit(e) { 
  let urlLote = ''
  let urlFarmacia = ''
  let urlGeneral = ''
  let urlproducto_medicamento=''
  let urlproducto_insumo= ''
  let dataLote = {}
  let dataFarmacia = {}
  let dataGeneral = {}
  let dataProducto_medicamento={}
  let dataProducto_insumo={}

  urlLote = 'http://localhost:8000/farmacia/almacena/v1/LoteCantidadEdita/'+this.state.lote+'/'
  urlFarmacia = 'http://localhost:8000/farmacia/almacena/v1/EstaFarmaciaEdita/'+this.state.lote+'/'
  urlGeneral = 'http://localhost:8000/farmacia/almacena/v1/ProductoEdita/'+this.props.codP+'/'
  urlproducto_medicamento= 'http://localhost:8000/farmacia/recetar/v1/Medicamento_RecetaEdita/'+this.props.id+'/'
  urlproducto_insumo = 'http://localhost:8000/farmacia/recetar/v1/Insumo_RecetaEdita/'+this.props.id+'/'
  
  const numLote = this.state.cantidadLote - this.props.cantidadP
  const numFarmacia = this.state.cantidadFarmacia - this.props.cantidadP
  const numGeneral= this.state.cantidadGeneral - this.props.cantidadP

  console.log('nueva cantidadLOTE jijiji: '+ this.state.cantidadLote)
  console.log('nueva cantidadPRESCRIPCION jijiji: '+ this.props.cantidadP)
  console.log('nueva cantidad RESTA : '+ numLote)
  console.log('nueva cantidadGENERAL jijiji: '+ this.state.cantidadGeneral)
  console.log('nueva cantidadPRESCRIPCION jijiji: '+ this.props.cantidadP)
  console.log('nueva cantidad RESTA : '+ numGeneral)

  dataLote = {
    cantidad : numLote
  }
  dataFarmacia = {
    stockAlmacen : numFarmacia
  }
  dataGeneral = {
    stockGeneral : numGeneral
  }

  dataProducto_medicamento= {
    lote_salida : this.state.lote,
    fecha_expiracion: this.state.fecha_expiracion,
    entregado:true,
  }

  dataProducto_insumo= {
    lote_salida : this.state.lote,
    fecha_expiracion: this.state.fecha_expiracion,
    entregado:true,
  }
    try {
      fetch(urlLote, {
      method: 'PUT',
      body: JSON.stringify(dataLote),
      headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
    } catch (e) {
        console.log(e);
    }
    try {

      fetch(urlFarmacia, {
      method: 'PUT',
      body: JSON.stringify(dataFarmacia),
      headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }


    try {
      fetch(urlGeneral, {
      method: 'PUT',
      body: JSON.stringify(dataGeneral),
      headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }

    try {
      fetch(urlproducto_medicamento, {
      method: 'PUT',
      body: JSON.stringify(dataProducto_medicamento),
      headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }
    this.setState({
      showEntrega:false,
    })

    console.log("rayos: "+urlproducto_insumo)
 if(urlproducto_insumo!==""){
    try {
      fetch(urlproducto_insumo, {
      method: 'PUT',
      body: JSON.stringify(dataProducto_insumo),
      headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

    } catch (e) {
        console.log(e);
    }
    this.setState({
      showEntrega:false,
    })
  }
  //FIN ENTREGA //
  this.setState({
    primary2: !this.state.primary2,
    showYaEntregado: true
  })

  // VALIDANDO UN PRODUCTO ///
  this.setState({
    cantidadFarmacia: numFarmacia
  })
  // FIN VALIDANDO UN PRODUCTO ///
}


render() {
  if(this.props.entregadom || this.props.entregadoi){
    if(this.props.producto==='medicamento'){
    return(
    <div >
    <Form onSubmit={this.props.onAddEntrega}>
    <center>
    <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check"></i><span>Medicamento Entregado</span></Button>
    </center>
    </Form>  
    </div>
    )
  }
    else{
      return(
        <div >
        <Form onSubmit={this.props.onAddEntrega}>
        <center>
        <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check"></i><span>Insumo Entregado</span></Button>
        </center>
        </Form>  
     </div>
        )
    }
  }
      else{
        return (
          <div >
             <Form onSubmit={this.props.onAddEntrega}>
            <Table hover bordered striped responsive size="sm">
              <tr bgcolor="#ffffff">
            <td style={{maxWidth: '200px', backgroundColor: '#ffffff'}}>
            {this.state.list_lote.length>1 && 
            
            <Input 
              type="select" 
              name="lote" 
              id="lote" 
              value={this.state.lote}
              required
              onChange={e => { this.setState({ lote: e.target.value }) }}
      
              onClick={ this.handleChange.bind(this)}>
              {/* <option value="" >Lote..   </option> */}
                {this.state.list_lote.map(item1 => (
                    <option value={item1.lote.id_lote} >{item1.lote.fecha_expiracion+' ('+item1.lote.id_lote+')'}</option>
                    ))}
            </Input>
            }
            { this.state.list_lote.length===1 &&
            <Input 
            type="text  " 
            name="lote" 
            id="lote" 
            value={this.state.list_lote[0].lote.fecha_expiracion+' ('+this.state.list_lote[0].lote.id_lote+')'}>
            </Input>
          }
          </td>
          <td style={{maxWidth: '80px', backgroundColor: '#ffffff'}}>
          <Input  type="text"  value ={this.state.cantidadFarmacia} name="cantidad"  id="text-input" onChange={ this.handleChange.bind(this)}></Input>
          </td>
          <td style={{maxWidth: '95px', backgroundColor: '#ffffff'}}>               
            {this.state.showEntrega && 
              <center>
                <Button size="sm" className="btn-twitter btn-brand mr-1 mb-1" onClick={this.onValidacion}><i className="fa fa-hand-lizard-o"></i><span>Confirmar</span></Button>
              </center>
            }
            {this.state.showYaEntregado &&
              <center>
              <Button size="sm" className="btn-vimeo btn-brand mr-1 mb-1" disabled><i className="fa fa-check-square-o"></i><span>Entregado</span></Button>
              </center>
            }
        </td>
      </tr>
    </Table>
  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
    <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
      <ModalBody>
        <strong>{this.state.message}</strong>
        { this.state.showCantFarmacia && <Alert color="success">
          <strong>{"REALICE PEDIDO DE ALMACEN O REALICE PEDIDO DE SSU"}</strong>                  
        </Alert>}  
      </ModalBody>
    <ModalFooter>
      <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
    </ModalFooter>
  </Modal>

  <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
    <form onSubmit={this.props.onAddEntrega}>
    <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
      <ModalBody>
        <center>
        <Col md="9">
        <Input type="text" name="contar"  value={"   ¿Desea Entregar el Medicamento/Insumo?  "}></Input>
        </Col>
        </center>
        <strong>{"Codigo :  "} </strong>
        {this.props.codP+" "}
        <strong>{"Nombre :  "}</strong>
        {this.props.nomP}
        <br/>
        <strong>{"Numero Lote:  "}</strong>
        {this.state.lote}
        <br/>
        <strong>{"Cantidad Recetada:  "}</strong>
        {this.props.cantidadP}
        <br/>
        <center>
        <Col xs="12" md="10">
        <Alert color="success">
        <strong>{"Cantidad Existente en Farmacia: "}</strong>{this.state.cantidadFarmacia} <strong>{" Saldo: "}</strong>{ this.state.numSaldo}
        </Alert>
        </Col>
        </center>
      </ModalBody>
      <ModalFooter>           
        <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
</Form>  
</div>      
)}
}
}
export default EntregaMedicamento;








