import React, { Component } from 'react';
import { 
  Button, 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Collapse,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import EntregaMedicamento from './EntregaMedicamento'

const styles2 = { color: 'white', backgroundColor: '#154360', textAlign:'center' }

class ElegirRecetaF extends Component {
  constructor(props) {
    super(props)

    this.state = {
      
     
        url_receta: '#/Recetas/RecetasPorEntregar/ElegirReceta',
        
        list: [],
        list_insumo:[],
        list_otro:[],
        codP:'A0601',
        nomP:'',
        lote:'',
        list_lote:[],

        info_Lote:'',
        cantidadP:0,

        contarItems:0,

        primary:false,
        primary2:false,
        message:'',
        personalList:[],

        personal:'',

        id:0,
        collapse:true,
        contMedicamento_Noentregado:[],
        contInsumo_Noentregado:[],
        //DESDE AQUI LOGIN
        logged_in: localStorage.getItem('token') ? true : false,
        nombre:'',
        paterno:'',
        materno:'',
        ci:0,
        
    }
    console.log(this.props.match.params)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
    this.onValidacion = this.onValidacion.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.toggleDatos = this.toggleDatos.bind(this)
    this.onCancelar = this.onCancelar.bind(this)
    this.print = this.print.bind(this)
    this.print2 = this.print2.bind(this)
    this.print3 = this.print3.bind(this)
  }

  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }
  togglePrimary2() {
    this.setState({ primary2: !this.state.primary2});
  }
  toggleDatos() {
    this.setState({ collapse: !this.state.collapse });
  }

  onCancelar(){
    this.props.history.push('/RecetasEnFarmacia/RecetasPorEntregar')
  }

async componentDidMount() {
  // DESDE AQUI CON EL LOGIN
  if(this.state.logged_in){
    if(typeof(localStorage.getItem('token')) === 'undefined'){
      window.location.href = '#/Login'
    }else{
      const respuesta7 = await fetch('http://localhost:8000/core/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
        })
        const datosUsuario = await respuesta7.json();
        this.setState({
          username:datosUsuario.username
        });

        try{
          // alert(this.state.username)
          const respuesta7 = await fetch('http://localhost:8000/farmacia/pedir/v1/ListaPersonalusuario/'+this.state.username+'/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
            const datosUsuario = await respuesta7.json();
            this.setState({
              nombre:datosUsuario[0].nombres,
              paterno: datosUsuario[0].primer_apellido,
              materno: datosUsuario[0].segundo_apellido,
              ci: datosUsuario[0].ci,
            });
            // alert(this.state.ci)
        } catch (e) {
            console.log(e);
        }
    }

  }
  else{
    window.location.href = '#/Login'
  }
// HASTA AQUI LOGIN 
  fetch('http://localhost:8000/farmacia/recetar/v1/detalleMedicamentoRecetaBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
  console.log("FINDRESPONSE=",findresponse)
    this.setState({
      list: findresponse,
        url_receta : this.state.url_receta +'/'+ this.props.match.params.id_receta +'/'+ this.props.match.params.id_consulta +'/'+ this.props.match.params.codigoAfiliacion +'/'+this.props.match.params.cedulaIdentidadA+'/'+ this.props.match.nombresA +'/'+ this.props.match.params.apellidoPaternoA +'/'+ this.props.match.params.apellidoMaternoA+'/'+this.props.match.params.nombresM+'/'+this.props.match.params.apellidoPaternoM+'/' +this.props.match.params.fecha +'/resultados',
    })
  });

  fetch('http://localhost:8000/farmacia/recetar/v1/DetalleInsumoRecetaBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_insumo: findresponse
      })  
  });
    
  fetch('http://localhost:8000/farmacia/recetar/v1/OtroMedicamentoBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_otro: findresponse
      }) 

  });
    
  fetch('http://localhost:8000/farmacia/pedir/v1/PersonalBusca/'+1+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())  
  .then((findresponse) => {
    this.setState({
      personalList: findresponse
    })
  });    

  fetch('http://localhost:8000/farmacia/recetar/v1/MedicamentoRecetaBuscaNoEntregado/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
  console.log("FINDRESPONSE=",findresponse)
    this.setState({
      contMedicamento_Noentregado: findresponse,
    })
  });

  
  fetch('http://localhost:8000/farmacia/recetar/v1/InsumoRecetaBuscaNoEntregado/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
  console.log("FINDRESPONSE=",findresponse)
    this.setState({
      contInsumo_Noentregado: findresponse,
    })
  });

  
}


  handleOnAddReceta3 (event) {

    event.preventDefault();

   
    this.setState({
      
      loteA: event.target.lote.value,
      
      
    });
    console.log(this.state.loteA)
    
 
  }





handleOnAddLote (event) {       
  event.preventDefault();

  
    this.setState({
      info_Lote: event.target.cantidad.value,
     
    });    

  }

  handleOnAddEntrega (e) {   
    e.preventDefault();
    if(e.target.contar.value !==' '){
      this.setState({
        contarItems:this.state.contarItems+1
      })

    }
    console.log("ESTA ENTREGANDO : "+ e.target.contar.value)
    console.log("Cantidad de Recetas : "+ this.state.list.length)
    console.log("Contando Items : "+ this.state.contarItems)
    if(this.state.contarItems===this.state.list.length){
      console.log("AHORA SI")

    }
  }

  onValidacion(e){
  
    const num=this.state.contMedicamento_Noentregado.length+this.state.contInsumo_Noentregado.length
    console.log("total insumo y medicamentos: "+num)
    if(this.state.contarItems!==num){
      this.setState({     
        primary: !this.state.primary,
        message: 'Faltan medicamentos/insumos por entregar',
      })
    }

    if(this.state.contarItems===num){
      this.setState({     
        primary2: !this.state.primary2,
      })
    } 
}

  handleChange(value) {
    //console.log(value); 
       
        fetch('http://localhost:8000/farmacia/almacena/v1/LoteProductoBusca/'+value+'/',{
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          } 
        })
        .then((Response)=>Response.json())
        .then((findresponse) => {
          console.log("FINDRESPONSE=",findresponse)
            this.setState({
              list_lote: findresponse
              
            })
        })
      
      return 0
  }

onFormSubmit(e){

  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options)
   
  let urlTermina = ''
  let dataTermina = {}

  urlTermina = 'http://localhost:8000/farmacia/recetar/v1/RecetaEstadoEdita/'+this.props.match.params.id_receta+'/'
 
  dataTermina = {
    estado : 'Entregado',
    farmaceutica : this.state.ci,
    fecha_entrega:hoy,
    hora_entrega:hora,
  }

    try {
      fetch(urlTermina, {
      method: 'PUT',
      body: JSON.stringify(dataTermina),
      headers:{
          'Content-Type': 'application/json',
          Authorization: `JWT ${localStorage.getItem('token')}`
      }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 
    } catch (e) {
        console.log(e);
    }
    this.props.history.push('/RecetasEnFarmacia/RecetasPorEntregar') 
    this.setState({     
      primary2: !this.state.primary2,  
    })
}

print(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(155, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.props.match.params.id_receta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(155, 19, "VALIDEZ DE ESTA RECETA 48 HORAS");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombresA +" "+ this.props.match.params.apellidoPaternoA+" "+ this.props.match.params.apellidoMaternoA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoAfiliacion)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision)

var res = doc.autoTableHtmlToJson(document.getElementById("tablaT"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaTP"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp: true});

window.open(doc.output('bloburl'), '_blank');
}

print2(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(155, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.props.match.params.id_receta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(155, 19, "VALIDEZ DE ESTA RECETA 48 HORAS");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombresA +" "+ this.props.match.params.apellidoPaternoA+" "+ this.props.match.params.apellidoMaternoA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoAfiliacion)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision)

var res = doc.autoTableHtmlToJson(document.getElementById("tablaP"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaPP"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp: true});

window.open(doc.output('bloburl'), '_blank');
}

print3(){  
  /*Actual time and date */
  var options2 = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric'
  }
  var options3 = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
var d = new Date();
var showDate = d.toLocaleString('es-bo', options2) 
var showTime = d.toLocaleString('es-bo', options3) 
/*Actual time and date */
var doc = new jsPDF('p', 'mm', 'letter'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
var logo = new Image();
logo.src = 'assets/img/dashboard/icono_promes.png';
doc.addImage(logo, 'JPEG', 15, 7, 20,20);

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(10);
doc.text(70, 20, "\"PROMES\"", null, null, 'center');

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(158, 15,  "NUMERO DE RECETA:");

doc.setFontType("normal");
doc.setFontType("bold");  
doc.setFontSize(8);
doc.text(195, 15, " "+this.props.match.params.id_receta);

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(158, 19, "ADQUIRIR EN FARMACIA EXTERNA");

doc.setFont("helvetica");
doc.setFontType("bold");   //italic
doc.setFontSize(13);
doc.text(105, 28, "RECETARIO", null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(7);
doc.text(189,23, showDate +' '+ showTime, null, null, 'center');

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(15,36, "NOMBRES Y APELLIDOS:");

doc.setFontType("normal");
doc.text(60, 36, " "+this.props.match.params.nombresA +" "+ this.props.match.params.apellidoPaternoA+" "+ this.props.match.params.apellidoMaternoA)

doc.setFontType("bold");
doc.text(15, 41, "CODIGO AFILIACION:");

doc.setFontType("normal");
doc.text(60, 41, " "+ this.props.match.params.codigoAfiliacion)

doc.setFontType("bold");
doc.text(124, 41, "FECHA Y HORA DE EMISION:");

doc.setFontType("normal");
doc.text(171, 41, " "+ this.props.match.params.fecha_emision+" "+this.props.match.params.hora_emision)


var res = doc.autoTableHtmlToJson(document.getElementById("tablaO"));
var res2 = doc.autoTableHtmlToJson(document.getElementById("tablaOP"));
doc.autoTable(res.columns, res.data, {
  // Styling
  theme: 'striped', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 43, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.autoTable(res2.columns, res2.data, {
  // Styling
  theme: 'plain', // 'striped', 'grid' or 'plain'
  styles: {},
  headerStyles: {},
  bodyStyles: {fontSize: 8},
  alternateRowStyles: {},
  columnStyles: {},

  // Properties
  startY: false, // false (indicates margin top value) or a number
  margin: {top: 100, left: 15}, // a number, array or object
  pageBreak: 'auto', // 'auto', 'avoid' or 'always'
  tableWidth: 'auto', // 'auto', 'wrap' or a number, 
  showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
  tableLineColor: 200, // number, array (see color section below)
  tableLineWidth: 0,
});

doc.setFont("helvetica");
doc.setFontType("bold");
doc.setFontSize(9);

doc.text(15, 155, "--------------------------------------------");
doc.text(15, 159, "    SELLO Y FIRMA MEDICO");

doc.text(75, 155, "---------------------------------------------------");
doc.text(75, 159, "           FIRMA PACIENTE C.I.");

doc.text(144, 155, "-----------------------------------------------------");
doc.text(144, 159, "   SELLO Y FIRMA FARMACEUTICA");
this.setState({ showImp1: true});
window.open(doc.output('bloburl'), '_blank');  
}

render() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
  
  var options = {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  }
  var d = new Date();
  var hora = d.toLocaleString('es-bo', options)  /*Time to show in input*/
  const tableM=this.state.list.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"medicamento"}</td>
        <td>{u.codigo.producto_farmaceutico.codigo} </td>
        <td>{u.codigo.producto_farmaceutico.nombre}</td>
        <td>{u.codigo.t_forma_farmaceutica.descripcion}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableMP=this.state.list.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- medicamento: "+u.prescripcion}</td>
      </tr>
    )
  })
  const tableI=this.state.list_insumo.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"insumo"}</td>
        <td>{u.codigo.producto_farmaceutico.codigo} </td>
        <td>{u.codigo.producto_farmaceutico.nombre}</td>
        <td>{u.codigo.t_presentacion.descripcion}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableIP=this.state.list_insumo.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- insumo: "+u.prescripcion}</td>
      </tr>
    )
  })

  const tableO=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{"producto externo"}</td>
        <td>{"SN"} </td>
        <td>{u.nombre}</td>
        <td>{u.forma_farmaceutica}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableOP=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- producto externo: "+u.prescripcion}</td>
      </tr>
    )
  })

  const tableO2=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
        <td>{i+1}</td>
        <td>{u.nombre}</td>
        <td>{u.forma_farmaceutica}</td>
        <td>{u.cantidad}</td>
      </tr>
    )
  })
  const tableOP2=this.state.list_otro.map((u, i)=> {
    return (
      <tr key = {i} bgcolor=" #ebf5fb ">
     
        <td>{i+1+".- "+u.prescripcion}</td>
      </tr>
    )
  })
return (
<div>
  <Table responsive striped hidden id="tablaT">
    <thead>
      <tr>
        <th>No.</th>
        <th>TIPO</th>
        <th>CODIGO</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableM}
    {tableI}
    {tableO}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaTP">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableMP}
    {tableIP}
    {tableOP}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaP">
    <thead>
      <tr>
        <th>No.</th>
        <th>TIPO</th>
        <th>CODIGO</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableM}
    {tableI}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaPP">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableMP}
    {tableIP}
    </tbody>
  </Table>
  <Table responsive striped hidden id="tablaO">
    <thead>
      <tr>
        <th>No.</th>
        <th>NOMBRE PRODUCTO</th>
        <th>FORMA/PRESENTACION</th>
        <th>CANTIDAD</th>
      </tr>
    </thead>
    <tbody>
    {tableO2}
    </tbody>
  </Table>

  <Table responsive striped hidden id="tablaOP">
    <thead>
      <tr>
        <th>INDICACIONES</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    {tableOP2}
    </tbody>
  </Table>
  <Row>
    <Col xs="12" md="12">
    <Card>
      <Form>
        <CardHeader>
        <i className=" 	fa fa-drivers-license-o"></i><strong>Detalle Receta Por Entregar</strong>
        <Button onClick={this.print3} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Externa
        </Button>
        <Button  onClick={this.print2} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta Promes
        </Button>
        <Button  onClick={this.print} color="link" className="btn btn-sm btn-nfo float-right">
          <i className="fa fa-print"></i> Imprimir Receta
        </Button>
        </CardHeader>
        <CardBody>
        <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
        <Form >
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">NOMBRES - APELLIDOS</Label>
          </Col>
          <Col  md="4">
            <Input type="text" id="text-input" name="nombreCompleto" value={this.props.match.params.nombresA +"   "+ this.props.match.params.apellidoPaternoA+"   "+ this.props.match.params.apellidoMaternoA} />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Consulta</Label>                     
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_consulta" value={this.props.match.params.id_consulta} disabled />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Receta</Label>
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_recetaA" value={this.props.match.params.id_receta} disabled />
          </Col>  
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Codigo de Afiliado</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="codigo" name="codigoAlifiacion" value={this.props.match.params.codigoAfiliacion} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="date" id="fecha_e" name="fecha_e" value={this.props.match.params.fecha_emision} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora_e" name="hora_e" value={this.props.match.params.hora_emision} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Prescrito por  Medico</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="medico"  value={"Dr. "+this.props.match.params.nombresM+" "+this.props.match.params.apellidoPaternoM} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Entrega</Label>
          </Col>
          <Col md="2">
          <Input type="date" id="fecha" name="fecha" className="form-control" defaultValue={hoy} disabled ref={fecha => this.fecha = fecha}/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Entrega</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora" name="hora" className="form-control"  defaultValue={hora} disabled ref= {hora => this.hora = hora}/>
          </Col>
        </FormGroup>
        <FormGroup row>        
          <Col md="2">
          <Label htmlFor="text-input">Receta Entregada Por</Label>
          </Col> 
          <Col md="4">
          <Input 
            type="text" 
            name="farmaceutica" 
            id="text" 
            required
            value={"Dr. "+this.state.nombre +" "+ this.state.paterno+ " "+ this.state.materno}
            onChange={e => this.setState({ personal: e.target.value })}>                                         
            {/* <option value="">Personal...</option>
              {this.state.personalList.map(item1 => (
                <option value={item1.ci}>{item1.nombre+" "+item1.apellidos}</option>
              ))} */}
          </Input>
          </Col>           
        </FormGroup>
        </Form>
          </Collapse>
          <Button size="sm" color="secondary" onClick={this.toggleDatos} style={{ marginBottom: '1rem' }}><i className="icon-note"></i><span> Datos Receta</span></Button>
            <Table hover bordered striped responsive size="sm">
              <thead  style={styles2} >
              <tr>
                <th><p></p>Nro.<p></p></th>
                <th><p></p>Codigo<p></p></th>
                <th><p></p>Nombre Producto<p></p></th> 
                <th><p></p>Forma/Pres.<p></p></th>
                <th><p></p>Cantidad<p></p></th>                         
                <th ><p></p>Prescripción(Indicación)<p></p></th> 
                <th bgcolor="#2874a6"><p></p>Acciones<p></p></th>                                 
              </tr>
              </thead>
            <tbody>
              {this.state.list.map((u, i)=> {   
                return (
                  <tr key = {i} bgcolor="#ebf5fb">
                    <td style={{maxWidth: '2px', backgroundColor: '#ebf5fb', textAlign: 'center'}}>{i+1}</td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}}   >{u.codigo.producto_farmaceutico.codigo} </td>
                    <td style={{maxWidth: '100px', backgroundColor: '#ebf5fb'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.codigo.t_forma_farmaceutica.descripcion} </td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb', textAlign: 'center'}} >{u.cantidad} </td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ebf5fb'}} >{u.prescripcion} </td>
                    <td style={{maxWidth: '500px', backgroundColor: '#ffffff'}}>                
                    <EntregaMedicamento id={u.id} codP={u.codigo.producto_farmaceutico.codigo} cantidadP={u.cantidad} nomP={u.codigo.producto_farmaceutico.nombre} entregadom={u.entregado} producto={'medicamento'} onAddEntrega={this.handleOnAddEntrega.bind(this)}/> 
                    </td>
                  </tr>
                )
              })}
                {this.state.list_insumo.map((u, i)=> {
                return (
                  <tr key = {i} bgcolor="#f2f4f4">
                      <td style={{maxWidth: '2px', backgroundColor: '#f2f4f4',textAlign: 'center'}}>{i+1}</td>
                      <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                      <td style={{maxWidth: '100px', backgroundColor: '#f2f4f4'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                      <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.codigo.t_presentacion.descripcion} </td>
                      <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4', textAlign: 'center'}} >{u.cantidad} </td>
                      <td style={{maxWidth: '50px', backgroundColor: '#f2f4f4'}} >{u.prescripcion} </td> 
                      <td style={{maxWidth: '500px', backgroundColor: '#ffffff'}}>                
                      <EntregaMedicamento id={u.id} codP={u.codigo.producto_farmaceutico.codigo} cantidadP={u.cantidad} nomP={u.codigo.producto_farmaceutico.nombre} entregadoi={u.entregado} producto={'insumo'} onAddEntrega={this.handleOnAddEntrega.bind(this)}/> 
                    </td>
                    </tr>
                        )
              })}
              {this.state.list_otro.map((u, i)=> {
                return (
                  <tr key = {i} bgcolor="#ffffff">
                    <td style={{maxWidth: '2px', backgroundColor: '#ffffff', textAlign: 'center'}}>{i+1}</td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{"SN"} </td>
                    <td style={{maxWidth: '100px', backgroundColor: '#ffffff'}} >{u.nombre} </td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.forma_farmaceutica} </td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}} >{u.cantidad} </td>
                    <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.prescripcion} </td>
                    <td style={{maxWidth: '500px', backgroundColor: '#ffffff', textAlign: 'center'}}>
                    {"Externo"}                
                    </td>
                  </tr>
                )
              })}
            </tbody>
            </Table>     
          {/* FIN DE REGISTRO */}
            <center>
            <Button onClick={this.onValidacion} className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-check-circle"></i><span>Terminar Entrega</span></Button>
            <Button onClick={this.onCancelar} className="btn-google-plus btn-brand mr-1 mb-1"><i className="fa fa-times-circle"></i><span>Cancelar Entrega</span></Button>
            </center>
          {/* FIN DE REGISTRO */}
        </CardBody>
      </Form>
    </Card>
    </Col>

  <Modal isOpen={this.state.primary} toggle={this.togglePrimary} className={'modal-warning ' + this.props.className}>
        <ModalHeader toggle={this.togglePrimary}>Advertencia</ModalHeader>
          <ModalBody>
            <strong>{this.state.message}</strong>
       
          </ModalBody>
        <ModalFooter>
          <Button color="warning"  onClick={this.togglePrimary}>Aceptar</Button>
        </ModalFooter>
  </Modal>
  
  <Form>
  <Modal isOpen={this.state.primary2} toggle={this.togglePrimary2} className={'modal-primary ' + this.props.className}>
    <form onSubmit={this.props.onAddEntrega}>
    <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
      
      <ModalBody>
        ¿Desea registrar los datos?
      </ModalBody>
      <ModalFooter>           
        <Button type="submit" onClick={this.onFormSubmit}  color="primary">Si</Button>{' '}
        <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
      </ModalFooter>
      </form>
  </Modal>
  </Form>
</Row>

</div>
    )
  }
}

export default ElegirRecetaF;