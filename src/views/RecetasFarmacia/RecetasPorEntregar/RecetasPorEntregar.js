import React, { Component } from 'react';
import { 
  Button, 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row, 
  FormGroup, 
  Input,
  Alert,
} from 'reactstrap';

import Search from 'react-search-box'
import ListaPorEntregar from '../ListaPorEntregar'

class RecetasPorEntregar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, 
      lista_receta: [],
      afiliados:[],
      lista_receta_fecha:[],
      lista_afiliado:[],
      lista_receta_hoy:[],
      lista_receta_todo:[],
      estadoR:'',
      fecha:'',
      fecha_hoy:'',
      showB:false,
      showA:false,
      showC:false,
      showD:false,
      showE:false,
      showNo:false,
      codigoAfiliado:'',
      message:'',
  }
    this.BuscaFecha = this.BuscaFecha.bind(this)
    this.BuscaHoy = this.BuscaHoy.bind(this)
    this.BuscaTodo = this.BuscaTodo.bind(this)
  }

async componentDidMount() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
      dd='0'+dd
    } 
    if(mm<10) {
      mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd
  this.setState({
    fecha_hoy: hoy,
    fecha:hoy,
  })
  const respuesta1 = await fetch('http://localhost:8000/farmacia/recetar/v1/detalleafiliados/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  });
  const afiliados = await respuesta1.json();
  const respuesta2 = await fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregarFecha/'+hoy+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  });
  const lista_receta = await respuesta2.json();
  
  if(lista_receta.length>0){
    this.setState({
      showA:true,
    })
  }else{
    this.setState({
      showNo:true,
      message:"No existen Recetas Por Entregar Hoy ("+hoy+")",
    })
  }
  this.setState({
    lista_receta,
    afiliados,
  });
}

BuscaAfiliado(value) {
  this.state.afiliados.map((item)=>{
  if (value === item.codAfiliado){    
  fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregarAfiliado/'+item.codAfiliado+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_afiliado:findresponse,
        codigoAfiliado:item.codAfiliado,
        showB:false,
        showA:false,
        showD:false,
        showC:true,
        showE:false,
        showNo:false,
        message:'',
      })
    }
    else{
      this.setState({
        showNo:true,
        showB:false,
        showA:false,
        showD:false,
        showC:false,
        showE:false,
        codigoAfiliado:value,
        message: value+" No tiene Recetas Por Entregar",
      })
    }
  });
  }
  return 0
  })
}

BuscaFecha(){
  fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregarFecha/'+this.state.fecha+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_receta_fecha:findresponse,
        showB:true,
        showA:false,
        showC:false,
        showD:false,
        showE:false,
        showNo:false,
      })
    }
    else{
      this.setState({
        showNo:true,
        showB:false,
        showA:false,
        showD:false,
        showC:false,
        showE:false,
        message:'En fecha '+this.state.fecha+' No existen Recetas Por Entregar',
      })
    }
  })
}

BuscaHoy(){
  fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregarFecha/'+this.state.fecha_hoy+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_receta_hoy:findresponse,
        showB:false,
        showA:false,
        showC:false,
        showD:true,
        showE:false,
        showNo:false,
      })
    }else{
      this.setState({
        showNo:true,
        showB:false,
        showA:false,
        showC:false,
        showD:false,
        showE:false,
        message:"No existen Recetas Por Entregar Hoy ("+this.state.fecha_hoy+")",
      })
    }
  })
}

BuscaTodo(){
  fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregar/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    if(findresponse.length>0){
      this.setState({
        lista_receta_todo:findresponse,
        showB:false,
        showA:false,
        showC:false,
        showD:false,
        showE:true,
        showNo:false,
      })
    }else{
      this.setState({
        lista_receta_todo:findresponse,
        showB:false,
        showA:false,
        showC:false,
        showD:false,
        showE:false,
        showNo:true,
        message:'No existe ninguna Receta Por Entregar'
      })
    }
  })
}

render() {
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1; //hoy es 0!
  var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    } 
    if(mm<10) {
        mm='0'+mm
    } 
  hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

  if (this.state.loading) {
    return (
      <div className="">Esperando conexion...</div>
    );
  }
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardHeader>
            <h3><i className="fa fa-th-list"></i> <strong>Recetas Por Entregar</strong></h3>
            </CardHeader>
            <CardBody>            
              <form>
                <FormGroup row>
                  <Col  md="3">
                  <Search                       
                  data={this.state.afiliados}
                  onChange={ this.BuscaAfiliado.bind(this) }
                  placeholder="Ingrese Codigo de Afiliado"
                  class="search-class"
                  type="string"
                  searchKey="codAfiliado"
                  />
                  </Col>
                  <Col xs="12" md="3">
                  <Input onChange={e => { this.setState({ fecha: e.target.value }) }} defaultValue={hoy} type="date" id="date-input" name="date-input" placeholder="date" />
                  </Col>
                  <Col xs="12" md="2">
                  <h3>
                  <Button onClick={ this.BuscaFecha}  size="sm" className="btn-twitter btn-brand mr-1 mb-1"><i className=" 	fa fa-search"></i><span>Buscar por Fecha</span></Button>
                  </h3>
                  </Col>                  
                  <Col  md="2">
                  <h3>
                  <Button onClick={ this.BuscaHoy}  size="sm" className="btn-facebook btn-brand mr-1 mb-1"><i className="fa fa-calendar-check-o"></i><span>Recetas del día de Hoy</span></Button>
                  </h3>
                  </Col>
                  <Col  md="2">
                  <h3>
                  <Button onClick={ this.BuscaTodo} size="sm" className="btn-xing btn-brand mr-1 mb-1"><i className="fa fa-copy"></i><span>Ver Todas la recetas</span></Button>
                  </h3>
                  </Col>
                </FormGroup>
              </form>
                {this.state.showA && <ListaPorEntregar lista_receta={this.state.lista_receta} titulo={'Recetas Por Entregar el dia de Hoy: '+this.state.fecha_hoy}/>}
                {this.state.showB && <ListaPorEntregar lista_receta={this.state.lista_receta_fecha} titulo={'Recetas Por Entregar en fecha: '+this.state.fecha}/>}
                {this.state.showC && <ListaPorEntregar lista_receta={this.state.lista_afiliado} titulo={'Receta Por Entregar al afiliado: '+this.state.codigoAfiliado}/>}
                {this.state.showD && <ListaPorEntregar lista_receta={this.state.lista_receta_hoy} titulo={'Recetas Por Entregar el dia de Hoy: '+this.state.fecha_hoy}/>}
                {this.state.showE && <ListaPorEntregar lista_receta={this.state.lista_receta_todo} titulo={'Recetas Por Entregar'}/>}
                {this.state.showNo && 
                  <Row>
                    <Col md="3"></Col>
                    <Col md="5">
                      <Alert color="danger">
                      <center>
                        <strong>{this.state.message}</strong>
                        </center>
                      </Alert>
                    </Col>
                  </Row>
                }
            </CardBody>
          </Card>
        </Col>
      </Row>            
    </div>
  );
}
}

export default RecetasPorEntregar;
