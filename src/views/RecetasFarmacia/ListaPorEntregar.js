import React, { Component } from 'react';
import { 
  Button, 
  NavLink, 
  Table,
  Badge,
} from 'reactstrap';

import ValidarEstado from './ValidarEstado'
let url_receta = '#/RecetasEnFarmacia/RecetasPorEntregar/ElegirReceta'

const tdStyle = {
  fontSize: '0.9em'
}

const trNavLink = {
  padding: '0',
  margin: '0'
}

class RecetasLista extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false, 
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="">Esperando conexion...</div>
      );
    }
    const recetas = this.props.lista_receta.map((item, indice)=>{
      return(
        <tr key={item.indice}>
        <td id={indice} style={tdStyle}>
          {indice+1}
        </td>
        <td id={indice} style={tdStyle}>
          {item.id_receta}
        </td>
        <td id={indice} style={tdStyle}>
        {item.consulta.afiliado.ci.nombres+"   "+ item.consulta.afiliado.ci.primer_apellido+"   "+ item.consulta.afiliado.ci.segundo_apellido  } 
        </td>
        <td id={indice} style={tdStyle}>
          {"Dr. "+item.consulta.medico.nombres+" "+item.consulta.medico.primer_apellido +" " + item.consulta.medico.segundo_apellido }
        </td>
        <td id={indice} style={tdStyle}>
          {item.fecha_emision}
        </td>
        <ValidarEstado estadoR={item.estado}/>
          <td id={indice} style={{textAlign: 'center'}}>
            <NavLink style={trNavLink} href = {url_receta +'/'+ item.id_receta +'/'+ item.consulta.id_consulta +'/'+ item.consulta.afiliado.codAfiliado +'/'+ item.consulta.afiliado.ci.nombres +'/'+ item.consulta.afiliado.ci.primer_apellido +'/'+ item.consulta.afiliado.ci.segundo_apellido +'/'+ item.consulta.medico.nombres+'/'+(item.consulta.medico.primer_apellido+" "+item.consulta.medico.segundo_apellido)+'/'+item.fecha_emision+'/'+item.hora_emision+'/'+item.farmaceutica.nombres+'/'+(item.farmaceutica.paterno+" "+item.farmaceutica.materno)+'/'+item.fecha_entrega+'/'+item.hora_entrega } >
            <Button size="sm" className="btn-twitter btn-brand mr-1 mb-1"><i className="fa fa-hand-lizard-o"></i><span>Entregar</span></Button>
            </NavLink>
          </td >

        </tr>
      )
    })
   
  return (
    <div className="animated fadeIn">
      <Badge className="mr-1" color="light"><h6><i className="fa fa-align-justify"></i>{' '+this.props.titulo}</h6></Badge>      
      <Table responsive>
        <thead>
          <tr>
            <th>Nro.</th>
            <th>ID Receta</th>
            <th>Nombres y Apellidos</th>
            <th>Medico</th>
            <th>Fecha Emisión</th>
            <th>Estado</th>
            <th><center>Acciones</center></th>
          </tr>
          </thead>
          <tbody>
            {recetas}
          </tbody>
      </Table>
    </div>
  );
  }
}
export default RecetasLista;
