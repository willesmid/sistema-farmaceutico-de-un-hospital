import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Row,
  Input,
  FormGroup, 
  InputGroup,
  InputGroupAddon,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  NavLink,
  FormText,
  Label,
} from 'reactstrap';
import ValidarEstado from '../ValidarEstado'
import Vencio from '../Vencio'
let url_receta = '#/RecetasEnFarmacia/RecetasAnuladas/AnularReceta/ElegirReceta'
const tdStyle = {
  fontSize: '0.9em'
}

const trNavLink = {
  padding: '0',
  margin: '0'
}
const styles2 = { color: 'white', backgroundColor: 'white' }
const styles3 = { color: '#f33c29' }

class AnularReceta extends Component {
  constructor(props) {
    super(props)
    this.activitiesRefs = [];
    this.activitiesRefs2 = true;
    this.state = {
      showCancelar:true,
      listapre: [],
      valor: '',
      checked2:false,
      checked:false,
      lista:'',
      showA:false,
      showB:false,
      showAD:true,
      showBD:true,
      medicamentos:[],
      primary:false,
      primary2:false,
      showObservacion:false,
      observacion:'',
      
    }
    this.onCancelar = this.onCancelar.bind(this)
    this.buscarValor = this.buscarValor.bind(this) 
  

    this.Anular = this.Anular.bind(this)
    this.Listar=this.Listar.bind(this)
    this.togglePrimary = this.togglePrimary.bind(this)
    this.togglePrimary2 = this.togglePrimary2.bind(this)
  }
  togglePrimary() {
    this.setState({ primary: !this.state.primary});
  }

  togglePrimary2() {
    this.setState({ 
      primary2: !this.state.primary2,
      showObservacion:false,
      observacion:'',
    });
  }
  

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregar/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const listapre = await respuesta.json();
      this.setState({
        listapre
      });
    } catch (e) {
      console.log(e);
    }
  }

  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  onCancelar(){
    this.props.history.push('/RecetasEnFarmacia/RecetasAnuladas') 
  }

onMarcarA(){
  var check = this.activitiesRefs2.checked
  console.log("por que no dara " + this.activitiesRefs2.checked)
  console.log("por que no daraasds " + check)
  if(this.activitiesRefs2.checked){
    
    for (let i = 0; i < this.activitiesRefs.length; i++) {
      if(this.activitiesRefs[i]!==null){
      this.activitiesRefs[i].checked=true
      console.log("y que es" + check)
    }else{
      
    }
  }
  
  }else{

    for (let i = 0; i < this.activitiesRefs.length; i++) {
      if(this.activitiesRefs[i]!==null){
      this.activitiesRefs[i].checked=false
      console.log("y que es" + check)
    }
    }
  }


}
onCheck(name, val) {
  const checkboxes = Object.assign({},this.state.checkboxes, {});
  for (let key in checkboxes) {
    checkboxes[key] = false;
  }
  checkboxes[name] = true;
  checkboxes.selected = val;
  this.setState({ checkboxes });
}


Anular(){
  if(this.state.observacion!==''){
  var productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
    if (check) productos = productos.concat([this.activitiesRefs[i].value])
    }
  }
  this.setState({
    medicamentos: this.state.medicamentos.concat([productos]),
  }); 
  for (let i = 0; i < productos.length; i++) {
  let url = ''
  let data = {}
  url = 'http://localhost:8000/farmacia/recetar/v1/RecetaEstadoEdita2/'+productos[i]+'/'
  data = {
    estado:"Anulada",
    observacion: this.state.observacion
  }
  try {
    fetch(url, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers:{
      'Content-Type': 'application/json',
      Authorization: `JWT ${localStorage.getItem('token')}`
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response)
      if(i===productos.length-1){
        window.location.reload()
      }   
  }); 
  } catch (e) {
      console.log(e);
  } 
  }  
  this.setState({
    primary2:false,
  })
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    this.activitiesRefs[i].checked=false
    console.log("y que es" + check)
  }
}
  }
  if(this.state.observacion===''){
    this.setState({
      showObservacion:true,
    })
  }
  



}
calcDays(fv){
  /*calcula los dias que hay entre dos fechas */
  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  var firstDate = new Date(Date.parse(fv));
  var secondDate = new Date();  //hoy
  var diffDays = Math.round((firstDate.getTime() - secondDate.getTime())/(oneDay));
  var abs =Math.abs(diffDays)
  console.log(abs)

  return abs
}

Listar(){
  if(this.state.lista==="VALIDOS"){
    var Validos=[]
    fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregar/',{
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      } 
    })
    .then((Response)=>Response.json())
    .then((findresponse) => {
    findresponse.map((item)=>{
      const dias = this.calcDays(item.fecha_emision)
      if(dias <= 2){
        Validos=Validos.concat(item)
      }
      return null
    })
    this.setState({
      listapre: Validos,
      showB:true,
      showBD:false,
    })
    })
  }else{
    if(this.state.lista==="NO VALIDOS"){
      var NoValidos=[]
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregar/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
      findresponse.map((item)=>{
        const dias = this.calcDays(item.fecha_emision)
        if(dias > 2){
          NoValidos=NoValidos.concat(item)
        }
        return null
      })
      this.setState({
        listapre: NoValidos,

        showB:true,
        showBD:false,
      })
      })

    }else{
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetasPorEntregar/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse) => {
       //   console.log("FINDRESPONSE=",findresponse)
        this.setState({
          listapre: findresponse,
          showB:false,
          showBD: true,
        })
      })

    }
  }
}

render() {
  let items_filtrados = this.state.listapre.filter(
    (item) => {
      return (item.id_receta+' '+item.consulta.afiliado.ci.nombres +' '+item.consulta.afiliado.ci.apellido_p+' '+item.consulta.afiliado.ci.apellido_m)
        .toUpperCase().indexOf(
          this.state.valor.toUpperCase()) !== -1;
    }
  );
  
  var productos = []
  for (let i = 0; i < this.activitiesRefs.length; i++) {
    if(this.activitiesRefs[i]!==null){
    var check = this.activitiesRefs[i].checked;
    if (check) productos = productos.concat([this.activitiesRefs[i].value])
    }
  }

return (
  <div className="animated fadeIn">
  <Row>
    <Col xs="12" sm="12">
      <Card>
        <CardHeader>
          <h3><i className=" 	fa fa-toggle-on"></i><strong> Anular Receta</strong></h3>
        </CardHeader>
        <CardBody>         
          <Col xs="12" md="12">
            <FormGroup row>
              <Col xs="12" md="5">
                <InputGroup>
                <InputGroupAddon addonType="prepend">
                </InputGroupAddon>
                <Input type="text" id="nombre" name="nombre" placeholder="Ingrese Codigo o Nombre" value={this.state.valor} onChange={this.buscarValor}/>
                <InputGroupAddon addonType="append">
                <Button type="button" color="primary" disabled><i className="fa fa-search"></i></Button>
                </InputGroupAddon>
                </InputGroup>
              </Col>
              <Col xs="12" md="5">
              <Input 
              type="select" 
              name="unidadProd" 
              id="unidadProd" 
              value={this.state.lista}
             
              onChange={e => { this.setState({ lista: e.target.value }) }}
              onClick={this.Listar}
              >
              <option value="" required>Estado de Medicamento...</option>
                <option value="VALIDOS">VALIDOS</option>
                <option value="NO VALIDOS">NO VALIDOS</option>
     
            </Input> 
              </Col>
    
              <Col xs="12" md="2">
              {this.state.showBD &&
              <h3>
              <Button size="sm" className="btn-youtube btn-brand mr-1 mb-1" disabled><i className="fa fa-trash-o"></i><span>Anular Receta</span></Button>                 
              </h3>
              }
              {this.state.showB &&
              <h3>
              <Button onClick={this.togglePrimary2} size="sm" className="btn-youtube btn-brand mr-1 mb-1"><i className="fa fa-trash-o"></i><span>Anular Receta</span></Button>                 
              </h3>
              }
              </Col>
         
            </FormGroup>
          </Col>
         
            <Table hover responsive size="sm">
              <thead>
                <tr>   
                  <th>Nro.</th>
                  <th>ID Receta</th>
                  <th>Nombres y Apellidos</th>
                  <th>Medico</th>
                  <th>F/Emision</th>
                  <th>H/Emision</th>
                  <th>Estado</th>  
                  <th>Validez</th>   
                  <th>Acciones</th>
                  <th style={styles2}>    
                  <center>
                    <input className="form-check-input" type="checkbox" value={0} ref={ref => this.activitiesRefs2 = ref} onClick={this.onMarcarA.bind(this)} />                         
                    .
                  </center>
                  </th>
                </tr>
              </thead>
              <tbody>
                {items_filtrados.map((item, indice) => (
                
                <tr key={indice}>

                      <td id={indice} style={tdStyle}>
                        {indice+1}
                      </td>
                      <td id={indice} style={tdStyle}>
                        {item.id_receta}
                      </td>
                      <td id={indice} style={tdStyle}>
                      {item.consulta.afiliado.ci.nombres+"   "+ item.consulta.afiliado.ci.primer_apellido+"   "+ item.consulta.afiliado.ci.segundo_apellido  } 
                      </td>
                      <td id={indice} style={tdStyle}>
                        {"Dr. "+item.consulta.medico.nombres+" "+item.consulta.medico.primer_apellido+" "+item.consulta.medico.segundo_apellido}
                      </td>
                      <td id={indice} style={tdStyle}>
                        {item.fecha_emision}
                      </td>

                      <td id={indice} style={tdStyle}>
                        {item.hora_emision}
                      </td>
                      <ValidarEstado estadoR={item.estado}/>
                      <Vencio fv={item.fecha_emision}/>
                      <td id={indice} style={{textAlign: 'center'}}>
                      <NavLink style={trNavLink} href = {url_receta +'/'+ item.id_receta +'/'+ item.consulta.id_consulta +'/'+ item.consulta.afiliado.codAfiliado +'/'+ item.consulta.afiliado.ci.nombres +'/'+ item.consulta.afiliado.ci.primer_apellido +'/'+ item.consulta.afiliado.ci.segundo_apellido +'/'+ item.consulta.medico.nombres+'/'+(item.consulta.medico.primer_apellido+" "+item.consulta.medico.segundo_apellido)+'/'+item.fecha_emision+'/'+item.hora_emision+'/'+item.farmaceutica.nombres+'/'+(item.farmaceutica.primer_apellido+" "+item.farmaceutica.segundo_apellido)+'/'+item.fecha_entrega+'/'+item.hora_entrega} >             
                        <Button size="sm" color="warning" className="btn-brand mr-1 mb-1" ><i className="fa fa-eye"></i><span>Ver</span></Button>
                      </NavLink>
                      </td >
                  <td>
                    <FormGroup check inline key={indice}>
                    <input className="form-check-input" type="checkbox" value={item.id_receta} ref={ref => this.activitiesRefs[indice] = ref} />
                    </FormGroup>
                  </td>
                </tr>
                ))}
              </tbody>
            </Table>  
            
        </CardBody>

        <CardFooter>
          <h3 align="right">
          <Button   type="reset" size="sm" color="danger" onClick={this.onCancelar}><i className="fa fa-ban"></i> Cancelar</Button>
          </h3>

        </CardFooter>
      </Card>  
    </Col>
  </Row>

  {/* MODAL ANULAR */}
  <Modal isOpen={this.state.primary2} className={'modal-primary ' + this.props.className}>
  <ModalHeader toggle={this.togglePrimary2}>Confirmación</ModalHeader>
    <ModalBody>
    <center>
    ¿Desea Anular la/las recetas?    
      <FormGroup row>
        <Col xs="12" md="2">
        <Label htmlFor="textarea-input">Observación</Label>
        </Col>
          <Col xs="12" md="10">
            <Input type="textarea" rows="1" placeholder="Plazo vencido" className="form-control" maxlength="50"
             defaultValue={this.state.observacion}
              onChange={e => this.setState({ observacion: e.target.value })} 
            />
            {
            this.state.showObservacion &&
            <FormText className="help-block" color= "#ca1125" style={styles3}>Escriba Observación</FormText>
            }
          </Col>            
      </FormGroup>       
      <Col sm="6">
      <Table  responsive size="sm">
      <thead>
        <tr>
          <th>Nro.</th>
          <th>Código</th>
        </tr>
      </thead>
      <tbody>
      {productos.map((item, indice) => ( 
        <tr key={indice}>
          <td>{indice + 1}</td>
          <td>{item}</td>
        </tr>
      ))}
      </tbody>
      </Table>
      </Col>
      </center>
    </ModalBody>
    <ModalFooter>
      <Button onClick={this.Anular} type="submit" color="primary">Si</Button>{' '}
      <Button color="secondary" onClick={this.togglePrimary2}>No</Button>
    </ModalFooter>
  </Modal>
  {/* FIN MODAL ANULAR*/}
</div>
);
}
}

export default AnularReceta;

    




