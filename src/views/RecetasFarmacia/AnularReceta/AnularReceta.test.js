import React from 'react';
import ReactDOM from 'react-dom';
import AnularReceta from './AnularReceta';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AnularReceta />, div);
  ReactDOM.unmountComponentAtNode(div);
});
