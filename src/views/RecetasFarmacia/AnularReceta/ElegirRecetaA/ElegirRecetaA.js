import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Input, 
  Label, 
  Row, 
  Table,
  Form,
  FormGroup
} from 'reactstrap';
const styles2 = { color: 'white', backgroundColor: '#7d6608', textAlign:'center' }
class ElegirRecetaA extends Component {
  constructor(props) {
    super(props)
    this.state = {
        url_receta: '#/RecetasEnConsultorio/RecetasEmitidas/ElegirReceta',
        list: [],
        list_insumo:[],
        list_otro:[],    
    }
    console.log(this.props.match.params)
  }

componentDidMount() {
  fetch('http://localhost:8000/farmacia/recetar/v1/detalleMedicamentoRecetaBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list: findresponse,
          url_receta : this.state.url_receta +'/'+ this.props.match.params.id_receta +'/'+ this.props.match.params.id_consulta +'/'+ this.props.match.params.codigoAfiliacion +'/'+this.props.match.params.cedulaIdentidadA+'/'+ this.props.match.nombresA +'/'+ this.props.match.params.apellidoPaternoA +'/'+ this.props.match.params.apellidoMaternoA+'/'+this.props.match.params.nombresM+'/'+this.props.match.params.apellidoPaternoM+'/' +this.props.match.params.fecha +'/resultados',
      })
      console.log("holasss")
      if (this.state.list.length===0) {
        this.setState({
          show: false,
          show2: true
        })
      }
      else{
        this.setState({
          show: true,
          show2: false
        })
        //document.write("SI hay "+ this.state.list.length);
      } 
  });

  
  fetch('http://localhost:8000/farmacia/recetar/v1/DetalleInsumoRecetaBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_insumo: findresponse
      })  
  });

  fetch('http://localhost:8000/farmacia/recetar/v1/OtroMedicamentoBusca/'+this.props.match.params.id_receta+'/',{
    headers: {
      Authorization: `JWT ${localStorage.getItem('token')}`
    } 
  })
  .then((Response)=>Response.json())
  .then((findresponse) => {
    console.log("FINDRESPONSE=",findresponse)
      this.setState({
        list_otro: findresponse
      })
      console.log(this.state.list_otro)
      console.log("holasss")    
      // console.log('LIST=',this.state.list.length)   
      if (this.state.list.length===0) {
        this.setState({
          show: false,
          show2: true
        })
        // document.write("No hay " + this.state.list.length);
      }
      else{
        this.setState({
          show: true,
          show2: false
        })
        //document.write("SI hay "+ this.state.list.length);
      }
  });
  
}


  render() {
    return (
      <div>
<Row>
  <Col xs="12" md="12">
   <Card>
      <CardHeader>
      <i className=" 	fa fa-drivers-license-o"></i><strong>Detalle Receta Anular</strong>
      </CardHeader>
      <CardBody>
        <Form >
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">NOMBRES - APELLIDOS</Label>
          </Col>
          <Col  md="4">
            <Input type="text" id="text-input" name="nombreCompleto" value={this.props.match.params.nombresA +"   "+ this.props.match.params.apellidoPaternoA+"   "+ this.props.match.params.apellidoMaternoA} />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Consulta</Label>                     
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_consulta" value={this.props.match.params.id_consulta} disabled />
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">N°Receta</Label>
          </Col>
          <Col md="2">
          <Input type="text" id="text-input" name="id_recetaA" value={this.props.match.params.id_receta} disabled />
          </Col>  
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Codigo de Afiliado</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="codigo" name="codigoAlifiacion" value={this.props.match.params.codigoAfiliacion} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="date" id="fecha_e" name="fecha_e" value={this.props.match.params.fecha_emision} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Emisión</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora_e" name="hora_e" value={this.props.match.params.hora_emision} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col md="2">
          <Label htmlFor="text-input">Prescrito por  Medico</Label>
          </Col> 
          <Col xs="12" md="4" >
          <Input type="text" id="medico"  value={"Dr. "+this.props.match.params.nombresM+" "+this.props.match.params.apellidoPaternoM} disabled/>
          </Col>     
          <Col md="1">
          <Label htmlFor="text-input">F/Entrega</Label>
          </Col>
          <Col md="2">
          <Input type="date" id="fecha" name="fecha" value={this.props.match.params.fecha_entrega} disabled/>
          </Col>
          <Col md="1">
          <Label htmlFor="text-input">H/Entrega</Label>
          </Col>
          <Col  md="2">
          <Input type="time" id="hora" name="hora" value={this.props.match.params.hora_entrega} disabled/>
          </Col>
        </FormGroup>
        <FormGroup row>        
          <Col md="2">
          <Label htmlFor="text-input">Receta Entregada Por</Label>
          </Col> 
          <Col md="4">
          <Input type="text" id="farmaceutica" name="farmaceutica" value={'Dr. '+this.props.match.params.farmaceuticaN+' '+ this.props.match.params.farmaceuticaA} disabled/>
          </Col>
        </FormGroup>
        </Form>

        <Table hover bordered striped responsive size="sm">
        <thead  style={styles2} >
        <tr>
          <th><p></p>Numero<p></p></th>
          <th><p></p>Codigo<p></p></th>
          <th><p></p>Nombre Producto<p></p></th> 
          <th><p></p>Forma/Presentacion<p></p></th>
          <th><p></p>Cantidad<p></p></th>                         
          <th ><p></p>Prescripción (Indicaciones)<p></p></th> 
          <th><p></p>Tipo de producto<p></p></th>                                 
        </tr>
        </thead>
         <tbody>
           {this.state.list.map((u, i)=> {
            return (
              <tr key = {i} bgcolor="#f9e79f">
                <td style={{maxWidth: '5px', backgroundColor: '#f9e79f', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#f9e79f'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                <td style={{maxWidth: '200px', backgroundColor: '#f9e79f'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#f9e79f'}} >{u.codigo.t_forma_farmaceutica.descripcion} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#f9e79f', textAlign: 'center'}} >{u.cantidad} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#f9e79f'}} >{u.prescripcion} </td> 
                <td style={{maxWidth: '50px', backgroundColor: '#f9e79f', textAlign: 'center'}} >{"medicamento"} </td> 
              </tr>
            )
           })}
          
          {this.state.list_insumo.map((u, i)=> {
             return (
              <tr key = {i} bgcolor="#fcf3cf">
                <td style={{maxWidth: '5px', backgroundColor: '#fcf3cf', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#fcf3cf'}} >{u.codigo.producto_farmaceutico.codigo} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#fcf3cf'}} >{u.codigo.producto_farmaceutico.nombre} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#fcf3cf'}} >{u.codigo.t_presentacion.descripcion} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#fcf3cf', textAlign: 'center'}} >{u.cantidad} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#fcf3cf'}} >{u.prescripcion} </td> 
                <td style={{maxWidth: '50px', backgroundColor: '#fcf3cf', textAlign: 'center'}} >{"insumo"}</td> 
              </tr>
            )
           })}
           {this.state.list_otro.map((u, i)=> {
             return (
              <tr key = {i} bgcolor="#ffffff">
                <td style={{maxWidth: '5px', backgroundColor: '#ffffff', textAlign: 'center'}}>{i+1}</td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{"SN"} </td>
                <td style={{maxWidth: '100px', backgroundColor: '#ffffff'}} >{u.nombre} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.forma_farmaceutica} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}} >{u.cantidad} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff'}} >{u.prescripcion} </td>
                <td style={{maxWidth: '50px', backgroundColor: '#ffffff', textAlign: 'center'}} >{"producto externo"}</td> 
              </tr>
                    )
           })}
         </tbody>
         </Table>
      </CardBody>
    </Card>
  </Col>
</Row>
</div>
    )
  }
}

export default ElegirRecetaA;
