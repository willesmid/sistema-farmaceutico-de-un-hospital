import React from 'react';
import ReactDOM from 'react-dom';
import NuevoMedicamento from './NuevoMedicamento';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NuevoMedicamento />, div);
  ReactDOM.unmountComponentAtNode(div);
});
