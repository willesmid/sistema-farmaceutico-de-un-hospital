import React, { Component } from 'react';
import {
  Button,
 
  Card,
  CardBody,

  CardHeader,
  Col,

  Form,
  FormGroup,

  Input,

  Label,
  Row,
} from 'reactstrap';


var url = 'http://127.0.0.1:8000/v1/ingresar/';

class NuevoMedicamento extends Component {

  constructor(props) {
    super(props)
    
    // this.state = {
      
    //   codigo: '',
    //   nombre: '',
    //   stockGeneral:'',

    //   success: false,
    //   timeout: 4000,
    //   showTable: false
    // };
    this.state = {    
      codigo:'',
      nombre:'', 
      stockGeneral:''
    }

    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.onFormSubmit = this.onFormSubmit.bind(this);
}



  onFormSubmit(e) {
    try {
         //alert(JSON.stringify(this.state, null, ' '));
         fetch(url, {
           method: 'POST', // or 'PUT'
           body: JSON.stringify(this.state), // data can be string or {object}!
           headers:{
             'Content-Type': 'application/json'
           }
         }).then(res => res.json())
         .catch(error => console.error('Error:', error))
         .then(response => console.log('Success:', response));
        // alert("SE GUARDO SATISFACTORIAMENTE EL OBJETIVO")
              
       } catch (e) {
         console.log(e);
       }
  }





  handleOnSubmit(e) {
    e.preventDefault()

    let data = {
      codigo: this.state.codigo.value,
      nombre: this.state.nombre.value,
      stockGeneral: this.state.stockGeneral.value
    }

    try {

      fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be string or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));

    } catch (e) {
      console.log(e);
    }
   
  }

  render() {
    return (
      <div className="animated fadeIn">

          <Row>
            <Col xs="12" md="12">
            <Card>
                <CardHeader>
                  <strong>Objetivos de Gestion</strong>
                </CardHeader>
                <CardBody>
                <Form onSubmit={this.onFormSubmit}>
                <FormGroup row>
                  <Col md="6">
                    <Label></Label>
                  </Col>
                  <Col md="3">
                    <Label htmlFor="text-input">codigo</Label>
                  </Col>
                  <Col xs="12" md="3">
                    <Input 
                    type="number" 
                    id="number-input" 
                    name="codigo" 
                  
                    placeholder="codigo"
                    value={this.state.codigo}
                    onChange={e => this.setState({ codigo: e.target.value })}
                    />
                  </Col>  
                </FormGroup>
                <FormGroup row>
                  <Col md="6">
                    <Label></Label>
                  </Col>
                  <Col md="3">
                    <Label htmlFor="text-input">nombre</Label>
                  </Col>
                  <Col  md="3">
                    <Input 
                    type="text-input" 
                    id="date-input" 
                    name="nombre" 
                    placeholder="nombre"  
                    value={this.state.nombre}
                    onChange={e => this.setState({ nombre: e.target.value })} 
                    />
                  </Col>
                </FormGroup>

                 <FormGroup row>
                  <Col md="6">
                    <Label></Label>
                  </Col>
                  <Col md="3">
                    <Label htmlFor="text-input">stockGeneral</Label>
                  </Col>
                  <Col  md="3">
                    <Input 
                    type="number" 
                    id="number-input" 
                    name="stockGeneral" 
                    placeholder="stockGeneral"  
                    value={this.state.stockGeneral}
                    onChange={e => this.setState({ stockGeneral: e.target.value })} 
                    />
                  </Col>
                </FormGroup>
                <hr/>
               
                <Button type="submit" color="success" className="mr-1">Guardar</Button>
                <Button type="reset" size="md" color="danger" className="mr-1"> Cancelar</Button>
               
                </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
          
            
        </div>
     );
    }
  }


export default NuevoMedicamento;

      

        // <Row>
        //   <Col xs="12" md="6">
        //     <Card>
        //       <CardHeader>
        //         <strong>Ingresar </strong> Medicamento
        //       </CardHeader>
        //       <CardBody>
        //         <form  encType="multipart/form-data" className="form-horizontal" onSubmit={this.handleOnSubmit} method="POST">
        //           <FormGroup row>
        //             <Col md="3">
        //               <Label>Static</Label>
        //             </Col>
        //             <Col xs="12" md="9">
        //               <p className="form-control-static">Username</p>
        //             </Col>
        //           </FormGroup>
        //           <FormGroup row>
        //             <Col md="3">
        //               <Label htmlFor="text-input">Codigo</Label>
        //             </Col>
        //             <Col xs="12" md="9">
                     
        //               <input type="text" id="text-input" name="text-input" placeholder="codigo" ref={codigo => this.state.codigo = codigo}/>
        //               <FormText color="muted">Codigo</FormText>
        //             </Col>
        //           </FormGroup>

        //           <FormGroup row>
        //             <Col md="3">
        //               <Label htmlFor="text-input">Nombre</Label>
        //             </Col>
        //             <Col xs="12" md="9">
        //               <input type="text" id="text-input" name="text-input" placeholder="Text"  ref={nombre => this.state.nombre = nombre}/>
        //               <FormText color="muted">Nombre</FormText>
        //             </Col>
        //           </FormGroup>

        //           <FormGroup row>
        //             <Col md="3">
        //               <Label htmlFor="text-input">Stock</Label>
        //             </Col>
        //             <Col xs="12" md="9">
        //               <input type="text" id="text-input" name="text-input" placeholder="Text" ref={stockGeneral => this.state.stockGeneral = stockGeneral}/>
        //               <FormText color="muted">Stock</FormText>
        //             </Col>
        //           </FormGroup>
        //           <CardFooter>
        //           <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
        //           <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
        //           </CardFooter>
                
        //            </form>
        //       </CardBody>
             
        //     </Card>
   
        //   </Col>
                 
        // </Row> 


// import React, { Component } from 'react';
// import { Button, Card, CardBody, CardHeader, Col, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';

// const tdStyle = {
//   fontSize: '0.9em'
// }

// class NuevoMedicamento extends Component {
//   constructor(props) {
//     super(props)

//     this.state = {
//         ordenes_laboratorio: [],
//         primary: false,
//     }

//     this.handleOnSubmit = this.handleOnSubmit.bind(this)
//     this.handleChange = this.handleChange.bind(this)
//     this.togglePrimary = this.togglePrimary.bind(this)

//   }

//   async componentDidMount() {
//     try {
//       const respuesta = await fetch('http://localhost:8000/v1/Medicamentos/');
//       const ordenes_laboratorio = await respuesta.json();



//       this.setState({
//         ordenes_laboratorio
//       });
//       console.log(ordenes_laboratorio)

//     } catch (e) {
//       console.log(e);
//     }
//   }

//   handleChange(e) {
  
//       let {value, name, id} = e.target
//       console.log(value, name, id)

//       let reporte_ego_copia = this.state.reporte_ego

//       if(name === "egoDescripcion"){
//           reporte_ego_copia[id].egoDescripcion = value
//       }else if(name === "egoValorReferencia"){
//           reporte_ego_copia[id].egoValorReferencia = value
//       }

//       this.setState({
//           reporte_ego : reporte_ego_copia
//       });

//       console.log(reporte_ego_copia[id].egoDescripcion+"=>"+value)
//       console.log(this.state.reporte_ego)
//   }

//   handleOnSubmit(e) {
//       e.preventDefault()

//       console.log(this.state.reporte_ego);
//       let url = ''
//       let data = {}

//       this.state.reporte_ego.map((item, indice)=>{
          
//           url = 'http://127.0.0.1:8000/v1/actualizaEgo/'+item.id+'/'
//           data = {
//               egoDescripcion: item.egoDescripcion,
//               egoValorReferencia: item.egoValorReferencia,
//               egoTipoExamen: item.egoTipoExamen
//           }

//           try {

//             fetch(url, {
//             method: 'PUT',
//             body: JSON.stringify(data),
//             headers:{
//                 'Content-Type': 'application/json'
//             }
//             }).then(res => res.json())
//             .catch(error => console.error('Error:', error))
//             .then(response => console.log('Success:', response)); //UTILIZAR UN SNACKBAR 

//           } catch (e) {
//               console.log(e);
//           }
//       })

//       this.setState({
//         primary: !this.state.primary,
//       });
      
//   }

//   togglePrimary() {
//     this.setState({
//       primary: !this.state.primary,
//     });
//   }

//   render() {

//     const oredenes = this.state.ordenes_laboratorio.map((item, indice)=>{
//       return(

//         <tr key={item.indice}>
          
//           <td id={indice} style={tdStyle}>
//             {indice+1}
//           </td>

//           <td id={indice} style={tdStyle}>
//             {item.producto_farmaceutico.codigo}
//           </td>

//           <td id={indice} style={tdStyle}>
//             {item.producto_farmaceutico.nombre} 
//           </td>

//           <td id={indice} style={tdStyle}>
//             {item.t_forma_farmaceutica.descripcion}
//           </td>

//           <td id={indice} style={tdStyle}>
//             {item.concentracion}
//           </td>


//           <td id={indice} style={tdStyle}>
//             {item.clasificacion}
//           </td>

//           <td id={indice} style={tdStyle}>
//             {item.producto_farmaceutico.stockGeneral}
//           </td>
          

          

//           <td id={indice}>
//             <Button color="primary" onClick={this.togglePrimary} className="mr-1" size="sm">Ver</Button>
//           </td>

//           <td id={indice}>
//             <Button color="primary" onClick={this.togglePrimary} className="mr-1" size="sm">Modificar</Button>
//           </td>

     
//         </tr>
//       )
//     })

//     return (
//       <div className="animated fadeIn">
//         <Row>
//           <Col>
//             <Card>
//               <CardHeader>
//                 <i className="fa fa-align-justify"></i> Medicamentos
//               </CardHeader>
//               <CardBody>
//                 <Table responsive>
//                   <thead>
//                   <tr>
//                     <th>Numero</th>
//                     <th>Codigo</th>
//                     <th>Nombre</th>
//                     <th>F.Farmaceutica</th>
//                     <th>Concentracion</th>
                  
//                     <th>Clasificacion</th>
//                     <th>Stock</th>
//                     <th> </th>
//                     <th> </th>
//                     <th></th>
//                   </tr>
//                   </thead>
//                   <tbody>
//                     {oredenes}
//                   </tbody>
//                 </Table>
//               </CardBody>
//             </Card>
//           </Col>
//         </Row>
        
//       </div>

//     );
//   }
// }

// export default NuevoMedicamento;

    


