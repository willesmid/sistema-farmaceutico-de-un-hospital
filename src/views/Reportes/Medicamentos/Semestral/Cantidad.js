import React, { Component } from 'react';
class Cantidad extends Component {
  constructor(props) {
    super(props);
      this.state = {
        cantR:0,
        cantP:0,
      };
  }

componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaMesSumMedicamento/'+this.props.mes+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantR: findresponse[0].sum
        });
      console.log("Cantidad Sumada Receta: "+findresponse[0].sum)
      })
    } catch (e) {
      console.log(e);
    }

    try {
      fetch('http://localhost:8000/farmacia/pedir/v1/PedidoMesSum/'+this.props.mes+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantP: findresponse[0].sum
        });
      console.log("Cantidad Sumada Pedido: "+findresponse[0].sum)
      })
    } catch (e) {
      console.log(e);
    }

  }

  render() {
    return(
      <td >
        {parseInt(this.state.cantR,10)+parseInt(this.state.cantP,10)}  
      </td>
    )
  }
}
export default Cantidad;