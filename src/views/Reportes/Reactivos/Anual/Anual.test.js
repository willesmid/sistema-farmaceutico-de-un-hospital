import React from 'react';
import ReactDOM from 'react-dom';
import Anual from './Anual';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Anual />, div);
  ReactDOM.unmountComponentAtNode(div);
});