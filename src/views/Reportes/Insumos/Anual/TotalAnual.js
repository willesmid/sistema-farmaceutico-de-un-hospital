import React, { Component } from 'react';
class TotalAnual extends Component {
  constructor(props) {
    super(props);
      this.state = {
        cantR:0,
        cantP:0,
      };
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaAnualSumTotalInsumo/'+this.props.anio+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{
        if(findresponse.sum!== null){    
        this.setState({
            cantR: findresponse.sum
        });
      }

      })
    } catch (e) {
      console.log(e);
    }
    console.log("heeee total trim:"+ this.props.mes1 + " " + this.props.mes2 + " " +this.props.mes3+ " " + this.props.anio)

    try {
      fetch('http://localhost:8000/farmacia/pedir/v1/PedidoAnualSumTotalInsumo/'+this.props.anio+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    

        if(findresponse.sum!== null){    
          this.setState({
            cantP: findresponse.sum
          });
        }
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }

  }

  render() {
    return(
    
      <div >
        {parseInt(this.state.cantR,10)+parseInt(this.state.cantP,10)}  
      </div>
      
    )
  }
}
export default TotalAnual;