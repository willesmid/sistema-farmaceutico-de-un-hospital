import React, { Component } from 'react';
import { 
  Table,
  CardHeader,
  Button,
} from 'reactstrap';
import * as jsPDF  from 'jspdf'
import 'jspdf-autotable'
import Cantidad from './Cantidad'
import Total from './Total'
import TotalAnual from './TotalAnual';

const styles2 = { color: 'white', backgroundColor: ' #273746 ', textAlign:'center' }
const tdStyle = {
  fontSize: '0.9em'
}

const trNavLink = {
  padding: '0',
  margin: '0'
}
class Anual extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      Reactivos:[],
    };
    this.print = this.print.bind(this)
  }

  async componentDidMount(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
    this.setState({
      fecha:hoy,
    })
    
    try {
      const respuesta = await fetch('http://localhost:8000/farmacia/almacena/v1/InsumoHospitalario/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      });
      const Reactivos = await respuesta.json();
      this.setState({
        Reactivos
      });
      console.log(Reactivos)

    } catch (e) {
      console.log(e);
    }
  }

  handleChange(value){
    this.setState({
      codigo:value,
    })  
  }

  print(){
    /*Actual time and date */
    var options2 = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
    }
    var options3 = {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }
    var d = new Date();
    var showDate = d.toLocaleString('es-bo', options2) 
    var showTime = d.toLocaleString('es-bo', options3) 
    /*Actual time and date */
    var doc = new jsPDF('l', 'mm', 'legal'); //horientacion (p - l), unidad (pt, mm, cm, in), formato (a3, a4, a5, letter, legal)
    var logo = new Image();
  
    logo.src = 'assets/img/dashboard/icono_promes.png';
    doc.addImage(logo, 'JPEG', 10, 7, 20,20);
  
    doc.setFont("helvetica");   
    doc.setFontSize(9);
    doc.text(57, 15, "PROGRAMA MEDICO ESTUDIANTIL", null, null, 'center');
    
    doc.setFont("helvetica");
    doc.setFontSize(9);
    doc.text(57, 19, "\"PROMES\"", null, null, 'center');

    doc.setFontType("normal");
    doc.setFontSize(9);
    doc.text(327,15, showDate +' '+ showTime, null, null, 'center');
    
    doc.setFont("helvetica");
    doc.setFontType("bold");   //italic
    doc.setFontSize(13);
    doc.text(180, 21, this.props.titulo+" "+this.props.anio, null, null, 'center');

    doc.setFont("helvetica");
    doc.setFontType("bold");   //italic
    doc.setFontSize(13);
    doc.text(180, 27, "ROTACIÓN DE INSUMOS MÉDICOS", null, null, 'center');
    
    var res = doc.autoTableHtmlToJson(document.getElementById("tabla"));
    doc.autoTable(res.columns, res.data, {
      // Styling
      theme: 'striped', // 'striped', 'grid' or 'plain'
      styles: {},
      headerStyles: {fontSize: 7},
      bodyStyles: {fontSize: 8},
      alternateRowStyles: {},
      columnStyles: {},
      // Properties
      startY: false, // false (indicates margin top value) or a number
      margin: {top: 31, left: 15}, // a number, array or object
      pageBreak: 'auto', // 'auto', 'avoid' or 'always'
      tableWidth: 'auto', // 'auto', 'wrap' or a number, 
      showHeader: 'firstPage', // 'everyPage', 'firstPage', 'never',
      tableLineColor: 200, // number, array (see color section below)
      tableLineWidth: 0,
    });
    window.open(doc.output('bloburl'), '_blank');
  }

  render() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */


    const ListaReactivos = this.state.Reactivos.map((item, indice)=>{
      return(
        <tr key={item.indice} style={tdStyle}> 
          <td style={tdStyle}>{indice+1}</td>
          <td style={tdStyle}>{item.producto_farmaceutico.codigo}</td>
          <td style={tdStyle}>{item.producto_farmaceutico.nombre}</td>
          <td style={tdStyle}>{item.t_presentacion.descripcion}</td>
          <td style={tdStyle}>{item.t_unidad.descripcion}</td>
          <Cantidad mes={this.props.mes1} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes2} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes3} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes4} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes5} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes6} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes7} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes8} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes9} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes10} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes11} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Cantidad mes={this.props.mes12} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          <Total mes1={this.props.mes1} mes2={this.props.mes2} mes3={this.props.mes3} mes4={this.props.mes4} mes5={this.props.mes5} mes6={this.props.mes6} anio={this.props.anio} codigo={item.producto_farmaceutico.codigo} />
          
        </tr>
      )
    })

return (
  <div className="animated fadeIn">
    <CardHeader>
      <i className=" fa fa-table"></i> <strong>{this.props.titulo + " " + this.props.anio}</strong> 
      <table style={trNavLink} align="right" >
        <tr>
          <td>
          <Button onClick={this.print} size="sm"  color="secondary" className="btn-brand mr-1 mb-1"><i className="fa fa-print"></i><span>Imprimir</span></Button>
          </td>
        </tr>
      </table> 
    </CardHeader>    
    <Table hover bordered striped responsive size="sm">
        <thead style={styles2}>
          <tr>
            <th rowSpan="2" style={tdStyle}><p/>Nro.<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>CODIGO<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>NOMBRE<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>PRESENTACION<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>UNIDAD DE MANEJO<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes1}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes2}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes3}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes4}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes5}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes6}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes7}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes8}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes9}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes10}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes11}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>{this.props.nomMes12}<p/></th>
            <th rowSpan="2" style={tdStyle}><p/>TOTAL<p/></th>
          </tr>
        </thead>
        <tbody>
          {ListaReactivos}
          <tr>
            <th colspan="17"></th>
            <td colspan="1" bgcolor="#424949" style={tdStyle} >
            <font color="#ffffff">
            <TotalAnual mes1={this.props.mes1} mes2={this.props.mes2} mes3={this.props.mes3} mes4={this.props.mes4} mes5={this.props.mes5} mes6={this.props.mes6} anio={this.props.anio}/>
            </font>
            </td>
          </tr> 
    </tbody>
    </Table>

{/* INICIO IMPRIMIR */}
<Table hover bordered striped responsive size="sm" hidden id="tabla">
    <thead style={styles2}>
      <tr>
        <th>Nro.</th>
        <th>CODIGO</th>
        <th>NOMBRE</th>
        <th>PRESENTACION</th>
        <th>UNIDAD DE MANEJO</th>
        <th>{this.props.nomMes1}</th>
        <th>{this.props.nomMes2}</th>
        <th>{this.props.nomMes3}</th>
        <th>{this.props.nomMes4}</th>
        <th>{this.props.nomMes5}</th>
        <th>{this.props.nomMes6}</th>
        <th>{this.props.nomMes7}</th>
        <th>{this.props.nomMes8}</th>
        <th>{this.props.nomMes9}</th>
        <th>{this.props.nomMes10}</th>
        <th>{this.props.nomMes11}</th>
        <th>{this.props.nomMes12}</th>
        <th>TOTAL</th>
      </tr>
    </thead>
    <tbody>
      {ListaReactivos}
      <tr>
        <td></td><td></td><td></td><td></td><td></td><td></td>
        <td></td><td></td><td></td><td></td><td></td><td>
        </td><td></td><td></td><td></td><td></td><td>TOTAL</td>
        <td>
        <TotalAnual mes1={this.props.mes1} mes2={this.props.mes2} mes3={this.props.mes3} mes4={this.props.mes4} mes5={this.props.mes5} mes6={this.props.mes6} anio={this.props.anio}/>
        </td>
      </tr> 
</tbody>
</Table>

{/* FIN IMPRIMIR */}
  </div>
    );
  }
}

export default Anual;
