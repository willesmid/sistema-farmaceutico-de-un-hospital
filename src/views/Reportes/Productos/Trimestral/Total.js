import React, { Component } from 'react';
class Total extends Component {
  constructor(props) {
    super(props);
      this.state = {
        cantRM:0,
        cantRI:0,
        cantP:0,
      };
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaTrimSumMedicamento/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantRM: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaTrimSumInsumo/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantRI: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }
    try {
      fetch('http://localhost:8000/farmacia/pedir/v1/PedidoTrimSum/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantP: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }

  }

  render() {

        return(
        
          <td >
            {parseInt(this.state.cantRM,10)+parseInt(this.state.cantRI,10)+parseInt(this.state.cantP,10)}  
          </td>
          
        )
 
  }
}
export default Total;