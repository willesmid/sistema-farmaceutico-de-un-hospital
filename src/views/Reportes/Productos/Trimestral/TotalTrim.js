import React, { Component } from 'react';
class TotalTrim extends Component {
  constructor(props) {
    super(props);
      this.state = {
        cantRM:0,
        cantRI:0,
        cantP:0,
      };
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaTrimSumTotalMedicamento/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.anio+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{
        if(findresponse.sum!== null){    
        this.setState({
            cantRM: findresponse.sum
        });
      }

      })
    } catch (e) {
      console.log(e);
    }
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaTrimSumTotalInsumo/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.anio+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{
        if(findresponse.sum!== null){    
        this.setState({
            cantRI: findresponse.sum
        });
      }

      })
    } catch (e) {
      console.log(e);
    }
    try {
      fetch('http://localhost:8000/farmacia/pedir/v1/PedidoTrimSumTotal/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.anio+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    

        if(findresponse.sum!== null){    
          this.setState({
            cantP: findresponse.sum
          });
        }
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return(
    
      <div >
        {parseInt(this.state.cantRM,10)+parseInt(this.state.cantRI,10)+parseInt(this.state.cantP,10)}  
      </div>
      
    )
  }
}
export default TotalTrim;