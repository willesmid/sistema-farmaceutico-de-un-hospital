import React from 'react';
import ReactDOM from 'react-dom';
import Trimestral from './Trimestral';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Trimestral />, div);
  ReactDOM.unmountComponentAtNode(div);
});