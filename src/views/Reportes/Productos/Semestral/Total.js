import React, { Component } from 'react';
class Total extends Component {
  constructor(props) {
    super(props);
      this.state = {
        cantRM:0,
        cantRI:0,
        cantP:0,
      };
  }

  async componentDidMount() { //ciclo de vida de react de forma asincrona
    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaSemSumMedicamento/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.mes4+'/'+this.props.mes5+'/'+this.props.mes6+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantRM: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }

    try {
      fetch('http://localhost:8000/farmacia/recetar/v1/RecetaSemSumInsumo/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.mes4+'/'+this.props.mes5+'/'+this.props.mes6+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantRI: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }

    try {
      fetch('http://localhost:8000/farmacia/pedir/v1/PedidoSemSum/'+this.props.mes1+'/'+this.props.mes2+'/'+this.props.mes3+'/'+this.props.mes4+'/'+this.props.mes5+'/'+this.props.mes6+'/'+this.props.anio+'/'+this.props.codigo+'/',{
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        } 
      })
      .then((Response)=>Response.json())
      .then((findresponse)=>{    
        this.setState({
            cantP: findresponse[0].sum
        });
      console.log("Cantidad Sumada: "+this.state.dataCant)
      })
    } catch (e) {
      console.log(e);
    }

  }

  render() {

        return(
        
          <td >
            {parseInt(this.state.cantRM,10)+parseInt(this.state.cantRI,10)+parseInt(this.state.cantP,10)}  
          </td>
          
        )
 
  }
}
export default Total;