import React, { Component } from 'react';
import { 
  Card, 
  CardBody, 
  CardHeader, 
  Col, 
  Row,  
  FormGroup, 
  Input,
  Button,
} from 'reactstrap';

import Trimestral from './Trimestral'
import Semestral from './Semestral'
import Anual from './Anual'

class Productos extends Component {

  constructor(props) {
    super(props);
   
    this.state = {
      Medicamentos:[],
      anio:'',
      trim:'Trimestre',
      ene:'01',
      feb:'02',
      mar:'03',
      abr:'04',
      may:'05',
      jun:'06',
      jul:'07',
      ago:'08',
      sep:'09',
      oct:'10',
      nov:'11',
      dic:'12',

      eneN:'ENERO',
      febN:'FEBRERO',
      marN:'MARZO',
      abrN:'ABRIL',
      mayN:'MAYO',
      junN:'JUNIO',
      julN:'JULIO',
      agoN:'AGOSTO',
      sepN:'SEPTIEMBRE',
      octN:'OCTUBRE',
      novN:'NOVIEMBRE',
      dicN:'DICIEMBRE',

      tituloTrim1:'INFORME PRIMER TRIMESTRE',
      tituloTrim2:'INFORME SEGUNDO TRIMESTRE',
      tituloTrim3:'INFORME TERCER TRIMESTRE',
      tituloTrim4:'INFORME CUARTO TRIMESTRE',

      tituloSem1:'INFORME PRIMER SEMESTRE',
      tituloSem2:'INFORME SEGUNDO SEMESTRE',
      tituloAnual:'INFORME FINAL',

      showTrim1:false,
      showTrim2:false,
      showTrim3:false,
      showTrim4:false,

      showSem1:false,
      showSem2:false,

      showAnual:false,
    };
    this.Trimestre1 = this.Trimestre1.bind(this)
    this.Trimestre2 = this.Trimestre2.bind(this)
    this.Trimestre3 = this.Trimestre3.bind(this)
    this.Trimestre4 = this.Trimestre4.bind(this)

    this.Semestre1 = this.Semestre1.bind(this)
    this.Semestre2 = this.Semestre2.bind(this)
    this.Anual = this.Anual.bind(this)

  }



  async componentDidMount(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */
    this.setState({
      fecha:hoy,
      anio:yyyy,
      showTrim1:true,
      
    })
    

  }

  handleChange(value){
    this.setState({
      codigo:value,
    })  
  }

Trimestre1(){
  this.setState({
    showTrim1:true,
    showTrim2:false,
    showTrim3:false,
    showTrim4:false,

    showSem1:false,
    showSem2:false,

    showAnual:false,
  })
}

Trimestre2(){
  this.setState({
    showTrim1:false,
    showTrim2:true,
    showTrim3:false,
    showTrim4:false,

    showSem1:false,
    showSem2:false,

    showAnual:false,
  })
}

Trimestre3(){
  this.setState({
    showTrim1:false,
    showTrim2:false,
    showTrim3:true,
    showTrim4:false,

    showSem1:false,
    showSem2:false,

    showAnual:false,
  })
}


Trimestre4(){
  this.setState({
    showTrim1:false,
    showTrim2:false,
    showTrim3:false,
    showTrim4:true,

    showSem1:false,
    showSem2:false,

    showAnual:false,
  })
}

Semestre1(){
  this.setState({
    showTrim1:false,
    showTrim2:false,
    showTrim3:false,
    showTrim4:false,

    showSem1:true,
    showSem2:false,

    showAnual:false,
  })
}
Semestre2(){
  this.setState({
    showTrim1:false,
    showTrim2:false,
    showTrim3:false,
    showTrim4:false,

    showSem1:false,
    showSem2:true,

    showAnual:false,
  })
}

Anual(){
  this.setState({
    showTrim1:false,
    showTrim2:false,
    showTrim3:false,
    showTrim4:false,

    showSem1:false,
    showSem2:false,

    showAnual:true,
  })
}

render() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
    hoy = yyyy+"-"+mm+"-"+dd /*Date to show in input */

return (
  <div className="animated fadeIn">
    <Row>
      <Col xs="12" md="12">
        <Card>
          <CardHeader>
          <h3><i className="fa fa-th-list"></i> <strong>REPORTES PRODUCTOS FARMACEUTICOS</strong></h3>
          </CardHeader>
          <CardBody>
          <Row>
            <Col xs="12" md="12">
            <form>
              <FormGroup row>
                <Col md="3">
                  <Input type="number" min="2018" 
                  onChange={e => { this.setState({ anio: e.target.value, showTrim1:false, showTrim2: false, showTrim3:false, showTrim4:false, 
                  showSem1:false, showSem2:false, showAnual:false,
                  }) }} defaultValue={yyyy} placeholder="año" />
                </Col>
                <Col md="4">
                  <Button  className="btn-behance btn-brand icon mr-1 mb-1" disabled>Informe Trimestral</Button>
                  <Button className="btn-facebook btn-brand icon mr-1 mb-1" onClick={this.Trimestre1}><strong>1</strong></Button>
                  <Button className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.Trimestre2}><strong>2</strong></Button>
                  <Button className="btn-facebook btn-brand icon mr-1 mb-1" onClick={this.Trimestre3}><strong>3</strong></Button>
                  <Button className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.Trimestre4}><strong>4</strong></Button>
                </Col>
                <Col md="3">
                  <Button  className="btn-vimeo btn-brand icon mr-1 mb-1" disabled>Informe Semestral</Button>
                  <Button className="btn-spotify btn-brand icon mr-1 mb-1" onClick={this.Semestre1}><strong>1</strong></Button>
                  <Button className="btn-vimeo btn-brand icon mr-1 mb-1" onClick={this.Semestre2}><strong>2</strong></Button>
                </Col>
                <Col md="2">
                  <Button className="btn-openid btn-brand icon mr-1 mb-1" onClick={this.Anual}> Informe Anual </Button>
                </Col>  
              </FormGroup>
            </form>
            </Col>
          </Row>
          {this.state.showTrim1 && 
          <Trimestral mes1={this.state.ene} mes2={this.state.feb} mes3={this.state.mar} anio={this.state.anio} 
          nomMes1={this.state.eneN} nomMes2={this.state.febN} nomMes3={this.state.marN} titulo={this.state.tituloTrim1} 
          />}
          {this.state.showTrim2 && 
          <Trimestral mes1={this.state.abr} mes2={this.state.may} mes3={this.state.jun} anio={this.state.anio} 
          nomMes1={this.state.abrN} nomMes2={this.state.mayN} nomMes3={this.state.junN} titulo={this.state.tituloTrim2}
          />}
          {this.state.showTrim3 && 
          <Trimestral mes1={this.state.jul} mes2={this.state.ago} mes3={this.state.sep} anio={this.state.anio} 
          nomMes1={this.state.julN} nomMes2={this.state.agoN} nomMes3={this.state.sepN} titulo={this.state.tituloTrim3}
          />}
          {this.state.showTrim4 && 
          <Trimestral mes1={this.state.oct} mes2={this.state.nov} mes3={this.state.dic} anio={this.state.anio} 
          nomMes1={this.state.octN} nomMes2={this.state.novN} nomMes3={this.state.dicN} titulo={this.state.tituloTrim4}
          />}

          {this.state.showSem1 && 
          <Semestral mes1={this.state.ene} mes2={this.state.feb} mes3={this.state.mar} mes4={this.state.abr}
          mes5={this.state.may} mes6={this.state.jun} anio={this.state.anio} 
          nomMes1={this.state.eneN} nomMes2={this.state.febN} nomMes3={this.state.marN} nomMes4={this.state.abrN}
          nomMes5={this.state.mayN} nomMes6={this.state.junN} titulo={this.state.tituloSem1}
          />}
          {this.state.showSem2 && 
          <Semestral mes1={this.state.jul} mes2={this.state.ago} mes3={this.state.sep} mes4={this.state.oct}
          mes5={this.state.nov} mes6={this.state.dic} anio={this.state.anio} 
          nomMes1={this.state.julN} nomMes2={this.state.agoN} nomMes3={this.state.sepN} nomMes4={this.state.octN}
          nomMes5={this.state.novN} nomMes6={this.state.dicN} titulo={this.state.tituloSem2}
          />}

          {this.state.showAnual && 
          <Anual mes1={this.state.ene} mes2={this.state.feb} mes3={this.state.mar} mes4={this.state.abr}
          mes5={this.state.may} mes6={this.state.jun} mes7={this.state.jul} mes8={this.state.ago} 
          mes9={this.state.sep} mes10={this.state.oct} mes11={this.state.nov} mes12={this.state.dic} 
          anio={this.state.anio} 
          nomMes1={this.state.eneN} nomMes2={this.state.febN} nomMes3={this.state.marN} nomMes4={this.state.abrN}
          nomMes5={this.state.mayN} nomMes6={this.state.junN} nomMes7={this.state.julN} nomMes8={this.state.agoN}
          nomMes9={this.state.sepN} nomMes10={this.state.octN} nomMes11={this.state.novN} nomMes12={this.state.dicN}
          titulo={this.state.tituloAnual}
          />}
          </CardBody> 
        </Card>
      </Col>
    </Row>
  </div>
    );
  }
}

export default Productos;
