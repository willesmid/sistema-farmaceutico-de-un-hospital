export default {
  items: [
    {
      name: 'Inicio',
      url: '/Inicio',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'HOSPITAL',
      },
    },
    { /* Consultorio Recetas */
      name: 'RECETAS EN CONSULTORIO',
      url: '/RecetasEnConsultorio',
      icon: 'fa fa-user-md',
      children: [
        {
          name: 'Consulta',
          url: '/RecetasEnConsultorio/Consulta',
          icon: 'fa fa-clipboard',
        },
        // {
        //   name: 'Recetar',
        //   url: '/RecetasEnConsultorio/Recetar',
        //   icon: 'fa fa-edit',
        // },
        {
          name: 'Recetas Emitidas',
          url: '/RecetasEnConsultorio/RecetasEmitidas',
          icon: 'fa fa-list-ul',
        },
      ],
    },
    {
      title: true,
      name: 'Pacientes en Cola',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Prueba',
      url: '/prueba',
      icon: 'icon-drop',
    },
    {
      name: ' Ver Pacientes',
      url: '/listapacientes',
      icon: 'icon-drop',
    },
    {
      title: true,
      name: 'Documentos del Paciente',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Historia Clinica',
      url: '/historiaclinica',
      icon: 'icon-drop',
    },
    {
      name: 'Hoja de Evolución',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Redactar',
          url: '/hojaevolucion-redactar',
          icon: 'icon-puzzle',
        },
        {
          name: 'Ver Hoja de Evolución',
          url: '/hojaevolucion-ver',
          icon: 'icon-puzzle',
        },
      ]
    },
    {
      name: 'Nota de Internacion',
      url: '/notainternacion',
      icon: 'icon-drop',
    },
    {
      name: 'Hoja de Referencia',
      url: '/hojareferencia',
      icon: 'icon-drop',
    },
    {
      name: 'Notas de Indicaciones',
      url: '/notaindicaciones',
      icon: 'icon-drop',
    },
    {
      name: 'Hoja de Consentimiento Q',
      url: '/hojaconsentimientoq',
      icon: 'icon-drop',
    },
    {
      name: 'Hoja de Consentimiento A',
      url: '/hojaconsentimientoa',
      icon: 'icon-drop',
    },
    {
      name: 'MANUAL DE USUARIO',
      url: 'https://drive.google.com/open?id=1DL4bRdrWLKClcoyfSFHDB0FCYR_07PBa',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'success',
    },

  ],
};
  



  